package me.bt.avea.presentation.trips;

import java.util.List;
import me.bt.avea.domain.entities.DataLightPosition;
import me.bt.avea.domain.entities.IntervalData;

public interface TripsView {
  void updateTrips(List<IntervalData> trips);

  void updateTripsError();

  void updateLightPositions(List<DataLightPosition> lightPositions);

  void updateLightPositionsError();
}
