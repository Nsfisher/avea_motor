package me.bt.avea.presentation.map;

import java.util.List;

import me.bt.avea.domain.entities.Mobile;
import me.bt.avea.domain.interactions.mobiles.MobilesInteractor;
import me.bt.avea.domain.interactions.mobiles.events.MobilesEvent;
import me.bt.avea.presentation.model.PresentationMobile;
import me.bt.base.domain.abstractions.Bus;
import me.bt.base.domain.interactors.InteractorInvoker;
import me.bt.base.presentation.Presenter;
import me.bt.base.presentation.model.mapper.base.ListMapper;

public class MobileMapPresenter extends Presenter {

    private final Bus bus;
    private final InteractorInvoker interactorInvoker;
    private final MobileMapView mobileMapView;
    private final MobilesInteractor mobilesInteractor;
    private final ListMapper<Mobile, PresentationMobile> listMapper;

    public MobileMapPresenter(Bus bus, InteractorInvoker interactorInvoker, MobileMapView mobileMapView, MobilesInteractor mobilesInteractor,
                              ListMapper<Mobile, PresentationMobile> listMapper) {
        this.bus = bus;
        this.interactorInvoker = interactorInvoker;
        this.mobileMapView = mobileMapView;
        this.mobilesInteractor = mobilesInteractor;
        this.listMapper = listMapper;
    }

    public void getMobiles() {
        interactorInvoker.execute(mobilesInteractor);
    }

    public void getMobilesSkipCache() {
        mobilesInteractor.setSkipCache(true);
        interactorInvoker.execute(mobilesInteractor);
    }

    public void onEvent(MobilesEvent event) {
        if (event.getError() == null) {
            List<PresentationMobile> list = listMapper.modelToData(event.getMobiles());
            mobileMapView.updateMobiles(list);
        } else {
            mobileMapView.updateMobilesError();
        }
    }

    @Override
    public void onResume() {
        bus.register(this);
    }

    @Override
    public void onPause() {
        bus.unregister(this);
    }
}

