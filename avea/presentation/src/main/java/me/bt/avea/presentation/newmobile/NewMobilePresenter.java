package me.bt.avea.presentation.newmobile;

import me.bt.avea.domain.interactions.adduser.AddUserInteractor;
import me.bt.avea.domain.interactions.register.events.RegisterEvent;
import me.bt.base.domain.abstractions.Bus;
import me.bt.base.domain.interactors.InteractorInvoker;
import me.bt.base.presentation.Presenter;

public class NewMobilePresenter extends Presenter {

  private final Bus bus;
  private final InteractorInvoker interactorInvoker;
  private final AddUserInteractor addUserInteractor;
  private final NewMobileView newMobileView;

  public NewMobilePresenter(Bus bus, InteractorInvoker interactorInvoker, NewMobileView newMobileView,
      AddUserInteractor addUserInteractor) {
    this.bus = bus;
    this.interactorInvoker = interactorInvoker;
    this.addUserInteractor = addUserInteractor;
    this.newMobileView = newMobileView;
  }

  public void addUser(String activation, String username, String password, String plaka, String nickname, String brand, String model,
      String fuelType, String engineInfo) {
    addUserInteractor.setUserInfo(activation, username, password, plaka, nickname, brand, model, fuelType, engineInfo);
    interactorInvoker.execute(addUserInteractor);
  }

  @Override public void onResume() {
    bus.register(this);
  }

  @Override public void onPause() {
    bus.unregister(this);
  }

  public void onEvent(RegisterEvent event) {
    if (event.getError() == null && event.getToken() != null) {

    }
  }
}

