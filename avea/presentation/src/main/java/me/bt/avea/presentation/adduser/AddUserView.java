package me.bt.avea.presentation.adduser;

public interface AddUserView {

  void showError(String errorDescription);

  void loginComplete();

  void loginError();
}
