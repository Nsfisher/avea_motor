package me.bt.avea.presentation.trips;

import java.util.Date;

import me.bt.avea.domain.entities.Mobile;
import me.bt.avea.domain.interactions.lightevents.LightPositionsInteractor;
import me.bt.avea.domain.interactions.lightevents.events.LightPositionsEvent;
import me.bt.avea.domain.interactions.mobiles.MobilesInteractor;
import me.bt.avea.domain.interactions.trips.TripsInteractor;
import me.bt.avea.domain.interactions.trips.events.TripsEvent;
import me.bt.avea.presentation.model.PresentationMobile;
import me.bt.base.domain.abstractions.Bus;
import me.bt.base.domain.interactors.InteractorInvoker;
import me.bt.base.presentation.Presenter;
import me.bt.base.presentation.model.mapper.base.ListMapper;

public class TripsPresenter extends Presenter {
    private final Bus bus;
    private final InteractorInvoker interactorInvoker;
    private final TripsView tripsView;
    private final ListMapper<Mobile, PresentationMobile> listMapper;
    private final MobilesInteractor mobilesInteractor;
    private final TripsInteractor tripsInteractor;
    private final LightPositionsInteractor lightPositionsInteractor;
    private final int MAX_POINTS = 1000;
    private final int QUERY_TYPE = 1;
    private final int QUERY_TYPE_LINE = 0;
    private final int LINE_MODE_DEFAULT = 0;
    private final int LINE_MODE_DETAIL = 0;

    public TripsPresenter(Bus bus, InteractorInvoker interactorInvoker, TripsView tripsView, MobilesInteractor mobilesInteractor,
                          TripsInteractor tripsInteractor, LightPositionsInteractor lightPositionsInteractor,
                          ListMapper<Mobile, PresentationMobile> listMapper) {
        this.bus = bus;
        this.interactorInvoker = interactorInvoker;
        this.tripsView = tripsView;
        this.mobilesInteractor = mobilesInteractor;
        this.lightPositionsInteractor = lightPositionsInteractor;
        this.listMapper = listMapper;
        this.tripsInteractor = tripsInteractor;
    }

    public void getLightPositions(String mobileId, String startDate, String endDate) {
        lightPositionsInteractor.setLightPositionsRequestParams(mobileId, startDate, endDate, MAX_POINTS, QUERY_TYPE_LINE, LINE_MODE_DEFAULT);
        interactorInvoker.execute(lightPositionsInteractor);
    }

    public void getLightPositionsForDetailScreen(String mobileId, String startDate, String endDate) {
        lightPositionsInteractor.setLightPositionsRequestParams(mobileId, startDate, endDate, MAX_POINTS, QUERY_TYPE, LINE_MODE_DETAIL);
        interactorInvoker.execute(lightPositionsInteractor);
    }

    public void getTrips(String mobileId, Date startDate, Date endDate) {
        tripsInteractor.setTripDetail(mobileId, startDate, endDate);
        interactorInvoker.execute(tripsInteractor);
    }

    public void onEvent(TripsEvent event) {
        if (event.getError() == null) {
            tripsView.updateTrips(event.getResponse());
        } else {
            tripsView.updateTripsError();
        }
    }

    public void onEvent(LightPositionsEvent event) {
        if (event.getError() == null) {
            tripsView.updateLightPositions(event.getLightPositions());
        } else {
            tripsView.updateLightPositionsError();
        }
    }

    @Override
    public void onResume() {
        bus.register(this);
    }

    @Override
    public void onPause() {
        bus.unregister(this);
    }
}

