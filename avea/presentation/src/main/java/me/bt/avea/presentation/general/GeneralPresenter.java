package me.bt.avea.presentation.general;

import java.util.Date;

import me.bt.avea.domain.interactions.general.GeneralInteractor;
import me.bt.avea.domain.interactions.general.events.GeneralEvent;
import me.bt.base.domain.abstractions.Bus;
import me.bt.base.domain.interactors.InteractorInvoker;
import me.bt.base.presentation.Presenter;

public class GeneralPresenter extends Presenter {

  private final Bus bus;
  private final InteractorInvoker interactorInvoker;
  private final GeneralInteractor generalInteractor;
  private final GeneralView generalView;

  public GeneralPresenter(Bus bus, InteractorInvoker interactorInvoker, GeneralView generalView, GeneralInteractor generalInteractor) {
    this.bus = bus;
    this.interactorInvoker = interactorInvoker;
    this.generalInteractor = generalInteractor;
    this.generalView = generalView;
  }

  public void getTerms() {
    generalInteractor.setRequest(GeneralInteractor.REQUEST_TERMS);
    interactorInvoker.execute(generalInteractor);
  }

  public void getBrands() {
    generalInteractor.setRequest(GeneralInteractor.REQUEST_GET_BRANDS);
    interactorInvoker.execute(generalInteractor);
  }
  public void getBrandModel(String brandId) {
    generalInteractor.setRequest(GeneralInteractor.REQUEST_GET_BRAND_MODELS);
    generalInteractor.setBrandId(brandId);
    interactorInvoker.execute(generalInteractor);
  }


  public void getLightTrips(String mobileId, Date startDate, Date endDate) {
    generalInteractor.setRequest(GeneralInteractor.REQUEST_LIGHTTRIPS);
    generalInteractor.setLightTrips(mobileId, startDate, endDate);
    interactorInvoker.execute(generalInteractor);
  }

  public void getEventScore(String mobileId, Date startDate, Date endDate) {
    generalInteractor.setRequest(GeneralInteractor.REQUEST_EVENT_SCORE);
    generalInteractor.setEventScore(mobileId, startDate, endDate);
    interactorInvoker.execute(generalInteractor);
  }

  @Override public void onResume() {
    bus.register(this);
  }

  @Override public void onPause() {
    bus.unregister(this);
  }

  public void onEvent(GeneralEvent event) {
    generalView.generalResponse(event);
  }

  public void getRegions() {
    generalInteractor.setRequest(GeneralInteractor.REQUEST_GET_REGIONS);
    interactorInvoker.execute(generalInteractor);
  }

  public void forgetPassword(String email) {
    generalInteractor.setRequest(GeneralInteractor.REQUEST_FORGET_PASSWORD);
    generalInteractor.setEmail(email);
    interactorInvoker.execute(generalInteractor);
  }

  public void getNotificationList() {
    generalInteractor.setRequest(GeneralInteractor.REQUEST_NOTIFICATION_LIST);
    interactorInvoker.execute(generalInteractor);
  }

  public void getFuelTypes() {
    generalInteractor.setRequest(GeneralInteractor.REQUEST_GET_FUEL_TYPES);
    interactorInvoker.execute(generalInteractor);
  }

  public void logout() {
    generalInteractor.setRequest(GeneralInteractor.REQUEST_LOGOUT);
    interactorInvoker.execute(generalInteractor);
  }
}

