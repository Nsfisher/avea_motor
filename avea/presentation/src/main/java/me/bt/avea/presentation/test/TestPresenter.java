package me.bt.avea.presentation.test;

import me.bt.avea.domain.entities.Mobile;
import me.bt.avea.domain.interactions.lightevents.LightEventsInteractor;
import me.bt.avea.domain.interactions.mobiles.MobilesInteractor;
import me.bt.avea.domain.interactions.mobiles.events.MobilesEvent;
import me.bt.avea.domain.interactions.register.RegisterInteractor;
import me.bt.avea.domain.interactions.register.events.RegisterEvent;
import me.bt.avea.domain.interactions.suggestion.SuggestionInteractor;
import me.bt.avea.domain.interactions.suggestion.events.SuggestionEvent;
import me.bt.avea.presentation.model.PresentationMobile;
import me.bt.base.domain.abstractions.Bus;
import me.bt.base.domain.interactors.InteractorInvoker;
import me.bt.base.presentation.Presenter;
import me.bt.base.presentation.model.mapper.base.ListMapper;

public class TestPresenter extends Presenter {

  private final Bus bus;
  private final InteractorInvoker interactorInvoker;
  private final RegisterInteractor registerInteractor;
  private final MobilesInteractor mobilesInteractor;
  private final SuggestionInteractor suggestionInteractor;
  private final LightEventsInteractor lightEventsInteractor;
  private final TestView testView;
  private final ListMapper<Mobile, PresentationMobile> listMapper;

  public TestPresenter(Bus bus, InteractorInvoker interactorInvoker, RegisterInteractor registerInteractor,
      MobilesInteractor mobilesInteractor, TestView testView, ListMapper<Mobile, PresentationMobile> listMapper,
      SuggestionInteractor suggestionInteractor, LightEventsInteractor lightEventsInteractor) {
    this.bus = bus;
    this.interactorInvoker = interactorInvoker;
    this.registerInteractor = registerInteractor;
    this.mobilesInteractor = mobilesInteractor;
    this.suggestionInteractor = suggestionInteractor;
    this.lightEventsInteractor = lightEventsInteractor;
    this.testView = testView;
    this.listMapper = listMapper;
  }

  public void registerUser(String username, String password, String company, String language) {
    registerInteractor.setData(username, password, company, language);
    interactorInvoker.execute(registerInteractor);
  }

  public void getMobiles() {
    interactorInvoker.execute(mobilesInteractor);
  }

  @Override public void onResume() {
    bus.register(this);
  }

  @Override public void onPause() {
    bus.unregister(this);
  }

  public void onEvent(RegisterEvent event) {
    if (event.getError() == null && event.getToken() != null) {
      testView.loginComplete();
    } else {
      testView.showLoginError();
    }
  }

  public void onEvent(MobilesEvent event) {
    if (event.getError() == null) {
      testView.refreshMobiles(listMapper.modelToData(event.getMobiles()));
    } else {
      testView.showRefreshMobilesError();
    }
  }

  public void onEvent(SuggestionEvent event) {
    if (event.getError() == null) {
      testView.refreshSuggestions(event.getSuggestions());
    } else {
      testView.showRefreshSuggestionsError();
    }
  }

  public void getLightEvents(String mobileId, int itemNumber) {
    lightEventsInteractor.setItemNumber(itemNumber);
    lightEventsInteractor.setMobileId(mobileId);
    interactorInvoker.execute(lightEventsInteractor);
  }

  public void getSuggestion(String keyword, double latitude, double longitude) {
    suggestionInteractor.setKeyword(keyword);
    suggestionInteractor.setLatitude(latitude);
    suggestionInteractor.setLongitude(longitude);
    interactorInvoker.execute(suggestionInteractor);
  }
}

