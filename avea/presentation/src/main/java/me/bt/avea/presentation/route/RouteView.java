package me.bt.avea.presentation.route;

import java.util.List;
import me.bt.avea.domain.entities.LightEvent;
import me.bt.avea.domain.entities.Suggestion;
import me.bt.avea.presentation.model.PresentationMobile;

public interface RouteView {
  void refreshSuggestions(List<Suggestion> suggestions);

  void showRefreshSuggestionsError();
}
