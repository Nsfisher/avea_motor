package me.bt.avea.presentation.notifications;

import java.util.List;
import me.bt.avea.domain.entities.LightEvent;
import me.bt.avea.presentation.model.PresentationMobile;

public interface NotificationsView {

  void refreshLightEventError();

  void refreshLightEvents(List<LightEvent> lightEvents);
}
