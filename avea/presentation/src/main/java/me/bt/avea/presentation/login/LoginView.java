package me.bt.avea.presentation.login;

public interface LoginView {

  void loginComplete();

  void loginError(String message);
}
