package me.bt.avea.presentation.general;

import me.bt.avea.domain.interactions.general.events.GeneralEvent;

public interface GeneralView {

  void generalResponse(GeneralEvent response);
}
