package me.bt.avea.presentation.test;

import java.util.List;
import me.bt.avea.domain.entities.Suggestion;
import me.bt.avea.presentation.model.PresentationMobile;

public interface TestView {

  void showLoginError();

  void loginComplete();

  void refreshMobiles(List<PresentationMobile> mobiles);

  void showRefreshMobilesError();

  void refreshSuggestions(List<Suggestion> suggestions);

  void showRefreshSuggestionsError();
}
