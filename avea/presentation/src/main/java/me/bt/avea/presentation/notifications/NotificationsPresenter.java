package me.bt.avea.presentation.notifications;

import me.bt.avea.domain.entities.Mobile;
import me.bt.avea.domain.interactions.lightevents.LightEventsInteractor;
import me.bt.avea.domain.interactions.lightevents.events.LightEventsEvent;
import me.bt.avea.domain.interactions.mobiles.MobilesInteractor;
import me.bt.avea.domain.interactions.mobiles.events.MobilesEvent;
import me.bt.avea.presentation.model.PresentationMobile;
import me.bt.base.domain.abstractions.Bus;
import me.bt.base.domain.interactors.InteractorInvoker;
import me.bt.base.presentation.Presenter;
import me.bt.base.presentation.model.mapper.base.ListMapper;

public class NotificationsPresenter extends Presenter {

  private final Bus bus;
  private final InteractorInvoker interactorInvoker;
  private final NotificationsView notificationsView;
  private final LightEventsInteractor lightEventsInteractor;
  private final ListMapper<Mobile, PresentationMobile> listMapper;

  public NotificationsPresenter(Bus bus, InteractorInvoker interactorInvoker, NotificationsView notificationsView,
      LightEventsInteractor lightEventsInteractor, ListMapper<Mobile, PresentationMobile> listMapper) {
    this.bus = bus;
    this.interactorInvoker = interactorInvoker;
    this.notificationsView = notificationsView;
    this.lightEventsInteractor = lightEventsInteractor;
    this.listMapper = listMapper;
  }

  public void getLightEvents(String mobileId) {
    lightEventsInteractor.setMobileId(mobileId);
    interactorInvoker.execute(lightEventsInteractor);
  }

  public void getLightEvents() {
    interactorInvoker.execute(lightEventsInteractor);
  }

  public void onEvent(LightEventsEvent event) {
    if (event.getError() == null && event.getLightEvents() != null) {
      notificationsView.refreshLightEvents(event.getLightEvents());
    } else {
      notificationsView.refreshLightEventError();
    }
  }

  @Override public void onResume() {
    bus.register(this);
  }

  @Override public void onPause() {
    bus.unregister(this);
  }
}

