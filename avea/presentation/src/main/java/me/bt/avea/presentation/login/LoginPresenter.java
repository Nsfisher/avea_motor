package me.bt.avea.presentation.login;

import me.bt.avea.domain.interactions.register.RegisterInteractor;
import me.bt.avea.domain.interactions.register.events.RegisterEvent;
import me.bt.avea.domain.interactions.register.exceptions.RegisterException;
import me.bt.base.domain.abstractions.Bus;
import me.bt.base.domain.interactors.InteractorInvoker;
import me.bt.base.presentation.Presenter;

public class LoginPresenter extends Presenter {

    private final Bus bus;
    private final InteractorInvoker interactorInvoker;
    private final RegisterInteractor registerInteractor;
    private final LoginView loginView;
    private final String COMPANY = "Avea";
    private final String LANGUAGE = "TR";

    public LoginPresenter(Bus bus, InteractorInvoker interactorInvoker, LoginView loginView, RegisterInteractor registerInteractor) {
        this.bus = bus;
        this.interactorInvoker = interactorInvoker;
        this.registerInteractor = registerInteractor;
        this.loginView = loginView;
    }

    public void login(String username, String password) {
        registerInteractor.setData(username, password, COMPANY, LANGUAGE);
        interactorInvoker.execute(registerInteractor);
    }

    @Override
    public void onResume() {
        bus.register(this);
    }

    @Override
    public void onPause() {
        bus.unregister(this);
    }

    public void onEvent(RegisterEvent event) {
        if (event.getError() == null && event.getToken() != null & event.status == null) {
            loginView.loginComplete();
        } else {
            loginView.loginError(((RegisterException) event.getError()).errorDesc);
            //TODO invalid login show error (check by error code)
        }
    }
}

