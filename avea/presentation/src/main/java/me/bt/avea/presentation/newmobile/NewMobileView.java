package me.bt.avea.presentation.newmobile;

public interface NewMobileView {

  void showError(String errorDescription);

  void addUserComplete();

  void addUserError();
}
