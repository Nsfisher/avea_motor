package me.bt.avea.presentation.adduser;

import me.bt.avea.domain.interactions.register.RegisterInteractor;
import me.bt.avea.domain.interactions.register.events.RegisterEvent;
import me.bt.base.domain.abstractions.Bus;
import me.bt.base.domain.interactors.InteractorInvoker;
import me.bt.base.presentation.Presenter;

public class AddUserPresenter extends Presenter {

  private final Bus bus;
  private final InteractorInvoker interactorInvoker;
  private final RegisterInteractor registerInteractor;
  private final AddUserView addUserView;
  private final String COMPANY = "avea";
  private final String LANGUAGE = "tr";

  public AddUserPresenter(Bus bus, InteractorInvoker interactorInvoker, AddUserView addUserView, RegisterInteractor registerInteractor) {
    this.bus = bus;
    this.interactorInvoker = interactorInvoker;
    this.registerInteractor = registerInteractor;
    this.addUserView = addUserView;
  }

  public void login(String username, String password) {
    registerInteractor.setData(username, password, COMPANY, LANGUAGE);
    if (username == null || username.length() == 0) {
      addUserView.showError("Ge�ersiz kullan?c? ad?."); //TODO hakan
    } else if (password == null || password.length() == 0) {
      addUserView.showError("Ge�ersiz ?ifre.");
    } else {
      interactorInvoker.execute(registerInteractor);
    }
  }

  @Override public void onResume() {
    bus.register(this);
  }

  @Override public void onPause() {
    bus.unregister(this);
  }

  public void onEvent(RegisterEvent event) {
    if (event.getError() == null && event.getToken() != null) {
      addUserView.loginComplete();
    } else {
      addUserView.loginError();
      //TODO invalid login show error (check by error code)
    }
  }
}

