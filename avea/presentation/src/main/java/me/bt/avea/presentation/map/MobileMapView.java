package me.bt.avea.presentation.map;

import java.util.List;
import me.bt.avea.presentation.model.PresentationMobile;

public interface MobileMapView {

  void updateMobiles(List<PresentationMobile> presentationMobiles);

  void updateMobilesError();
}
