package me.bt.avea.presentation.model.Mapper;

import me.bt.avea.domain.entities.Mobile;
import me.bt.avea.presentation.model.PresentationMobile;
import me.bt.base.presentation.model.mapper.base.Mapper;

public class PresentationMobileMapper implements Mapper<Mobile, PresentationMobile> {

    @Override
    public PresentationMobile modelToData(Mobile model) {
        if (model == null) {
            return null;
        }
        PresentationMobile presentationMobile = new PresentationMobile();
        presentationMobile.setAddr1(model.getAddr1());
        presentationMobile.setMobile(model.getMobile());
        presentationMobile.setAlias(model.getAlias());
        presentationMobile.setLbsCustomerPoint(model.getLbsCustomerPoint());
        presentationMobile.setPosLatitude(model.getPosLatitude());
        presentationMobile.setPosLongitude(model.getPosLongitude());
        presentationMobile.setSpeedExceedMax(model.getSpeedExceedMax());
        presentationMobile.setSpeedExceedLimit(model.getSpeedExceedLimit());
        presentationMobile.setPosSpeed(model.getPosSpeed());
        presentationMobile.setPosTimestamp(model.getPosTimestamp());
        presentationMobile.setPosTimestampStrtime(model.getPosTimestampStrtime());
        presentationMobile.setPosDirectionValue(model.getPosDirectionValue());
        return presentationMobile;
    }

    @Override
    public Mobile dataToModel(PresentationMobile data) {
        if (data == null) {
            return null;
        }
        Mobile mobile = new Mobile();
        mobile.setAddr1(data.getAddr1());
        mobile.setMobile(data.getMobile());
        mobile.setAlias(data.getAlias());
        mobile.setLbsCustomerPoint(data.getLbsCustomerPoint());
        mobile.setPosLatitude(data.getPosLatitude());
        mobile.setPosLongitude(data.getPosLongitude());
        mobile.setSpeedExceedMax(data.getSpeedExceedMax());
        mobile.setSpeedExceedLimit(data.getSpeedExceedLimit());
        mobile.setPosSpeed(data.getPosSpeed());
        mobile.setPosTimestamp(data.getPosTimestamp());
        mobile.setPosTimestampStrtime(data.getPosTimestampStrtime());
        mobile.setPosDirectionValue(data.getPosDirectionValue());
        return mobile;
    }
}
