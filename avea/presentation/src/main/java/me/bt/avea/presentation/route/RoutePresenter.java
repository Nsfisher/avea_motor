package me.bt.avea.presentation.route;

import me.bt.avea.domain.interactions.suggestion.SuggestionInteractor;
import me.bt.avea.domain.interactions.suggestion.events.SuggestionEvent;
import me.bt.base.domain.abstractions.Bus;
import me.bt.base.domain.interactors.InteractorInvoker;
import me.bt.base.presentation.Presenter;

public class RoutePresenter extends Presenter {

    private final Bus bus;
    private final InteractorInvoker interactorInvoker;
    private final SuggestionInteractor suggestionInteractor;
    private final RouteView routeView;

    public RoutePresenter(Bus bus, InteractorInvoker interactorInvoker, RouteView routeView, SuggestionInteractor suggestionInteractor) {
        this.bus = bus;
        this.interactorInvoker = interactorInvoker;
        this.suggestionInteractor = suggestionInteractor;
        this.routeView = routeView;
    }

    @Override
    public void onResume() {
        bus.register(this);
    }

    @Override
    public void onPause() {
        bus.unregister(this);
    }

    public void onEvent(SuggestionEvent event) {
        if (event.getError() == null) {
            routeView.refreshSuggestions(event.getSuggestions());
        } else {
            routeView.showRefreshSuggestionsError();
        }
    }

    public void getSuggestion(String keyword, double latitude, double longitude) {
        suggestionInteractor.setKeyword(keyword);
        suggestionInteractor.setLatitude(latitude);
        suggestionInteractor.setLongitude(longitude);
        interactorInvoker.execute(suggestionInteractor);
    }
}