package me.bt.avea.presentation.model;

public class PresentationMobile {

    public Integer activityStatus;
    public String addr1;
    public String addr2;
    public String alias;
    public Integer icon_type;
    public String imageName;
    public Integer kontakDurumu;
    public String lbsCustomerPoint;
    public Double lbsCustomerPointDistance;
    public String lbsPoint;
    public Double lbsPointDistance;
    public Integer mobile;
    public Integer motorBlokajFlag;
    public Integer motorBlokajStatus;
    public Integer msg_flag;
    public Integer posDirectionValue;
    public Double posLatitude;
    public Double posLongitude;
    public Integer posSpeed;
    public String posTimestamp;
    public String posTimestampStrtime;
    public String SDisi3;
    public String SIsi1;
    public String SIsi2;
    public String SKapi;
    public String SNem;
    public Integer speedExceedLimit;
    public Integer speedExceedMax;
    public Integer speedLimit;
    public Integer telemetryFlag;
    public Integer themeIconId;
    public boolean isSelected = false;
    public boolean isSelectAll;

    public boolean checked = false;

    public boolean isChecked() {
        return checked;
    }

    public void setIsChecked(boolean checked) {
        this.checked = checked;
    }

    public Integer getThemeIconId() {
        return themeIconId;
    }

    public void setThemeIconId(Integer themeIconId) {
        this.themeIconId = themeIconId;
    }

    public Integer getActivityStatus() {
        return activityStatus;
    }

    public void setActivityStatus(Integer activityStatus) {
        this.activityStatus = activityStatus;
    }

    public String getAddr1() {
        return addr1;
    }

    public void setAddr1(String addr1) {
        this.addr1 = addr1;
    }

    public String getAddr2() {
        return addr2;
    }

    public void setAddr2(String addr2) {
        this.addr2 = addr2;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public Integer getIcon_type() {
        return icon_type;
    }

    public void setIcon_type(Integer icon_type) {
        this.icon_type = icon_type;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public Integer getKontakDurumu() {
        return kontakDurumu;
    }

    public void setKontakDurumu(Integer kontakDurumu) {
        this.kontakDurumu = kontakDurumu;
    }

    public String getLbsCustomerPoint() {
        return lbsCustomerPoint;
    }

    public void setLbsCustomerPoint(String lbsCustomerPoint) {
        this.lbsCustomerPoint = lbsCustomerPoint;
    }

    public Double getLbsCustomerPointDistance() {
        return lbsCustomerPointDistance;
    }

    public void setLbsCustomerPointDistance(Double lbsCustomerPointDistance) {
        this.lbsCustomerPointDistance = lbsCustomerPointDistance;
    }

    public String getLbsPoint() {
        return lbsPoint;
    }

    public void setLbsPoint(String lbsPoint) {
        this.lbsPoint = lbsPoint;
    }

    public Double getLbsPointDistance() {
        return lbsPointDistance;
    }

    public void setLbsPointDistance(Double lbsPointDistance) {
        this.lbsPointDistance = lbsPointDistance;
    }

    public Integer getMobile() {
        return mobile;
    }

    public void setMobile(Integer mobile) {
        this.mobile = mobile;
    }

    public Integer getMotorBlokajFlag() {
        return motorBlokajFlag;
    }

    public void setMotorBlokajFlag(Integer motorBlokajFlag) {
        this.motorBlokajFlag = motorBlokajFlag;
    }

    public Integer getMotorBlokajStatus() {
        return motorBlokajStatus;
    }

    public void setMotorBlokajStatus(Integer motorBlokajStatus) {
        this.motorBlokajStatus = motorBlokajStatus;
    }

    public Integer getMsg_flag() {
        return msg_flag;
    }

    public void setMsg_flag(Integer msg_flag) {
        this.msg_flag = msg_flag;
    }

    public Integer getPosDirectionValue() {
        return posDirectionValue;
    }

    public void setPosDirectionValue(Integer posDirectionValue) {
        this.posDirectionValue = posDirectionValue;
    }

    public Double getPosLatitude() {
        return posLatitude;
    }

    public void setPosLatitude(Double posLatitude) {
        this.posLatitude = posLatitude;
    }

    public Double getPosLongitude() {
        return posLongitude;
    }

    public void setPosLongitude(Double posLongitude) {
        this.posLongitude = posLongitude;
    }

    public Integer getPosSpeed() {
        return posSpeed;
    }

    public void setPosSpeed(Integer posSpeed) {
        this.posSpeed = posSpeed;
    }

    public String getPosTimestamp() {
        return posTimestamp;
    }

    public void setPosTimestamp(String posTimestamp) {
        this.posTimestamp = posTimestamp;
    }

    public String getPosTimestampStrtime() {
        return posTimestampStrtime;
    }

    public void setPosTimestampStrtime(String posTimestampStrtime) {
        this.posTimestampStrtime = posTimestampStrtime;
    }

    public String getSDisi3() {
        return SDisi3;
    }

    public void setSDisi3(String SDisi3) {
        this.SDisi3 = SDisi3;
    }

    public String getSIsi1() {
        return SIsi1;
    }

    public void setSIsi1(String SIsi1) {
        this.SIsi1 = SIsi1;
    }

    public String getSIsi2() {
        return SIsi2;
    }

    public void setSIsi2(String SIsi2) {
        this.SIsi2 = SIsi2;
    }

    public String getSKapi() {
        return SKapi;
    }

    public void setSKapi(String SKapi) {
        this.SKapi = SKapi;
    }

    public String getSNem() {
        return SNem;
    }

    public void setSNem(String SNem) {
        this.SNem = SNem;
    }

    public Integer getSpeedExceedLimit() {
        return speedExceedLimit;
    }

    public void setSpeedExceedLimit(Integer speedExceedLimit) {
        this.speedExceedLimit = speedExceedLimit;
    }

    public Integer getSpeedExceedMax() {
        return speedExceedMax;
    }

    public void setSpeedExceedMax(Integer speedExceedMax) {
        this.speedExceedMax = speedExceedMax;
    }

    public Integer getSpeedLimit() {
        return speedLimit;
    }

    public void setSpeedLimit(Integer speedLimit) {
        this.speedLimit = speedLimit;
    }

    public Integer getTelemetryFlag() {
        return telemetryFlag;
    }

    public void setTelemetryFlag(Integer telemetryFlag) {
        this.telemetryFlag = telemetryFlag;
    }
}
