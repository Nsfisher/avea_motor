package me.bt.base.presentation;

public abstract class Presenter {
  public abstract void onResume();

  public abstract void onPause();
}
