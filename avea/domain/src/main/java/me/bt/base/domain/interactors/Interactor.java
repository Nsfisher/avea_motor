package me.bt.base.domain.interactors;

public interface Interactor {
  void execute();
}
