package me.bt.base.domain.abstractions;

public interface Bus {
  void post(Object object);

  void register(Object object);

  void unregister(Object object);
}