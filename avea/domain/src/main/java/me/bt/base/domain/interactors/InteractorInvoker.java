package me.bt.base.domain.interactors;

public interface InteractorInvoker {
  void execute(Interactor interactor);

  void execute(Interactor interactor, InteractorPriority priority);
}
