package me.bt.avea.domain.interactions.mobiles;

import me.bt.avea.domain.entities.Mobile;
import me.bt.avea.domain.interactions.mobiles.events.MobileEvent;
import me.bt.avea.domain.interactions.mobiles.exception.MobileException;
import me.bt.avea.domain.repository.MobilesRepository;
import me.bt.base.domain.abstractions.Bus;
import me.bt.base.domain.interactors.Interactor;

public class MobileInteractor implements Interactor {
  private Bus bus;
  private MobilesRepository repository;
  private String mobileId;

  public MobileInteractor(Bus bus, MobilesRepository repository) {
    this.bus = bus;
    this.repository = repository;
  }

  public void setMobileId(String mobileId) {
    this.mobileId = mobileId;
  }

  @Override public void execute() {
    MobileEvent event = new MobileEvent();
    try {
      Mobile mobile = repository.getMobile(mobileId);
      event.setMobile(mobile);
    } catch (MobileException e) {
      event.setError(e);
    }
    bus.post(event);
  }
}
