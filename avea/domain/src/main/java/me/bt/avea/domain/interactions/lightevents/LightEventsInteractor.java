package me.bt.avea.domain.interactions.lightevents;

import java.util.List;
import me.bt.avea.domain.entities.LightEvent;
import me.bt.avea.domain.interactions.lightevents.events.LightEventsEvent;
import me.bt.avea.domain.interactions.lightevents.exceptions.LightEventsException;
import me.bt.avea.domain.repository.LightEventsRepository;
import me.bt.base.domain.abstractions.Bus;
import me.bt.base.domain.interactors.Interactor;

public class LightEventsInteractor implements Interactor {
  private static final int DEFAULT_ITEM_SIZE = 10;
  private Bus bus;
  private LightEventsRepository repository;
  private String mobileId;
  private int itemNumber = DEFAULT_ITEM_SIZE;

  public LightEventsInteractor(Bus bus, LightEventsRepository repository) {
    this.bus = bus;
    this.repository = repository;
  }

  @Override public void execute() {
    LightEventsEvent event = new LightEventsEvent();
    try {
      List<LightEvent> response = repository.getLightEvents(itemNumber);
      event.setLightEvents(response);
    } catch (LightEventsException e) {
      event.setError(e);
    }
    bus.post(event);
  }

  public void setMobileId(String mobileId) {
    this.mobileId = mobileId;
  }

  public void setItemNumber(int itemNumber) {
    this.itemNumber = itemNumber;
  }
}
