package me.bt.avea.domain.entities;

public class Suggestion {

  public double distance;
  public String fromTable;
  public String id;
  public double latitude;
  public double longitude;
  public String name;
}