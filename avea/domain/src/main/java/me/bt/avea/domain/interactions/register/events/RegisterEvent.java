package me.bt.avea.domain.interactions.register.events;

import me.bt.base.domain.interactors.BaseEvent;

public class RegisterEvent extends BaseEvent {

  public String status;

  private String token;

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }
}