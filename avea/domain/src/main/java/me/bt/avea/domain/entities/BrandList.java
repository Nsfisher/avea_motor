package me.bt.avea.domain.entities;

import com.google.gson.annotations.Expose;

import java.util.List;

public class BrandList extends BaseResponse {
    @Expose
    public List<Brand> brand;
}

