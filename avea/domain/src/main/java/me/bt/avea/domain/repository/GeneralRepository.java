package me.bt.avea.domain.repository;

import java.util.Date;

import me.bt.avea.domain.entities.BrandList;
import me.bt.avea.domain.entities.BrandModelList;
import me.bt.avea.domain.entities.EngineTypeList;
import me.bt.avea.domain.entities.EventScore;
import me.bt.avea.domain.entities.FuelTypeList;
import me.bt.avea.domain.entities.GeneralResponse;
import me.bt.avea.domain.entities.LightTrips;
import me.bt.avea.domain.entities.NotificationList;
import me.bt.avea.domain.entities.Regions;
import me.bt.avea.domain.entities.Terms;
import me.bt.avea.domain.interactions.general.exceptions.BrandListException;
import me.bt.avea.domain.interactions.general.exceptions.BrandModelListException;
import me.bt.avea.domain.interactions.general.exceptions.EngineTypeListException;
import me.bt.avea.domain.interactions.general.exceptions.EventScoreException;
import me.bt.avea.domain.interactions.general.exceptions.ForgetPasswordException;
import me.bt.avea.domain.interactions.general.exceptions.FuelTypeListException;
import me.bt.avea.domain.interactions.general.exceptions.LightTripsException;
import me.bt.avea.domain.interactions.general.exceptions.LogoutException;
import me.bt.avea.domain.interactions.general.exceptions.NotificationListException;
import me.bt.avea.domain.interactions.general.exceptions.RegionsException;
import me.bt.avea.domain.interactions.general.exceptions.TermsException;

public interface GeneralRepository {
    LightTrips getLightTrips(String mobileId, Date startDate, Date endDate) throws LightTripsException;

    EventScore getEventScore(String mobileId, Date startDate, Date endDate) throws EventScoreException;

    Regions getRegions() throws RegionsException;

    Terms getTerms() throws TermsException;

    GeneralResponse forgetPassword(String email) throws ForgetPasswordException;

    NotificationList getNotificationList() throws NotificationListException;

    BrandList getBrands() throws BrandListException;

    BrandModelList getBrandModels(String brandId) throws BrandModelListException;

    EngineTypeList getEngineType(String modelId) throws EngineTypeListException;

    FuelTypeList getFuelTypes() throws FuelTypeListException;

    GeneralResponse logout() throws LogoutException;
}
