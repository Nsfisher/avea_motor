package me.bt.avea.domain.repository;

import java.util.List;
import me.bt.avea.domain.entities.LightEvent;
import me.bt.avea.domain.interactions.lightevents.exceptions.LightEventsException;

public interface LightEventsRepository {
  List<LightEvent> getLightEvents(String mobileId, int itemNumber) throws LightEventsException;

  List<LightEvent> getLightEvents(int itemNumber) throws LightEventsException;
}
