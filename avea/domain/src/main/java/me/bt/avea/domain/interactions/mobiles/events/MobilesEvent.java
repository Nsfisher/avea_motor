package me.bt.avea.domain.interactions.mobiles.events;

import java.util.List;
import me.bt.avea.domain.entities.Mobile;
import me.bt.base.domain.interactors.BaseEvent;

public class MobilesEvent extends BaseEvent {

  private List<Mobile> mobiles;

  public List<Mobile> getMobiles() {
    return mobiles;
  }

  public void setMobiles(List<Mobile> mobiles) {
    this.mobiles = mobiles;
  }
}