package me.bt.avea.domain.interactions.trips;

import java.util.Date;
import java.util.List;
import me.bt.avea.domain.entities.IntervalData;
import me.bt.avea.domain.interactions.trips.events.TripsEvent;
import me.bt.avea.domain.interactions.trips.exceptions.TripsException;
import me.bt.avea.domain.repository.TripsRepository;
import me.bt.base.domain.abstractions.Bus;
import me.bt.base.domain.interactors.Interactor;

public class TripsInteractor implements Interactor {
  private Bus bus;
  private TripsRepository repository;
  private Date startDate;
  private Date endDate;
  private String mobileId;

  public TripsInteractor(Bus bus, TripsRepository repository) {
    this.bus = bus;
    this.repository = repository;
  }

  public void setTripDetail(String mobileId, Date startDate, Date endDate) {
    this.startDate = startDate;
    this.endDate = endDate;
    this.mobileId = mobileId;
  }

  @Override public void execute() {
    TripsEvent event = new TripsEvent();
    try {
      List<IntervalData> response = repository.getTrips(mobileId, startDate, endDate);
      event.setResponse(response);
    } catch (TripsException e) {
      event.setError(e);
    }
    bus.post(event);
  }
}
