package me.bt.avea.domain.entities;

import java.io.Serializable;

public class Region implements Serializable {

  public String id;
  public String latitude;
  public String longitude;
  public String name;
  public String tolerance;
}
