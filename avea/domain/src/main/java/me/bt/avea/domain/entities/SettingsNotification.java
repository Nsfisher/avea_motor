package me.bt.avea.domain.entities;

public class SettingsNotification {
  public String description;
  public String key;
  public String status;
}
