package me.bt.avea.domain.interactions.adduser;

import me.bt.avea.domain.entities.BaseResponse;
import me.bt.avea.domain.interactions.adduser.events.AddUserEvent;
import me.bt.avea.domain.interactions.adduser.exceptions.AddUserException;
import me.bt.avea.domain.repository.AddUserRepository;
import me.bt.base.domain.abstractions.Bus;
import me.bt.base.domain.interactors.Interactor;

public class AddUserInteractor implements Interactor {
  private Bus bus;
  private AddUserRepository repository;
  private String activation;
  private String username;
  private String password;
  private String plaka;
  private String nickname;
  private String brand;
  private String model;
  private String fuelType;
  private String engineInfo;

  public AddUserInteractor(Bus bus, AddUserRepository repository) {
    this.bus = bus;
    this.repository = repository;
  }

  public void setUserInfo(String activation, String username, String password, String plaka, String nickname, String brand, String model,
      String fuelType, String engineInfo) {
    this.activation = activation;
    this.username = username;
    this.password = password;
    this.plaka = plaka;
    this.nickname = nickname;
    this.brand = brand;
    this.model = model;
    this.fuelType = fuelType;
    this.engineInfo = engineInfo;
  }

  @Override public void execute() {
    AddUserEvent event = new AddUserEvent();
    try {
      BaseResponse response = repository.addUser(activation, username, password, plaka, nickname, brand, model, fuelType, engineInfo);
      event.setResponse(response);
    } catch (AddUserException e) {
      event.setError(e);
    }
    bus.post(event);
  }
}
