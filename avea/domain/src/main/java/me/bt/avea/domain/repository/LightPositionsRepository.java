package me.bt.avea.domain.repository;

import java.util.List;
import me.bt.avea.domain.entities.DataLightPosition;
import me.bt.avea.domain.interactions.lightevents.exceptions.LightPositionsException;

public interface LightPositionsRepository {
  List<DataLightPosition> getLightPositions(String mobileId, String startDate, String endDate, int maxPoints, int queryMode, int lineMode)
      throws LightPositionsException;
}
