package me.bt.avea.domain.repository;

import java.util.List;
import me.bt.avea.domain.entities.Mobile;
import me.bt.avea.domain.interactions.mobiles.exception.MobileException;
import me.bt.avea.domain.interactions.mobiles.exception.MobilesException;

public interface MobilesRepository {
  List<Mobile> getMobiles() throws MobilesException;

  List<Mobile> getMobilesSkipCache() throws MobilesException;

  Mobile getMobile(String mobileId) throws MobileException;
}
