package me.bt.avea.domain.interactions.trips.events;

import java.util.List;
import me.bt.avea.domain.entities.IntervalData;
import me.bt.base.domain.interactors.BaseEvent;

public class TripsEvent extends BaseEvent {

  private List<IntervalData> response;

  public List<IntervalData> getResponse() {
    return response;
  }

  public void setResponse(List<IntervalData> response) {
    this.response = response;
  }
}