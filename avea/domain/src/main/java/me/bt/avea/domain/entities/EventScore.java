package me.bt.avea.domain.entities;

import java.util.List;

public class EventScore {
  public String errorCount;
  public String score;
  public List<EventScoreLg> lg;
}
