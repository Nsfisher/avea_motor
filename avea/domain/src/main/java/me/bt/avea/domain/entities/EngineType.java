package me.bt.avea.domain.entities;

import com.google.gson.annotations.Expose;

public class EngineType {
  @Expose public String id;
  @Expose public String modelId;
  @Expose public String modelSeries;
}
