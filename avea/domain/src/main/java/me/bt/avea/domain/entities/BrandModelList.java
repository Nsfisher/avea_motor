package me.bt.avea.domain.entities;

import com.google.gson.annotations.Expose;
import java.util.List;

public class BrandModelList {
  @Expose public List<BrandModel> model;
}
