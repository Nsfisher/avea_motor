package me.bt.avea.domain.repository;

import me.bt.avea.domain.interactions.register.exceptions.RegisterException;

public interface RegisterRepository {
  String register(String username, String password, String company, String language) throws RegisterException;
}
