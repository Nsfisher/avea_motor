package me.bt.avea.domain.entities;

import com.google.gson.annotations.Expose;

public class FuelType {

    @Expose
    public String fuelType;
    @Expose
    public String id;
}
