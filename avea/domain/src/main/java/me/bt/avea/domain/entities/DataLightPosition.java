package me.bt.avea.domain.entities;

import java.io.Serializable;

public class DataLightPosition implements Serializable {
    public String eventCode;
    public String eventDescription;
    public String latitude;
    public String location;
    public String longitude;
}
