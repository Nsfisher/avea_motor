package me.bt.avea.domain.interactions.register;

import me.bt.avea.domain.interactions.register.events.RegisterEvent;
import me.bt.avea.domain.interactions.register.exceptions.RegisterException;
import me.bt.avea.domain.repository.RegisterRepository;
import me.bt.base.domain.abstractions.Bus;
import me.bt.base.domain.interactors.Interactor;

public class RegisterInteractor implements Interactor {
    private Bus bus;
    private RegisterRepository repository;
    private String username;
    private String password;
    private String company;
    private String language;

    public RegisterInteractor(Bus bus, RegisterRepository repository) {
        this.bus = bus;
        this.repository = repository;
    }

    public void setData(String username, String password, String company, String language) {
        this.username = username;
        this.password = password;
        this.company = company;
        this.language = language;
    }

    @Override
    public void execute() {
        RegisterEvent event = new RegisterEvent();
        try {
            String token = repository.register(username, password, company, language);
            event.setToken(token);
        } catch (RegisterException e) {
            event.setError(e);
        }
        bus.post(event);
    }
}
