package me.bt.avea.domain.interactions.suggestion;

import java.util.List;
import me.bt.avea.domain.entities.Suggestion;
import me.bt.avea.domain.interactions.suggestion.events.SuggestionEvent;
import me.bt.avea.domain.interactions.suggestion.exceptions.SuggestionException;
import me.bt.avea.domain.repository.SuggestionRepository;
import me.bt.base.domain.abstractions.Bus;
import me.bt.base.domain.interactors.Interactor;

public class SuggestionInteractor implements Interactor {
  private Bus bus;
  private SuggestionRepository repository;
  private String keyword;
  private double latitude;
  private double longitude;

  public SuggestionInteractor(Bus bus, SuggestionRepository repository) {
    this.bus = bus;
    this.repository = repository;
  }

  @Override public void execute() {
    SuggestionEvent event = new SuggestionEvent();
    try {
      List<Suggestion> suggestions = repository.getSuggestion(keyword, latitude, longitude);
      event.setSuggestions(suggestions);
    } catch (SuggestionException e) {
      event.setError(e);
    }
    bus.post(event);
  }

  public void setKeyword(String keyword) {
    this.keyword = keyword;
  }

  public void setLatitude(double latitude) {
    this.latitude = latitude;
  }

  public void setLongitude(double longitude) {
    this.longitude = longitude;
  }
}
