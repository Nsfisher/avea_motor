package me.bt.avea.domain.entities;

public class LightEvent {
  public String alias;
  public String eventCode;
  public String eventDescription;
  public String eventValue1;
  public String eventValue2;
  public String eventValue3;
  public String exceptionDescription;
  public String exceptionNo;
  public String lbsLocation;
  public String mobile;
  public String posLatitude;
  public String posLongitude;
  public String posSpeed;
  public String posTimeStamp;
  public String posTimeStr;
  public String transactionId;
  public boolean isRead = true; //TODO hakan false
}
