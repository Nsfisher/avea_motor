package me.bt.avea.domain.interactions.adduser.events;

import me.bt.avea.domain.entities.BaseResponse;
import me.bt.base.domain.interactors.BaseEvent;

public class AddUserEvent extends BaseEvent {

  private BaseResponse response;

  public BaseResponse getResponse() {
    return response;
  }

  public void setResponse(BaseResponse response) {
    this.response = response;
  }
}