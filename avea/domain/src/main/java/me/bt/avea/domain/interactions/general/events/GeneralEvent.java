package me.bt.avea.domain.interactions.general.events;

import me.bt.base.domain.interactors.BaseEvent;

public class GeneralEvent extends BaseEvent {

  private Object data;
  private int requestType;

  public Object getData() {
    return data;
  }

  public void setData(Object data) {
    this.data = data;
  }

  public int getRequestType() {
    return requestType;
  }

  public void setRequestType(int requestType) {
    this.requestType = requestType;
  }
}