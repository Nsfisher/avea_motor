package me.bt.avea.domain.interactions.lightevents.events;

import java.util.List;
import me.bt.avea.domain.entities.LightEvent;
import me.bt.base.domain.interactors.BaseEvent;

public class LightEventsEvent extends BaseEvent {

  private List<LightEvent> lightEvents;

  public List<LightEvent> getLightEvents() {
    return lightEvents;
  }

  public void setLightEvents(List<LightEvent> lightEvents) {
    this.lightEvents = lightEvents;
  }
}