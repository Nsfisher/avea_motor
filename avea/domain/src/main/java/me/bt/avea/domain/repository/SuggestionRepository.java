package me.bt.avea.domain.repository;

import java.util.List;
import me.bt.avea.domain.entities.Suggestion;
import me.bt.avea.domain.interactions.suggestion.exceptions.SuggestionException;

public interface SuggestionRepository {
  List<Suggestion> getSuggestion(String keyword, double latitude, double longitude) throws SuggestionException;
}
