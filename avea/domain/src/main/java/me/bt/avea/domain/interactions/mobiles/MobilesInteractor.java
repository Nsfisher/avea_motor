package me.bt.avea.domain.interactions.mobiles;

import java.util.List;

import me.bt.avea.domain.entities.Mobile;
import me.bt.avea.domain.interactions.mobiles.events.MobilesEvent;
import me.bt.avea.domain.interactions.mobiles.exception.MobilesException;
import me.bt.avea.domain.repository.MobilesRepository;
import me.bt.base.domain.abstractions.Bus;
import me.bt.base.domain.interactors.Interactor;

public class MobilesInteractor implements Interactor {
    private Bus bus;
    private MobilesRepository repository;
    private boolean skipCache = false;

    public MobilesInteractor(Bus bus, MobilesRepository repository) {
        this.bus = bus;
        this.repository = repository;
    }

    public void setSkipCache(boolean skipCache) {
        this.skipCache = skipCache;
    }

    @Override
    public void execute() {
        MobilesEvent event = new MobilesEvent();
        try {
            List<Mobile> mobiles;
            if (skipCache) {
                mobiles = repository.getMobilesSkipCache();
            } else {
                mobiles = repository.getMobiles();
            }
            event.setMobiles(mobiles);
        } catch (MobilesException e) {
            event.setError(e);
        }
        bus.post(event);
    }
}
