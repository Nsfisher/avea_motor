package me.bt.avea.domain.interactions.general;

import java.util.Date;

import me.bt.avea.domain.interactions.general.events.GeneralEvent;
import me.bt.avea.domain.interactions.general.exceptions.BrandListException;
import me.bt.avea.domain.interactions.general.exceptions.BrandModelListException;
import me.bt.avea.domain.interactions.general.exceptions.EngineTypeListException;
import me.bt.avea.domain.interactions.general.exceptions.EventScoreException;
import me.bt.avea.domain.interactions.general.exceptions.ForgetPasswordException;
import me.bt.avea.domain.interactions.general.exceptions.FuelTypeListException;
import me.bt.avea.domain.interactions.general.exceptions.LightTripsException;
import me.bt.avea.domain.interactions.general.exceptions.LogoutException;
import me.bt.avea.domain.interactions.general.exceptions.NotificationListException;
import me.bt.avea.domain.interactions.general.exceptions.RegionsException;
import me.bt.avea.domain.interactions.general.exceptions.TermsException;
import me.bt.avea.domain.repository.GeneralRepository;
import me.bt.base.domain.abstractions.Bus;
import me.bt.base.domain.interactors.Interactor;

public class GeneralInteractor implements Interactor {
  public static final int REQUEST_TERMS = 0;
  public static final int REQUEST_LIGHTTRIPS = 1;
  public static final int REQUEST_EVENT_SCORE = 2;
  public static final int REQUEST_GET_REGIONS = 3;
  public static final int REQUEST_FORGET_PASSWORD = 4;
  public static final int REQUEST_NOTIFICATION_LIST = 5;
  public static final int REQUEST_GET_BRANDS = 6;
  public static final int REQUEST_GET_BRAND_MODELS = 7;
  public static final int REQUEST_GET_ENGINE_TYPE = 8;
  public static final int REQUEST_GET_FUEL_TYPES = 9;
  public static final int REQUEST_LOGOUT = 10;

  private Bus bus;
  private GeneralRepository repository;
  private int requestCode;
  private String mobileId;
  private Date startDate;
  private Date endDate;
  private String email;
  private String brandId;
  private String modelId;

  public GeneralInteractor(Bus bus, GeneralRepository repository) {
    this.bus = bus;
    this.repository = repository;
  }

  public void setRequest(int requestCode) {
    this.requestCode = requestCode;
  }

  public void setBrandId(String brandId) {
    this.brandId = brandId;
  }

  public void setLightTrips(String mobileId, Date startDate, Date endDate){
    this.mobileId = mobileId;
    this.startDate = startDate;
    this.endDate = endDate;
  }

  public void setEventScore(String mobileId, Date startDate, Date endDate){
    this.mobileId = mobileId;
    this.startDate = startDate;
    this.endDate = endDate;
  }

  @Override public void execute() {
    GeneralEvent event = new GeneralEvent();
    switch (requestCode) {
      case REQUEST_EVENT_SCORE:
        try {
          event.setData(repository.getEventScore(mobileId, startDate, endDate));
          event.setRequestType(REQUEST_EVENT_SCORE);
        } catch (EventScoreException e) {
          event.setError(e);
        }
        break;
      case REQUEST_LIGHTTRIPS:
        try {
          event.setData(repository.getLightTrips(mobileId, startDate, endDate));
          event.setRequestType(REQUEST_LIGHTTRIPS);
        } catch (LightTripsException e) {
          event.setError(e);
        }
        break;
      case REQUEST_GET_REGIONS:
        try {
          event.setData(repository.getRegions());
          event.setRequestType(REQUEST_GET_REGIONS);
        } catch (RegionsException e) {
          event.setError(e);
        }
        break;
      case REQUEST_TERMS:
        try{
          event.setData(repository.getTerms());
          event.setRequestType(REQUEST_TERMS);
        }
        catch (TermsException ex){
          event.setError(ex);
        }
        break;
      case REQUEST_FORGET_PASSWORD:
        try{
          event.setData(repository.forgetPassword(email));
          event.setRequestType(REQUEST_FORGET_PASSWORD);
        }
        catch (ForgetPasswordException ex){
          event.setError(ex);
        }
        break;
      case REQUEST_NOTIFICATION_LIST:
        try{
          event.setData(repository.getNotificationList());
          event.setRequestType(REQUEST_NOTIFICATION_LIST);
        }
        catch (NotificationListException ex){
          event.setError(ex);
        }
        break;
      case REQUEST_GET_BRANDS:
        try{
          event.setData(repository.getBrands());
          event.setRequestType(REQUEST_GET_BRANDS);
        }
        catch (BrandListException ex){
          event.setError(ex);
        }
        break;
      case REQUEST_GET_BRAND_MODELS:
        try{
          event.setData(repository.getBrandModels(brandId));
          event.setRequestType(REQUEST_GET_BRAND_MODELS);
        }
        catch (BrandModelListException ex){
          event.setError(ex);
        }
        break;
      case REQUEST_GET_ENGINE_TYPE:
        try{
          event.setData(repository.getEngineType(modelId));
          event.setRequestType(REQUEST_GET_ENGINE_TYPE);
        }
        catch (EngineTypeListException ex){
          event.setError(ex);
        }
        break;
      case REQUEST_GET_FUEL_TYPES:
        try{
          event.setData(repository.getFuelTypes());
          event.setRequestType(REQUEST_GET_FUEL_TYPES);
        }
        catch (FuelTypeListException ex){
          event.setError(ex);
        }
        break;
      case REQUEST_LOGOUT:
        try{
          event.setData(repository.logout());
          event.setRequestType(REQUEST_LOGOUT);
        }
        catch (LogoutException ex){
          event.setError(ex);
        }
        break;
    }
    bus.post(event);
  }

  public void setEmail(String email) {
    this.email = email;
  }
  public void setModelId(String modelId) {
    this.modelId= modelId;
  }


}
