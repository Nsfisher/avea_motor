package me.bt.avea.domain.entities;

public class GeneralResponse {

  public String data;
  public String errorDesc;
  public String status;
}
