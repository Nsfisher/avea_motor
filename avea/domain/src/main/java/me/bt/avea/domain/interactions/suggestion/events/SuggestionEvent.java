package me.bt.avea.domain.interactions.suggestion.events;

import java.util.List;
import me.bt.avea.domain.entities.Suggestion;
import me.bt.base.domain.interactors.BaseEvent;

public class SuggestionEvent extends BaseEvent {

  private List<Suggestion> suggestions;

  public List<Suggestion> getSuggestions() {
    return suggestions;
  }

  public void setSuggestions(List<Suggestion> suggestions) {
    this.suggestions = suggestions;
  }
}