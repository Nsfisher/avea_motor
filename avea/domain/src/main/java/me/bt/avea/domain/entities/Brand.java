package me.bt.avea.domain.entities;

import com.google.gson.annotations.Expose;

public class Brand {
  @Expose public String id;
  @Expose public String name;
}
