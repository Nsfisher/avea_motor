package me.bt.avea.domain.entities;

public class BaseResponse {
  public String data;
  public String errorDesc;
  public int status;
}
