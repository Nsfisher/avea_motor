package me.bt.avea.domain.interactions.lightevents;

import java.util.List;
import me.bt.avea.domain.entities.DataLightPosition;
import me.bt.avea.domain.interactions.lightevents.events.LightPositionsEvent;
import me.bt.avea.domain.interactions.lightevents.exceptions.LightPositionsException;
import me.bt.avea.domain.repository.LightPositionsRepository;
import me.bt.base.domain.abstractions.Bus;
import me.bt.base.domain.interactors.Interactor;

public class LightPositionsInteractor implements Interactor {
  private Bus bus;
  private LightPositionsRepository repository;
  private String mobileId;
  private int queryMode;
  private int maxPoints;
  private String endDate;
  private String startDate;
  private int lineMode;

  public LightPositionsInteractor(Bus bus, LightPositionsRepository repository) {
    this.bus = bus;
    this.repository = repository;
  }

  @Override public void execute() {
    LightPositionsEvent event = new LightPositionsEvent();
    try {
      List<DataLightPosition> response = repository.getLightPositions(mobileId, startDate, endDate, maxPoints, queryMode, lineMode);
      event.setLightPositions(response);
    } catch (LightPositionsException e) {
      event.setError(e);
    }
    bus.post(event);
  }

  public void setLightPositionsRequestParams(String mobileId, String startDate, String endDate, int maxPoints, int queryMode,
      int lineMode) {
    this.mobileId = mobileId;
    this.startDate = startDate;
    this.endDate = endDate;
    this.maxPoints = maxPoints;
    this.queryMode = queryMode;
    this.lineMode = lineMode;
  }
}
