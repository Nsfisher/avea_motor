package me.bt.avea.domain.interactions.lightevents.events;

import java.util.List;
import me.bt.avea.domain.entities.DataLightPosition;
import me.bt.base.domain.interactors.BaseEvent;

public class LightPositionsEvent extends BaseEvent {

  private List<DataLightPosition> lightPositions;

  public List<DataLightPosition> getLightPositions() {
    return lightPositions;
  }

  public void setLightPositions(List<DataLightPosition> lightPositions) {
    this.lightPositions = lightPositions;
  }
}