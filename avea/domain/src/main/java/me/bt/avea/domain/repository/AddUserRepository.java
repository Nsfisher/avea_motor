package me.bt.avea.domain.repository;

import me.bt.avea.domain.entities.BaseResponse;
import me.bt.avea.domain.interactions.adduser.exceptions.AddUserException;

public interface AddUserRepository {
  BaseResponse addUser(String activation, String username, String password, String plaka, String nickname, String brand, String model,
      String fuelType, String engineInfo) throws AddUserException;
}
