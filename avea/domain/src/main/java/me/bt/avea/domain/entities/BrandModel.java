package me.bt.avea.domain.entities;

import com.google.gson.annotations.Expose;

public class BrandModel {

    @Expose
    public String id;
    @Expose
    public String markaId;
    @Expose
    public String name;
}
