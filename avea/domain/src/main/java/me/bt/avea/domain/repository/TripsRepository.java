package me.bt.avea.domain.repository;

import java.util.Date;
import java.util.List;
import me.bt.avea.domain.entities.IntervalData;
import me.bt.avea.domain.interactions.trips.exceptions.TripsException;

public interface TripsRepository {
  List<IntervalData> getTrips(String mobileId, Date startTime, Date endTime) throws TripsException;
}
