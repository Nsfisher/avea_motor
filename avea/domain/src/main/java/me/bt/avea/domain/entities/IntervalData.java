package me.bt.avea.domain.entities;

import java.io.Serializable;
import java.util.List;

public class IntervalData implements Serializable {
  public String alias;
  public String startPosTimeStamp;
  public String stopPosTimeStamp;
  public String startLbsLocation;
  public String stopLbsLocation;
  public String distanceTako;
  public String deltaTime;
  public String stopKm;
  public String speedMax;
  public String speedAvg;
  public String startKm;
  public String rolantiTotal;
  public String distanceGps;
  public String speedExceedTime;
  public String eventCount;
  public List<LogEvent> logEvents;
}
