package me.bt.avea.domain.interactions.mobiles.events;

import me.bt.avea.domain.entities.Mobile;
import me.bt.base.domain.interactors.BaseEvent;

public class MobileEvent extends BaseEvent {

  private Mobile mobile;

  public Mobile getMobile() {
    return mobile;
  }

  public void setMobile(Mobile mobile) {
    this.mobile = mobile;
  }
}