package me.bt.avea.domain.entities;

import java.io.Serializable;

public class LogEvent implements Serializable {
  public String count;
  public String eventCode;
  public String exceptionNo;

  public String getExceptionNo() {
    return exceptionNo;
  }

  public void setExceptionNo(String exceptionNo) {
    this.exceptionNo = exceptionNo;
  }

  public String getEventCode() {
    return eventCode;
  }

  public void setEventCode(String eventCode) {
    this.eventCode = eventCode;
  }

  public String getCount() {
    return count;
  }

  public void setCount(String count) {
    this.count = count;
  }
}
