
-keep class com.wnafee.vector.** { *; }

-keep class com.squareup.moshi.** { *; }
-keep interface com.squareup.moshi.** { *; }
-dontwarn com.squareup.moshi.**

-dontwarn rx.**
-dontwarn retrofit.**
-dontwarn okio.**
-keep class retrofit.** { *; }
-keepclasseswithmembers class * {
    @retrofit.http.* <methods>;
}

# Okio
-keep class sun.misc.Unsafe { *; }
-dontwarn java.nio.file.*
-dontwarn org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement
-dontwarn okio.**

# OkHttp
-keepattributes Signature
-keepattributes *Annotation*
-keep class com.squareup.okhttp.** { *; }
-keep interface com.squareup.okhttp.** { *; }
-dontwarn com.squareup.okhttp.**

-keepattributes EnclosingMethod

# Gson specific classes
-keep class sun.misc.Unsafe { *; }
-keep class com.google.gson.stream.** { *; }

-keepattributes InnerClasses

# Retrofit 1.X
-keep class com.squareup.okhttp.** { *; }
-keep class retrofit.** { *; }
-keep interface com.squareup.okhttp.** { *; }

-dontwarn com.squareup.okhttp.**
-dontwarn okio.**
-dontwarn retrofit.**
-dontwarn rx.**

-keepclasseswithmembers class * {
    @retrofit.http.* <methods>;
}
-keepattributes Exceptions


# Renderscript support

-keepclasseswithmembernames class * {
    native <methods>;
}

-keep class android.support.v8.renderscript.** { *; }

-dontwarn android.support.design.**
-keep class android.support.design.** { *; }
-keep interface android.support.design.** { *; }
-keep public class android.support.design.R$* { *; }

-keep public class android.support.v7.widget.** { *; }
-keep public class android.support.v7.internal.widget.** { *; }
-keep public class android.support.v7.internal.view.menu.** { *; }

-keep public class * extends android.support.v4.view.ActionProvider {
    public <init>(android.content.Context);
}

-keep public class android.support.v7.preference.** { *; }

-keep public class android.support.v14.preference.** { *; }

# Crashlytics 1.+
-keep class com.crashlytics.** { *; }
-keepattributes SourceFile,LineNumberTable

#CardView
-keep class android.support.v7.widget.RoundRectDrawable { *; }

# ButterKnife 6
-keep class butterknife.** { *; }
-dontwarn butterknife.internal.**
-keep class **$$ViewInjector { *; }
-keepclasseswithmembernames class * {
    @butterknife.* <fields>;
}

-keepclasseswithmembernames class * {
    @butterknife.* <methods>;
}

# RxJava 0.21
-keep class rx.schedulers.Schedulers {
    public static <methods>;
}
-keep class rx.schedulers.ImmediateScheduler {
    public <methods>;
}
-keep class rx.schedulers.TestScheduler {
    public <methods>;
}
-keep class rx.schedulers.Schedulers {
    public static ** test();
}

## GreenRobot EventBus specific rules ##
# https://github.com/greenrobot/EventBus/blob/master/HOWTO.md#proguard-configuration
-keepclassmembers class ** {
    public void onEvent*(***);
}

# Only required if you use AsyncExecutor
-keepclassmembers class * extends de.greenrobot.event.util.ThrowableFailureEvent {
    public <init>(java.lang.Throwable);
}

# Don't warn for missing support classes
-dontwarn de.greenrobot.event.util.*$Support
-dontwarn de.greenrobot.event.util.*$SupportManagerFragment

## joda-time-android 2.8.0
# This is only necessary if you are not including the optional joda-convert dependency
-dontwarn org.joda.convert.FromString
-dontwarn org.joda.convert.ToString

#Calligraphy
-keepclassmembers class fqcn.of.javascript.interface.for.webview {
   public *;
}
-keep class uk.co.chrisjenx.calligraphy.* { *; }
-keep class uk.co.chrisjenx.calligraphy.*$* { *; }

-keep class me.zhanghai.android.materialprogressbar.** { *; }

-keep class cn.pedant.SweetAlert.Rotate3dAnimation {
    public <init>(...);
}

-keep class com.google.zxing.client.android.camera.open.**
-keep class com.google.zxing.client.android.camera.exposure.**
-keep class com.google.zxing.client.android.common.executor.**

# OrmLite uses reflection
-keep class com.j256.**
-keepclassmembers class com.j256.** { *; }
-keep enum com.j256.**
-keepclassmembers enum com.j256.** { *; }
-keep interface com.j256.**
-keepclassmembers interface com.j256.** { *; }
# Keep the helper class and its constructor
-keep class * extends com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper
-keepclassmembers class * extends com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper {
  public <init>(android.content.Context);
}

# Keep the annotations
-keepattributes *Annotation*

# Keep all model classes that are used by OrmLite
# Also keep their field names and the constructor
-keep @com.j256.ormlite.table.DatabaseTable class * {
    @com.j256.ormlite.field.DatabaseField <fields>;
    @com.j256.ormlite.field.ForeignCollectionField <fields>;
    <init>();
}
-keepclassmembers class **DateTime {
    <init>(long);
    long getMillis();
}

#Dagger
-keepattributes *Annotation*

-keepclassmembers,allowobfuscation class * {
    @javax.inject.* *;
    @dagger.* *;
    <init>();
}

-keep class * extends dagger.internal.Binding
-keep class * extends dagger.internal.ModuleAdapter

-keep class **$$ModuleAdapter
-keep class **$$InjectAdapter
-keep class **$$StaticInjection

-keep class dagger.* { *; }

-keep class javax.inject.* { *; }
-keep class * extends dagger.internal.Binding
-keep class * extends dagger.internal.ModuleAdapter
-keep class * extends dagger.internal.StaticInjection

-keep !abstract class me.bt.motor.di.** { *; }
-keep !abstract class me.bt.base.di.** { *; }
-keep !abstract class me.bt.base.** { *; }

-keepnames class dagger.Lazy

-keep class me.bt.avea.domain.entities.** { *; }
-keep class me.bt.motor.di.** { *; }

#        libRecyclerView = 'com.android.support:recyclerview-v7:22.0.+'
#        libRecyclerAdapters = 'com.github.cmc00022:easyrecycleradapters:1.0.5'
#        libTray = 'net.grandcentrix.tray:tray:0.9.1'
#
#        libPathQueue = 'com.path:android-priority-jobqueue:1.1.2'
#        libMappers = 'com.mobandme:android-transformer:1.1.1'
#        libMappersCompiler = 'com.mobandme:android-transformer-compiler:1.1.1'
#
#        libOrmlite = 'com.j256.ormlite:ormlite-android:4.48'
#        libSnackBars = 'com.nispok:snackbar:2.10.9'
#        libByteUnits = 'com.jakewharton.byteunits:byteunits:0.9.0'
#        libReactiveLocation = 'pl.charmas.android:android-reactive-location:0.6@aar'
#        libMarkView = 'com.github.xiprox.markview:library:1.0.0'
#        libZxing = 'com.google.zxing:core:3.2.0'
#        libAdvRecyclerView = 'com.h6ah4i.android.widget.advrecyclerview:advrecyclerview:0.7.1'
#        libDrawer = 'com.mikepenz:materialdrawer:3.0.4@aar'
#        libSeekBar = 'org.adw.library:discrete-seekbar:1.0.0'
#        libSupertooltips = 'com.nhaarman.supertooltips:library:3.0.0'
#        libMaterialIcons = 'com.malinskiy:materialicons:1.0.1'
#        libMaterialPre = 'com.github.navasmdc:MaterialDesign:1.5@aar'
