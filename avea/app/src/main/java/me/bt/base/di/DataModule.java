package me.bt.base.di;

import android.app.Application;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.path.android.jobqueue.JobManager;
import com.squareup.okhttp.Cache;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import me.bt.motor.di.AveaDataModule;
import me.bt.motor.BuildConfig;
import me.bt.base.data.Clock;
import me.bt.base.data.DateConverter;
import me.bt.base.data.IntentFactory;
import me.bt.base.data.NullStringToEmptyAdapterFactory;
import me.bt.base.data.RetrofitLog;
import me.bt.base.data.UserAgent;
import me.bt.base.domain.BusImp;
import me.bt.base.domain.InteractorInvokerImp;
import me.bt.base.domain.abstractions.Bus;
import me.bt.base.domain.interactors.InteractorInvoker;
import me.bt.base.ui.imageloader.ImageLoader;
import me.bt.base.ui.imageloader.PicassoImageLoader;
import me.bt.motor.util.Utils;
import retrofit.Endpoint;
import retrofit.Endpoints;
import timber.log.Timber;

import static android.content.Context.MODE_PRIVATE;
import static com.jakewharton.byteunits.DecimalByteUnit.MEGABYTES;
import static java.util.concurrent.TimeUnit.SECONDS;

@Module(
        includes = {
                InteractorsModule.class, RepositoryModule.class, AveaDataModule.class
        },
        complete = false,
        library = true)
public class DataModule {

    static final int DISK_CACHE_SIZE = (int) MEGABYTES.toBytes(50);

    static OkHttpClient createOkHttpClient(Application app) {
        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(10, SECONDS);
        client.setReadTimeout(10, SECONDS);
        client.setWriteTimeout(10, SECONDS);

        // Install an HTTP cache in the application cache directory.
        File cacheDir = new File(app.getCacheDir(), "http");
        Cache cache = null;
        try {
            cache = new Cache(cacheDir, DISK_CACHE_SIZE);
        } catch (IOException e) {
            e.printStackTrace();
        }
        client.setCache(cache);

        Utils.installCertificatePinning(client);
        return client;
    }

    @Provides
    @Singleton
    Endpoint provideEndpoint() {
        return Endpoints.newFixedEndpoint(BuildConfig.API_URL);
    }

    @Provides
    @Singleton
    @RetrofitLog
    boolean provideRetrofitLog() {
        return BuildConfig.RETROFIT_LOG;
    }

    @Provides
    @Singleton
    @UserAgent
    String provideUserAgent() {
        return String.format("Sample-Android;%s;%s;%s;%d;", Build.MANUFACTURER, Build.MODEL, Build.VERSION.RELEASE, BuildConfig.VERSION_CODE);
    }

    @Provides
    @Singleton
    Bus provideEventbus() {
        return new BusImp();
    }

    @Provides
    @Singleton
    JobManager provideJobManager(Application app) {
        return new JobManager(app);
    }

    @Provides
    @Singleton
    InteractorInvoker provideInteractorInvoker(JobManager jobManager, Bus bus) {
        return new InteractorInvokerImp(jobManager);
    }

    @Provides
    @Singleton
    ImageLoader provideImageLoader(Picasso picasso) {
        return new PicassoImageLoader(picasso);
    }

    @Provides
    @Singleton
    Gson provideGson() {
        return new GsonBuilder().setDateFormat("yyyy-MM-dd+HH:mm:ss")
                .registerTypeAdapter(Date.class, new DateConverter())
                .registerTypeAdapterFactory(new NullStringToEmptyAdapterFactory())
                .create(); //TODO remove
    }

    @Provides
    @Singleton
    Clock provideClock() {
        return Clock.REAL;
    }

    @Provides
    @Singleton
    IntentFactory provideIntentFactory() {
        return IntentFactory.REAL;
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient(Application app) {
        return createOkHttpClient(app);
    }

    @Provides
    @Singleton
    Picasso providePicasso(Application app, OkHttpClient client) {
        return new Picasso.Builder(app).downloader(new OkHttpDownloader(client)).listener(new Picasso.Listener() {
            @Override
            public void onImageLoadFailed(Picasso picasso, Uri uri, Exception e) {
                Timber.e(e, "Failed to load image: %s", uri);
            }
        }).build();
    }

    @Provides
    @Singleton
    SharedPreferences provideSharedPreferences(Application app) {
        return app.getSharedPreferences("mebtbase", MODE_PRIVATE);
    }
}
