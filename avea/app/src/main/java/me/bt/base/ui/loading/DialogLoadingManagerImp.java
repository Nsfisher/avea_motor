package me.bt.base.ui.loading;

import android.app.Activity;

import com.afollestad.materialdialogs.MaterialDialog;

import me.bt.motor.R;

public class DialogLoadingManagerImp implements LoadingManager {

    private Activity activity;
    private MaterialDialog dialog;

    public DialogLoadingManagerImp(Activity activity) {
        this.activity = activity;
    }

    @Override
    public void showLoading() {
        showLoading("", "", false);
    }

    @Override
    public void showLoading(String title, String message) {
        showLoading("", message, false);
    }

    @Override
    public void showLoading(String message) {
        showLoading("", message);
    }

    @Override
    public void showLoading(int message) {
        showLoading(activity.getResources().getString(message));
    }

    @Override
    public void showLoading(String message, boolean isCancelable) {
        showLoading(message, isCancelable);
    }

    @Override
    public void showLoading(String title, String message, boolean isCancelable) {
        MaterialDialog.Builder builder = new MaterialDialog.Builder(activity); //.progress(true, 0)
        if (title != null && title.length() > 0) {
            builder = builder.title(title);
        }
        if (message != null && message.length() > 0) {
            builder = builder.content(message);
        }
        if (isCancelable) {
            builder = builder.cancelable(true);
        }
        if (dialog != null && dialog.isShowing()) {
            try {
                dialog.dismiss();
            } catch (Exception ex) {
                //ignore
            }
        }
        builder = builder.customView(R.layout.loading, false);

        dialog = builder.build();
        dialog.show();
    }

    @Override
    public void dismissLoading() {
        if (dialog != null) {
            try {
                dialog.dismiss();
            } catch (Exception ex) {
                //ignore
            }
        }
    }
}
