package me.bt.base.di;

import android.app.Application;
import android.os.Build;
import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;
import me.bt.base.BaseApplication;
import me.bt.base.di.qualifiers.ApiLevel;
import net.grandcentrix.tray.TrayAppPreferences;

@Module(
    includes = {
        DataModule.class, UiModule.class
    },
    injects = BaseApplication.class,
    library = true) public class AppModule {

  private final Application app;

  public AppModule(Application app) {
    this.app = app;
  }

  @Provides @Singleton Application provideApplication() {
    return app;
  }

  @Provides @Singleton TrayAppPreferences providePreferences() {
    return new TrayAppPreferences(app);
  }

  @Provides @Singleton @ApiLevel int provideApiLevel() {
    return Build.VERSION.SDK_INT;
  }
}
