package me.bt.base.data;

import android.content.Context;
import dagger.ObjectGraph;

public final class Injector {
  private static final String INJECTOR_SERVICE = "me.bt.base.injector";

  private Injector() {
    throw new AssertionError("No instances.");
  }

  @SuppressWarnings("ResourceType") // Explicitly doing a custom service.
  public static ObjectGraph obtain(Context context) {
    return (ObjectGraph) context.getSystemService(INJECTOR_SERVICE);
  }

  public static boolean matchesService(String name) {
    return INJECTOR_SERVICE.equals(name);
  }
}
