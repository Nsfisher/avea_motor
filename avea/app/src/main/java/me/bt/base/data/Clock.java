package me.bt.base.data;

public interface Clock {
  Clock REAL = new Clock() {
    @Override public long millis() {
      return System.currentTimeMillis();
    }

    @Override public long nanos() {
      return System.nanoTime();
    }
  };

  long millis();

  long nanos();
}
