package me.bt.base.util;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;

import me.bt.motor.modules.settings.WebViewActivity;
import timber.log.Timber;

public class AndroidUtils {

    private static final String TAG = AndroidUtils.class.getName();

    public static void openUrlWithBrowser(String url, Activity activity) {
        if (!url.startsWith("https://") && !url.startsWith("http://")) {
            url = "http://" + url;
        }
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        try {
            activity.startActivity(intent);
        } catch (ActivityNotFoundException exception) {
            if (exception.getMessage() != null) {
                Timber.e(TAG, exception.getMessage());
            }
        }
    }


    public static void openUrlWithActivity(String url, Activity activity) {
        if (!url.startsWith("https://") && !url.startsWith("http://")) {
            url = "http://" + url;
        }
        Intent intent = new Intent(activity, WebViewActivity.class);
        intent.putExtra(WebViewActivity.EXTRA_URL, url);
        try {
            activity.startActivity(intent);
        } catch (ActivityNotFoundException exception) {
            if (exception.getMessage() != null) {
                Timber.e(TAG, exception.getMessage());
            }
        }
    }

}
