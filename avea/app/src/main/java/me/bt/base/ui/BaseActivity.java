package me.bt.base.ui;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.google.gson.Gson;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import net.grandcentrix.tray.TrayAppPreferences;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.ButterKnife;
import dagger.ObjectGraph;
import me.bt.avea.data.preferences.PrefConstants;
import me.bt.motor.modules.login.SplashActivity;
import me.bt.motor.modules.map.MobileMapActivity;
import me.bt.motor.modules.notifications.NotificationsActivity;
import me.bt.motor.modules.performance.PerformanceActivity;
import me.bt.motor.modules.route.RouteActivity;
import me.bt.motor.modules.settings.SettingsActivity;
import me.bt.motor.modules.trips.TripsActivity;
import me.bt.motor.network.TokenCheckRequest;
import me.bt.avea.presentation.model.PresentationMobile;
import me.bt.base.BaseApplication;
import me.bt.base.DataManager;
import me.bt.motor.R;
import me.bt.base.data.Injector;
import me.bt.base.di.ActivityModule;
import me.bt.motor.util.Utils;

public class BaseActivity extends AppCompatActivity {

    @Inject
    AppContainer appContainer;
    private ObjectGraph activityGraph;
    private OkHttpClient mClient = new OkHttpClient();
    private Gson mGson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        activityGraph = new ActivityInjector(this).createGraph(getModules());
        Utils.installCertificatePinning(mClient);
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
        ViewGroup container = appContainer.bind(this);
        int layoutId = onCreateViewId();
        if (layoutId != 0) {
            LayoutInflater inflater = getLayoutInflater();
            inflater.inflate(layoutId, container);
            ButterKnife.inject(this, container);
        }
    }


    protected void saveSelectedIdToDisk(PresentationMobile selected) {
        getAppPreferences().put(PrefConstants.DEFAULT_MOBILE_ID, selected.getMobile());
    }

    @Override
    protected void onStart() {
        super.onStart();
        try {
            if (DataManager.getInstance().isActivityRunning)
                checkToken();
        } catch (Exception e) {

        }

    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private void checkToken() {

        TokenCheckRequest tokenCheckRequest = new TokenCheckRequest();

        final TrayAppPreferences appPreferences = new TrayAppPreferences(getApplicationContext());
        String token = appPreferences.getString(PrefConstants.TOKEN, "");
        tokenCheckRequest.token = token;


        if (TextUtils.isEmpty(token)) {
            return;
        }
        String url = tokenCheckRequest.toString();
        try {
            Request request = new Request.Builder()
                    .url(url)
                    .get()
                    .build();

            mClient.newCall(request).enqueue(new Callback() {

                @Override
                public void onFailure(Request request, IOException e) {

                }

                @Override
                public void onResponse(Response response) throws IOException {
                    if (!response.isSuccessful())
                        throw new IOException("Unexpected code " + response);
                    String resp = response.body().string();
                    boolean isValid = Boolean.parseBoolean(resp);
                    if (!isValid) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                showRestartDialog();
                            }
                        });
                    }
                }
            });
        } catch (Exception e) {

        }
    }

    private void showRestartDialog() {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Oturum Süreniz dolmuştur");
        alertDialog.setMessage(getString(R.string.desc_area_name));

        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                        Intent mStartActivity = new Intent(BaseActivity.this, SplashActivity.class);
                        int mPendingIntentId = 123456;
                        PendingIntent mPendingIntent = PendingIntent.getActivity(BaseActivity.this, mPendingIntentId, mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT);
                        AlarmManager mgr = (AlarmManager) BaseActivity.this.getSystemService(Context.ALARM_SERVICE);
                        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent);
                        System.exit(0);

                    }
                });

        alertDialog.setCancelable(true);
        alertDialog.show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
    }

    @Override
    protected void onDestroy() {
        activityGraph = null;
        super.onDestroy();
    }

    protected BaseApplication getApp() {
        return (BaseApplication) getApplication();
    }

    protected TrayAppPreferences getAppPreferences() {
        return getApp().getAppPreferences();
    }

    public int onCreateViewId() {
        return 0;
    }

    public ObjectGraph getActivityGraph() {
        return activityGraph;
    }

    public void inject(Object obj) {
        if (activityGraph != null) {
            activityGraph.inject(obj);
        }
    }

    protected void initDrawer(final BaseActivity activity, Toolbar toolbar) {
        int selectedItem = 0;
        if (activity.getClass().equals(MobileMapActivity.class)) {
            selectedItem = 0;
        } else if (activity.getClass().equals(TripsActivity.class)) {
            selectedItem = 1;
        } else if (activity.getClass().equals(RouteActivity.class)) {
            selectedItem = 2;
        } else if (activity.getClass().equals(PerformanceActivity.class)) {
            selectedItem = 3;
        } else if (activity.getClass().equals(NotificationsActivity.class)) {
            selectedItem = 4;
        } else if (activity.getClass().equals(SettingsActivity.class)) {
            selectedItem = 5;
        }

        final Drawer result = new DrawerBuilder().withActivity(activity)
                .withToolbar(toolbar)
                .addDrawerItems(new PrimaryDrawerItem().withIcon(R.drawable.ic_map_gray_36dp)
                                .withSelectedIcon(R.drawable.ic_map_red_36dp)
                                .withName(R.string.map)
                                .withTag(MobileMapActivity.class), new PrimaryDrawerItem().withIcon(R.drawable.ic_send_gray_36dp)
                                .withSelectedIcon(R.drawable.ic_send_red_36dp)
                                .withName(R.string.trips)
                                .withTag(TripsActivity.class),

                        new PrimaryDrawerItem().withIcon(R.drawable.ic_directions_gray_36dp)
                                .withSelectedIcon(R.drawable.ic_directions_red_36dp)
                                .withName(R.string.route)
                                .withTag(RouteActivity.class),

                        new PrimaryDrawerItem().withIcon(R.drawable.ic_data_usage_gray_36dp)
                                .withSelectedIcon(R.drawable.ic_data_usage_red_36dp)
                                .withName(R.string.performance)
                                .withTag(PerformanceActivity.class),

                        new PrimaryDrawerItem().withIcon(R.drawable.ic_notifications_gray_36dp)
                                .withSelectedIcon(R.drawable.ic_notifications_red_36dp)
                                .withName(R.string.notifications)
                                .withTag(NotificationsActivity.class),

                        new PrimaryDrawerItem().withIcon(R.drawable.ic_settings_gray_36dp)
                                .withSelectedIcon(R.drawable.ic_settings_red_36dp)
                                .withName(R.string.settings)
                                .withTag(SettingsActivity.class)

                )
                .withSelectedItem(selectedItem)
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(AdapterView<?> parent, View view, int position, long id, IDrawerItem drawerItem) {
                        Class nextClass = (Class) drawerItem.getTag();
                        startActivity(new Intent(activity, nextClass));
                        return false;
                    }
                })

                .build();
    }

    protected List<Object> getModules() {
        return new ArrayList<>();
    }

    @Override
    public Object getSystemService(@NonNull String name) {
        if (Injector.matchesService(name)) {
            return activityGraph;
        }
        return super.getSystemService(name);
    }

    static class ActivityInjector {

        private AppCompatActivity activity;

        public ActivityInjector(AppCompatActivity activity) {
            this.activity = activity;
        }

        public ObjectGraph createGraph() {
            return createGraph(new ArrayList<>());
        }

        public ObjectGraph createGraph(List<Object> modules) {
            BaseApplication app = BaseApplication.get(activity);
            ObjectGraph graph = app.getObjectGraph().plus(getCombinedModules(modules).toArray());
            graph.inject(activity);
            return graph;
        }

        private List<Object> getCombinedModules(List<Object> modules) {
            List<Object> combined = new ArrayList<>(modules);
            combined.add(new ActivityModule(activity));
            return combined;
        }
    }
}
