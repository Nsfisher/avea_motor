package me.bt.base;

/**
 * Created by yenerciftci on 19/08/15.
 */
public class DataManager {

    private static DataManager Instance;
    public long timeStamp;
    public boolean isActivityRunning;

    public synchronized static DataManager getInstance() {
        if (Instance == null) {
            Instance = new DataManager();
        }
        return Instance;
    }


}
