package me.bt.base.ui.errors;

public interface ErrorManager {
  void showError(String error);

  void showError(int error);

  void showError();
}
