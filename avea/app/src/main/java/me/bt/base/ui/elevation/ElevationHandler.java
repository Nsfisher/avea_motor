package me.bt.base.ui.elevation;

import android.view.View;

public interface ElevationHandler {
  void setElevation(View view, float elevation);

  void setDefaultElevation(View view);

  public interface Factory {
    ElevationHandler createElevationHandler();
  }
}
