package me.bt.base;

import android.app.Application;
import android.content.Context;
import android.support.annotation.NonNull;

import com.squareup.leakcanary.LeakCanary;

import net.danlew.android.joda.JodaTimeAndroid;
import net.grandcentrix.tray.TrayAppPreferences;

import javax.inject.Inject;

import dagger.ObjectGraph;
import me.bt.base.data.Injector;
import me.bt.base.data.LumberYard;
import me.bt.base.ui.ActivityHierarchyServer;
import me.bt.motor.BuildConfig;
import timber.log.Timber;

public abstract class BaseApplication extends Application {

    @Inject
    ActivityHierarchyServer activityHierarchyServer;
    @Inject
    LumberYard lumberYard;
    @Inject
    TrayAppPreferences appPreferences;
    private ObjectGraph objectGraph;

    public static BaseApplication get(Context context) {
        return (BaseApplication) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        initObjectGraph();
        setupLogs();
        setupLibs();
        registerActivityLifecycleCallbacks(activityHierarchyServer);
    }

    private void setupLibs() {
        JodaTimeAndroid.init(this);
    }

    private void setupLogs() {
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
        lumberYard.cleanUp();
        Timber.plant(lumberYard.tree());
        LeakCanary.install(this);
    }

    private void initObjectGraph() {
        objectGraph = ObjectGraph.create(Modules.list(this));
        objectGraph.inject(this);
        inject(this);
    }

    public void inject(Object object) {
        objectGraph.inject(object);
    }

    public ObjectGraph getObjectGraph() {
        return objectGraph;
    }

    @Override
    public Object getSystemService(@NonNull String name) {
        if (Injector.matchesService(name)) {
            return objectGraph;
        }
        return super.getSystemService(name);
    }

    public TrayAppPreferences getAppPreferences() {
        return appPreferences;
    }
}
