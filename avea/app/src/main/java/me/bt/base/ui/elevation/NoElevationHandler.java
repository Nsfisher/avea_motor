package me.bt.base.ui.elevation;

import android.view.View;

public class NoElevationHandler implements ElevationHandler {

  @Override public void setElevation(View view, float elevation) {
  }

  @Override public void setDefaultElevation(View view) {
  }
}
