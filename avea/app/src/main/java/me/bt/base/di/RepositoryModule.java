package me.bt.base.di;

import dagger.Module;

@Module(
    includes = {
        ApiModule.class, BddModule.class
    },
    complete = false,
    library = true) public class RepositoryModule {

}
