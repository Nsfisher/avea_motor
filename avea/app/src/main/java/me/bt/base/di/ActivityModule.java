package me.bt.base.di;

import android.app.Application;
import android.content.Context;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import me.bt.base.ui.elevation.ElevationHandler;
import me.bt.base.ui.elevation.ElevationHandlerFactory;
import me.bt.base.ui.errors.ErrorManager;
import me.bt.base.ui.errors.SnackbarErrorManagerImp;
import me.bt.base.ui.loading.LoadingManager;
import me.bt.base.ui.loading.SweetAlertLoadingManagerImp;
import me.bt.base.ui.transitions.WindowTransitionListener;
import me.bt.base.ui.transitions.WindowTransitionListenerFactory;
import me.bt.motor.di.AveaActivityModule;
import me.bt.motor.ui.handlers.NavbarHandler;
import pl.charmas.android.reactivelocation.ReactiveLocationProvider;

@Module(
    includes = {
        AveaActivityModule.class
    },
    addsTo = AppModule.class,
    library = true) public class ActivityModule {

  private AppCompatActivity activity;

  public ActivityModule(AppCompatActivity activity) {
    this.activity = activity;
  }

  @Provides ActionBar provideActionBar() {
    return activity.getSupportActionBar();
  }

  @Provides Context provideContext() {
    return activity;
  }

  @Provides AppCompatActivity provideActivity() {
    return activity;
  }

  @Provides ErrorManager provideErrorManager() {
    return new SnackbarErrorManagerImp(activity);
  }

  @Provides LoadingManager provideLoadingManager() {
    return new SweetAlertLoadingManagerImp(activity);
  }

  @Provides Window provideWindow() {
    return activity.getWindow();
  }

  @Provides @Singleton ElevationHandler.Factory provideElevationHandlerFactory(ElevationHandlerFactory factoryImp) {
    return factoryImp;
  }

  @Provides @Singleton ElevationHandler provideElevationHandler(ElevationHandler.Factory factory) {
    return factory.createElevationHandler();
  }

  @Provides @Singleton WindowTransitionListener.Factory provideWindowTransitionListenerFactory(WindowTransitionListenerFactory factoryImp) {
    return factoryImp;
  }

  @Provides @Singleton WindowTransitionListener provideElevationHandler(WindowTransitionListener.Factory factory) {
    return factory.createWindowTransitionListener();
  }

  @Provides @Singleton ReactiveLocationProvider provideLocationProvider(Application app) { //TODO hakan
    return new ReactiveLocationProvider(app);
  }

  @Provides @Singleton NavbarHandler provideNavbarHandler() {
    return new NavbarHandler();
  }
}
