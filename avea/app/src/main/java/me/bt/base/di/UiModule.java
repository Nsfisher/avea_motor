package me.bt.base.di;

import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;
import me.bt.base.ui.ActivityHierarchyServer;
import me.bt.base.ui.AppContainer;
import me.bt.base.ui.BaseActivity;

@Module(
    injects = {
        BaseActivity.class
    },
    complete = false,
    library = true) public class UiModule {

  @Provides @Singleton AppContainer provideAppContainer() {
    return AppContainer.DEFAULT;
  }

  @Provides @Singleton ActivityHierarchyServer provideActivityHierarchyServer() {
    return ActivityHierarchyServer.NONE;
  }
}
