package me.bt.base.ui.loading;

public interface LoadingManager {
    void showLoading();

    void showLoading(String title, String message);

    void showLoading(String message);

    void showLoading(int message);

    void showLoading(String message, boolean isCancelable);

    void showLoading(String title, String message, boolean isCancelable);

    void dismissLoading();
}
