package me.bt.base.ui.errors;

import android.app.Activity;
import android.text.TextUtils;

import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.enums.SnackbarType;

public class SnackbarErrorManagerImp implements ErrorManager {

    private Activity activity;

    public SnackbarErrorManagerImp(Activity activity) {
        this.activity = activity;
    }

    @Override
    public void showError(String error) {
        if (!TextUtils.isEmpty(error)) {
            SnackbarManager.show(Snackbar.with(activity).type(SnackbarType.MULTI_LINE).text(error));
        }
    }

    @Override
    public void showError(int error) {
        showError(activity.getResources().getString(error));
    }

    @Override
    public void showError() {
        SnackbarManager.show(Snackbar.with(activity).text("Hata")); //TODO hakan
    }
}
