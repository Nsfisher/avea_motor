package me.bt.base.ui.loading;

import android.app.Activity;

import cn.pedant.SweetAlert.SweetAlertDialog;
import me.bt.motor.R;

public class SweetAlertLoadingManagerImp implements LoadingManager {

    private Activity activity;
    private SweetAlertDialog pDialog;

    public SweetAlertLoadingManagerImp(Activity activity) {
        this.activity = activity;
    }

    @Override
    public void showLoading() {
        showLoading("", "", false);
    }

    @Override
    public void showLoading(String title, String message) {
        showLoading("", message, false);
    }

    @Override
    public void showLoading(String message) {
        showLoading("", message);
    }

    @Override
    public void showLoading(int message) {
        showLoading(activity.getResources().getString(message));
    }

    @Override
    public void showLoading(String message, boolean isCancelable) {
        showLoading("", message, isCancelable);
    }

    @Override
    public void showLoading(String title, String message, boolean isCancelable) {
        pDialog = new SweetAlertDialog(activity, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(activity.getResources().getColor(R.color.primary));
        if (message != null && message.length() > 0) {
            pDialog.setTitleText(message);
        } else {
            pDialog.setTitleText("");
        }
        pDialog.setCancelable(isCancelable);
        pDialog.show();
    }

    @Override
    public void dismissLoading() {
        if (pDialog != null) {
            try {
                pDialog.dismiss();
            } catch (Exception ex) {
                //ignore
            }
        }
    }
}
