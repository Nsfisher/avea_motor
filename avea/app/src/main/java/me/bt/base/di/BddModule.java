package me.bt.base.di;

import android.app.Application;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import me.bt.avea.data.repository.DatabaseHelper;
import me.bt.avea.data.repository.caching.strategy.CachingStrategy;
import me.bt.avea.data.repository.caching.strategy.list.ListCachingStrategy;
import me.bt.avea.data.repository.caching.strategy.nullsafe.NotNullCachingStrategy;
import me.bt.avea.data.repository.caching.strategy.ttl.TtlCachingStrategy;
import me.bt.avea.data.repository.mobiles.datasources.bdd.MobileBddDataSourceImp;
import me.bt.avea.data.repository.mobiles.datasources.bdd.entities.BddMobile;
import me.bt.avea.data.repository.mobiles.datasources.bdd.persistors.MobilePersistor;
import me.bt.avea.data.repository.mobiles.datasources.bdd.persistors.Persistor;
import me.bt.avea.repository.mobiles.datasources.MobileBddDataSource;
import me.bt.base.data.DatabaseName;
import me.bt.motor.BuildConfig;

@Module(
    complete = false,
    library = true) public class BddModule {

  @Provides @Singleton CachingStrategy<BddMobile> provideContactCachingStrategy() {
    return new NotNullCachingStrategy<>();
  }

  @Provides @Singleton ListCachingStrategy<BddMobile> provideListContactCachingStrategy() {
    return new ListCachingStrategy<>(new TtlCachingStrategy<BddMobile>(10, TimeUnit.MINUTES));
  }

  @Provides @Singleton MobileBddDataSource provideMobilesBddDataSource(Persistor<BddMobile> persistor, DatabaseHelper helper,
      CachingStrategy<BddMobile> singleContactCachingStrategy, ListCachingStrategy<BddMobile> listCachingStrategy) {
    return new MobileBddDataSourceImp(persistor, helper.getMobileDao(), singleContactCachingStrategy, listCachingStrategy);
  }

  @Provides @Singleton Persistor<BddMobile> provideContactPersistor(DatabaseHelper helper) {
    return new MobilePersistor(helper);
  }

  @Provides @Singleton public DatabaseHelper provideDatabaseHelper(@DatabaseName String databaseName, Application app) {
    return new DatabaseHelper(databaseName, app);
  }

  @Provides @Singleton @DatabaseName String provideDatabaseName() {
    return "cleancontacts" + (BuildConfig.DEBUG ? "-dev" : "") + ".db";
  }
}
