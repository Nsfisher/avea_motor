package me.bt.base.data;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class DateConverter implements JsonSerializer<Date> {

  @Override public JsonElement serialize(Date src, Type typeOfSrc, JsonSerializationContext context) {
    final DateFormat df = new SimpleDateFormat("yyyy-MM-dd+HH:mm:ss");
    return new JsonPrimitive(df.format(src));
  }
}
