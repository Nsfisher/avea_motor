package me.bt.base.ui.transitions;

public class NoWindowTransitionListener implements WindowTransitionListener {

  private WindowTransitionEndListener endListener;

  @Override public boolean start() {
    if (endListener != null) {
      endListener.onEndTransition();
    }
    return false;
  }

  @Override public void setupListener(WindowTransitionEndListener endListener) {
    this.endListener = endListener;
  }
}
