package me.bt.base.di;

import com.google.gson.Gson;
import com.squareup.okhttp.OkHttpClient;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import me.bt.base.data.api.GithubService;
import me.bt.motor.di.AveaApiModule;
import retrofit.Endpoint;
import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

@Module(
    complete = false,
    includes = {
        AveaApiModule.class
    },
    library = true) public class ApiModule {

  @Provides @Singleton @Named("Api") OkHttpClient provideApiClient(OkHttpClient client) {
    return client.clone();
  }

  @Provides @Singleton RestAdapter provideRestAdapter(Endpoint endpoint, @Named("Api") OkHttpClient client, Gson gson) {
    return new RestAdapter.Builder().setClient(new OkClient(client)).setEndpoint(endpoint).setConverter(new GsonConverter(gson)).build();
  }

  @Provides @Singleton GithubService provideGithubService(RestAdapter restAdapter) {
    return restAdapter.create(GithubService.class);
  } //TODO
}
