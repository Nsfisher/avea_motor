package me.bt.opensource.directions.route;

//. by Haseem Saheed
public interface Parser {
    public Route parse();
}