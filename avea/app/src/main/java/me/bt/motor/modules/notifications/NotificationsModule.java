package me.bt.motor.modules.notifications;

import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;
import me.bt.avea.domain.entities.Mobile;
import me.bt.avea.domain.interactions.lightevents.LightEventsInteractor;
import me.bt.avea.domain.interactions.mobiles.MobilesInteractor;
import me.bt.avea.presentation.model.PresentationMobile;
import me.bt.avea.presentation.notifications.NotificationsPresenter;
import me.bt.avea.presentation.notifications.NotificationsView;
import me.bt.base.di.ActivityModule;
import me.bt.base.domain.abstractions.Bus;
import me.bt.base.domain.interactors.InteractorInvoker;
import me.bt.base.presentation.model.mapper.base.ListMapper;

@Module(
    addsTo = ActivityModule.class,
    library = true,
    injects = NotificationsActivity.class) public class NotificationsModule {

  private final NotificationsView notificationsView;

  public NotificationsModule(NotificationsView notificationsView) {
    this.notificationsView = notificationsView;
  }

  @Provides @Singleton NotificationsPresenter provideNotificationsPresenter(Bus bus, InteractorInvoker interactorInvoker,
      MobilesInteractor mobilesInteractor, ListMapper<Mobile, PresentationMobile> listMapper, LightEventsInteractor lightEventsInteractor) {
    return new NotificationsPresenter(bus, interactorInvoker, notificationsView, lightEventsInteractor, listMapper);
  }
}

