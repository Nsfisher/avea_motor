package me.bt.motor.modules.login;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AlertDialog;

import com.google.gson.Gson;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import net.grandcentrix.tray.TrayAppPreferences;
import net.grandcentrix.tray.accessor.ItemNotFoundException;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.Calendar;
import java.util.Date;

import me.bt.avea.data.preferences.PrefConstants;
import me.bt.motor.R;
import me.bt.motor.network.CheckAppVersionRequest;
import me.bt.motor.network.response.CheckAppVersionResponse;
import me.bt.motor.util.Utils;


/**
 * Created by yenerciftci on 29/07/15.
 */
public class SplashActivity extends Activity {
    private static final long SPLASH_DISPLAY_LENGTH = 2000;
    private OkHttpClient mClient;
    private Gson mGson;
    private boolean isResponseCatch;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        mClient = new OkHttpClient();
        Utils.installCertificatePinning(mClient);
        mGson = new Gson();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                versionController();
            }
        }, 1500);
    }


    private void versionController() {
                /* New Handler to start the Menu-Activity
         * and close this Splash-Screen after some seconds.*/
        try {
            String versionName = getPackageManager()
                    .getPackageInfo(getPackageName(), 0).versionName;
            String[] versions = versionName.split("\\.");

            try {
                versions[2] = versions[2].split("-")[0];
            } catch (Exception e) {

            }
            CheckAppVersionRequest checkApp = new CheckAppVersionRequest();

            Request request = new Request.Builder()
                    .url(checkApp.toString())
                    .get()
                    .build();

            mClient.newCall(request).enqueue(new Callback() {

                @Override
                public void onFailure(Request request, IOException e) {
                    isResponseCatch = true;
                    startLoginActivtiy();
                }

                @Override
                public void onResponse(Response response) throws IOException {
                    isResponseCatch = true;
                    if (!response.isSuccessful()) {
                        startLoginActivtiy();
                    }

                    String resp = response.body().string();
                    final CheckAppVersionResponse checkAppVersionResponse = mGson.fromJson(resp, CheckAppVersionResponse.class);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            rpCheckApp(checkAppVersionResponse);
                        }
                    });
                }
            });


        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

//
//        Runnable progressRunnable = new Runnable() {
//
//            @Override
//            public void run() {
//                if (!isResponseCatch) {
//                    startLoginActivtiy();
//                }
//            }
//        };
//
//        Handler pdCanceller = new Handler();
//        pdCanceller.postDelayed(progressRunnable, 15000);
    }

    private void rpCheckApp(CheckAppVersionResponse checkAppVersionResponse) {
        String versionName = null;
        try {
            versionName = getPackageManager()
                    .getPackageInfo(getPackageName(), 0).versionName;

            String[] versions = versionName.split("\\.");

            try {
                versions[2] = versions[2].split("-")[0];
            } catch (Exception e) {
                startLoginActivtiy();
            }

            String[] serverVersion = checkAppVersionResponse.androidVer.split("\\.");

            if (!versions[0].equals(serverVersion[0])) {
                appMajorUpdate(checkAppVersionResponse);
            } else if (!versions[1].equals(serverVersion[1])) {
                appMajorUpdate(checkAppVersionResponse);
            } else if (!versions[2].equals(serverVersion[2])) {
                appMinorUpdate(checkAppVersionResponse);
            } else {
                startLoginActivtiy();
            }


        } catch (PackageManager.NameNotFoundException e) {
            startLoginActivtiy();
            e.printStackTrace();
        } catch (Exception e) {
            startLoginActivtiy();
        }
    }

    private void startLoginActivtiy() {
        Intent mainIntent = new Intent(SplashActivity.this, LoginActivity.class);
        SplashActivity.this.startActivity(mainIntent);
        SplashActivity.this.finish();
    }

    private void appMajorUpdate(final CheckAppVersionResponse checkAppVersionResponse) {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle(getString(R.string.wrn_new_version));
        alertDialog.setMessage(checkAppVersionResponse.changeDesc);

        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Güncelle",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        openGooglePlay(checkAppVersionResponse);
                        finish();
                    }
                });

        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    private void appMinorUpdate(final CheckAppVersionResponse checkAppVersionResponse) {
        final TrayAppPreferences appPreferences = new TrayAppPreferences(getApplicationContext());

        try {
            Date date = null;
            try {
                date = new Date(appPreferences.getString(PrefConstants.MINOR_UPDATE_DATE));
            } catch (ItemNotFoundException e) {
                e.printStackTrace();
            }

            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            Calendar cal2 = Calendar.getInstance();
            cal2.setTime(new Date());
            if (cal.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR)) {
                startLoginActivtiy();
                return;
            }
        } catch (Exception e) {

        }


        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle(getString(R.string.wrn_new_version));
        alertDialog.setMessage(checkAppVersionResponse.changeDesc);

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        openGooglePlay(checkAppVersionResponse);
                        finish();
                    }
                });

        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startLoginActivtiy();

            }
        });

        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Yarın hatırlat", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                final TrayAppPreferences appPreferences = new TrayAppPreferences(getApplicationContext());
                appPreferences.put(PrefConstants.MINOR_UPDATE_DATE, new Date().toString());
                startLoginActivtiy();

            }
        });


        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    private void openGooglePlay(CheckAppVersionResponse checkAppVersionResponse) {
        String url = checkAppVersionResponse.andoridUrl;
        if (!url.startsWith("http://") && !url.startsWith("https://"))
            url = "http://" + url;
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);
    }

    @Override
    protected void onDestroy() {
        mClient = null;
        super.onDestroy();
    }

    /**
     * Instances of static inner classes do not hold an implicit
     * reference to their outer class.
     */
    private static class MyHandler extends Handler {
        private final WeakReference<SplashActivity> mActivity;

        public MyHandler(SplashActivity activity) {
            mActivity = new WeakReference<SplashActivity>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            SplashActivity activity = mActivity.get();
            if (activity != null) {
                // ...
            }
        }
    }

    private final MyHandler mHandler = new MyHandler(this);

    /**
     * Instances of anonymous classes do not hold an implicit
     * reference to their outer class when they are "static".
     */
    private static final Runnable sRunnable = new Runnable() {
        @Override
        public void run() {

        }
    };


}
