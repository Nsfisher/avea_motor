package me.bt.motor;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;
import me.bt.base.BaseApplication;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class AveaApplication extends BaseApplication {

    @Override
    public void onCreate() {
        super.onCreate();

        if (!BuildConfig.DEBUG) {
            Fabric.with(this, new Crashlytics());
        }
        // TODO Timber.plant(new CrashlyticsTree());
        setupFont();
    }

    private void setupFont() {
        CalligraphyConfig.initDefault(
                new CalligraphyConfig.Builder().setDefaultFontPath("fonts/OpenSans-Regular.ttf").setFontAttrId(R.attr.fontPath).build());
    }
}
