package me.bt.motor.modules.general;

import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;
import me.bt.avea.domain.interactions.general.GeneralInteractor;
import me.bt.motor.modules.login.ForgetPasswordActivity;
import me.bt.motor.modules.newmobile.NewMobileActivity;
import me.bt.motor.modules.performance.PerformanceActivity;
import me.bt.motor.modules.route.RouteActivity;
import me.bt.motor.modules.settings.AreaDefinitionActivity;
import me.bt.motor.modules.settings.AreaDefinitionMapActivity;
import me.bt.motor.modules.settings.SettingsActivity;
import me.bt.motor.modules.settings.SettingsNotificationsActivity;
import me.bt.avea.presentation.general.GeneralPresenter;
import me.bt.avea.presentation.general.GeneralView;
import me.bt.base.di.ActivityModule;
import me.bt.base.domain.abstractions.Bus;
import me.bt.base.domain.interactors.InteractorInvoker;

@Module(
    addsTo = ActivityModule.class,
    library = true,
    complete = false,
    injects = { NewMobileActivity.class, SettingsNotificationsActivity.class, SettingsActivity.class, PerformanceActivity.class, RouteActivity.class, AreaDefinitionActivity.class, AreaDefinitionMapActivity.class, ForgetPasswordActivity.class }) public class GeneralModule {

  private GeneralView generalView;

  public GeneralModule(GeneralView generalView) {
    this.generalView = generalView;
  }

  @Provides @Singleton GeneralPresenter provideGeneralPresenter(Bus bus, InteractorInvoker interactorInvoker,
      GeneralInteractor generalInteractor) {
    return new GeneralPresenter(bus, interactorInvoker, generalView, generalInteractor);
  }
}
