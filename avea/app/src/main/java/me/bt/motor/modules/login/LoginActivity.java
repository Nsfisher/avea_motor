package me.bt.motor.modules.login;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.nhaarman.supertooltips.ToolTip;
import com.nhaarman.supertooltips.ToolTipRelativeLayout;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import butterknife.InjectView;
import butterknife.OnClick;
import me.bt.avea.data.preferences.PrefConstants;
import me.bt.avea.presentation.login.LoginPresenter;
import me.bt.avea.presentation.login.LoginView;
import me.bt.base.ui.BaseActivity;
import me.bt.base.ui.elevation.ElevationHandler;
import me.bt.base.ui.errors.ErrorManager;
import me.bt.base.ui.imageloader.ImageLoader;
import me.bt.base.ui.loading.LoadingManager;
import me.bt.motor.BuildConfig;
import me.bt.motor.R;
import me.bt.motor.modules.adduser.AddUserActivity;
import me.bt.motor.modules.map.MobileMapActivity;


public class LoginActivity extends BaseActivity implements LoginView {

    @Inject
    LoginPresenter presenter;
    @Inject
    ErrorManager errorManager;
    @Inject
    LoadingManager loadingManager;
    @Inject
    ElevationHandler elevationHandler;
    @Inject
    ImageLoader imageLoader;

    @InjectView(R.id.tv_forget_password)
    TextView tv_forget_password;
    @InjectView(R.id.btn_register)
    Button btn_register;
    @InjectView(R.id.btn_login)
    Button btn_login;
    @InjectView(R.id.et_email)
    EditText et_email;
    @InjectView(R.id.et_password)
    EditText et_password;
    @InjectView(R.id.tt_error)
    ToolTipRelativeLayout tt_error;

    @InjectView(R.id.cb_remember)
    CheckBox sw_remember;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //checkToken(); //TODO hakan
        setupUI();
    }

    @Override
    public int onCreateViewId() {
        return R.layout.activity_login;
    }

    protected List<Object> getModules() {
        return Arrays.<Object>asList(new LoginModule(this));
    }

    private void checkToken() {
        String token = getAppPreferences().getString(PrefConstants.TOKEN, ""); //TODO
        if (token.length() > 0) {
            loginComplete();
        }
    }

    private void setupUI() {
        et_email.addTextChangedListener(credentialsWatcher);
        et_password.addTextChangedListener(credentialsWatcher);
        if (BuildConfig.DEBUG) {
            et_email.setText("atstest@avea.com.tr");
            et_password.setText("123456");
        }

        boolean isRemember = getAppPreferences().getBoolean(PrefConstants.IS_REMEMBER_CHECKED, false);

        if (isRemember) {
            String psw = getAppPreferences().getString(PrefConstants.P_WORD, "");
            String username = getAppPreferences().getString(PrefConstants.U_NAME, "");
            et_email.setText(username);
            et_password.setText(psw);

        }
        sw_remember.setChecked(isRemember);
        elevationHandler.setElevation(btn_login, 3);
    }

    private TextWatcher credentialsWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            validateLoginCredentials();
        }
    };

    private void validateLoginCredentials() {
        if (et_password.getText().length() > 0 && et_email.getText().length() > 0) {
            //TODO email verification
            btn_login.setBackgroundColor(getResources().getColor(R.color.accent));
            btn_login.setTextColor(getResources().getColor(R.color.white));
            btn_login.setEnabled(true);
        } else {
            btn_login.setEnabled(false);
        }
    }

    @OnClick(R.id.btn_login)
    void onLoginClicked() {
        String email = et_email.getText().toString();
        if (TextUtils.isEmpty(email)) {
            ToolTip toolTip = new ToolTip().withText(getString(R.string.error_wrong_email))
                    .withColor(Color.RED)
                    .withTextColor(Color.WHITE)
                    .withShadow()
                    .withAnimationType(ToolTip.AnimationType.NONE);
            tt_error.showToolTipForView(toolTip, findViewById(R.id.v_error_email));
            errorManager.showError(getString(R.string.error_wrong_email));
            return;
        }
        if (et_password.getText().toString().length() < 2) {
            ToolTip toolTip = new ToolTip().withText(getString(R.string.error_wrong_password))
                    .withColor(Color.RED)
                    .withTextColor(Color.WHITE)
                    .withShadow()
                    .withAnimationType(ToolTip.AnimationType.NONE);
            tt_error.showToolTipForView(toolTip, findViewById(R.id.et_password));
            errorManager.showError(getString(R.string.error_wrong_password));
            return;
        }
        loadingManager.showLoading(getString(R.string.please_wait));
        presenter.login(email, et_password.getText().toString());
    }

    @OnClick(R.id.tv_forget_password)
    void onForgetPasswordClicked() {
        startActivity(new Intent(LoginActivity.this, ForgetPasswordActivity.class));

        //TODO hakan sifre ekrani
    }

    @OnClick(R.id.btn_register)
    void onActivateClicked() {
        startActivity(new Intent(LoginActivity.this, AddUserActivity.class));
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    public void loginComplete() {

        String saveUsername = getAppPreferences().getString(PrefConstants.U_NAME, "");
        String currentUsername = et_email.getText().toString();
        if (!saveUsername.equals(currentUsername)) {
            getAppPreferences().put(PrefConstants.DEFAULT_SAVED_ID, "");
            getAppPreferences().put(PrefConstants.DEFAULT_MOBILE_ALIAS, "");
        }
        getAppPreferences().put(PrefConstants.U_NAME, et_email.getText().toString());
        getAppPreferences().put(PrefConstants.P_WORD, et_password.getText().toString());
        getAppPreferences().put(PrefConstants.IS_REMEMBER_CHECKED, sw_remember.isChecked());

        loadingManager.dismissLoading();

        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());
        if (status == ConnectionResult.SUCCESS) {
            startActivity(new Intent(LoginActivity.this, MobileMapActivity.class));
            finish();
        } else if (status == ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED) {
            Toast.makeText(this, "please udpate your google play service", Toast.LENGTH_SHORT).show();
        } else if (status == ConnectionResult.API_UNAVAILABLE) {
            Toast.makeText(this, "please install your google play service", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "please install your google play service 2", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void loginError(String message) {
        loadingManager.dismissLoading();
        errorManager.showError(message);
    }
}
