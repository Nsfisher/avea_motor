package me.bt.motor.modules.adduser;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.zxing.integration.android.IntentIntegrator;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import butterknife.InjectView;
import butterknife.OnClick;
import me.bt.motor.AveaConstants;
import me.bt.avea.data.preferences.PrefConstants;
import me.bt.avea.domain.entities.Terms;
import me.bt.avea.domain.entities.TermsParameter;
import me.bt.avea.domain.interactions.general.GeneralInteractor;
import me.bt.avea.domain.interactions.general.events.GeneralEvent;
import me.bt.motor.modules.newmobile.NewMobileActivity;
import me.bt.motor.network.CheckUserRequest;
import me.bt.motor.network.response.BaseResponse;
import me.bt.avea.presentation.general.GeneralPresenter;
import me.bt.avea.presentation.general.GeneralView;
import me.bt.avea.presentation.login.LoginPresenter;
import me.bt.avea.presentation.login.LoginView;
import me.bt.avea.util.AveaUtils;
import me.bt.motor.R;
import me.bt.base.ui.BaseActivity;
import me.bt.base.ui.elevation.ElevationHandler;
import me.bt.base.ui.errors.ErrorManager;
import me.bt.base.ui.loading.LoadingManager;
import me.bt.base.util.AndroidUtils;
import me.bt.motor.util.Utils;

public class AddUserActivity extends BaseActivity implements GeneralView, LoginView {

    @InjectView(R.id.tv_login)
    TextView tv_login;
    @InjectView(R.id.iv_qr)
    ImageView iv_qr;
    @InjectView(R.id.et_phone)
    EditText et_phone;
    @InjectView(R.id.et_activation)
    EditText et_activation;
    @InjectView(R.id.et_email)
    EditText et_email;
    @InjectView(R.id.et_password)
    EditText et_password;
    @InjectView(R.id.btn_next)
    Button btn_next;
    @InjectView(R.id.toolbar)
    Toolbar toolbar;
    @Inject
    ElevationHandler elevationHandler;
    @Inject
    ErrorManager errorManager;
    @Inject
    LoadingManager loadingManager;
    @Inject
    GeneralPresenter generalPresenter;
    @Inject
    LoginPresenter loginPresenter;

    private Terms terms;
    private OkHttpClient mClient;
    private Gson mGson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initToolbar();
        initUi();
        mClient = new OkHttpClient();
        Utils.installCertificatePinning(mClient);
        mGson = new Gson();
    }


    private void initUi() {
        et_activation.setText(NewMobileActivity.activationStr);
        et_password.setText(NewMobileActivity.passwordStr);
        et_email.setText(NewMobileActivity.usernameStr);
        et_password.setText(NewMobileActivity.passwordStr);
    }

    @Override
    public int onCreateViewId() {
        return R.layout.activity_add_user;
    }

    protected List<Object> getModules() {
        return Arrays.<Object>asList(new AddUserModule(this, this));
    }

    private void initToolbar() {
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            elevationHandler.setDefaultElevation(toolbar);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View back) {
                    onBackPressed();
                }
            });
        }
    }

    @OnClick(R.id.btn_next)
    void onNextClicked() {
        if (isInputValid()) {
            loginPresenter.login(AveaConstants.TEST_USER, AveaConstants.TEST_PASSWORD);
            loadingManager.showLoading(R.string.loading_message);
        }

    }

    private boolean isInputValid() {
        if (et_phone.getText().toString().length() != 10) {
            errorManager.showError(R.string.error_wrong_phone);
            return false;
        }
        if (et_activation.getText().length() < 1) {
            errorManager.showError(R.string.error_wrong_activation);
            return false;
        }

        if (!AveaUtils.isValidEmailAddress(et_email.getText().toString())) {
            errorManager.showError(R.string.error_wrong_email);
            return false;
        }

        if (et_password.getText().toString().length() < 1) {
            errorManager.showError(R.string.error_wrong_password);
            return false;
        }

        return true;
    }

    @OnClick({R.id.tv_legal, R.id.tv_nda})
    void onLegalTextClicked() {
        if (terms == null) {
            loadingManager.showLoading();
            generalPresenter.getTerms();
        } else {
            openLegalPage();
        }
    }

    private void openLegalPage() {
        if (terms != null) {
            for (TermsParameter parameter : terms.parameter) {
                if (parameter.name.startsWith("CONDITIONS_")) {
                    AndroidUtils.openUrlWithActivity(parameter.data, this);
                    break;
                }
            }
        }
    }

    @OnClick(R.id.tv_login)
    void onLoginClicked() {
        onBackPressed();
    }

    @OnClick(R.id.iv_qr)
    void onQrClicked() {
        new IntentIntegrator(this).initiateScan();
    }

    @Override
    protected void onPause() {
        super.onPause();
        generalPresenter.onPause();
        loginPresenter.onPause();
    }


    @Override
    protected void onResume() {
        super.onResume();
        generalPresenter.onResume();
        loginPresenter.onResume();
    }

    @Override
    public void generalResponse(GeneralEvent response) {
        loadingManager.dismissLoading();
        if (response.getRequestType() == GeneralInteractor.REQUEST_TERMS) {
            if (response.getError() == null) {
                terms = (Terms) response.getData();
                openLegalPage();
            }
        }
    }

    @Override
    public void loginComplete() {
        loadingManager.dismissLoading();
        srCheckUser();
    }

    @Override
    public void loginError(String message) {
        loadingManager.dismissLoading();
        errorManager.showError(message);
    }

    private void srCheckUser() {
        CheckUserRequest checkApp = new CheckUserRequest();
        checkApp.token = getAppPreferences().getString(PrefConstants.TOKEN, "");
        checkApp.username = et_email.getText().toString();
        checkApp.password = et_password.getText().toString();
        checkApp.activation = et_activation.getText().toString();

        Request request = new Request.Builder()
                .url(checkApp.toString())
                .get()
                .build();
        mClient.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Request request, IOException e) {

            }

            @Override
            public void onResponse(Response response) throws IOException {

                if (!response.isSuccessful()) {
                    errorManager.showError(getString(R.string.error));
                }

                String resp = response.body().string();
                final BaseResponse baseResponse = mGson.fromJson(resp, BaseResponse.class);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        rpCheckUser(baseResponse);
                    }
                });
            }
        });
    }

    private void rpCheckUser(BaseResponse baseResponse) {
        if (baseResponse.status.equals("0")) {
            Intent intent = new Intent(AddUserActivity.this, NewMobileActivity.class);
            intent.putExtra(NewMobileActivity.EXTRA_PASSWORD, et_password.getText().toString());
            intent.putExtra(NewMobileActivity.EXTRA_USERNAME, et_email.getText().toString());
            intent.putExtra(NewMobileActivity.EXTRA_ACTIVATION, et_activation.getText().toString());
            intent.putExtra(NewMobileActivity.EXTRA_PHONE, et_phone.getText().toString());
            startActivity(intent);
        } else errorManager.showError(baseResponse.errorDesc);

    }
}
