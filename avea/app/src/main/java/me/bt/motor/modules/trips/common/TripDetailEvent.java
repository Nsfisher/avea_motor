package me.bt.motor.modules.trips.common;

import java.io.Serializable;
import java.util.List;
import me.bt.avea.domain.entities.DataLightPosition;
import me.bt.avea.domain.entities.IntervalData;

public class TripDetailEvent  implements Serializable {

  private final IntervalData intervalData;
  private final List<DataLightPosition> lightPositions;
  private String mobileId;

  public TripDetailEvent(IntervalData intervalData, List<DataLightPosition> lightPositions) {
    this.intervalData = intervalData;
    this.lightPositions = lightPositions;
  }

  public IntervalData getIntervalData() {
    return intervalData;
  }

  public List<DataLightPosition> getLightPositions() {
    return lightPositions;
  }

  public String getMobileId() {
    return mobileId;
  }

  public void setMobileId(String mobileId) {
    this.mobileId = mobileId;
  }
}
