package me.bt.motor.modules.map.adapters;

import android.content.Context;
import android.view.ViewGroup;
import com.carlosdelachica.easyrecycleradapters.adapter.BaseEasyViewHolderFactory;
import com.carlosdelachica.easyrecycleradapters.adapter.EasyViewHolder;
import me.bt.motor.ui.items.MobileViewHolder;

public class MobileViewHolderFactory extends BaseEasyViewHolderFactory {

  public MobileViewHolderFactory(Context context) {
    super(context);
  }

  @Override public EasyViewHolder create(Class valueClass, ViewGroup parent) {
    return new MobileViewHolder(context, parent);
  }
}

