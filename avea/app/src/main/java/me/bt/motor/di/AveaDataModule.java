package me.bt.motor.di;

import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;
import me.bt.avea.domain.entities.Mobile;
import me.bt.avea.presentation.model.Mapper.PresentationMobileMapper;
import me.bt.avea.presentation.model.PresentationMobile;
import me.bt.base.presentation.model.mapper.base.ListMapper;

@Module(
    includes = {
        AveaInteractorsModule.class, AveaRepositoryModule.class,
    },
    complete = false,
    library = true) public class AveaDataModule {

  @Provides @Singleton ListMapper<Mobile, PresentationMobile> providePresentationMobileMapper(
      PresentationMobileMapper presentationMobileMapper) {
    return new ListMapper<>(presentationMobileMapper);
  }

  @Provides @Singleton PresentationMobileMapper providePresentationMobileMapper() {
    return new PresentationMobileMapper();
  }
}
