package me.bt.motor.di;

import android.app.Application;

import com.google.gson.Gson;
import com.squareup.okhttp.OkHttpClient;

import net.grandcentrix.tray.TrayAppPreferences;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import me.bt.avea.data.repository.mobiles.datasources.api.ApiNetworkError;
import me.bt.avea.data.repository.mobiles.datasources.api.MobileApiService;
import me.bt.avea.data.repository.mobiles.datasources.api.implementations.AddUserNetworkDataSourceImp;
import me.bt.avea.data.repository.mobiles.datasources.api.implementations.GeneralNetworkDataSourceImp;
import me.bt.avea.data.repository.mobiles.datasources.api.implementations.LightEventsNetworkDataSourceImp;
import me.bt.avea.data.repository.mobiles.datasources.api.implementations.LightPositionsNetworkDataSourceImp;
import me.bt.avea.data.repository.mobiles.datasources.api.implementations.MobilesNetworkDataSourceImp;
import me.bt.avea.data.repository.mobiles.datasources.api.implementations.RegisterNetworkDataSourceImp;
import me.bt.avea.data.repository.mobiles.datasources.api.implementations.SuggestionNetworkDataSourceImp;
import me.bt.avea.data.repository.mobiles.datasources.api.implementations.TripsNetworkDataSourceImp;
import me.bt.avea.repository.adduser.datasources.AddUserNetworkDataSource;
import me.bt.avea.repository.general.datasources.GeneralNetworkDataSource;
import me.bt.avea.repository.lightevents.LightEventsNetworkDataSource;
import me.bt.avea.repository.lightevents.LightPositionsNetworkDataSource;
import me.bt.avea.repository.mobiles.datasources.MobilesNetworkDataSource;
import me.bt.avea.repository.register.datasources.RegisterNetworkDataSource;
import me.bt.avea.repository.suggestion.datasource.SuggestionNetworkDataSource;
import me.bt.avea.repository.trips.TripsNetworkDataSource;
import me.bt.base.data.RetrofitLog;
import me.bt.base.data.UserAgent;
import me.bt.motor.util.Utils;
import retrofit.Endpoint;
import retrofit.ErrorHandler;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

@Module(
    complete = false,
    library = true) public class AveaApiModule {

  @Provides @Singleton RegisterNetworkDataSource provideRegisterNetworkDataSource(MobileApiService apiService, Application application) {
    return new RegisterNetworkDataSourceImp(apiService, application);
  }

  @Provides @Singleton MobilesNetworkDataSource getMobilesNetworkDataSource(MobileApiService apiService, Application application) {
    return new MobilesNetworkDataSourceImp(apiService, application);
  }

  @Provides @Singleton AddUserNetworkDataSource getAddUserNetworkDataSource(MobileApiService apiService, Application application) {
    return new AddUserNetworkDataSourceImp(apiService, application);
  }

  @Provides @Singleton SuggestionNetworkDataSource getSuggestionNetworkDataSource(MobileApiService apiService, Application application,
      TrayAppPreferences preferences) {
    return new SuggestionNetworkDataSourceImp(apiService, application, preferences);
  }

  @Provides @Singleton LightEventsNetworkDataSource getLightEventsNetworkDataSource(MobileApiService apiService, Application application,
      TrayAppPreferences preferences) {
    return new LightEventsNetworkDataSourceImp(apiService, application, preferences);
  }

  @Provides @Singleton TripsNetworkDataSource getTripsNetworkDataSource(MobileApiService apiService, Application application,
      TrayAppPreferences preferences) {
    return new TripsNetworkDataSourceImp(apiService, application, preferences);
  }

  @Provides @Singleton GeneralNetworkDataSource getGeneralNetworkDataSource(MobileApiService apiService, Application application) {
    return new GeneralNetworkDataSourceImp(apiService, application);
  }

  @Provides @Singleton LightPositionsNetworkDataSource getLightPositionsNetworkDataSource(MobileApiService apiService, Application application, TrayAppPreferences preferences) {
    return new LightPositionsNetworkDataSourceImp(apiService, application, preferences);
  }

  @Provides @Singleton MobileApiService provideMobileApiService(Application app,
                                                                @UserAgent final String userAgent,
                                                                Endpoint endPoint,
                                                                @RetrofitLog boolean logTraces,
                                                                Gson gson) {
    OkHttpClient client = new OkHttpClient();
//    client.setHostnameVerifier(SSLUtils.getEmptyHostnameVerifier());
//    client.setSslSocketFactory(SSLUtils.getPinnedCertSslSocketFactory(app, R.raw.mystore, BuildConfig.PROD_KEY_STORE_PASS));
    Utils.installCertificatePinning(client);

    return new RestAdapter.Builder().setEndpoint(endPoint)
        .setLogLevel(logTraces ? RestAdapter.LogLevel.FULL : RestAdapter.LogLevel.NONE)
        .setConverter(new GsonConverter(gson))
        .setClient(new OkClient(client))
        .setRequestInterceptor(new RequestInterceptor() {
          @Override public void intercept(RequestFacade requestFacade) {
            requestFacade.addHeader("User-Agent", userAgent);
          }
        })
        .setErrorHandler(new ErrorHandler() {
          @Override public Throwable handleError(RetrofitError cause) {
            ApiNetworkError networkError = new ApiNetworkError();
            networkError.setStackTrace(cause.getStackTrace());
            return networkError;
          }
        })
        .build()
        .create(MobileApiService.class);
  }
}