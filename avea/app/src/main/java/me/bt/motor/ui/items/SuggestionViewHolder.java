package me.bt.motor.ui.items;

import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.carlosdelachica.easyrecycleradapters.adapter.EasyViewHolder;

import butterknife.ButterKnife;
import butterknife.InjectView;
import me.bt.avea.domain.entities.Suggestion;
import me.bt.motor.util.Utils;
import me.bt.motor.R;

public class SuggestionViewHolder extends EasyViewHolder<Suggestion> {

    private final Resources resources;
    @InjectView(R.id.tv_distance)
    TextView tv_distance;
    @InjectView(R.id.tv_location)
    TextView tv_location;
    @InjectView(R.id.tv_description)
    TextView tv_description;

    public SuggestionViewHolder(Context context, ViewGroup parent, Resources resources) {
        super(context, parent, R.layout.item_suggestion);
        ButterKnife.inject(this, itemView);
        this.resources = resources;
    }

    @Override
    public void bindTo(Suggestion suggestion) {
        if (suggestion == null) {
            return;
        }
        if (suggestion.name != null) {
            tv_location.setText(suggestion.name);
        }
        if (suggestion.fromTable != null) {
            tv_description.setText(suggestion.fromTable);
            tv_description.setVisibility(View.GONE);
        }

        tv_distance.setText(Utils.getDistanceFromMeters(suggestion.distance / 1000)); //TODO one unit ??
    }
}
