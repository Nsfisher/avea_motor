package me.bt.motor.network;

/**
 * Created by yenerciftci on 28/07/15.
 */
public class BaseRequest {
    protected static final String CUSTOMER = "/adduser?";
    protected static final String CUSTOMER_BASE = "/customerpoint/?";
    protected static final String ALARMS_SET = "/alarm/set?";
    protected static final String BRANDS = "/brands/?";
    protected static final String MODELS = "/brand/models/?";
    protected static final String SERIES = "/model/series/?";
    protected static final String VALIDATE = "/auth/validate?";
    protected static final String CHECK_APP_VER = "/checkappversion";
    protected static final String CHECK_USER = "/checkuser?";

    public String token;
}
