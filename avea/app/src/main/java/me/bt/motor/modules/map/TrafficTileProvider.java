package me.bt.motor.modules.map;

import com.google.android.gms.maps.model.UrlTileProvider;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Locale;

public class TrafficTileProvider extends UrlTileProvider {

    public TrafficTileProvider(int width, int height) {
        super(width, height);
    }

    @Override
    public URL getTileUrl(int x, int y, int zoom) {
        BoundingBox tileBox = BoundingBox.tile2boundingBox(x, y, zoom);
        String baseUrl = "http://msn.tr.mapserver.be-mobile.be/p?service=wms&version=1.1.1&request=GetMap&Layers=turkey_links&Styles=default&format=image/png&TRANSPARENT=TRUE&SRS=EPSG:4326&BBOX=%f,%f,%f,%f&WIDTH=%d&HEIGHT=%d";
        Locale en = new Locale("en");
        String tileUrl = String.format(en, baseUrl, tileBox.west, tileBox.south, tileBox.east, tileBox.north, 256, 256);
        //String s = String.format("http://my.image.server/images/%d/%d/%d.png", zoom, x, y);

        if (!checkTileExists(x, y, zoom)) {
            return null;
        }

        try {
            return new URL(tileUrl);
        } catch (MalformedURLException e) {
            //log error
//            Mint.logException(e);
            return null;
        }
    }


    /*
     * Check that the tile server supports the requested x, y and zoom.
     * Complete this stub according to the tile range you support.
     * If you support a limited range of tiles at different zoom levels, then you
     * need to define the supported x, y range at each zoom level.
     */
    private boolean checkTileExists(int x, int y, int zoom) {


        return true;
    }

}
