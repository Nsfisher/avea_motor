package me.bt.motor.modules.calendar;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.joda.time.DateTime;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import butterknife.InjectView;
import butterknife.OnClick;
import me.bt.base.ui.BaseActivity;
import me.bt.base.ui.elevation.ElevationHandler;
import me.bt.motor.R;
import me.bt.motor.util.Utils;
import me.bt.opensource.andexert.calendarlistview.library.DatePickerController;
import me.bt.opensource.andexert.calendarlistview.library.DayPickerView;
import me.bt.opensource.andexert.calendarlistview.library.SimpleMonthAdapter;

public class CalendarActivity extends BaseActivity implements DatePickerController {

    public static final String START_DATE = "START_DATE";
    public static final String END_DATE = "END_DATE";
    @InjectView(R.id.dpv_calendar)
    DayPickerView dpv_calendar;
    @InjectView(R.id.tv_start_date)
    TextView tv_start_date;
    @InjectView(R.id.tv_end_date)
    TextView tv_end_date;
    @InjectView(R.id.tv_title)
    TextView tv_title;
    @InjectView(R.id.toolbar)
    Toolbar toolbar;
    @InjectView(R.id.iv_right)
    ImageView iv_right;
    @Inject
    ElevationHandler elevationHandler;
    private SimpleMonthAdapter.SelectedDays<SimpleMonthAdapter.CalendarDay> selectedDays;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupUI();
        initToolbar();

//        dpv_calendar.
    }

    private void initToolbar() {
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            tv_title.setText(R.string.activity_private_select);
            tv_title.setVisibility(View.VISIBLE);
            elevationHandler.setDefaultElevation(toolbar);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View back) {
                    onBackPressed();
                }
            });
        }
    }

    @OnClick(R.id.iv_right)
    void onCheckClicked() {
        Intent intent = new Intent();
        if (selectedDays != null) {
            setResult(Activity.RESULT_OK, intent);
            intent.putExtra(START_DATE, selectedDays.getFirst());
            intent.putExtra(END_DATE, selectedDays.getLast());
        } else {
            setResult(Activity.RESULT_CANCELED, intent);
        }
        finish();
    }

    @Override
    public int onCreateViewId() {
        return R.layout.activity_calendar;
    }

    protected List<Object> getModules() {
        return Arrays.<Object>asList(new CalendarModule());
    }

    private void setupUI() {
        dpv_calendar.setController(this);
        iv_right.setVisibility(View.VISIBLE);
        iv_right.setImageResource(R.drawable.ic_check_white_36dp);
//        dpv_calendar.date
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public int getMaxYear() {
        DateTime date = new DateTime();
        date.plusMonths(2);
        return date.getYear() + 1;
    }

    @Override
    public int getMinYear() {
        return new DateTime().getYear() - 3;
    }

    @Override
    public void onDayOfMonthSelected(int day, int month, int year) {

    }

    @Override
    public void onDateRangeSelected(SimpleMonthAdapter.SelectedDays<SimpleMonthAdapter.CalendarDay> selectedDays) {
        Date dateFirst = selectedDays.getFirst().getDate();
        Date dateLast = selectedDays.getLast().getDate();
        this.selectedDays = selectedDays;
        if (dateFirst.getTime() < dateLast.getTime()) {
            tv_start_date.setText(Utils.getLocalDateStringWithYear(dateFirst));
            tv_end_date.setText(Utils.getLocalDateStringWithYear(dateLast));
            this.selectedDays.setFirst(selectedDays.getFirst());
            this.selectedDays.setLast(selectedDays.getLast());
        } else {
            tv_start_date.setText(Utils.getLocalDateStringWithYear(dateLast));
            tv_end_date.setText(Utils.getLocalDateStringWithYear(dateFirst));
            this.selectedDays.setFirst(selectedDays.getLast());
            this.selectedDays.setLast(selectedDays.getFirst());
        }
    }
}
