package me.bt.motor.modules.route;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.carlosdelachica.easyrecycleradapters.adapter.EasyRecyclerAdapter;
import com.carlosdelachica.easyrecycleradapters.adapter.EasyViewHolder;
import com.carlosdelachica.easyrecycleradapters.recycler_view_manager.EasyRecyclerViewManager;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.Arrays;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.inject.Inject;

import butterknife.InjectView;
import me.bt.avea.domain.entities.Suggestion;
import me.bt.avea.presentation.map.MobileMapPresenter;
import me.bt.avea.presentation.map.MobileMapView;
import me.bt.avea.presentation.model.PresentationMobile;
import me.bt.avea.presentation.route.RoutePresenter;
import me.bt.avea.presentation.route.RouteView;
import me.bt.base.ui.BaseActivity;
import me.bt.base.ui.elevation.ElevationHandler;
import me.bt.base.ui.loading.LoadingManager;
import me.bt.motor.AveaConstants;
import me.bt.motor.R;
import me.bt.motor.modules.map.MobileMapModule;
import me.bt.motor.modules.map.adapters.MobileSpinnerAdapter;
import me.bt.motor.modules.route.adapters.SuggestionViewHolderFactory;
import me.bt.motor.ui.items.SuggestionViewHolder;
import me.bt.motor.util.Utils;
import me.bt.opensource.directions.route.Route;
import me.bt.opensource.directions.route.Routing;
import me.bt.opensource.directions.route.RoutingListener;

public class RouteActivity extends BaseActivity
        implements MobileMapView, RouteView, AdapterView.OnItemSelectedListener, EasyViewHolder.OnItemClickListener,
        RoutingListener { //TODO hakan remove
    @InjectView(R.id.toolbar)
    Toolbar toolbar;
    @InjectView(R.id.iv_right)
    ImageView iv_right;
    @InjectView(R.id.sp_mobiles)
    Spinner sp_mobiles;
    @InjectView(R.id.et_arrival)
    EditText et_arrival;
    @InjectView(R.id.rv_routes)
    RecyclerView rv_routes;
    @InjectView(R.id.cv_routes)
    CardView cv_routes;

    @Inject
    MobileMapPresenter mobilePresenter;
    @Inject
    RoutePresenter routePresenter;
    @Inject
    ElevationHandler elevationHandler;
    @Inject
    LoadingManager loadingManager;

    private List<PresentationMobile> presentationMobiles;
    private MobileSpinnerAdapter spinnerAdapter;
    private GoogleMap map;
    private PresentationMobile selectedMobile;
    private EasyRecyclerAdapter adapter;
    private EasyRecyclerViewManager recyclerViewManager;
    private List<Suggestion> routeSuggestions;
    private Marker vehicleMarker;
    private int lastSelected;
    private ProgressDialog mProgress;
    private boolean mIsCalledManuel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initToolbar();
        initDrawer(this, toolbar);
        setupMap();
        setupUI();
        startServiceCalls();
        mProgress = new ProgressDialog(this);

    }

    private void setupUI() {
        setupRecycleView();
        setupListeners();
    }

    private void setupListeners() {
        et_arrival.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(final TextView tv, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if (selectedMobile != null && tv.length() > 0) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                loadingManager.showLoading(R.string.please_wait);
                                routePresenter.getSuggestion(tv.getText().toString(), selectedMobile.posLatitude, selectedMobile.posLongitude);
                            }
                        });
                    }
                    return true;
                }
                return false;
            }
        });
        et_arrival.addTextChangedListener(mWatcher);
    }

    private TextWatcher mWatcher = new TextWatcher() {
        private Timer timer = new Timer();
        private final long DELAY = 800; // in ms

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(final Editable s) {
            if (mIsCalledManuel) {
                mIsCalledManuel = false;
                return;
            }
            timer.cancel();
            timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            String text = et_arrival.getText().toString();
                            if (selectedMobile != null && text.length() > 0) {

                                if (mProgress == null) {
                                    mProgress = new ProgressDialog(RouteActivity.this);
                                }

                                if (mProgress != null && !mProgress.isShowing())
                                    mProgress.show();

                                routePresenter.getSuggestion(text, selectedMobile.posLatitude, selectedMobile.posLongitude);
                            } else if (text.length() < 1) {
                                cv_routes.setVisibility(View.GONE);
                            }
                        }
                    });
                }

            }, DELAY);
        }
    };

    private void setupMap() {
        map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(AveaConstants.TURKEY, 15));
        map.getUiSettings().setZoomControlsEnabled(true);
    }

    private void startServiceCalls() {
        mobilePresenter.getMobiles();
    }

    @Override
    public int onCreateViewId() {
        return R.layout.activity_route;
    }

    protected List<Object> getModules() {
        return Arrays.<Object>asList(new MobileMapModule(this), new RouteModule(this));
    }

    private void initToolbar() {
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            elevationHandler.setDefaultElevation(toolbar);
            iv_right.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mobilePresenter.onPause();
        routePresenter.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mobilePresenter.onResume();
        routePresenter.onResume();
    }

    @Override
    public void onBackPressed() {
        if (cv_routes.getVisibility() == View.VISIBLE) {
            cv_routes.setVisibility(View.GONE);
        } else super.onBackPressed();
    }

    @Override
    public void updateMobiles(List<PresentationMobile> presentationMobiles) {
        this.presentationMobiles = presentationMobiles;
        sp_mobiles.setVisibility(View.VISIBLE);
        spinnerAdapter = new MobileSpinnerAdapter(this, presentationMobiles);
        sp_mobiles.setAdapter(spinnerAdapter);
        sp_mobiles.setOnItemSelectedListener(this);

        int defaultPosition = Utils.getDefaultMobilePosition(presentationMobiles, getAppPreferences());
        if (defaultPosition >= 0 && presentationMobiles.size() > defaultPosition) {
            sp_mobiles.setSelection(defaultPosition);
        }
    }

    @Override
    public void updateMobilesError() {
        loadingManager.dismissLoading();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        selectedMobile = spinnerAdapter.getItem(position);
        lastSelected = position;
        saveSelectedIdToDisk(selectedMobile);
        showMobileLocation();
    }

    private void showMobileLocation() {
        map.clear();
        if (map != null
                && selectedMobile != null
                && selectedMobile.getPosLatitude() != 0
                && selectedMobile.getPosLongitude() != 0
                && selectedMobile.getPosDirectionValue() != null) {
            LatLng position = new LatLng(selectedMobile.getPosLatitude(), selectedMobile.getPosLongitude());
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(selectedMobile.getPosLatitude(), selectedMobile.getPosLongitude()), 15));
//            if (vehicleMarker == null) {
            vehicleMarker =
                    map.addMarker(new MarkerOptions().position(position).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_vehicle_pin)));
            vehicleMarker.setRotation(selectedMobile.getPosDirectionValue());
//            } else {
//                vehicleMarker.setPosition(position);
//                vehicleMarker.setRotation(selectedMobile.getPosDirectionValue());
//            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void refreshSuggestions(List<Suggestion> suggestions) {
        loadingManager.dismissLoading();
        cv_routes.setVisibility(View.VISIBLE);
        routeSuggestions = suggestions;
        adapter.clear();
        recyclerViewManager.addAll(suggestions);

        hideProgres();

    }

    private void setupRecycleView() {
        SuggestionViewHolderFactory suggestionViewHolderFactory = new SuggestionViewHolderFactory(this, getResources());
        adapter = new EasyRecyclerAdapter(suggestionViewHolderFactory, Suggestion.class, SuggestionViewHolder.class);
        recyclerViewManager =
                new EasyRecyclerViewManager.Builder(rv_routes, adapter).loadingListTextColor(android.R.color.black).clickListener(this).build();
    }

    @Override
    public void showRefreshSuggestionsError() {
        loadingManager.dismissLoading();
        hideProgres();
    }

    @Override
    protected void onDestroy() {
        et_arrival.removeTextChangedListener(mWatcher);
        super.onDestroy();
    }

    private void hideProgres() {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mProgress.dismiss();
                }
            });
        } catch (Exception e) {

        }
    }

    @Override
    public void onItemClick(int position, View view) {
        if (routeSuggestions != null && routeSuggestions.size() > position) {
            double destinationLat = routeSuggestions.get(position).latitude;
            double destinationLng = routeSuggestions.get(position).longitude;

            String name = routeSuggestions.get(position).name;
            et_arrival.removeTextChangedListener(mWatcher);
            et_arrival.setText(name);
            et_arrival.setSelection(et_arrival.getText().length());
            et_arrival.addTextChangedListener(mWatcher);


            LatLng start = new LatLng(selectedMobile.posLatitude, selectedMobile.posLongitude);
            LatLng end = new LatLng(destinationLat, destinationLng);
            Routing routing = new Routing(Routing.TravelMode.DRIVING);
            routing.registerListener(this);
            routing.execute(start, end);
            showMobileLocation();
        }
    }

    @Override
    public void onRoutingFailure() {

    }

    @Override
    public void onRoutingStart() {

    }

    @Override
    public void onRoutingSuccess(PolylineOptions mPolyOptions, Route route) {
        if (map != null) {
            map.clear();
            PolylineOptions polyoptions = new PolylineOptions();
            polyoptions.color(Color.RED);
            polyoptions.width(15);
            polyoptions.addAll(mPolyOptions.getPoints());
            map.addPolyline(polyoptions);
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            for (LatLng aPoint : mPolyOptions.getPoints()) {
                builder.include(aPoint);
            }
            LatLngBounds bounds = builder.build();
            int padding = (int) Utils.convertDpToPixel(16, this);
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, padding);
            map.moveCamera(cameraUpdate);

            if (mPolyOptions.getPoints().size() > 1) {
                map.addMarker(new MarkerOptions().position(mPolyOptions.getPoints().get(0))
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_vehicle_pin)));

                map.addMarker(new MarkerOptions().position(mPolyOptions.getPoints().get(mPolyOptions.getPoints().size() - 1))
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_start_mid)));
            }

            cv_routes.setVisibility(View.GONE);
        }
    }
}

