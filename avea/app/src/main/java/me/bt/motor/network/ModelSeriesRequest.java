package me.bt.motor.network;

import me.bt.base.util.Constants;

/**
 * Created by yenerciftci on 30/07/15.
 */
public class ModelSeriesRequest extends BaseRequest {
    public String modelId;

    @Override
    public String toString() {
        return Constants.APP_URL + SERIES
                + "token=" + token + "&modelId=" + modelId;
    }
}
