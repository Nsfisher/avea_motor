package me.bt.motor.modules.route.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.view.ViewGroup;

import com.carlosdelachica.easyrecycleradapters.adapter.BaseEasyViewHolderFactory;
import com.carlosdelachica.easyrecycleradapters.adapter.EasyViewHolder;

import me.bt.motor.ui.items.SuggestionViewHolder;

public class SuggestionViewHolderFactory extends BaseEasyViewHolderFactory {

    private final Resources resources;

    public SuggestionViewHolderFactory(Context context, Resources resources) {
        super(context);
        this.resources = resources;
    }

    @Override
    public EasyViewHolder create(Class valueClass, ViewGroup parent) {
        return new SuggestionViewHolder(context, parent, resources);
    }
}

