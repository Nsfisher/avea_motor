package me.bt.motor.modules.trips.common.data;

import java.util.List;
import me.bt.avea.domain.entities.DataLightPosition;
import me.bt.avea.domain.entities.IntervalData;

public abstract class AbstractExpandableDataProvider {
  public abstract int getGroupCount();

  public abstract int getChildCount(int groupPosition);

  public abstract GroupData getGroupItem(int groupPosition);

  public abstract ChildData getChildItem(int groupPosition, int childPosition);

  public abstract void moveGroupItem(int fromGroupPosition, int toGroupPosition);

  public abstract void moveChildItem(int fromGroupPosition, int fromChildPosition, int toGroupPosition, int toChildPosition);

  public abstract void removeGroupItem(int groupPosition);

  public abstract void removeChildItem(int groupPosition, int childPosition);

  public abstract long undoLastRemoval();

  public static abstract class BaseData {

    public abstract int getSwipeReactionType();

    public abstract IntervalData getIntervalData();

    public abstract boolean isPinnedToSwipeLeft();

    public abstract void setPinnedToSwipeLeft(boolean pinned);
  }

  public static abstract class GroupData extends BaseData {
    public abstract boolean isSectionHeader();

    public abstract long getGroupId();
  }

  public static abstract class ChildData extends BaseData {
    public abstract List<DataLightPosition> getLightPositions();
    public abstract void setLightPositions(List<DataLightPosition> positions);
    public abstract long getChildId();
  }
}
