package me.bt.motor.modules.settings;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.carlosdelachica.easyrecycleradapters.adapter.EasyRecyclerAdapter;
import com.carlosdelachica.easyrecycleradapters.adapter.EasyViewHolder;
import com.carlosdelachica.easyrecycleradapters.recycler_view_manager.EasyRecyclerViewManager;
import com.google.gson.Gson;

import net.grandcentrix.tray.accessor.ItemNotFoundException;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import butterknife.InjectView;
import me.bt.avea.data.preferences.PrefConstants;
import me.bt.motor.modules.map.MobileMapModule;
import me.bt.motor.modules.settings.adapters.DefaultMobileViewHolderFactory;
import me.bt.avea.presentation.map.MobileMapPresenter;
import me.bt.avea.presentation.map.MobileMapView;
import me.bt.avea.presentation.model.PresentationMobile;
import me.bt.motor.ui.items.DefaultMobileViewHolder;
import me.bt.motor.R;
import me.bt.base.ui.BaseActivity;
import me.bt.base.ui.elevation.ElevationHandler;
import me.bt.base.ui.loading.LoadingManager;

public class DefaultMobileActivity extends BaseActivity implements MobileMapView, EasyViewHolder.OnItemClickListener {

    @Inject
    MobileMapPresenter mobilePresenter;
    @Inject
    LoadingManager loadingManager;
    @InjectView(R.id.toolbar)
    Toolbar toolbar;
    @InjectView(R.id.rv_default_mobile)
    RecyclerView rv_default_mobile;
    @Inject
    ElevationHandler elevationHandler;
    private int selectedIndex = -1;
    private List<PresentationMobile> presentationMobiles;
    private PresentationMobile selectedMobile;
    private EasyRecyclerAdapter adapter;
    private EasyRecyclerViewManager recyclerViewManager;
    private Gson mGson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initToolbar();
        mGson = new Gson();
        setupRecycleView();
        startServiceCalls();
    }

    private void initToolbar() {
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            toolbar.setTitle(R.string.activity_default_mobile);
            elevationHandler.setDefaultElevation(toolbar);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View back) {
                    onBackPressed();
                }
            });
        }
    }

    private void setupRecycleView() {
        DefaultMobileViewHolderFactory defaultMobileViewHolderFactory = new DefaultMobileViewHolderFactory(this, getResources());
        adapter = new EasyRecyclerAdapter(defaultMobileViewHolderFactory, PresentationMobile.class, DefaultMobileViewHolder.class);
        recyclerViewManager = new EasyRecyclerViewManager.Builder(rv_default_mobile, adapter).loadingListTextColor(android.R.color.black)
                .clickListener(this)
                .divider(R.drawable.list_divider)
                .build();
    }


    private void startServiceCalls() {
        mobilePresenter.getMobiles();
    }

    @Override
    public int onCreateViewId() {
        return R.layout.activity_default_mobile;
    }

    protected List<Object> getModules() {
        return Arrays.<Object>asList(new MobileMapModule(this));
    }

    @Override
    public void updateMobiles(List<PresentationMobile> presentationMobiles) {
        this.presentationMobiles = presentationMobiles;
        int defaultMobileId = 0;
        try {
            defaultMobileId = getAppPreferences().getInt(PrefConstants.DEFAULT_MOBILE_ID_INDEX);
        } catch (ItemNotFoundException e) {
            try {
                defaultMobileId = getAppPreferences().getInt(PrefConstants.DEFAULT_MOBILE_ID);
            } catch (ItemNotFoundException e1) {
                defaultMobileId = 0;
            }
        }
        if (defaultMobileId > 0) {
            for (PresentationMobile mobile : presentationMobiles) {
                if (mobile.mobile == defaultMobileId) {
                    mobile.checked = true;
                }
            }
        }
        recyclerViewManager.addAll(presentationMobiles);
    }

    @Override
    public void updateMobilesError() {

    }

    @Override
    protected void onPause() {
        super.onPause();
        mobilePresenter.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mobilePresenter.onResume();
    }

    @Override
    public void onItemClick(int position, View view) {
        if (presentationMobiles != null && presentationMobiles.size() > position) {
            PresentationMobile presentationMobile = presentationMobiles.get(position);

            String username = getAppPreferences().getString(PrefConstants.U_NAME, "");
            getAppPreferences().put(PrefConstants.DEFAULT_SAVED_ID, presentationMobile.mobile);
            getAppPreferences().put(PrefConstants.DEFAULT_MOBILE_ALIAS, presentationMobile.alias);
            getAppPreferences().put(PrefConstants.DEFAULT_MOBILE_ID_INDEX, presentationMobile.mobile);

//            DefaultMobile defaultMobile = new DefaultMobile();
//            defaultMobile.defaultUsername = username;
//            default
//            getAppPreferences().
            finish();
        }
    }
}
