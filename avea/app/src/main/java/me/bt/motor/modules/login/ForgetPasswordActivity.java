package me.bt.motor.modules.login;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import butterknife.InjectView;
import butterknife.OnClick;
import me.bt.avea.domain.entities.GeneralResponse;
import me.bt.avea.domain.interactions.general.GeneralInteractor;
import me.bt.avea.domain.interactions.general.events.GeneralEvent;
import me.bt.avea.presentation.general.GeneralPresenter;
import me.bt.avea.presentation.general.GeneralView;
import me.bt.avea.presentation.login.LoginPresenter;
import me.bt.avea.presentation.login.LoginView;
import me.bt.base.ui.BaseActivity;
import me.bt.base.ui.elevation.ElevationHandler;
import me.bt.base.ui.errors.ErrorManager;
import me.bt.base.ui.loading.LoadingManager;
import me.bt.motor.AveaConstants;
import me.bt.motor.R;
import me.bt.motor.modules.general.GeneralModule;
import me.bt.motor.util.Utils;

public class ForgetPasswordActivity extends BaseActivity implements GeneralView, LoginView {

    @Inject
    LoginPresenter loginPresenter;
    @Inject
    GeneralPresenter presenter;
    @Inject
    ErrorManager errorManager;
    @Inject
    LoadingManager loadingManager;
    @Inject
    ElevationHandler elevationHandler;

    @InjectView(R.id.btn_send_password)
    Button btn_send_password;
    @InjectView(R.id.et_email)
    EditText et_email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public int onCreateViewId() {
        return R.layout.activity_forget_password;
    }

    protected List<Object> getModules() {
        return Arrays.asList(new GeneralModule(this), new LoginModule(this));
    }

    @OnClick(R.id.btn_send_password)
    void onSendClicked() {
        String email = et_email.getText().toString();
        if (Utils.isValidEmailAddress(email)) {
            loginPresenter.login(AveaConstants.TEST_USER, AveaConstants.TEST_PASSWORD);
            loadingManager.showLoading(R.string.loading_message);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.onPause();
        loginPresenter.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onResume();
        loginPresenter.onResume();
    }

    @Override
    public void generalResponse(GeneralEvent response) {
        loadingManager.dismissLoading();
        if (response.getRequestType() == GeneralInteractor.REQUEST_FORGET_PASSWORD) {
            if (response.getError() == null) {
                GeneralResponse generalResponse = (GeneralResponse) response.getData();
                if (generalResponse.errorDesc != null) {
                    errorManager.showError(generalResponse.errorDesc);
                }
            }
        }
    }

    @Override
    public void loginComplete() {
        presenter.forgetPassword(et_email.getText().toString());
    }

    @Override
    public void loginError(String mes) {
        loadingManager.dismissLoading();
    }
}
