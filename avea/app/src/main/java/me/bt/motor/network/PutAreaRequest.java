package me.bt.motor.network;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import me.bt.base.util.Constants;

/**
 * Created by yenerciftci on 28/07/15.
 */
public class PutAreaRequest extends BaseRequest {
    public String regionName;
    public String coors;
    public double latitude, longitude, radius;
//    @property(nonatomic) double latitude,longitude,radius;


    @Override
    public String toString() {
        try {
            return Constants.APP_URL + CUSTOMER_BASE
                    + "token=" + token
                    + "&" + "regionName=" + URLEncoder.encode(regionName, "UTF-8")
                    + "&" + "coors=" + coors
                    + "&" + "latitude=" + latitude
                    + "&" + "longitude=" + longitude
                    + "&" + "radius=" + radius;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();

            return Constants.APP_URL + CUSTOMER_BASE
                    + "token=" + token
                    + "&" + "regionName=" + regionName
                    + "&" + "coors=" + coors
                    + "&" + "latitude=" + latitude
                    + "&" + "longitude=" + longitude
                    + "&" + "radius=" + radius;
        }
    }

}
