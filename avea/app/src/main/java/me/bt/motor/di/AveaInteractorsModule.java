package me.bt.motor.di;

import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;
import me.bt.avea.domain.interactions.adduser.AddUserInteractor;
import me.bt.avea.domain.interactions.general.GeneralInteractor;
import me.bt.avea.domain.interactions.lightevents.LightEventsInteractor;
import me.bt.avea.domain.interactions.lightevents.LightPositionsInteractor;
import me.bt.avea.domain.interactions.mobiles.MobileInteractor;
import me.bt.avea.domain.interactions.mobiles.MobilesInteractor;
import me.bt.avea.domain.interactions.register.RegisterInteractor;
import me.bt.avea.domain.interactions.suggestion.SuggestionInteractor;
import me.bt.avea.domain.interactions.trips.TripsInteractor;
import me.bt.avea.domain.repository.AddUserRepository;
import me.bt.avea.domain.repository.GeneralRepository;
import me.bt.avea.domain.repository.LightEventsRepository;
import me.bt.avea.domain.repository.LightPositionsRepository;
import me.bt.avea.domain.repository.MobilesRepository;
import me.bt.avea.domain.repository.RegisterRepository;
import me.bt.avea.domain.repository.SuggestionRepository;
import me.bt.avea.domain.repository.TripsRepository;
import me.bt.base.domain.abstractions.Bus;

@Module(
    complete = false,
    library = true) public class AveaInteractorsModule {

  @Provides @Singleton RegisterInteractor provideRegisterInteractor(Bus bus, RegisterRepository repository) { //TODO hakan
    return new RegisterInteractor(bus, repository);
  }

  @Provides @Singleton MobilesInteractor provideMobilesInteractor(Bus bus, MobilesRepository repository) { //TODO hakan
    return new MobilesInteractor(bus, repository);
  }

  @Provides @Singleton MobileInteractor provideMobileInteractor(Bus bus, MobilesRepository repository) { //TODO hakan
    return new MobileInteractor(bus, repository);
  }

  @Provides @Singleton AddUserInteractor provideAddUserInteractor(Bus bus, AddUserRepository repository) { //TODO hakan
    return new AddUserInteractor(bus, repository);
  }

  @Provides @Singleton SuggestionInteractor provideSuggestionInteractor(Bus bus, SuggestionRepository repository) { //TODO hakan
    return new SuggestionInteractor(bus, repository);
  }

  @Provides @Singleton LightEventsInteractor provideLightEventsInteractor(Bus bus, LightEventsRepository repository) { //TODO hakan
    return new LightEventsInteractor(bus, repository);
  }

  @Provides @Singleton TripsInteractor provideTripsInteractor(Bus bus, TripsRepository repository) { //TODO hakan
    return new TripsInteractor(bus, repository);
  }

  @Provides @Singleton GeneralInteractor provideGeneralInteractor(Bus bus, GeneralRepository repository) { //TODO hakan
    return new GeneralInteractor(bus, repository);
  }

  @Provides @Singleton LightPositionsInteractor provideLightPositionsInteractor(Bus bus, LightPositionsRepository repository) { //TODO hakan
    return new LightPositionsInteractor(bus, repository);
  }
}
