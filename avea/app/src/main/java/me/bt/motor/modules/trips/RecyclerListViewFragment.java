package me.bt.motor.modules.trips;

import android.app.Fragment;
import android.graphics.drawable.NinePatchDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.h6ah4i.android.widget.advrecyclerview.animator.GeneralItemAnimator;
import com.h6ah4i.android.widget.advrecyclerview.animator.RefactoredDefaultItemAnimator;
import com.h6ah4i.android.widget.advrecyclerview.decoration.ItemShadowDecorator;
import com.h6ah4i.android.widget.advrecyclerview.decoration.SimpleListDividerDecorator;
import com.h6ah4i.android.widget.advrecyclerview.expandable.RecyclerViewExpandableItemManager;
import com.h6ah4i.android.widget.advrecyclerview.utils.WrapperAdapterUtils;

import de.greenrobot.event.EventBus;
import me.bt.motor.R;
import me.bt.motor.modules.trips.adapters.TripExpandableItemAdapter;
import me.bt.motor.modules.trips.common.TripLightPointsEvent;
import me.bt.motor.modules.trips.common.data.AbstractExpandableDataProvider;

public class RecyclerListViewFragment extends Fragment {
    private static final String SAVED_STATE_EXPANDABLE_ITEM_MANAGER = "RecyclerViewExpandableItemManager";

    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.Adapter mWrappedAdapter;
    private RecyclerViewExpandableItemManager mRecyclerViewExpandableItemManager;
    private TripExpandableItemAdapter myItemAdapter;
    private int mCurrentPos;

    public RecyclerListViewFragment() {
        super();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_recycler_list_view, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupAdapter(savedInstanceState);
    }

    private void setupAdapter(Bundle savedInstanceState) {
        mRecyclerView = (RecyclerView) getView().findViewById(R.id.recycler_view);
        final Parcelable eimSavedState =
                (savedInstanceState != null) ? savedInstanceState.getParcelable(SAVED_STATE_EXPANDABLE_ITEM_MANAGER) : null;

        mRecyclerViewExpandableItemManager = new RecyclerViewExpandableItemManager(eimSavedState);

        mLayoutManager = new LinearLayoutManager(getActivity());
        getDataProvider();

        myItemAdapter = new TripExpandableItemAdapter(getDataProvider(), getFragmentManager(), getActivity(), mRecyclerViewExpandableItemManager);

        mAdapter = myItemAdapter;

        mWrappedAdapter = mRecyclerViewExpandableItemManager.createWrappedAdapter(myItemAdapter);

        final GeneralItemAnimator animator = new RefactoredDefaultItemAnimator();
        animator.setSupportsChangeAnimations(false);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mWrappedAdapter);
        mRecyclerView.setItemAnimator(animator);
        mRecyclerView.setHasFixedSize(false);

        if (!supportsViewElevation()) {
            mRecyclerView.addItemDecoration(
                    new ItemShadowDecorator((NinePatchDrawable) getResources().getDrawable(R.drawable.material_shadow_z1_)));
        }
        mRecyclerView.addItemDecoration(new SimpleListDividerDecorator(getResources().getDrawable(R.drawable.list_divider), true));
        mRecyclerViewExpandableItemManager.attachRecyclerView(mRecyclerView);
        ;

        myItemAdapter.setOnExpandListener(new TripExpandableItemAdapter.OnExpandListener() {
            @Override
            public void onExpanded(int index) {
//                mRecyclerViewExpandableItemManager.sc(index);
                mCurrentPos = index;
                mRecyclerView.scrollToPosition(index);
                mRecyclerView.getLayoutManager().scrollToPosition(index);
            }
        });
    }



    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        if (mRecyclerViewExpandableItemManager != null) {
            outState.putParcelable(SAVED_STATE_EXPANDABLE_ITEM_MANAGER, mRecyclerViewExpandableItemManager.getSavedState());
        }
    }

    @Override
    public void onDestroyView() {
        clearViews();
        super.onDestroyView();
    }

    private void clearViews() {
        if (mRecyclerViewExpandableItemManager != null) {
            mRecyclerViewExpandableItemManager.release();
            mRecyclerViewExpandableItemManager = null;
        }

        if (mRecyclerView != null) {
            mRecyclerView.setItemAnimator(null);
            mRecyclerView.setAdapter(null);
            mRecyclerView = null;
        }

        if (mWrappedAdapter != null) {
            WrapperAdapterUtils.releaseAll(mWrappedAdapter);
            mWrappedAdapter
                    = null;
        }

        mAdapter = null;
        mLayoutManager = null;
    }

    private boolean supportsViewElevation() {
        return (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP);
    }

    public AbstractExpandableDataProvider getDataProvider() {
        return ((TripsActivity) getActivity()).getDataProvider();
    }

    public void refreshAdapter() {
        clearViews();
        setupAdapter(null);
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        EventBus.getDefault().unregister(this);
    }

    public void onEvent(TripLightPointsEvent event) {
        if (myItemAdapter != null) {
            myItemAdapter.updateLightPoints(event);
//            myItemAdapter.notify();
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
//                    mRecyclerViewExpandableItemManager.collapseGroup(mCurrentPos);
                    mRecyclerViewExpandableItemManager.expandGroup(mCurrentPos);

//                    mCurrentPos.
//                    myItemAdapter.notifyDataSetChanged();
                }
            });
        }
    }
}
