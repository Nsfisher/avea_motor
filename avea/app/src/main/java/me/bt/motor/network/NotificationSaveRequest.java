package me.bt.motor.network;

import me.bt.base.util.Constants;

/**
 * Created by yenerciftci on 20/08/15.
 */
public class NotificationSaveRequest extends BaseRequest {
    public String key;
    public String value;
//    @property(nonatomic) double latitude,longitude,radius;


    @Override
    public String toString() {
        return Constants.APP_URL + ALARMS_SET
                + "token=" + token
                + "&" + "alarms" + "=" + key + "," + value;
    }
}
