package me.bt.motor.network;

import me.bt.base.util.Constants;

/**
 * Created by yenerciftci on 19/08/15.
 */
public class TokenCheckRequest extends BaseRequest {
    public String token;

    @Override
    public String toString() {
        return Constants.APP_URL + VALIDATE
                + "token=" + token;
    }
}
