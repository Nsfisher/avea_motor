package me.bt.motor.modules.map;

import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;
import me.bt.avea.domain.entities.Mobile;
import me.bt.avea.domain.interactions.mobiles.MobilesInteractor;
import me.bt.motor.modules.performance.PerformanceActivity;
import me.bt.motor.modules.route.RouteActivity;
import me.bt.motor.modules.settings.DefaultMobileActivity;
import me.bt.motor.modules.trips.TripsActivity;
import me.bt.avea.presentation.map.MobileMapPresenter;
import me.bt.avea.presentation.map.MobileMapView;
import me.bt.avea.presentation.model.PresentationMobile;
import me.bt.base.di.ActivityModule;
import me.bt.base.domain.abstractions.Bus;
import me.bt.base.domain.interactors.InteractorInvoker;
import me.bt.base.presentation.model.mapper.base.ListMapper;

@Module(
    addsTo = ActivityModule.class,
    library = true,
    complete = false,
    injects = {MobileMapActivity.class, RouteActivity.class, TripsActivity.class, PerformanceActivity.class, DefaultMobileActivity.class}) public class MobileMapModule {

  private MobileMapView mobileMapView;

  public MobileMapModule(MobileMapView mobileMapView) {
    this.mobileMapView = mobileMapView;
  }

  @Provides @Singleton MobileMapPresenter provideMobileMapPresenter(Bus bus, InteractorInvoker interactorInvoker,
      MobilesInteractor mobilesInteractor, ListMapper<Mobile, PresentationMobile> listMapper) {
    return new MobileMapPresenter(bus, interactorInvoker, mobileMapView, mobilesInteractor, listMapper);
  }
}

