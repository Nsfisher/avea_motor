package me.bt.motor.modules.notifications;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.carlosdelachica.easyrecycleradapters.adapter.EasyRecyclerAdapter;
import com.carlosdelachica.easyrecycleradapters.recycler_view_manager.EasyRecyclerViewManager;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import butterknife.InjectView;
import me.bt.avea.data.preferences.PrefConstants;
import me.bt.avea.domain.entities.LightEvent;
import me.bt.avea.presentation.model.PresentationMobile;
import me.bt.avea.presentation.notifications.NotificationsPresenter;
import me.bt.avea.presentation.notifications.NotificationsView;
import me.bt.base.ui.BaseActivity;
import me.bt.base.ui.elevation.ElevationHandler;
import me.bt.base.ui.errors.ErrorManager;
import me.bt.base.ui.loading.LoadingManager;
import me.bt.motor.R;
import me.bt.motor.modules.notifications.adapters.LightEventViewHolderFactory;
import me.bt.motor.ui.items.LightEventViewHolder;

public class
        NotificationsActivity extends BaseActivity implements NotificationsView {

    public static String MOBILE_ID = "MOBILE_ID";
    @InjectView(R.id.rv_notifications)
    RecyclerView rv_notifications;
    @Inject
    NotificationsPresenter presenter;
    @Inject
    ErrorManager errorManager;
    @Inject
    ElevationHandler elevationHandler;
    @Inject
    LoadingManager loadingManager;
    @InjectView(R.id.toolbar)
    Toolbar toolbar;
    @InjectView(R.id.tv_no_item)
    TextView tv_no_item;
    private EasyRecyclerAdapter adapter;
    private EasyRecyclerViewManager recyclerViewManager;
    private List<PresentationMobile> presentationMobiles;
    private int selectedMobileID = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupUI();
        initToolbar();
        getDefaultMobile();
        startServiceCalls();
    }

    private void initToolbar() {
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            toolbar.setTitle(R.string.activity_notifications);
            elevationHandler.setDefaultElevation(toolbar);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View back) {
                    onBackPressed();
                }
            });
        }
    }

    @Override
    public int onCreateViewId() {
        return R.layout.activity_notifications;
    }

    protected List<Object> getModules() {
        return Arrays.<Object>asList(new NotificationsModule(this));
    }

    private void getDefaultMobile() {
        selectedMobileID = getAppPreferences().getInt(PrefConstants.DEFAULT_MOBILE_ID, 0);
    }

    private void startServiceCalls() {
        loadingManager.showLoading(R.string.loading_message);
        presenter.getLightEvents();
    }

    private void setupUI() {
        setupRecycleView();
    }

    private void setupRecycleView() {
        LightEventViewHolderFactory lightEventViewHolderFactory = new LightEventViewHolderFactory(this, getResources());
        adapter = new EasyRecyclerAdapter(lightEventViewHolderFactory, LightEvent.class, LightEventViewHolder.class);
        recyclerViewManager = new EasyRecyclerViewManager.Builder(rv_notifications, adapter).loadingListTextColor(android.R.color.black)
                .build();
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    public void refreshLightEventError() {
        loadingManager.dismissLoading();
    }

    @Override
    public void refreshLightEvents(List<LightEvent> lightEvents) {
        loadingManager.dismissLoading();
        recyclerViewManager.addAll(lightEvents);
        if (lightEvents.size() == 0) {
            tv_no_item.setVisibility(View.VISIBLE);
            rv_notifications.setVisibility(View.GONE);
        } else {
            tv_no_item.setVisibility(View.GONE);
            rv_notifications.setVisibility(View.VISIBLE);
        }
    }

}
