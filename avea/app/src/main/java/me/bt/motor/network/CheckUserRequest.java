package me.bt.motor.network;

import java.net.URLEncoder;

import me.bt.base.util.Constants;

/**
 * Created by yenerciftci on 28/07/15.
 */
public class CheckUserRequest extends BaseRequest {
    public String username, password, activation;

    @Override
    public String toString() {
        try {
            return Constants.APP_URL + CHECK_USER
                    + "token=" + token
                    + "&" + "username=" + URLEncoder.encode(username, "UTF-8")
                    + "&" + "password=" + URLEncoder.encode(password, "UTF-8")
                    + "&" + "activation=" + URLEncoder.encode(activation, "UTF-8");
        } catch (Exception e) {
            return null;
        }


    }
//    358639051000142;

}
