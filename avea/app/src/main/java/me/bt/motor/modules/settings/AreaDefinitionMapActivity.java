package me.bt.motor.modules.settings;

import android.content.DialogInterface;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import butterknife.InjectView;
import me.bt.avea.data.preferences.PrefConstants;
import me.bt.avea.domain.entities.Region;
import me.bt.avea.domain.interactions.general.events.GeneralEvent;
import me.bt.avea.presentation.general.GeneralPresenter;
import me.bt.avea.presentation.general.GeneralView;
import me.bt.base.ui.BaseActivity;
import me.bt.base.ui.elevation.ElevationHandler;
import me.bt.base.ui.loading.LoadingManager;
import me.bt.motor.AveaConstants;
import me.bt.motor.R;
import me.bt.motor.modules.general.GeneralModule;
import me.bt.motor.network.DeleteAreaRequest;
import me.bt.motor.network.PutAreaRequest;
import me.bt.motor.network.response.BaseResponse;
import me.bt.motor.util.Utils;
import me.bt.opensource.mapareas.MapAreaManager;
import me.bt.opensource.mapareas.MapAreaMeasure;
import me.bt.opensource.mapareas.MapAreaWrapper;
import pl.charmas.android.reactivelocation.ReactiveLocationProvider;
import rx.functions.Action0;
import rx.functions.Action1;

public class AreaDefinitionMapActivity extends BaseActivity implements GeneralView, GoogleMap.OnMapLongClickListener, View.OnClickListener {

    public static final String EXTRA_REGION = "EXTRA_REGION";
    public static final String EXTRA_TYPE = "EXTRA_TYPE";
    public static final String TYPE_CREATE_NEW = "TYPE_CREATE_NEW";

    private static final String JSON = "application/json; charset=utf-8";

    public static String latUsk = "41.030091";
    public static String lonUsk = "29.033721";
//            , 29.033721

    @Inject
    LoadingManager loadingManager;
    @Inject
    ElevationHandler elevationHandler;
    @Inject
    GeneralPresenter generalPresenter;
    @InjectView(R.id.toolbar)
    Toolbar toolbar;
    @InjectView(R.id.dsb_radius)
    DiscreteSeekBar dsb_radius;
    @InjectView(R.id.et_area_name)
    EditText etAreaName;
    @InjectView(R.id.tv_area_save)
    TextView tvAreaSave;
    private Region region;
    private GoogleMap map;
    private MapAreaManager circleManager;
    private OkHttpClient client = new OkHttpClient();
    private Gson gson;
    private String typeGeofence;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.installCertificatePinning(client);
        initToolbar();
        startServiceCalls();

        ReactiveLocationProvider locationProvider = new ReactiveLocationProvider(this);
        locationProvider.getLastKnownLocation()
                .subscribe(new Action1<Location>() {
                    @Override
                    public void call(Location location) {
                        latUsk = String.valueOf(location.getLatitude());
                        lonUsk = String.valueOf(location.getLongitude());
                        goOnAfterLocation();
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        goOnAfterLocation();
                    }
                }, new Action0() {
                    @Override
                    public void call() {
                        goOnAfterLocation();
                    }
                });

    }

    private void goOnAfterLocation() {
        getIntentData();
        setupMap();
        setupUI();

        showInfoDialog();
    }

    private void showInfoDialog() {
        boolean isDontShow = getAppPreferences().getBoolean(PrefConstants.DONT_SHOW_AREA_DIALOG, false);
        if (isDontShow) {
            return;
        }
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Bilgilendirme");
        alertDialog.setMessage(getString(R.string.def_create_area_tutorial));

        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.layout_dont_show_again, null, false);
        View llShow = v.findViewById(R.id.ll_dont_show_again);
        final CheckBox checkBox = (CheckBox) v.findViewById(R.id.cb_dont_show);
        llShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkBox.performClick();
            }
        });
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        getAppPreferences().put(PrefConstants.DONT_SHOW_AREA_DIALOG, checkBox.isChecked());
                        dialog.dismiss();
                    }
                });
        alertDialog.setView(v);

        alertDialog.show();

    }

    private void setupMap() {
        map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(AveaConstants.TURKEY, 15));
        map.getUiSettings().setMapToolbarEnabled(false);
        map.getUiSettings().setMyLocationButtonEnabled(false);
        //TODO: değiştir bunu yener
        map.getUiSettings().setZoomControlsEnabled(true);
        map.setOnMapLongClickListener(this);
    }

    private void setupUI() {

        tvAreaSave.setOnClickListener(this);

        if (region == null) {
            return;
        }
        etAreaName.setText(region.name);

        LatLng location = new LatLng(Double.parseDouble(region.latitude), Double.parseDouble(region.longitude));

        map.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 15));

        circleManager =
                new MapAreaManager(map, 4, Color.RED, Color.HSVToColor(70, new float[]{1, 1, 200}), R.drawable.ic_alert, R.drawable.ic_alert,
                        0.5f, 0.5f, 0.5f, 0.5f, new MapAreaMeasure(Double.parseDouble(region.tolerance), MapAreaMeasure.Unit.meters),
                        new MapAreaManager.CircleManagerListener() {
                            @Override
                            public void onResizeCircleEnd(MapAreaWrapper draggableCircle) {
                                dsb_radius.setProgress((int) draggableCircle.getRadius());
                                draggableCircle.getRadius();
                            }

                            @Override
                            public void onCreateCircle(MapAreaWrapper draggableCircle) {
                            }

                            @Override
                            public void onMoveCircleEnd(MapAreaWrapper draggableCircle) {
                            }

                            @Override
                            public void onMoveCircleStart(MapAreaWrapper draggableCircle) {
                            }

                            @Override
                            public void onResizeCircleStart(MapAreaWrapper draggableCircle) {
                            }

                            @Override
                            public void onMinRadius(MapAreaWrapper draggableCircle) {

                            }

                            @Override
                            public void onMaxRadius(MapAreaWrapper draggableCircle) {
                            }
                        });

        circleManager.setMaxRadius(5000);
        circleManager.setMinRadius(50);

        dsb_radius.setProgress((int) Double.parseDouble(region.tolerance));

        circleManager.add(new MapAreaWrapper(map, location, Double.parseDouble(region.tolerance), 4, Color.RED,
                Color.HSVToColor(70, new float[]{1, 1, 200}), 50, 5000, R.drawable.ic_red_oval, R.drawable.ic_resize));


        dsb_radius.setOnProgressChangeListener(new DiscreteSeekBar.OnProgressChangeListener() {
            @Override
            public void onProgressChanged(DiscreteSeekBar discreteSeekBar, int value, boolean b) {
                circleManager.getCircles().get(0).setRadius(discreteSeekBar.getProgress());
            }

            @Override
            public void onStartTrackingTouch(DiscreteSeekBar discreteSeekBar) {

            }

            @Override
            public void onStopTrackingTouch(DiscreteSeekBar discreteSeekBar) {

            }
        });
    }

    private void getIntentData() {
        try {
            if (getIntent() != null && getIntent().getExtras() != null && getIntent().getExtras().containsKey(EXTRA_REGION)) {
                region = (Region) getIntent().getExtras().get(EXTRA_REGION);
            }
        } catch (Exception e) {

        }
        try {
            typeGeofence = getIntent().getExtras().getString(EXTRA_TYPE);
            if (typeGeofence.equals(TYPE_CREATE_NEW)) {
                region = new Region();
                region.latitude = latUsk;
                region.longitude = lonUsk;
                region.tolerance = "50.0";

            }
        } catch (Exception e) {
        }


    }

    private void initToolbar() {
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            toolbar.setTitle(R.string.activity_area_definations);
            elevationHandler.setDefaultElevation(toolbar);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View back) {
                    onBackPressed();
                }
            });
        }
    }

    private void startServiceCalls() {
        generalPresenter.getRegions();
    }

    @Override
    public int onCreateViewId() {
        return R.layout.activity_area_definition_map;
    }

    protected List<Object> getModules() {
        return Arrays.<Object>asList(new GeneralModule(this));
    }

    @Override
    protected void onPause() {
        super.onPause();
        generalPresenter.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        generalPresenter.onResume();
    }

    @Override
    public void generalResponse(GeneralEvent response) {

    }

    @Override
    public void onMapLongClick(LatLng latLng) {
        circleManager.getCircles().get(0).setCenter(latLng);
    }


    private void delete(String url) throws IOException {

        Request request = new Request.Builder()
                .url(url)
                .delete()
                .build();

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Request request, IOException e) {
                dismisLoadingBar();
            }

            @Override
            public void onResponse(Response response) throws IOException {
                dismisLoadingBar();
                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
                String resp = response.body().string();
                gson = new Gson();
                BaseResponse deleteResp = gson.fromJson(resp, BaseResponse.class);
                rpDelete(deleteResp);
            }
        });

        showLoading();
    }

    private void putArea(String url) throws IOException {

//        showLoading();

        RequestBody body = RequestBody.create(MediaType.parse(JSON), "");

//       RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .url(url)
                .put(body)
                .build();

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Request request, IOException e) {
                dismisLoadingBar();
            }

            @Override
            public void onResponse(Response response) throws IOException {
                dismisLoadingBar();
                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
                String resp = response.body().string();
                gson = new Gson();
                final BaseResponse putResp = gson.fromJson(resp, BaseResponse.class);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(AreaDefinitionMapActivity.this, putResp.errorDesc, Toast.LENGTH_LONG).show();
                        if (putResp.status.equals("0")) {
                            AreaDefinitionMapActivity.this.finish();
                        }
                        dismisLoadingBar();
                    }
                });

            }
        });
    }

    private void dismisLoadingBar() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                loadingManager.dismissLoading();
            }
        });
    }

    private void showLoading() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                loadingManager.showLoading(getString(R.string.please_wait));
            }
        });
    }


    private void rpDelete(BaseResponse deleteResp) {
        if (deleteResp.status.equals("0")) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    createArea();
                }
            });
        } else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(AreaDefinitionMapActivity.this, getString(R.string.error_area_def), Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    private void createArea() {
        MapAreaWrapper circle = circleManager.getCircles().get(0);
        String name = etAreaName.getText().toString().replace(" ", "");
        if (TextUtils.isEmpty(name)) {
            Toast.makeText(this, getString(R.string.please_def_area_name), Toast.LENGTH_LONG).show();
            loadingManager.dismissLoading();
            dismisLoadingBar();
            return;
        }
        PutAreaRequest putAreaRequest = new PutAreaRequest();

        putAreaRequest.regionName = etAreaName.getText().toString().replace(" ", "");
        putAreaRequest.regionName = putAreaRequest.regionName.trim();
        putAreaRequest.token = getAppPreferences().getString(PrefConstants.TOKEN, "");
        putAreaRequest.latitude = circle.getCenter().latitude;
        putAreaRequest.longitude = circle.getCenter().longitude;
        putAreaRequest.radius = circle.getRadius();

        String url = putAreaRequest.toString();
        try {
            putArea(url);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        loadingManager.showLoading(getString(R.string.please_wait));
        String name = etAreaName.getText().toString();
        if (TextUtils.isEmpty(name)) {
            Toast.makeText(this, getString(R.string.please_def_area_name), Toast.LENGTH_LONG).show();
            loadingManager.dismissLoading();
            dismisLoadingBar();
            return;
        }
        if (typeGeofence != null && typeGeofence.equals(TYPE_CREATE_NEW)) {
            createArea();
            return;
        }
        DeleteAreaRequest req = new DeleteAreaRequest();
        req.token = getAppPreferences().getString(PrefConstants.TOKEN, ""); //TODO
        req.regionId = region.id;

        try {
            delete(req.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}