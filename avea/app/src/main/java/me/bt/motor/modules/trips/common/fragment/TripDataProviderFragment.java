package me.bt.motor.modules.trips.common.fragment;

import android.app.Fragment;
import android.os.Bundle;

import java.util.List;

import me.bt.avea.domain.entities.IntervalData;
import me.bt.motor.modules.trips.common.data.AbstractExpandableDataProvider;
import me.bt.motor.modules.trips.common.data.TripExpandableDataProvider;

public class TripDataProviderFragment extends Fragment {
    private TripExpandableDataProvider mDataProvider;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);
        mDataProvider = new TripExpandableDataProvider(null);
    }

    public AbstractExpandableDataProvider getDataProvider() {
        return mDataProvider;
    }

    public void updateTrips(List<IntervalData> trips) {
        mDataProvider = new TripExpandableDataProvider(trips);
    }
}
