package me.bt.motor.modules.newmobile;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import me.bt.avea.domain.interactions.adduser.AddUserInteractor;
import me.bt.avea.domain.interactions.register.RegisterInteractor;
import me.bt.avea.presentation.login.LoginPresenter;
import me.bt.avea.presentation.login.LoginView;
import me.bt.avea.presentation.newmobile.NewMobilePresenter;
import me.bt.avea.presentation.newmobile.NewMobileView;
import me.bt.base.di.ActivityModule;
import me.bt.base.domain.abstractions.Bus;
import me.bt.base.domain.interactors.InteractorInvoker;

@Module(
        addsTo = ActivityModule.class,
        library = true,
        complete = false,
        injects = NewMobileActivity.class)
public class NewMobileModule {

    private LoginView loginView;
    private NewMobileView newUserView;

    public NewMobileModule(NewMobileView newUserView, LoginView loginView) {
        this.newUserView = newUserView;
        this.loginView = loginView;
    }

    @Provides
    @Singleton
    NewMobilePresenter provideNewMobilePresenter(Bus bus, InteractorInvoker interactorInvoker,
                                                 AddUserInteractor addUserInteractor) {
        return new NewMobilePresenter(bus, interactorInvoker, newUserView, addUserInteractor);
    }

    @Provides
    @Singleton
    LoginPresenter provideLoginPresenter(Bus bus, InteractorInvoker interactorInvoker,
                                         RegisterInteractor registerInteractor) {
        return new LoginPresenter(bus, interactorInvoker, loginView, registerInteractor);
    }
}

