package me.bt.motor.ui.handlers;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import me.bt.motor.modules.map.MobileMapActivity;
import me.bt.motor.modules.notifications.NotificationsActivity;
import me.bt.motor.modules.performance.PerformanceActivity;
import me.bt.motor.modules.route.RouteActivity;
import me.bt.motor.modules.settings.SettingsActivity;
import me.bt.motor.modules.trips.TripsActivity;
import me.bt.motor.R;
import me.bt.base.ui.BaseActivity;

public class NavbarHandler {
  private final int ACTIVITY_MAP = 0;
  private final int ACTIVITY_TRIPS = 1;
  private final int ACTIVITY_ROUTE = 2;
  private final int ACTIVITY_PERORMANCE = 3;
  private final int ACTIVITY_SETTINGS = 4;
  private AppCompatActivity activity;
  private LinearLayout ll_nav_map;
  private LinearLayout ll_nav_trips;
  private LinearLayout ll_nav_route;
  private LinearLayout ll_nav_performance;
  private LinearLayout ll_nav_settings;
  private TextView tv_nav_map;
  private TextView tv_nav_performance;
  private TextView tv_nav_route;
  private TextView tv_nav_settings;
  private TextView tv_nav_trips;
  private ImageView iv_nav_map;
  private ImageView iv_nav_performance;
  private ImageView iv_nav_route;
  private ImageView iv_nav_settings;
  private ImageView iv_nav_trips;

  private int defaultColor;
  private int selectedColor;
  private int currentActivity = -1;

  public void setNavbar(View navbar, BaseActivity activity) {
    this.activity = activity;
    Class activityClass = activity.getClass();
    if (activityClass.equals(MobileMapActivity.class) || activityClass.equals(NotificationsActivity.class)) {
      currentActivity = ACTIVITY_MAP;
    } else if (activityClass.equals(SettingsActivity.class)) {
      currentActivity = ACTIVITY_SETTINGS;
    } else if (activityClass.equals(RouteActivity.class)) {
      currentActivity = ACTIVITY_ROUTE;
    } else if (activityClass.equals(TripsActivity.class)) {
      currentActivity = ACTIVITY_TRIPS;
    } else if (activityClass.equals(PerformanceActivity.class)) {
      currentActivity = ACTIVITY_PERORMANCE;
    }
    setupNavbar(navbar, currentActivity);
  }

  private void setupNavbar(View navbar, int currentActivity) {
    init();
    getNavbarViews(navbar);
    setDefaults();
    setupClickListeners();
    markSelectedTab(currentActivity);
  }

  private void markSelectedTab(int currentActivity) {
    switch (currentActivity) {
      case ACTIVITY_MAP:
        setImageResource(iv_nav_map, R.drawable.ic_map_active);
        setTextViewColor(tv_nav_map, selectedColor);
        break;
      case ACTIVITY_PERORMANCE:
        setImageResource(iv_nav_performance, R.drawable.ic_map_active);
        setTextViewColor(tv_nav_performance, selectedColor);
        break;
      case ACTIVITY_TRIPS:
        setImageResource(iv_nav_trips, R.drawable.ic_trips_active);
        setTextViewColor(tv_nav_trips, selectedColor);
        break;
      case ACTIVITY_SETTINGS:
        setImageResource(iv_nav_settings, R.drawable.ic_setings_active);
        setTextViewColor(tv_nav_settings, selectedColor);
        break;
      case ACTIVITY_ROUTE:
        setImageResource(iv_nav_route, R.drawable.ic_route_active);
        setTextViewColor(tv_nav_route, selectedColor);
        break;
    }
  }

  private void init() {
    defaultColor = activity.getResources().getColor(R.color.text_primary_dark);
    selectedColor = activity.getResources().getColor(R.color.text_accent);
  }

  private void setupClickListeners() {
    if (ll_nav_map != null) {
      ll_nav_map.setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View v) {
          if (currentActivity != ACTIVITY_MAP) {
            nextActivity(new Intent(activity, MobileMapActivity.class));
          }
        }
      });
    }

    if (ll_nav_performance != null) {
      ll_nav_performance.setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View v) {
          if (currentActivity != ACTIVITY_PERORMANCE) {
            nextActivity(new Intent(activity, PerformanceActivity.class));
          }
        }
      });
    }

    if (ll_nav_trips != null) {
      ll_nav_trips.setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View v) {
          if (currentActivity != ACTIVITY_TRIPS) {
            nextActivity(new Intent(activity, TripsActivity.class));
          }
        }
      });
    }

    if (ll_nav_route != null) {
      ll_nav_route.setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View v) {
          if (currentActivity != ACTIVITY_ROUTE) {
            nextActivity(new Intent(activity, RouteActivity.class));
          }
        }
      });
    }

    if (ll_nav_settings != null) {
      ll_nav_settings.setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View v) {
          if (currentActivity != ACTIVITY_SETTINGS) {
            nextActivity(new Intent(activity, SettingsActivity.class));
          }
        }
      });
    }
  }

  private void nextActivity(Intent intent) {
    activity.startActivity(intent);
    activity.finish();
  }

  private void setDefaults() {
    setTextViewColor(tv_nav_map, defaultColor);
    setTextViewColor(tv_nav_performance, defaultColor);
    setTextViewColor(tv_nav_route, defaultColor);
    setTextViewColor(tv_nav_settings, defaultColor);
    setTextViewColor(tv_nav_trips, defaultColor);
    setImageResource(iv_nav_map, R.drawable.ic_map);
    setImageResource(iv_nav_performance, R.drawable.ic_performance);
    setImageResource(iv_nav_settings, R.drawable.ic_settings);
    setImageResource(iv_nav_trips, R.drawable.ic_trips);
    setImageResource(iv_nav_route, R.drawable.ic_route);
  }

  private void setImageResource(ImageView imageView, int resourceId) {
    if (imageView == null) {
      return;
    }
    imageView.setImageResource(resourceId);
  }

  private void setTextViewColor(TextView textView, int defaultColor) {
    if (textView == null) {
      return;
    }
    textView.setTextColor(defaultColor);
  }

  private void getNavbarViews(View navbar) {
    if (navbar == null) {
      return;
    }
    ll_nav_map = (LinearLayout) navbar.findViewById(R.id.ll_nav_map);
    ll_nav_performance = (LinearLayout) navbar.findViewById(R.id.ll_nav_performance);
    ll_nav_route = (LinearLayout) navbar.findViewById(R.id.ll_nav_route);
    ll_nav_settings = (LinearLayout) navbar.findViewById(R.id.ll_nav_settings);
    ll_nav_trips = (LinearLayout) navbar.findViewById(R.id.ll_nav_trips);
    tv_nav_map = (TextView) navbar.findViewById(R.id.tv_nav_map);
    tv_nav_performance = (TextView) navbar.findViewById(R.id.tv_nav_performance);
    tv_nav_route = (TextView) navbar.findViewById(R.id.tv_nav_route);
    tv_nav_settings = (TextView) navbar.findViewById(R.id.tv_nav_settings);
    tv_nav_trips = (TextView) navbar.findViewById(R.id.tv_nav_trips);
    iv_nav_map = (ImageView) navbar.findViewById(R.id.iv_nav_map);
    iv_nav_performance = (ImageView) navbar.findViewById(R.id.iv_nav_performance);
    iv_nav_route = (ImageView) navbar.findViewById(R.id.iv_nav_route);
    iv_nav_settings = (ImageView) navbar.findViewById(R.id.iv_nav_settings);
    iv_nav_trips = (ImageView) navbar.findViewById(R.id.iv_nav_trips);
  }
}
