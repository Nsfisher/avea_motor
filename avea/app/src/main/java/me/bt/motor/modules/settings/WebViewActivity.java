package me.bt.motor.modules.settings;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.webkit.WebView;

import me.bt.motor.R;

/**
 * Created by yenerciftci on 31/08/15.
 */
public class WebViewActivity extends Activity {
    public static final String EXTRA_URL = "EXTRA_URL";

    private WebView mWebView;
    private Object extras;
    private String mUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        mWebView = (WebView) findViewById(R.id.web_view);
        getBundleExtra();
        mWebView.loadUrl(mUrl);
        if (TextUtils.isEmpty(mUrl)) {
            finish();
        }

    }


    public void getBundleExtra() {
        if (getIntent() != null && getIntent().getExtras() != null && getIntent().getExtras().containsKey(EXTRA_URL)) {
            mUrl = getIntent().getExtras().getString(EXTRA_URL);
        }
    }
}
