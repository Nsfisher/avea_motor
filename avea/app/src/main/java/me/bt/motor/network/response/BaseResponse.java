package me.bt.motor.network.response;

/**
 * Created by yenerciftci on 28/07/15.
 */
public class BaseResponse {
    public String data;
    public String errorDesc;
    public String status;
}
