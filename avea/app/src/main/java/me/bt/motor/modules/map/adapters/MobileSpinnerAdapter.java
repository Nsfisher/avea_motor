package me.bt.motor.modules.map.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import java.util.List;

import me.bt.avea.presentation.model.PresentationMobile;
import me.bt.motor.R;

public class MobileSpinnerAdapter extends BaseAdapter implements SpinnerAdapter {

    Context context;

    public List<PresentationMobile> getData() {
        return data;
    }

    public void setData(List<PresentationMobile> data) {
        this.data = data;
    }

    private List<PresentationMobile> data;

    public MobileSpinnerAdapter(Context context, List<PresentationMobile> data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public PresentationMobile getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    static class ViewHolder {
        TextView tv_name;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.row_mobile_spinner, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.tv_name = (TextView) convertView.findViewById(R.id.tv_name);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        if (data.get(position).getAlias() != null) {
            viewHolder.tv_name.setText(data.get(position).getAlias());
        }
        return convertView;
    }
}