package me.bt.motor.modules.notifications.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.view.ViewGroup;

import com.carlosdelachica.easyrecycleradapters.adapter.BaseEasyViewHolderFactory;
import com.carlosdelachica.easyrecycleradapters.adapter.EasyViewHolder;

import me.bt.motor.ui.items.LightEventViewHolder;

public class LightEventViewHolderFactory extends BaseEasyViewHolderFactory {

    private final Resources resources;

    public LightEventViewHolderFactory(Context context, Resources resources) {
        super(context);
        this.resources = resources;
    }

    @Override
    public EasyViewHolder create(Class valueClass, ViewGroup parent) {
        return new LightEventViewHolder(context, parent, resources);
    }
}

