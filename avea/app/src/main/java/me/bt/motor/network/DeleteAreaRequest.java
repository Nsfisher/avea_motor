package me.bt.motor.network;

import me.bt.base.util.Constants;

/**
 * Created by yenerciftci on 28/07/15.
 */
public class DeleteAreaRequest extends BaseRequest {
    public String regionId;

    @Override
    public String toString() {
        return Constants.APP_URL + CUSTOMER_BASE + "token=" + token + "&" + "regionId=" + regionId;
    }
}
