package me.bt.motor.network.response;

/**
 * Created by yenerciftci on 20/08/15.
 */
public class CheckAppVersionResponse extends BaseResponse {

    public String andoridUrl;
    public String androidVer;
    public String changeDesc;
    public String iosUrl;
    public String iosVer;
}
