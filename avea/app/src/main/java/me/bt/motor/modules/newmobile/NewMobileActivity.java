package me.bt.motor.modules.newmobile;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.gson.Gson;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import com.weiwangcn.betterspinner.library.BetterSpinner;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import butterknife.InjectView;
import butterknife.OnClick;
import me.bt.avea.data.preferences.PrefConstants;
import me.bt.avea.domain.entities.Brand;
import me.bt.avea.domain.entities.BrandList;
import me.bt.avea.domain.entities.BrandModel;
import me.bt.avea.domain.entities.BrandModelList;
import me.bt.avea.domain.entities.FuelType;
import me.bt.avea.domain.entities.FuelTypeList;
import me.bt.avea.domain.entities.ModelSeries;
import me.bt.avea.domain.entities.ModelSeriesList;
import me.bt.avea.domain.interactions.general.GeneralInteractor;
import me.bt.avea.domain.interactions.general.events.GeneralEvent;
import me.bt.avea.presentation.general.GeneralPresenter;
import me.bt.avea.presentation.general.GeneralView;
import me.bt.avea.presentation.login.LoginPresenter;
import me.bt.avea.presentation.login.LoginView;
import me.bt.avea.presentation.newmobile.NewMobilePresenter;
import me.bt.avea.presentation.newmobile.NewMobileView;
import me.bt.motor.R;
import me.bt.base.ui.BaseActivity;
import me.bt.base.ui.elevation.ElevationHandler;
import me.bt.base.ui.errors.ErrorManager;
import me.bt.base.ui.loading.LoadingManager;
import me.bt.motor.modules.general.GeneralModule;
import me.bt.motor.modules.map.MobileMapActivity;
import me.bt.motor.network.AddUserRequest;
import me.bt.motor.network.BrandRequest;
import me.bt.motor.network.ModelRequest;
import me.bt.motor.network.ModelSeriesRequest;
import me.bt.motor.network.response.BaseResponse;
import me.bt.motor.util.Utils;

public class NewMobileActivity extends BaseActivity implements NewMobileView, GeneralView, LoginView {

    public static final String EXTRA_PASSWORD = "EXTRA_PASSWORD";
    public static final String EXTRA_USERNAME = "EXTRA_USERNAME";
    public static final String EXTRA_ACTIVATION = "EXTRA_ACTIVATION";
    public static final String EXTRA_PHONE = "EXTRA_PHONE";

    @Inject
    NewMobilePresenter presenter;
    @Inject
    GeneralPresenter generalPresenter;
    @Inject
    ErrorManager errorManager;
    @Inject
    ElevationHandler elevationHandler;
    @Inject
    LoadingManager loadingManager;
    @InjectView(R.id.et_mobile)
    EditText et_mobile;
    @InjectView(R.id.et_nickname)
    EditText et_nickname;
    @InjectView(R.id.et_activation)
    EditText et_activation;

    @Inject
    LoginPresenter loginPresenter;

    @InjectView(R.id.spn_model)
    BetterSpinner spn_model;
    @InjectView(R.id.spn_brand)
    BetterSpinner spn_brand;
    @InjectView(R.id.spn_engine_info)
    BetterSpinner spn_engine_info;
    @InjectView(R.id.spn_fuel_type)
    BetterSpinner spn_fuel_type;
    @InjectView(R.id.spn_series)
    BetterSpinner spn_series;
    @InjectView(R.id.toolbar)
    Toolbar toolbar;

    private String activation = "";
    private String userName = "";
    private String password = "";
    private OkHttpClient client;
    private Gson gson;
    private ModelSeriesList modelSeries;
    private BrandModelList modelList;
    private FuelTypeList fuelTypeList;
    private BrandList brandList;
    private FuelType fuelType;
    private Brand brand;
    private ModelSeries modelSerie;
    public static String usernameStr, passwordStr, activationStr, phoneNumberStr;
    private String phoneNumber;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        client = new OkHttpClient();
        Utils.installCertificatePinning(client);
        getExtras();
        initToolbar();
        startServiceCalls();
        setUi(-1);

        try {
            getBrand();
        } catch (IOException e) {
            dismisLoadingBar();
            e.printStackTrace();
        }

        if (TextUtils.isEmpty(activation)) {
            et_activation.setVisibility(View.VISIBLE);
        } else {
            et_activation.setVisibility(View.GONE);
        }


    }

    private void setUi(int index) {
        ArrayList<String> list = new ArrayList<>();
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, list);
        list.add(getString(R.string.brand_hint));
        if (index < 1) {
            spn_brand.setAdapter(adapter);
        }

        list = new ArrayList<>();
        list.add(getString(R.string.fuel_type_hint));
        adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, list);

        if (index == -1) {
            spn_fuel_type.setAdapter(adapter);
        }

        list = new ArrayList<>();
        list.add(getString(R.string.model_hint));
        adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, list);
        if (index < 3) {
            spn_model.setAdapter(adapter);
            spn_model.setHint(getString(R.string.model));
            spn_model.setText("");
        }


        list = new ArrayList<>();
        list.add(getString(R.string.engine_info_hint));
        adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, list);
        if (index < 4) {
            spn_engine_info.setAdapter(adapter);
            spn_engine_info.setHint(getString(R.string.engine_info_hint));
            spn_engine_info.setText("");
        }


        list = new ArrayList<>();
        list.add(getString(R.string.engine_info_hint));
        adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, list);
        if (index < 5) {
            spn_series.setAdapter(adapter);
            spn_series.setHint(getString(R.string.series_hint));
            spn_series.setText("");
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        generalPresenter.onPause();
        presenter.onPause();
        loginPresenter.onPause();
    }

    @Override
    public int onCreateViewId() {
        return R.layout.activity_new_mobile;
    }

    protected List<Object> getModules() {
        return Arrays.<Object>asList(new NewMobileModule(this, this), new GeneralModule(this));
    }

    private void startServiceCalls() {
        generalPresenter.getFuelTypes();
    }

    private void initToolbar() {
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            elevationHandler.setDefaultElevation(toolbar);
            getSupportActionBar().setTitle(R.string.activity_new_mobile);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View back) {
                    onBackPressed();
                }
            });
        }
    }

    private void getExtras() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(EXTRA_ACTIVATION)) {
                activation = bundle.getString(EXTRA_ACTIVATION);
            }
            if (bundle.containsKey(EXTRA_PASSWORD)) {
                password = bundle.getString(EXTRA_PASSWORD);
            }
            if (bundle.containsKey(EXTRA_USERNAME)) {
                userName = bundle.getString(EXTRA_USERNAME);
            }

            if (bundle.containsKey(EXTRA_PHONE)) {
                phoneNumber = bundle.getString(EXTRA_PHONE);
            }
        }
    }

    @OnClick(R.id.btn_add)
    void onAddClicked() {
        AddUserRequest addUserRequest = new AddUserRequest();
        if (!TextUtils.isEmpty(password)) {

        }
        String nick = et_nickname.getText().toString();
        String mobile = et_mobile.getText().toString();
        if (TextUtils.isEmpty(nick) || TextUtils.isEmpty(mobile)) {
            Toast.makeText(this, "Zorunlu alanları doldurunuz", Toast.LENGTH_LONG).show();
            return;
        }
        if (TextUtils.isEmpty(activation)) {
            activation = et_activation.getText().toString();
        }

        if (TextUtils.isEmpty(activation)) {
            errorManager.showError("Lütfen aktivasyon kodunu giriniz");
        }

        addUserRequest.token = getAppPreferences().getString(PrefConstants.TOKEN, "");
        addUserRequest.plaka = et_mobile.getText().toString();
        addUserRequest.takmaIsim = et_nickname.getText().toString();
        addUserRequest.marka = spn_brand.getText().toString();
        addUserRequest.model = spn_model.getText().toString();
        addUserRequest.yakitTipi = spn_fuel_type.getText().toString();
        addUserRequest.motorBilgisi = spn_series.getText().toString();
        addUserRequest.activation = activation;
        addUserRequest.phone = phoneNumber;
        if (TextUtils.isEmpty(userName)) {
            addUserRequest.username = getAppPreferences().getString(PrefConstants.U_NAME, "");
            addUserRequest.password = getAppPreferences().getString(PrefConstants.P_WORD, "");
            addUserRequest.queryMode = "1";
        } else {
            addUserRequest.username = userName;
            addUserRequest.password = password;
            addUserRequest.queryMode = "0";
        }

        try {
            srPutNewMobile(addUserRequest.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void srPutNewMobile(String url) throws IOException {

        showLoading();

        RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), "");
        URI uri = null;
        try {
            uri = new URI(url);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        Request request = new Request.Builder()
                .url(uri.toASCIIString())
                .put(body)
                .build();

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Request request, IOException e) {
                dismisLoadingBar();
            }

            @Override
            public void onResponse(Response response) throws IOException {
                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
                String resp = response.body().string();
                gson = new Gson();
                final BaseResponse putResp = gson.fromJson(resp, BaseResponse.class);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Toast.makeText(NewMobileActivity.this, putResp.errorDesc, Toast.LENGTH_LONG).show();
                            if (putResp == null) {
                                errorManager.showError(getString(R.string.error));
                                return;
                            }
                        } catch (Exception e) {

                        }

                        if (putResp.status.equals("0")) {
                            if (!TextUtils.isEmpty(password)) {
                                loginPresenter.login(userName, password);
                                loadingManager.showLoading(getString(R.string.please_wait), true);
                            } else {
                                NewMobileActivity.this.finish();
                            }

                        }
                        dismisLoadingBar();
                    }
                });

            }
        });
    }

    private boolean isInputValid() {
        if (et_mobile.getText().toString().length() == 0) {
            errorManager.showError(R.string.error_empty_mobile);
            return false;
        } else {
            return true;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        generalPresenter.onResume();
        presenter.onResume();
        loginPresenter.onResume();
    }

    @Override
    public void showError(String errorDescription) {
        loadingManager.dismissLoading();
        errorManager.showError(errorDescription);
    }

    @Override
    public void addUserComplete() {
        loadingManager.dismissLoading();
    }

    @Override
    public void addUserError() {
        loadingManager.dismissLoading();
        errorManager.showError(R.string.error_generic);
    }

    @Override
    public void generalResponse(GeneralEvent response) {
        if (response.getRequestType() == GeneralInteractor.REQUEST_GET_FUEL_TYPES) {
            if (response.getError() == null) {
                FuelTypeList fuelTypeList = (FuelTypeList) response.getData();
                dismisLoadingBar();
                rpFuelType(fuelTypeList);
            }
        } else if (response.getRequestType() == GeneralInteractor.REQUEST_GET_BRANDS) {
            if (response.getError() == null) {
                setUi(0);
                BrandList brandList = (BrandList) response.getData();
                String tes = "a";
                String[] brands = new String[brandList.brand.size()];
                for (int i = 0; i < brandList.brand.size(); i++) {
                    brands[i] = brandList.brand.get(i).name;
                }

                ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                        android.R.layout.simple_dropdown_item_1line, brands);

                spn_brand.setAdapter(adapter);

            }
        }
    }


    public AdapterView.OnItemClickListener brandItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            try {
                brand = brandList.brand.get(position);
                getModel(brand.id);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    };


    public AdapterView.OnItemClickListener seriesItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            try {
                modelSerie = modelSeries.modelSeries.get(position);
            } catch (Exception e) {

            }

        }
    };


    public AdapterView.OnItemClickListener fueltypeClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            fuelType = fuelTypeList.fuelTypes.get(position);
        }
    };

    public AdapterView.OnItemClickListener modelClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            try {
                BrandModel m = modelList.model.get(position);
                try {
                    getSeries(m.id);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {

            }

        }
    };

    private void rpFuelType(FuelTypeList fuelTypeList) {
//        String tes = "a";
        this.fuelTypeList = fuelTypeList;
        String[] brands = new String[fuelTypeList.fuelTypes.size()];
        for (int i = 0; i < fuelTypeList.fuelTypes.size(); i++) {
            brands[i] = fuelTypeList.fuelTypes.get(i).fuelType;
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, brands);

        spn_fuel_type.setAdapter(adapter);
        spn_fuel_type.setOnItemClickListener(fueltypeClickListener);
    }

    private void getSeries(String modelId) throws IOException {

        showLoading();

        ModelSeriesRequest req = new ModelSeriesRequest();
        req.token = getAppPreferences().getString(PrefConstants.TOKEN, "");
        req.modelId = modelId;

        Request request = new Request.Builder()
                .url(req.toString())
                .get()
                .build();

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Request request, IOException e) {
                dismisLoadingBar();
            }

            @Override
            public void onResponse(Response response) throws IOException {
                dismisLoadingBar();
                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
                String resp = response.body().string();
                gson = new Gson();
                final ModelSeriesList series = gson.fromJson(resp, ModelSeriesList.class);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        setUi(3);
                        rpSeries(series);
                    }
                });

            }
        });
    }


    private void getModel(String brandId) throws IOException {

        showLoading();

        ModelRequest req = new ModelRequest();
        req.token = getAppPreferences().getString(PrefConstants.TOKEN, "");
        req.markaId = brandId;

        Request request = new Request.Builder()
                .url(req.toString())
                .get()
                .build();


        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Request request, IOException e) {
                dismisLoadingBar();
            }

            @Override
            public void onResponse(Response response) throws IOException {
                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
                String resp = response.body().string();
                gson = new Gson();
                final BrandModelList putResp = gson.fromJson(resp, BrandModelList.class);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        setUi(2);
                        rpModel(putResp);
                        dismisLoadingBar();
                    }
                });

            }
        });
    }


    private void getBrand() throws IOException {

        showLoading();

        BrandRequest req = new BrandRequest();
        req.token = getAppPreferences().getString(PrefConstants.TOKEN, "");

        Request request = new Request.Builder()
                .url(req.toString())
                .get()
                .build();

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Request request, IOException e) {
                dismisLoadingBar();
            }

            @Override
            public void onResponse(Response response) throws IOException {
                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
                String resp = response.body().string();
                gson = new Gson();
                final BrandList putResp = gson.fromJson(resp, BrandList.class);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dismisLoadingBar();
                        rpBrand(putResp);
                    }
                });

            }
        });
    }

    private void rpBrand(BrandList brandResp) {
        try {
            brandList = brandResp;

            ArrayList<String> list = new ArrayList<>();
            for (Brand b : brandList.brand) list.add(b.name);

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_dropdown_item_1line, list);
            spn_brand.setAdapter(adapter);
            spn_brand.setOnItemClickListener(brandItemClickListener);

        } catch (Exception e) {
            e.printStackTrace();
            dismisLoadingBar();
        }
    }

    private void rpModel(BrandModelList brandModelList) {
        this.modelList = brandModelList;
        try {
            ArrayList<String> list = new ArrayList<>();
            for (BrandModel b : brandModelList.model) list.add(b.name);

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_dropdown_item_1line, list);
            spn_model.setAdapter(adapter);
            spn_model.setOnItemClickListener(modelClickListener);

        } catch (Exception e) {
            e.printStackTrace();
            dismisLoadingBar();
        }
    }

    private void rpSeries(ModelSeriesList series) {
        modelSeries = series;

        try {
            ArrayList<String> list = new ArrayList<>();
            for (ModelSeries b : series.modelSeries) list.add(b.modelSeries);

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_dropdown_item_1line, list);
            spn_series.setAdapter(adapter);
            spn_series.setOnItemClickListener(seriesItemClickListener);

        } catch (Exception e) {
            e.printStackTrace();
            dismisLoadingBar();
        }

    }


    private void showLoading() {
        loadingManager.dismissLoading();
        loadingManager.showLoading(getString(R.string.please_wait));
    }

    private void dismisLoadingBar() {
        loadingManager.dismissLoading();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (!TextUtils.isEmpty(password)) {
            usernameStr = userName;
            passwordStr = password;
            activationStr = activation;
            phoneNumberStr = phoneNumber;
        } else {
            usernameStr = "";
            passwordStr = "";
            activationStr = "";
            phoneNumberStr = "";
        }
    }

    @Override
    public void loginComplete() {
        loadingManager.dismissLoading();
        String saveUsername = getAppPreferences().getString(PrefConstants.U_NAME, "");
        String currentUsername = userName;
        if (!saveUsername.equals(currentUsername)) {
            getAppPreferences().put(PrefConstants.DEFAULT_SAVED_ID, "");
            getAppPreferences().put(PrefConstants.DEFAULT_MOBILE_ALIAS, "");
        }
        getAppPreferences().put(PrefConstants.U_NAME, userName);
        getAppPreferences().put(PrefConstants.P_WORD, password);


        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());
        if (status == ConnectionResult.SUCCESS) {
            startActivity(new Intent(NewMobileActivity.this, MobileMapActivity.class));
            finish();
        } else if (status == ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED) {
            Toast.makeText(this, "please udpate your google play service", Toast.LENGTH_SHORT).show();
        } else if (status == ConnectionResult.API_UNAVAILABLE) {
            Toast.makeText(this, "please install your google play service", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "please install your google play service 2", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void loginError(final String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                loadingManager.dismissLoading();
                errorManager.showError(message);
            }
        });

    }
}

