package me.bt.motor.modules.adduser;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import me.bt.avea.domain.interactions.general.GeneralInteractor;
import me.bt.avea.domain.interactions.register.RegisterInteractor;
import me.bt.avea.presentation.general.GeneralPresenter;
import me.bt.avea.presentation.general.GeneralView;
import me.bt.avea.presentation.login.LoginPresenter;
import me.bt.avea.presentation.login.LoginView;
import me.bt.base.di.ActivityModule;
import me.bt.base.domain.abstractions.Bus;
import me.bt.base.domain.interactors.InteractorInvoker;

@Module(
        addsTo = ActivityModule.class,
        library = true,
        injects = AddUserActivity.class)
public class AddUserModule {

    private GeneralView generalView;
    private LoginView loginView;

    public AddUserModule(GeneralView generalView, LoginView loginView) {
        this.generalView = generalView;
        this.loginView = loginView;
    }

    @Provides
    @Singleton
    GeneralPresenter provideGeneralPresenter(Bus bus, InteractorInvoker interactorInvoker,
                                             GeneralInteractor generalInteractor) {
        return new GeneralPresenter(bus, interactorInvoker, generalView, generalInteractor);
    }


    @Provides
    @Singleton
    LoginPresenter provideLoginPresenter(Bus bus, InteractorInvoker interactorInvoker,
                                         RegisterInteractor registerInteractor) {
        return new LoginPresenter(bus, interactorInvoker, loginView, registerInteractor);
    }
}