package me.bt.motor.modules.login;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import me.bt.avea.domain.interactions.register.RegisterInteractor;
import me.bt.avea.presentation.login.LoginPresenter;
import me.bt.avea.presentation.login.LoginView;
import me.bt.base.di.ActivityModule;
import me.bt.base.domain.abstractions.Bus;
import me.bt.base.domain.interactors.InteractorInvoker;

@Module(
        addsTo = ActivityModule.class,
        library = true,
        complete = false,
        injects = {LoginActivity.class, ForgetPasswordActivity.class})
public class LoginModule {
    private LoginView loginView;

    public LoginModule(LoginView loginView) {
        this.loginView = loginView;
    }

    @Provides
    @Singleton
    LoginPresenter provideLoginPresenter(Bus bus, InteractorInvoker interactorInvoker,
                                         RegisterInteractor registerInteractor) {
        return new LoginPresenter(bus, interactorInvoker, loginView, registerInteractor);
    }
}
