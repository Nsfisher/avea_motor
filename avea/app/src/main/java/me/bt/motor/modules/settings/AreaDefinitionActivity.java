package me.bt.motor.modules.settings;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.carlosdelachica.easyrecycleradapters.adapter.EasyRecyclerAdapter;
import com.carlosdelachica.easyrecycleradapters.adapter.EasyViewHolder;
import com.carlosdelachica.easyrecycleradapters.recycler_view_manager.EasyRecyclerViewManager;
import com.software.shell.fab.ActionButton;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import butterknife.InjectView;
import me.bt.avea.domain.entities.Region;
import me.bt.avea.domain.entities.Regions;
import me.bt.avea.domain.interactions.general.GeneralInteractor;
import me.bt.avea.domain.interactions.general.events.GeneralEvent;
import me.bt.motor.modules.general.GeneralModule;
import me.bt.motor.modules.settings.adapters.AreaDefinitionViewHolderFactory;
import me.bt.avea.presentation.general.GeneralPresenter;
import me.bt.avea.presentation.general.GeneralView;
import me.bt.motor.ui.items.AreaDefinationViewHolder;
import me.bt.motor.R;
import me.bt.base.ui.BaseActivity;
import me.bt.base.ui.elevation.ElevationHandler;
import me.bt.base.ui.loading.LoadingManager;

public class AreaDefinitionActivity extends BaseActivity implements GeneralView, EasyViewHolder.OnItemClickListener {

    @Inject
    LoadingManager loadingManager;
    @Inject
    ElevationHandler elevationHandler;
    @Inject
    GeneralPresenter generalPresenter;
    @InjectView(R.id.toolbar)
    Toolbar toolbar;
    @InjectView(R.id.fab_add)
    ActionButton fab_add;
    @InjectView(R.id.rv_area)
    RecyclerView rv_area;
    private EasyRecyclerAdapter adapter;
    private EasyRecyclerViewManager recyclerViewManager;
    private List<Region> regions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initToolbar();
        startServiceCalls();
        setupRecycleView();
        startServiceCalls();
        setupActionButton();
    }

    private void setupActionButton() {
        fab_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AreaDefinitionActivity.this, AreaDefinitionMapActivity.class);
                intent.putExtra(AreaDefinitionMapActivity.EXTRA_TYPE, AreaDefinitionMapActivity.TYPE_CREATE_NEW);
                startActivity(intent);
            }
        });
    }

    private void setupRecycleView() {
        AreaDefinitionViewHolderFactory areaDefinitionViewHolderFactory = new AreaDefinitionViewHolderFactory(this, getResources());
        adapter = new EasyRecyclerAdapter(areaDefinitionViewHolderFactory, Region.class, AreaDefinationViewHolder.class);
        recyclerViewManager = new EasyRecyclerViewManager.Builder(rv_area, adapter).loadingListTextColor(android.R.color.black)
                .divider(R.drawable.list_divider)
                .clickListener(this)
                .build();
    }

    private void initToolbar() {
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            toolbar.setTitle(R.string.activity_area_definations);
            elevationHandler.setDefaultElevation(toolbar);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View back) {
                    onBackPressed();
                }
            });
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        startServiceCalls();
    }

    private void startServiceCalls() {
        generalPresenter.getRegions();
    }

    @Override
    public int onCreateViewId() {
        return R.layout.activity_area_definition;
    }

    protected List<Object> getModules() {
        return Arrays.<Object>asList(new SettingsModule(), new GeneralModule(this));
    }

    @Override
    protected void onPause() {
        super.onPause();
        generalPresenter.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        generalPresenter.onResume();
    }

    @Override
    public void onItemClick(int position, View view) {
        if (regions != null && regions.size() > position) {
            Intent intent = new Intent(this, AreaDefinitionMapActivity.class);
            intent.putExtra(AreaDefinitionMapActivity.EXTRA_REGION, regions.get(position));
            startActivity(intent);
        }
    }

    @Override
    public void generalResponse(GeneralEvent response) {
        if (response.getRequestType() == GeneralInteractor.REQUEST_GET_REGIONS) {
            Regions regions = (Regions) response.getData();
            adapter.clear();
            this.regions = regions.region;
            recyclerViewManager.addAll(regions.region);
        }
    }
}
