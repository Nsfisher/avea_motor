package me.bt.motor.modules.settings;

import dagger.Module;
import me.bt.base.di.ActivityModule;

@Module(
    addsTo = ActivityModule.class,
    library = true,
    complete = false,
    injects = {SettingsActivity.class, SettingsNotificationsActivity.class, AreaDefinitionActivity.class}) public class SettingsModule {

  public SettingsModule() {
  }
}

