package me.bt.motor.modules.test;

import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;
import me.bt.avea.domain.entities.Mobile;
import me.bt.avea.domain.interactions.lightevents.LightEventsInteractor;
import me.bt.avea.domain.interactions.mobiles.MobilesInteractor;
import me.bt.avea.domain.interactions.register.RegisterInteractor;
import me.bt.avea.domain.interactions.suggestion.SuggestionInteractor;
import me.bt.avea.presentation.model.PresentationMobile;
import me.bt.avea.presentation.test.TestPresenter;
import me.bt.avea.presentation.test.TestView;
import me.bt.base.di.ActivityModule;
import me.bt.base.domain.abstractions.Bus;
import me.bt.base.domain.interactors.InteractorInvoker;
import me.bt.base.presentation.model.mapper.base.ListMapper;

@Module(
    addsTo = ActivityModule.class,
    library = true,
    injects = TestActivity.class) public class TestModule {
  private TestView testView;

  public TestModule(TestView testView) {
    this.testView = testView;
  }

  @Provides @Singleton TestPresenter provideLoginPresenter(Bus bus, InteractorInvoker interactorInvoker,
      RegisterInteractor registerInteractor, MobilesInteractor mobilesInteractor, ListMapper<Mobile, PresentationMobile> listMapper,
      SuggestionInteractor suggestionInteractor, LightEventsInteractor lightEventsInteractor) {
    return new TestPresenter(bus, interactorInvoker, registerInteractor, mobilesInteractor, testView, listMapper, suggestionInteractor,
        lightEventsInteractor);
  }
}
