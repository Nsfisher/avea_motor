package me.bt.motor.modules.route;

import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;
import me.bt.avea.domain.interactions.suggestion.SuggestionInteractor;
import me.bt.avea.presentation.route.RoutePresenter;
import me.bt.avea.presentation.route.RouteView;
import me.bt.base.di.ActivityModule;
import me.bt.base.domain.abstractions.Bus;
import me.bt.base.domain.interactors.InteractorInvoker;

@Module(
    addsTo = ActivityModule.class,
    library = true,
    complete = false,
    injects = RouteActivity.class) public class RouteModule {

  private final RouteView routeView;

  public RouteModule(RouteView routeView) {
    this.routeView = routeView;
  }

  @Provides @Singleton RoutePresenter provideRoutePresenter(Bus bus, InteractorInvoker interactorInvoker,
      SuggestionInteractor suggestionInteractor) {
    return new RoutePresenter(bus, interactorInvoker, routeView, suggestionInteractor);
  }
}

