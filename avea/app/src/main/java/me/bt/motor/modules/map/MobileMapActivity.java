package me.bt.motor.modules.map;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.TileOverlay;
import com.google.android.gms.maps.model.TileOverlayOptions;
import com.malinskiy.materialicons.widget.IconTextView;
import com.software.shell.fab.ActionButton;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import butterknife.InjectView;
import butterknife.OnClick;
import me.bt.avea.data.preferences.PrefConstants;
import me.bt.avea.presentation.map.MobileMapPresenter;
import me.bt.avea.presentation.map.MobileMapView;
import me.bt.avea.presentation.model.PresentationMobile;
import me.bt.base.DataManager;
import me.bt.base.services.RegistrationIntentService;
import me.bt.base.ui.BaseActivity;
import me.bt.base.ui.elevation.ElevationHandler;
import me.bt.base.ui.errors.ErrorManager;
import me.bt.base.util.Constants;
import me.bt.motor.AveaConstants;
import me.bt.motor.R;
import me.bt.motor.modules.map.adapters.MobileSpinnerAdapter;
import me.bt.motor.util.Utils;
import pl.charmas.android.reactivelocation.ReactiveLocationProvider;
import rx.Subscription;
import rx.functions.Action1;

public class MobileMapActivity extends BaseActivity implements MobileMapView, AdapterView.OnItemSelectedListener {

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String TAG = "MainActivity";

    @Inject
    ReactiveLocationProvider locationProvider;
    @Inject
    MobileMapPresenter presenter;
    @Inject
    ErrorManager errorManager;
    @Inject
    ElevationHandler elevationHandler;
    @InjectView(R.id.rl_speed_limit)
    RelativeLayout rl_speed_limit;
    @InjectView(R.id.rl_refresh)
    RelativeLayout rl_refresh;
    @InjectView(R.id.tv_speed)
    TextView tv_speed;
    @InjectView(R.id.toolbar)
    Toolbar toolbar;
    @InjectView(R.id.sp_mobiles)
    Spinner sp_mobiles;
    @InjectView(R.id.fab_traffic)
    ActionButton fab_traffic;
    @InjectView(R.id.fab_location)
    ActionButton fab_location;
    @InjectView(R.id.tv_last_update)
    TextView tv_last_update;
    @InjectView(R.id.itv_refresh)
    IconTextView itv_refresh;
    private GoogleMap map;
    private Subscription subscription;
    private List<PresentationMobile> presentationMobiles;
    private PresentationMobile selectedMobile;
    private MobileSpinnerAdapter spinnerAdapter;
    private Marker vehicleMarker;
    private ArrayList<Marker> vehicleMarkers = new ArrayList<>();
    private ArrayList<Marker> directionMarkers = new ArrayList<>();

    private final int AUTO_REFRESH_INTERVAL = 15000;
    private Handler refreshHandler;
    private boolean isTrafficEnabled = false;
    private TileOverlay mTrafficTileOverlay;
    private int mLastSelectedPost = 0;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private Marker directionMarker;
    private int mSelected = -1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DataManager.getInstance().isActivityRunning = true;
        DataManager.getInstance().timeStamp = System.currentTimeMillis();

        startGcmService();
        refreshHandler = new Handler();
        setupUI();
        initToolbar();
        initDrawer(this, toolbar);
        startServiceCalls();
    }

    private void startGcmService() {
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                SharedPreferences sharedPreferences =
                        PreferenceManager.getDefaultSharedPreferences(context);
                boolean sentToken = sharedPreferences
                        .getBoolean(Constants.SENT_TOKEN_TO_SERVER, false);
                if (sentToken) {
                    errorManager.showError("Push Notifcation kay?t edildi");
//                    mInformationTextView.setText(getString(R.string.gcm_send_message));
                } else {
                    errorManager.showError("Push Notifcation bir hata meydana geldi");
//                    mInformationTextView.setText(getString(R.string.token_error_message));
                }
            }
        };

        if (checkPlayServices()) {
            // Start IntentService to register this application with GCM.
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }
    }

    Runnable refreshRunnable = new Runnable() {
        @Override
        public void run() {
            startServiceCalls();
            refreshHandler.postDelayed(refreshRunnable, AUTO_REFRESH_INTERVAL);
        }
    };

    @OnClick(R.id.fab_traffic)
    void onTrafficClicked() {
        if (isTrafficEnabled) {
            disableTraffic();
        } else {
            enableTraffic();
        }
    }

    @OnClick(R.id.itv_refresh)
    void onRefreshClicked() {
        startServiceCalls();
    }

    @OnClick(R.id.fab_location)
    void onMylocationClicked() {
        if (map != null && map.getMyLocation() != null) {
            map.moveCamera(
                    CameraUpdateFactory.newLatLngZoom(new LatLng(map.getMyLocation().getLatitude(), map.getMyLocation().getLongitude()), 15));
        }
    }

    private void initToolbar() {
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            elevationHandler.setDefaultElevation(toolbar);
        }
    }

    @Override
    public int onCreateViewId() {
        return R.layout.activity_map;
    }

    protected List<Object> getModules() {
        return Arrays.<Object>asList(new MobileMapModule(this));
    }

    private void startServiceCalls() {
        presenter.getMobilesSkipCache();
    }

    private void setupLocationProvider() {
        LocationRequest request =
                LocationRequest.create().setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY).setNumUpdates(5).setInterval(100);

        ReactiveLocationProvider locationProvider = new ReactiveLocationProvider(this);
        subscription = locationProvider.getUpdatedLocation(request).subscribe(new Action1<Location>() {
            @Override
            public void call(Location location) {
                moveCamera(new LatLng(location.getLatitude(), location.getLongitude()));
            }
        });
    }

    private void moveCamera(LatLng location) {
        if (map != null) {
            CameraPosition camPos =
                    new CameraPosition.Builder().target(location)
                            .zoom(12f)
                            .bearing(0)
                            .tilt(25)
                            .build();
//            map.moveCamera(camPos);

            map.animateCamera(CameraUpdateFactory.newCameraPosition(camPos), 500, null);
//            map.animate
        }
    }

    private void setupUI() {
        setupMap();
    }

    private void setupMap() {
        map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(AveaConstants.TURKEY, 15));
        map.getUiSettings().setMapToolbarEnabled(false);
        map.getUiSettings().setMyLocationButtonEnabled(false);
        map.setMyLocationEnabled(true);
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.onPause();
        refreshHandler.removeCallbacks(refreshRunnable);
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onResume();
        refreshRunnable.run();
        checkTrafficEnabled();
    }

    private void checkTrafficEnabled() {
        boolean isTrafficEnabled = getAppPreferences().getBoolean(PrefConstants.TRAFFIC_ENABLED, false);
        if (isTrafficEnabled) {
            enableTraffic();
        } else {
            disableTraffic();
        }
    }

    private void disableTraffic() {
        fab_traffic.setImageResource(R.drawable.ic_traffic_black);
        fab_traffic.setButtonColor(getResources().getColor(R.color.white));
        isTrafficEnabled = false;
        if (mTrafficTileOverlay != null) {
            mTrafficTileOverlay.clearTileCache();
            mTrafficTileOverlay.remove();
        }
        getAppPreferences().put(PrefConstants.TRAFFIC_ENABLED, isTrafficEnabled);
    }

    private void enableTraffic() {
        isTrafficEnabled = true;
        fab_traffic.setButtonColor(getResources().getColor(R.color.accent));
        fab_traffic.setImageResource(R.drawable.ic_traffic_white);
        getAppPreferences().put(PrefConstants.TRAFFIC_ENABLED, isTrafficEnabled);

        TrafficTileProvider tileProvider = new TrafficTileProvider(256, 256);
        mTrafficTileOverlay = this.map.addTileOverlay(new TileOverlayOptions().tileProvider(tileProvider));

    }

    @Override
    protected void onStop() {
        if (subscription != null) {
            subscription.unsubscribe();
        }
        super.onStop();
    }

    @Override
    public void updateMobiles(List<PresentationMobile> presentationMobiles) {
        PresentationMobile preMobFirst = new PresentationMobile();
        preMobFirst.setAlias(getString(R.string.all_mobiles));
        preMobFirst.isSelectAll = true;
        preMobFirst.setMobile(-12);
        presentationMobiles.add(0, preMobFirst);
        this.presentationMobiles = presentationMobiles;

        sp_mobiles.setVisibility(View.VISIBLE);
        if (spinnerAdapter == null) {
            spinnerAdapter = new MobileSpinnerAdapter(this, this.presentationMobiles);
            int position = 1;
            try {
                String alias = getAppPreferences().getString(PrefConstants.DEFAULT_SAVED_ID, "0");
                Integer defaultMobile = Integer.parseInt(alias);
                PresentationMobile pm;

                for (int j = 0; j < presentationMobiles.size(); j++) {
                    pm = presentationMobiles.get(j);
                    if (pm.mobile.intValue() == defaultMobile.intValue()) {
                        position = j;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            sp_mobiles.setAdapter(spinnerAdapter);
            sp_mobiles.setOnItemSelectedListener(this);
            sp_mobiles.setSelection(position);

        } else {
//            spinnerAdapter = new MobileSpinnerAdapter(this, this.presentationMobiles);
            spinnerAdapter.setData(presentationMobiles);
//            sp_mobiles.setAdapter(spinnerAdapter);
            spinnerAdapter.notifyDataSetChanged();
//            sp_mobiles.setSelection(mLastSelectedPost);
            int pos = sp_mobiles.getSelectedItemPosition();
            boolean isAnim = false;
            if (mSelected != pos) {
                isAnim = true;
            }
            PresentationMobile selected = spinnerAdapter.getItem(pos);
            mSelected = pos;
            if (pos == 0) {
                mobileSelected(selected, isAnim);
            } else {
                mobileSelected(selected, true);
            }


        }
    }

    @Override
    public void updateMobilesError() {
        //TODO hakan show refresh button & error
    }

    private void mobileSelected(PresentationMobile selected, boolean isAnimateCam) {
        if (selected == null) {
            return;
        }
        saveSelectedIdToDisk(selected);
        moveToSelectedMobileLocation(selected, isAnimateCam);
        setupSpeedLimitUI(selected);
        setupRefreshLayout(selected);
    }

    private void setupRefreshLayout(PresentationMobile selected) {
        try {
            if (tv_last_update != null) {
                if (selected.isSelectAll) {
                    tv_last_update.setText(getString(R.string.last_refresh_time) + " " + Utils.getTimeString());
                } else {
                    try {
                        if (TextUtils.isEmpty(selected.getPosTimestamp())) throw new Exception();
                        tv_last_update.setText(getString(R.string.last_refresh_time) + " " + selected.getPosTimestamp());
                    } catch (Exception e) {
                        tv_last_update.setText(getString(R.string.last_refresh_time) + " " + Utils.getTimeString());
                    }
                }
            }
        } catch (Exception e) {

        }

    }


    private void moveToSelectedMobileLocation(PresentationMobile selected, boolean isAnimateCam) {

        if (selected.isSelectAll) {
//            if (vehicleMarkers != null && presentationMobiles != null && vehicleMarkers.size() == presentationMobiles.size() - 1)
//                return;

            clearMarkers();

            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            ArrayList<Marker> mr;
            for (PresentationMobile pm : presentationMobiles) {
                if (havePositionPresMob(pm)) {
                    mr = addMarker(pm);
                    builder.include(mr.get(0).getPosition());
                    vehicleMarkers.add(mr.get(0));
                    directionMarkers.add(mr.get(1));
                }
            }
            final LatLngBounds bounds = builder.build();
            int padding = 120; // offset from edges of the map in pixels
            final CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
            if (isAnimateCam)
                map.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                    @Override
                    public void onMapLoaded() {
                        map.animateCamera(cu);
                    }
                });
        } else if (havePositionPresMob(selected)) {
            clearMarkers();
            LatLng position = new LatLng(selected.getPosLatitude(), selected.getPosLongitude());
            if (isAnimateCam)
                moveCamera(position);

            if (vehicleMarker == null) {
                ArrayList<Marker> markers;
                markers = addMarker(selected);
                vehicleMarker = markers.get(0);
                directionMarker = markers.get(1);
            } else {
                vehicleMarker.setPosition(position);
                directionMarker.setRotation(selected.getPosDirectionValue());
                directionMarker.setPosition(position);
            }
        }
    }

    private void clearMarkers() {
        for (Marker mrk : vehicleMarkers) mrk.remove();
        vehicleMarkers = new ArrayList<>();

        for (Marker mrk : directionMarkers) mrk.remove();
        directionMarkers = new ArrayList<>();

    }

    private boolean havePositionPresMob(PresentationMobile preMob) {
        try {
            return (preMob.getPosLatitude() != 0 && preMob.getPosLongitude() != 0 && preMob.getPosDirectionValue() != null);
        } catch (Exception e) {
            return false;
        }
    }

    private ArrayList<Marker> addMarker(PresentationMobile selected) {
        ArrayList<Marker> markers = new ArrayList<>();
        LatLng position = new LatLng(selected.getPosLatitude(), selected.getPosLongitude());
        Marker markerDirection, markerVehicle;
        markerDirection =
                map.addMarker(new MarkerOptions().position(position).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_vehicle_pinn)));

        markerVehicle =
                map.addMarker(new MarkerOptions().position(position).icon(BitmapDescriptorFactory.fromResource(R.drawable.vehiclee)));

//        marker2.setRotation(selected.getPosDirectionValue());
        float f = (float) 0.5;
        markerDirection.setAnchor(f, f);
        markerVehicle.setAnchor(f, f);
        markerDirection.setFlat(true);
        markerDirection.setRotation(selected.getPosDirectionValue());

        markers.add(markerVehicle);
        markers.add(markerDirection);

        return markers;
    }

    private void setupSpeedLimitUI(PresentationMobile selected) {
        if (selected.getSpeedExceedLimit() != null && selected.getPosSpeed() != null) {
            if (selected.getSpeedExceedLimit() > selected.getPosSpeed()) {
                rl_speed_limit.setBackgroundColor(getResources().getColor(R.color.map_speed_exceed_max_bg));
                tv_speed.setText(String.valueOf(selected.getSpeedExceedMax()));
            } else {
                rl_speed_limit.setBackgroundColor(getResources().getColor(R.color.map_speed_exceed_bg));
            }
            tv_speed.setText(String.valueOf(selected.getPosSpeed()));
            rl_speed_limit.setVisibility(View.VISIBLE);
            rl_refresh.setVisibility(View.VISIBLE);
        } else if (selected.isSelectAll) {
            rl_refresh.setVisibility(View.VISIBLE);
            rl_speed_limit.setVisibility(View.GONE);
        } else {
            rl_speed_limit.setVisibility(View.GONE);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        mLastSelectedPost = position;
        PresentationMobile selected = spinnerAdapter.getItem(position);
        mobileSelected(selected, true);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        DataManager.getInstance().isActivityRunning = false;
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(TAG, "");
                errorManager.showError("This device is not supported gcm");
//                finish();
            }
            return false;
        }
        return true;
    }

}


