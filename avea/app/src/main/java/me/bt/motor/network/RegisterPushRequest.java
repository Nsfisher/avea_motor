package me.bt.motor.network;

import java.net.URLEncoder;

import me.bt.base.util.Constants;

/**
 * Created by yenerciftci on 24/08/15.
 */
public class RegisterPushRequest extends BaseRequest {

    private String DeviceType = "Android";
    public String Username;
    public String Password;
    public String DeviceId;
    public String Token;
    private String AppName = "Avea";

//    @property(nonatomic) double latitude,longitude,radius;


    @Override
    public String toString() {
        try {
            return Constants.APP_URL_REGISTER_PUSH
                    + "Username=" + URLEncoder.encode(Username, "UTF-8")
                    + "&" + "Token=" + Token
                    + "&" + "Password=" + Password
                    + "&" + "DeviceType=" + DeviceType
                    + "&" + "DeviceId=" + DeviceId
                    + "&" + "AppName=" + AppName;
        } catch (Exception e) {
            return Constants.APP_URL_REGISTER_PUSH
                    + "Username=" + Username
                    + "&" + "Token=" + Token
                    + "&" + "Password=" + Password
                    + "&" + "DeviceType=" + DeviceType
                    + "&" + "DeviceId=" + DeviceId
                    + "&" + "AppName=" + AppName;
        }

    }

}
