package me.bt.motor.network;

import java.net.URLEncoder;

import me.bt.base.util.Constants;

/**
 * Created by yenerciftci on 28/07/15.
 */
public class AddUserRequest extends BaseRequest {
    public String username, password, plaka, takmaIsim, marka, model, yakitTipi, motorBilgisi, activation, queryMode = "0";
    public String phone;

    @Override
    public String toString() {
        try {

//            if (!TextUtils.isEmpty(username)) {
            return Constants.APP_URL + CUSTOMER
                    + "token=" + token
                    + "&" + "username=" + URLEncoder.encode(username, "UTF-8")
                    + "&" + "password=" + URLEncoder.encode(password, "UTF-8")
                    + "&" + "takmaIsim=" + URLEncoder.encode(takmaIsim, "UTF-8")
                    + "&" + "plaka=" + URLEncoder.encode(plaka, "UTF-8")
                    + "&" + "marka=" + URLEncoder.encode(marka, "UTF-8")
                    + "&" + "yakitTipi=" + URLEncoder.encode(yakitTipi, "UTF-8")
                    + "&" + "motorBilgisi=" + URLEncoder.encode(motorBilgisi, "UTF-8")
                    + "&" + "queryMode=" + URLEncoder.encode(queryMode, "UTF-8")
                    + "&" + "phone=" + URLEncoder.encode(phone, "UTF-8")
                    + "&" + "activation=" + URLEncoder.encode(activation, "UTF-8");
//            } else {
//                return Constants.APP_URL + CUSTOMER
//                        + "token=" + token
//                        + "&" + "takmaIsim=" + URLEncoder.encode(takmaIsim, "UTF-8")
//                        + "&" + "plaka=" + URLEncoder.encode(plaka, "UTF-8")
//                        + "&" + "marka=" + URLEncoder.encode(marka, "UTF-8")
//                        + "&" + "yakitTipi=" + URLEncoder.encode(yakitTipi, "UTF-8")
//                        + "&" + "motorBilgisi=" + URLEncoder.encode(motorBilgisi, "UTF-8")
//                        + "&" + "activation=" + URLEncoder.encode(activation, "UTF-8");
//            }

        } catch (Exception e) {
            return null;
        }


    }
//    358639051000142;

}
