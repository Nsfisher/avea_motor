package me.bt.motor.util;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.util.DisplayMetrics;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.squareup.okhttp.CertificatePinner;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Response;

import net.grandcentrix.tray.TrayAppPreferences;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Hours;
import org.joda.time.Minutes;
import org.joda.time.Seconds;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.IOException;
import java.security.cert.Certificate;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

import me.bt.avea.data.preferences.PrefConstants;
import me.bt.avea.domain.entities.DataLightPosition;
import me.bt.avea.domain.entities.IntervalData;
import me.bt.avea.presentation.model.PresentationMobile;
import me.bt.motor.R;
import me.bt.motor.modules.trips.TripsActivity;
import timber.log.Timber;

public class Utils {

    public static String formatDate(String startPosTimeStamp) {
        try {
            DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyyMMddHHmmss");
            DateTime startDate = formatter.parseDateTime(startPosTimeStamp);
            DateTime now = new DateTime();
            int days = Days.daysBetween(startDate.toLocalDate(), now.toLocalDate()).getDays();
            StringBuilder builder = new StringBuilder();
            if (days == 0) {
                builder.append("Bugun, ");
            } else if (days == 1) {
                builder.append("Dun, ");
            } else {
                builder.append(Utils.getLocalDateStringWithYear(startDate.toDate()));
                builder.append(", ");
            }

            if (startDate.getHourOfDay() < 10) {
                builder.append("0");
            }
            builder.append(startDate.getHourOfDay());
            builder.append(":");
            if (startDate.getMinuteOfHour() < 10) {
                builder.append("0");
            }
            builder.append(startDate.getMinuteOfHour());
            return builder.toString();
        } catch (Exception ex) {
        }
        return "";
    }

    public static String dateAgo(String startPosTimeStamp, Resources resources) {
        try {
            DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyyMMddHHmmss");
            DateTime startDate = formatter.parseDateTime(startPosTimeStamp);
            DateTime now = new DateTime();
            int days = Days.daysBetween(startDate, now).getDays();
            int hours = Hours.hoursBetween(startDate, now).getHours() % 24;
            int minutes = Minutes.minutesBetween(startDate, now).getMinutes() % 60;
            int seconds = Seconds.secondsBetween(startDate, now).getSeconds() % 60;

            StringBuilder builder = new StringBuilder();

            if (days != 0) {
                builder.append(days);
                builder.append(" ");
                builder.append(resources.getString(R.string.day));
            }

            if (hours != 0) {
                if (days != 0) {
                    builder.append(", ");
                }
                builder.append(hours);
                builder.append(" ");
                builder.append(resources.getString(R.string.hour));
            }

            if (minutes != 0) {
                if (hours != 0) {
                    builder.append(", ");
                }
                builder.append(minutes);
                builder.append(" ");
                builder.append(resources.getString(R.string.minute));
            }

            builder.append(" ");
            builder.append(resources.getString(R.string.before));

            return builder.toString();
        } catch (Exception ex) {
        }
        return "";
    }

    public static String dateToString(String startPosTimeStamp, Resources resources) {
        try {
            DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyyMMddHHmmss");
            DateTime startDate = formatter.parseDateTime(startPosTimeStamp);
            formatter = DateTimeFormat.forPattern("dd MM yyy HH:mm:ss");
//            formatter.parseDateTime(startDate.toDateTime());
            return Utils.getLocalDateString(startDate.toDate());
//            return formatter.print(startDate);
        } catch (Exception e) {
            return "";
        }
    }


    public static String dateToStringTime(String startPosTimeStamp, Resources resources) {
        try {
            DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyyMMddHHmmss");
            DateTime startDate = formatter.parseDateTime(startPosTimeStamp);
            formatter = DateTimeFormat.forPattern("dd MM yyy HH:mm:ss");
//            formatter.parseDateTime(startDate.toDateTime());
            return Utils.getLocalDateStringTime(startDate.toDate());
//            return formatter.print(startDate);
        } catch (Exception e) {
            return "";
        }
    }


    public static String dateDifference(String startPosTimeStamp, String stopPosTimeStamp, Resources resources) {
        try {
            DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyyMMddHHmmss");
            DateTime startDate = formatter.parseDateTime(startPosTimeStamp);
            DateTime stopDate = formatter.parseDateTime(stopPosTimeStamp);
            int days = Days.daysBetween(startDate, stopDate).getDays();
            int hours = Hours.hoursBetween(startDate, stopDate).getHours() % 24;
            int minutes = Minutes.minutesBetween(startDate, stopDate).getMinutes() % 60;
            int seconds = Seconds.secondsBetween(startDate, stopDate).getSeconds() % 60;

            StringBuilder builder = new StringBuilder();

            if (days > 0) {
                builder.append(days);
                builder.append(" ");
                builder.append(resources.getString(R.string.day));
                builder.append(" ");
            }
            if (hours > 0) {
                builder.append(hours);
                builder.append(" sa ");
            }

            if (minutes > 0) {
                builder.append(minutes);
//                builder.append(" dk ");
            }
            return builder.toString();
        } catch (Exception ex) {
            return "";
        }
    }

    public static float convertDpToPixel(float dp, Context context) {
        if (context == null) {
            return 0;
        }
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return px;
    }

    public static float convertPixelsToDp(float px, Context context) {
        if (context == null) {
            return 0;
        }
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / (metrics.densityDpi / 160f);
        return dp;
    }

    public static HashMap<String, Marker> setupMapWithPoints(final GoogleMap map, List<DataLightPosition> dataLightPositions, final Context context,
                                                             IntervalData intervalData) {
        final HashMap<String, Marker> markerHashMap = new HashMap<>();
        if (dataLightPositions == null) {
            dataLightPositions = TripsActivity.lights;
        }

        if (dataLightPositions != null && map != null && intervalData != null) {
            LatLng ISTANBUL = new LatLng(41.0192598, 28.9782394);


            map.moveCamera(CameraUpdateFactory.newLatLngZoom(ISTANBUL, 13));
            Marker marker;
            try {

                final List<DataLightPosition> finalDataLightPositions = dataLightPositions;
                map.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                    @Override
                    public void onMapLoaded() {

                        PolylineOptions options = new PolylineOptions();
                        LatLngBounds.Builder builder = new LatLngBounds.Builder();
                        options.width(15).color(Color.RED);

                        for (int k = 0; k < finalDataLightPositions.size(); k++) {

                            DataLightPosition dataLightPosition = finalDataLightPositions.get(k);
                            LatLng markerPosition =
                                    new LatLng(Double.parseDouble(dataLightPosition.latitude), Double.parseDouble(dataLightPosition.longitude));
                            builder.include(markerPosition);
                            options.add(markerPosition);
                            MarkerOptions markerOptions = new MarkerOptions();
                            markerOptions.position(markerPosition);
                            if (k == 0) {
                                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_oval_start_small));
                                map.addMarker(markerOptions);
                            } else if (k == finalDataLightPositions.size() - 1) {
                                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_oval_finish_small));
                                map.addMarker(markerOptions);
                            } else if (dataLightPosition.eventDescription != null && dataLightPosition.eventDescription.length() > 0) {
//                                if (!TextUtils.isEmpty(dataLightPosition.eventCode)) {
//                                    markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_fault_small))
//                                            .title(dataLightPosition.eventDescription);
//                                    if (dataLightPosition.location != null && dataLightPosition.location.length() > 0) {
//                                        markerOptions.snippet(dataLightPosition.location);
//                                    }
//                                    markerHashMap.put(dataLightPosition.toString(), map.addMarker(markerOptions));
//                                }
                            }
                        }
                        map.addPolyline(options);
                        LatLngBounds bounds = builder.build();
                        int padding = (int) Utils.convertDpToPixel(16, context);
                        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                        map.moveCamera(cameraUpdate);
                    }
                });
            } catch (Exception ex) {
                if (ex.getMessage() != null) {
                    Timber.e(ex.getMessage());
                }
                Log.e("expand", "exception");
            }
        }

        return markerHashMap;
    }


    public static HashMap<String, Marker> setupMapWithPointsDetai(final GoogleMap map, List<DataLightPosition> dataLightPositions, final Context context,
                                                                  IntervalData intervalData) {
        final HashMap<String, Marker> markerHashMap = new HashMap<>();
        if (dataLightPositions == null) {
            dataLightPositions = TripsActivity.lights;
        }

        if (dataLightPositions != null && map != null && intervalData != null) {
            LatLng ISTANBUL = new LatLng(41.0192598, 28.9782394);


            map.moveCamera(CameraUpdateFactory.newLatLngZoom(ISTANBUL, 13));
            Marker marker;
            try {

                final List<DataLightPosition> finalDataLightPositions = dataLightPositions;


                PolylineOptions options = new PolylineOptions();
                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                options.width(15).color(Color.RED);

                for (int k = 0; k < finalDataLightPositions.size(); k++) {

                    DataLightPosition dataLightPosition = finalDataLightPositions.get(k);
                    LatLng markerPosition =
                            new LatLng(Double.parseDouble(dataLightPosition.latitude), Double.parseDouble(dataLightPosition.longitude));
                    builder.include(markerPosition);
                    options.add(markerPosition);
                    MarkerOptions markerOptions = new MarkerOptions();
                    markerOptions.position(markerPosition);
                    if (k == 0) {
                        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_oval_start_small));
                        map.addMarker(markerOptions);
                    } else if (k == finalDataLightPositions.size() - 1) {
                        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_oval_finish_small));
                        map.addMarker(markerOptions);
                    } else if (dataLightPosition.eventDescription != null && dataLightPosition.eventDescription.length() > 0) {
//                                if (!TextUtils.isEmpty(dataLightPosition.eventCode)) {
//                                    markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_fault_small))
//                                            .title(dataLightPosition.eventDescription);
//                                    if (dataLightPosition.location != null && dataLightPosition.location.length() > 0) {
//                                        markerOptions.snippet(dataLightPosition.location);
//                                    }
//                                    markerHashMap.put(dataLightPosition.toString(), map.addMarker(markerOptions));
//                                }
                    }
                }
                map.addPolyline(options);
                LatLngBounds bounds = builder.build();
                int padding = (int) Utils.convertDpToPixel(16, context);
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                map.moveCamera(cameraUpdate);

            } catch (Exception ex) {
                if (ex.getMessage() != null) {
                    Timber.e(ex.getMessage());
                }
                Log.e("expand", "exception");
            }
        }

        return markerHashMap;
    }

    public static HashMap<String, Marker> setupMapWithPointsDetail(final GoogleMap map, List<DataLightPosition> dataLightPositions, final Context context) {
        final HashMap<String, Marker> markerHashMap = new HashMap<>();
        if (dataLightPositions == null) {
            dataLightPositions = TripsActivity.lights;
        }

        if (dataLightPositions != null && map != null) {
            LatLng ISTANBUL = new LatLng(41.0192598, 28.9782394);


            map.moveCamera(CameraUpdateFactory.newLatLngZoom(ISTANBUL, 13));
            Marker marker;
            try {

                final List<DataLightPosition> finalDataLightPositions = dataLightPositions;


                PolylineOptions options = new PolylineOptions();
                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                options.width(15).color(Color.RED);

                for (int k = 0; k < finalDataLightPositions.size(); k++) {

                    DataLightPosition dataLightPosition = finalDataLightPositions.get(k);
                    LatLng markerPosition =
                            new LatLng(Double.parseDouble(dataLightPosition.latitude), Double.parseDouble(dataLightPosition.longitude));
                    builder.include(markerPosition);
                    options.add(markerPosition);
                    MarkerOptions markerOptions = new MarkerOptions();
                    markerOptions.position(markerPosition);
//                            if (k == 0) {
////                                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_oval_start_small));
////                                map.addMarker(markerOptions);
//                            } else if (k == finalDataLightPositions.size() - 1) {
////                                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_oval_finish_small));
////                                map.addMarker(markerOptions);
//                            } else {
//                                if (!TextUtils.isEmpty(dataLightPosition.eventCode)) {
                    markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_fault_small))
                            .title(dataLightPosition.eventDescription);
                    if (dataLightPosition.location != null && dataLightPosition.location.length() > 0) {
                        markerOptions.snippet(dataLightPosition.location);
                    }
                    markerHashMap.put(dataLightPosition.toString(), map.addMarker(markerOptions));
//                                }
//                            }
                }
//                        map.addPolyline(options);
//                        LatLngBounds bounds = builder.build();
//                        int padding = (int) Utils.convertDpToPixel(16, context);
//                        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, padding);
//                        map.moveCamera(cameraUpdate);

            } catch (Exception ex) {
                if (ex.getMessage() != null) {
                    Timber.e(ex.getMessage());
                }
                Log.e("expand", "exception");
            }
        }

        return markerHashMap;
    }

    public static String getDistanceFromMeters(double distance) {
        try {
//            if (distance < 1000) {
//                String formatted = String.format("%.1f", distance);
//                return formatted + "km";
//            } else {
            String formatted = String.format("%.1f", distance);
            return formatted + "km";
//            }
        } catch (Exception ex) {
            return "";
        }
    }

    public static boolean isValidEmailAddress(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }

    public static String getLocalDateString(Date startDate) {
        Locale locale = new Locale("tr", "TR");
        DateFormat formatter = new SimpleDateFormat("dd MMMM", locale);
        return formatter.format(startDate);
    }

    public static String getLocalDateStringTime(Date startDate) {
        Locale locale = new Locale("tr", "TR");
        DateFormat formatter = new SimpleDateFormat("dd MMMM yyyy HH:mm", locale);
        return formatter.format(startDate);
    }

    public static String getLocalDateStringWithYear(Date startDate) {
        Locale locale = new Locale("tr", "TR");
        DateFormat formatter = new SimpleDateFormat("dd MMMM yyyy", locale);
        return formatter.format(startDate);
    }

    public static String getTimeString() {
        DateTimeFormatter formatter = DateTimeFormat.forPattern("dd.MM.yyyy HH:mm:ss");
        DateTime now = new DateTime();
        return formatter.print(now);
    }

    public static int getDefaultMobilePosition(List<PresentationMobile> presentationMobiles, TrayAppPreferences appPreferences) {
        String mobileId = appPreferences.getString(PrefConstants.DEFAULT_MOBILE_ID, "");
        if (mobileId != null && !mobileId.equalsIgnoreCase("")) {
            for (int i = 0; i < presentationMobiles.size(); i++) {
                if (presentationMobiles.get(i) != null && presentationMobiles.get(i).getMobile() != null) {
                    if (presentationMobiles.get(i).getMobile().toString().equalsIgnoreCase(mobileId)) {
                        return i;
                    }
                }
            }
        }
        return -1;
    }

    public static int getSelectedMobilePosition(List<PresentationMobile> presentationMobiles, TrayAppPreferences appPreferences) {
        String mobileId = appPreferences.getString(PrefConstants.DEFAULT_MOBILE_ID, "");
        if (mobileId != null && !mobileId.equalsIgnoreCase("")) {
            for (int i = 0; i < presentationMobiles.size(); i++) {
                if (presentationMobiles.get(i) != null && presentationMobiles.get(i).getMobile() != null) {
                    if (presentationMobiles.get(i).getMobile().toString().equalsIgnoreCase(mobileId)) {
                        return i;
                    }
                }
            }
        }
        return -1;
    }

    public static int round(double d) {
        double dAbs = Math.abs(d);
        int i = (int) dAbs;
        double result = dAbs - (double) i;
        if (result < 0.5) {
            return d < 0 ? -i : i;
        } else {
            return d < 0 ? -(i + 1) : i + 1;
        }
    }

    public static OkHttpClient installCertificatePinning(OkHttpClient client) {
//        CertificatePinner certificatePinner = new CertificatePinner.Builder()
//                .add("*.infomobil.com.tr", "sha1/AAAAAAAAAAAAAAAAAAAAAAAAAAA=")
////                .add("*.infotech.com.tr", "sha1/k3jv6A3enDQVz8yNwpOBdIKjK14=")
//                .build();
//        client.setCertificatePinner(certificatePinner);
        client.setHostnameVerifier(new HostnameVerifier() {
            @Override
            public boolean verify(String hostname, SSLSession session) {
                //TODO: Make this more restrictive
                return true;
            }
        });

        final List<String> certificates = Arrays.asList("sha1/k3jv6A3enDQVz8yNwpOBdIKjK14=",
                "sha1/o5OZxATDsgmwgcIfIWIneMJ0jkw=",
                "sha1/wHqYaI2J+6sFZAwRfap9ZbjKzE4=");

        client.networkInterceptors().add(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                for (Certificate certificate : chain.connection().getHandshake().peerCertificates()) {
                    String pin = CertificatePinner.pin(certificate);
                    if (!certificates.contains(pin)) {
                        throw new IOException("Blacklisted peer certificate: " + pin);
                    }
                }
                return chain.proceed(chain.request());
            }
        });
        return client;
    }
}