package me.bt.motor.ui.items;

import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.carlosdelachica.easyrecycleradapters.adapter.EasyViewHolder;

import butterknife.ButterKnife;
import butterknife.InjectView;
import me.bt.avea.domain.entities.LightEvent;
import me.bt.motor.util.Utils;
import me.bt.motor.R;

public class LightEventViewHolder extends EasyViewHolder<LightEvent> {

    private final Resources resources;
    @InjectView(R.id.tv_event)
    TextView tv_event;
    @InjectView(R.id.tv_date)
    TextView tv_date;
    @InjectView(R.id.v_circle)
    View v_circle;

    public LightEventViewHolder(Context context, ViewGroup parent, Resources resources) {
        super(context, parent, R.layout.item_notification);
        ButterKnife.inject(this, itemView);
        this.resources = resources;
    }

    @Override
    public void bindTo(LightEvent lightEvent) {
        if (lightEvent == null) {
            return;
        }
        if (lightEvent.eventDescription != null) {
            tv_event.setText(lightEvent.eventDescription);
        }
        if (lightEvent.posTimeStr != null) {
            tv_date.setText(Utils.dateToStringTime(lightEvent.posTimeStr, resources));
        }
        if (!lightEvent.isRead) {
            v_circle.setVisibility(View.VISIBLE);
        }
    }
}
