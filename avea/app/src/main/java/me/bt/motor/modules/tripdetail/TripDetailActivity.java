package me.bt.motor.modules.tripdetail;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import butterknife.InjectView;
import me.bt.avea.domain.entities.DataLightPosition;
import me.bt.avea.domain.entities.IntervalData;
import me.bt.motor.modules.trips.TripsModule;
import me.bt.motor.modules.trips.common.TripDetailEvent;
import me.bt.avea.presentation.model.TripsBundle;
import me.bt.avea.presentation.trips.TripsPresenter;
import me.bt.avea.presentation.trips.TripsView;
import me.bt.motor.util.Utils;
import me.bt.motor.R;
import me.bt.base.ui.BaseActivity;
import me.bt.base.ui.elevation.ElevationHandler;
import me.bt.base.ui.loading.LoadingManager;

public class TripDetailActivity extends BaseActivity implements TripsView {

    public static final String EXTRA_QUERY = "EXTRA_QUERY";
    @InjectView(R.id.toolbar)
    Toolbar toolbar;
    @InjectView(R.id.tv_date)
    TextView tv_date;
    @InjectView(R.id.tv_duration)
    TextView tv_duration;
    @InjectView(R.id.tv_distance)
    TextView tv_distance;
    @InjectView(R.id.tv_avg_speed)
    TextView tv_avg_speed;
    @InjectView(R.id.tv_title)
    TextView tv_title;
    @InjectView(R.id.ll_drive_logs)
    LinearLayout ll_drive_logs;
    @InjectView(R.id.iv_map)
    ImageView iv_map;
    @InjectView(R.id.sv_root)
    ScrollView sv_root;
    @Inject
    LoadingManager loadingManager;
    @Inject
    ElevationHandler elevationHandler;
    @Inject
    TripsPresenter presenter;
    public static final String EXTRA_DETAIL = "detail";
    private TripDetailEvent detailEvent;
    private GoogleMap map;
    static final LatLng ISTANBUL = new LatLng(41.0192598, 28.9782394);
    private HashMap<String, Marker> mMarkerMap;
    private Gson mGson;
    private TripsBundle mTripBundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getIntentData();
        initToolbar();
        setupUI();
        setupMap();
        startServiceCalls();
    }

    private void setupMap() {
//        mMarkerMap = Utils.setupMapWithPoints(map, detailEvent.getLightPositions(), this, detailEvent.getIntervalData());

        iv_map.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        sv_root.requestDisallowInterceptTouchEvent(true);
                        return false;
                    case MotionEvent.ACTION_UP:
                        sv_root.requestDisallowInterceptTouchEvent(false);
                        return true;
                    case MotionEvent.ACTION_MOVE:
                        sv_root.requestDisallowInterceptTouchEvent(true);
                        return false;
                    default:
                        return true;
                }
            }
        });
    }

    private void startServiceCalls() {
        if (detailEvent == null || detailEvent.getIntervalData() == null) {
            return;
        }


        presenter.getLightPositionsForDetailScreen(mTripBundle.mobileId, mTripBundle.startDate, mTripBundle.endDate);
        loadingManager.showLoading(getString(R.string.please_wait));
//        presenter.getLightPositionsForDetailScreen(mTripBundle.mobileId, mTripBundle.startPosTimeStamp, mTripBundle.stopPosTimeStamp);
    }

    private void getIntentData() {
        if (getIntent() != null && getIntent().getExtras() != null && getIntent().getExtras().containsKey(EXTRA_DETAIL)) {
            try {
                detailEvent = (TripDetailEvent) getIntent().getExtras().get(EXTRA_DETAIL);
            } catch (Exception ex) {

            }
        }

        if (getIntent() != null && getIntent().getExtras() != null && getIntent().getExtras().containsKey(EXTRA_QUERY)) {
            try {
                String trips = (String) getIntent().getExtras().get(EXTRA_QUERY);
                mGson = new Gson();
                mTripBundle = mGson.fromJson(trips, TripsBundle.class);
            } catch (Exception ex) {

            }
        }

    }

    private void setupUI() {
        map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();

        if (detailEvent == null) {
            return;
        }

        List<DataLightPosition> lightPositions = detailEvent.getLightPositions();
        IntervalData intervalData = detailEvent.getIntervalData();

        if (lightPositions != null) {

        }

        if (intervalData != null) {
            if (intervalData.distanceGps != null) {

                try {
                    Double distance = Double.valueOf(intervalData.distanceGps);
                    tv_distance.setText(String.format("%.2f", distance / 1000));
                } catch (Exception ex) {

                }
            }

            if (intervalData.speedAvg != null) {
                tv_avg_speed.setText(intervalData.speedAvg);
            }

            if (intervalData.startPosTimeStamp != null) {
                tv_date.setText(Utils.dateAgo(intervalData.startPosTimeStamp, getResources()));
            }

            if (intervalData.startPosTimeStamp != null && intervalData.stopPosTimeStamp != null) {
                tv_duration.setText(Utils.dateDifference(intervalData.startPosTimeStamp, intervalData.stopPosTimeStamp, getResources()));
            }

//            if (intervalData.logEvents != null && intervalData.logEvents.size() > 0) {
//                ll_drive_logs.setVisibility(View.VISIBLE);
//
//                List<DataLightPosition> filtered = new ArrayList<>();
//
//                for (int k = 0; k < lightPositions.size(); k++) {
//                    if (lightPositions.get(k).eventDescription != null && lightPositions.get(k).eventDescription.length() > 0
//                            &&
//                            lightPositions.get(k).eventCode != null && lightPositions.get(k).eventCode.length() > 0
//                            ) {
//                        filtered.add(lightPositions.get(k));
//                    }
//                }
//
//
////                Collections.reverse(filtered);
//                final CameraPosition.Builder builder = new CameraPosition.Builder();
//                ;
//                final Marker[] m = new Marker[1];
//                for (int k = 0; k < filtered.size(); k++) {
//                    DataLightPosition position = filtered.get(k);
//                    RelativeLayout rl_fault = (RelativeLayout) getLayoutInflater().inflate(R.layout.item_trip_detail_fault, null);
//                    TextView tv_value = (TextView) rl_fault.findViewById(R.id.tv_value);
//                    TextView tv_detail = (TextView) rl_fault.findViewById(R.id.tv_detail);
//                    tv_value.setText(String.valueOf(k + 1));
//                    tv_detail.setText(position.eventDescription);
//                    rl_fault.setTag(position);
//                    rl_fault.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            try {
//                                DataLightPosition l = (DataLightPosition) v.getTag();
//                                m[0] = mMarkerMap.get(l.toString());
//                                m[0].showInfoWindow();
//                                CameraPosition b = builder.target(m[0].getPosition())
//                                        .zoom(15.5f)
//                                        .bearing(0)
//                                        .tilt(25)
//                                        .build();
//
//                                final CameraUpdate cu = CameraUpdateFactory.newCameraPosition(b);
//                                map.animateCamera(cu);
//
//                            } catch (Exception e) {
//
//                            }
//                        }
//                    });
//                    ll_drive_logs.addView(rl_fault);
//                }
//            }
        }
    }

    private void initToolbar() {
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            if (detailEvent.getIntervalData() != null && detailEvent.getIntervalData().alias != null) {
                tv_title.setText(detailEvent.getIntervalData().alias);
                tv_title.setVisibility(View.VISIBLE);
            }
            elevationHandler.setDefaultElevation(toolbar);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View back) {
                    onBackPressed();
                }
            });
        }
    }

    @Override
    public int onCreateViewId() {
        return R.layout.activity_trip_detail;
    }

    protected List<Object> getModules() {
        return Arrays.<Object>asList(new TripsModule(this));
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    public void updateTrips(List<IntervalData> trips) {

    }

    @Override
    public void updateTripsError() {

    }

    @Override
    public void updateLightPositions(final List<DataLightPosition> lightPositions) {
        try {
            loadingManager.dismissLoading();
            map.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                @Override
                public void onMapLoaded() {
                    Utils.setupMapWithPoints(map, detailEvent.getLightPositions(), TripDetailActivity.this, detailEvent.getIntervalData());
                    mMarkerMap = Utils.setupMapWithPointsDetail(map, lightPositions, TripDetailActivity.this);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
            ;
        }

//        if (intervalData.logEvents != null && intervalData.logEvents.size() > 0) {


        ll_drive_logs.setVisibility(View.VISIBLE);

        List<DataLightPosition> filtered = new ArrayList<>();

        for (int k = 0; k < lightPositions.size(); k++) {
            if (lightPositions.get(k).eventDescription != null && lightPositions.get(k).eventDescription.length() > 0
                    &&
                    lightPositions.get(k).eventCode != null && lightPositions.get(k).eventCode.length() > 0
                    ) {
                filtered.add(lightPositions.get(k));
            }
        }


//                Collections.reverse(filtered);
        final CameraPosition.Builder builder = new CameraPosition.Builder();
        final Marker[] m = new Marker[1];
        for (int k = 0; k < filtered.size(); k++) {
            DataLightPosition position = filtered.get(k);
            RelativeLayout rl_fault = (RelativeLayout) getLayoutInflater().inflate(R.layout.item_trip_detail_fault, null);
            TextView tv_value = (TextView) rl_fault.findViewById(R.id.tv_value);
            TextView tv_detail = (TextView) rl_fault.findViewById(R.id.tv_detail);
            tv_value.setText(String.valueOf(k + 1));
            tv_detail.setText(position.eventDescription);
            rl_fault.setTag(position);
            rl_fault.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    try {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                DataLightPosition l = (DataLightPosition) v.getTag();
                                m[0] = mMarkerMap.get(l.toString());
                                m[0].showInfoWindow();
                                CameraPosition b = builder.target(m[0].getPosition())
                                        .zoom(15.5f)
                                        .bearing(0)
                                        .tilt(25)
                                        .build();

                                final CameraUpdate cu = CameraUpdateFactory.newCameraPosition(b);
                                map.animateCamera(cu);
                            }
                        });

                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                }
            });
            ll_drive_logs.addView(rl_fault);
        }

//        }
    }


    @Override
    public void updateLightPositionsError() {
        loadingManager.dismissLoading();
    }
}
