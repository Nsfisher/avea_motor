package me.bt.motor.modules.trips;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.Gson;
import com.pnikosis.materialishprogress.ProgressWheel;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import butterknife.InjectView;
import de.greenrobot.event.EventBus;
import me.bt.avea.data.preferences.PrefConstants;
import me.bt.avea.domain.entities.DataLightPosition;
import me.bt.avea.domain.entities.IntervalData;
import me.bt.motor.modules.calendar.CalendarActivity;
import me.bt.motor.modules.map.MobileMapModule;
import me.bt.motor.modules.map.adapters.MobileSpinnerAdapter;
import me.bt.motor.modules.tripdetail.TripDetailActivity;
import me.bt.motor.modules.trips.adapters.TripExpandableItemAdapter;
import me.bt.motor.modules.trips.common.TripDetailEvent;
import me.bt.motor.modules.trips.common.TripLightPointsEvent;
import me.bt.motor.modules.trips.common.data.AbstractExpandableDataProvider;
import me.bt.motor.modules.trips.common.fragment.TripDataProviderFragment;
import me.bt.avea.presentation.map.MobileMapPresenter;
import me.bt.avea.presentation.map.MobileMapView;
import me.bt.avea.presentation.model.PresentationMobile;
import me.bt.avea.presentation.model.TripsBundle;
import me.bt.avea.presentation.trips.TripsPresenter;
import me.bt.avea.presentation.trips.TripsView;
import me.bt.motor.util.Utils;
import me.bt.motor.R;
import me.bt.base.domain.abstractions.Bus;
import me.bt.base.ui.BaseActivity;
import me.bt.base.ui.elevation.ElevationHandler;
import me.bt.base.ui.errors.ErrorManager;
import me.bt.base.ui.loading.LoadingManager;
import me.bt.opensource.andexert.calendarlistview.library.SimpleMonthAdapter;

public class TripsActivity extends BaseActivity implements TripsView, MobileMapView, AdapterView.OnItemSelectedListener {
    public static List<DataLightPosition> lights;
    private static final String SAVED_STATE_EXPANDABLE_ITEM_MANAGER = "RecyclerViewExpandableItemManager";
    private static final int REQUEST_PICK_DATE = 1238;
    @Inject
    TripsPresenter presenter;
    @Inject
    MobileMapPresenter mobilePresenter;
    @Inject
    ErrorManager errorManager;
    @Inject
    ElevationHandler elevationHandler;
    @Inject
    Bus bus;
    @InjectView(R.id.toolbar)
    Toolbar toolbar;
    @InjectView(R.id.sp_mobiles)
    Spinner sp_mobiles;
    @InjectView(R.id.iv_right)
    ImageView iv_right;
    @InjectView(R.id.tv_no_item)
    TextView tv_no_item;
    @InjectView(R.id.container)
    RelativeLayout container;
    @InjectView(R.id.pw_loading)
    ProgressWheel pw_loading;
    @Inject
    LoadingManager loadingManager;
    private List<PresentationMobile> presentationMobiles;
    private int selectedMobileId;
    private PresentationMobile selectedMobile;
    private MobileSpinnerAdapter spinnerAdapter;
    private Date endDate = new Date();
    private Date startDate = new Date();
    private int selectedIndex = 0;
    private static final String FRAGMENT_TAG_DATA_PROVIDER = "data provider";
    private static final String FRAGMENT_LIST_VIEW = "list view";
    private int expendedListPosition = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getDefaultMobileId();
        initToolbar();
        initDrawer(this, toolbar);
        setupUI();
        startServiceCalls();

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction().add(new TripDataProviderFragment(), FRAGMENT_TAG_DATA_PROVIDER).commit();
            getFragmentManager().beginTransaction().add(R.id.container, new RecyclerListViewFragment(), FRAGMENT_LIST_VIEW).commit();
        }
    }

    private void setupUI() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_WEEK, -7);
        startDate = cal.getTime();
        endDate = new Date();
        selectedIndex = 2;
        pw_loading.setBarColor(getResources().getColor(R.color.accent));
        pw_loading.setBarWidth((int) Utils.convertDpToPixel(4, this));
        pw_loading.setCircleRadius((int) Utils.convertDpToPixel(80, this));
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.onPause();
        mobilePresenter.onPause();
        EventBus.getDefault().unregister(this);

    }

    @Override
    public int onCreateViewId() {
        return R.layout.activity_trips;
    }

    protected List<Object> getModules() {
        return Arrays.<Object>asList(new TripsModule(this), new MobileMapModule(this));
    }

    private boolean supportsViewElevation() {
        return (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP);
    }

    private void initToolbar() {
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            elevationHandler.setDefaultElevation(toolbar);
            iv_right.setVisibility(View.VISIBLE);
            iv_right.setImageResource(R.drawable.ic_event_white);
            iv_right.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showDatePickerDialog();
                }
            });
        }
    }

    private void showDatePickerDialog() {
        MaterialDialog.Builder builder = new MaterialDialog.Builder(this);
        builder.title(R.string.filter)
                .items(R.array.dates)
                .itemsCallbackSingleChoice(selectedIndex, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        selectedIndex = which;
                        Calendar cal = Calendar.getInstance();
                        switch (which) {
                            case 0:
                                startDate = new DateTime().withTimeAtStartOfDay().toDate();
                                endDate = new Date();
                                break;
                            case 1:
                                cal.add(Calendar.DAY_OF_WEEK, -1);
                                startDate = cal.getTime();
                                endDate = new DateTime().withTimeAtStartOfDay().toDate();
                                break;
                            case 2:
                                cal.add(Calendar.DAY_OF_WEEK, -7);
                                startDate = cal.getTime();
                                endDate = new Date();
                                break;
                            case 3:
                                cal.add(Calendar.MONTH, -1);
                                startDate = cal.getTime();
                                endDate = new Date();
                                break;
                            case 4:
                                startActivityForResult(new Intent(TripsActivity.this, CalendarActivity.class), REQUEST_PICK_DATE);
                                return true;
                        }
                        getTrips();
                        return true;
                    }
                })
                .positiveText(R.string.ok)
                .negativeText(R.string.cancel)
                .build()
                .show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_PICK_DATE) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null && data.getExtras() != null) {
                    Bundle extras = data.getExtras();
                    if (extras.containsKey(CalendarActivity.END_DATE) && extras.containsKey(CalendarActivity.START_DATE)) {
                        SimpleMonthAdapter.CalendarDay startDate = (SimpleMonthAdapter.CalendarDay) extras.get(CalendarActivity.START_DATE);
                        SimpleMonthAdapter.CalendarDay endDate = (SimpleMonthAdapter.CalendarDay) extras.get(CalendarActivity.END_DATE);
                        this.startDate = startDate.getDate();
                        this.endDate = endDate.getDate();
                        getTrips();
                    }
                }
            }
        }
    }

    private void getTrips() {
        loadingManager.showLoading(getString(R.string.loading_message), true);
        presenter.getTrips(String.valueOf(selectedMobile.getMobile()), startDate, endDate);
    }

    private void getDefaultMobileId() {
        selectedMobileId = getAppPreferences().getInt(PrefConstants.DEFAULT_MOBILE_ID, 0);
    }

    private void startServiceCalls() {
        mobilePresenter.getMobiles();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onResume();
        mobilePresenter.onResume();
        EventBus.getDefault().register(this);
    }

    private void mobileSelected(PresentationMobile selected) {
        if (selected == null) {
            return;
        }


        selectedMobile = selected;
        presenter.getTrips(String.valueOf(selected.getMobile().intValue()), startDate, new Date());
        pw_loading.setVisibility(View.VISIBLE);
        container.setVisibility(View.GONE);
        tv_no_item.setVisibility(View.GONE);
    }

    @Override
    public void updateMobiles(List<PresentationMobile> presentationMobiles) {
        if (this.presentationMobiles == null || this.presentationMobiles.size() == 0) {
            Collections.reverse(presentationMobiles);
            this.presentationMobiles = presentationMobiles;
            sp_mobiles.setVisibility(View.VISIBLE);
            spinnerAdapter = new MobileSpinnerAdapter(this, presentationMobiles);
            sp_mobiles.setAdapter(spinnerAdapter);
            sp_mobiles.setOnItemSelectedListener(this);

            int defaultPosition = Utils.getDefaultMobilePosition(presentationMobiles, getAppPreferences());
            if (defaultPosition >= 0 && presentationMobiles.size() > defaultPosition) {
                sp_mobiles.setSelection(defaultPosition);
            }
        }
    }

    @Override
    public void updateMobilesError() {

    }

    @Override
    public void updateTrips(List<IntervalData> trips) {
        if (trips == null || trips.size() == 0) {
            container.setVisibility(View.GONE);
            tv_no_item.setVisibility(View.VISIBLE);
            pw_loading.setVisibility(View.GONE);
        } else {
            final DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyyMMddHHmmss");
            Collections.sort(trips, new Comparator<IntervalData>() {
                @Override
                public int compare(IntervalData lhs, IntervalData rhs) {
                    Date start = formatter.parseDateTime(rhs.startPosTimeStamp).toDate();
                    Date finish = formatter.parseDateTime(lhs.startPosTimeStamp).toDate();
                    if (start.getTime() < finish.getTime()) {
                        return -1;
                    } else if (start.getTime() == finish.getTime()) {
                        return 0;
                    } else {
                        return 1;
                    }
                }
            });
            container.setVisibility(View.VISIBLE);
            tv_no_item.setVisibility(View.GONE);
            pw_loading.setVisibility(View.GONE);
            loadingManager.dismissLoading();
            final Fragment profviderFragment = getFragmentManager().findFragmentByTag(FRAGMENT_TAG_DATA_PROVIDER);
            ((TripDataProviderFragment) profviderFragment).updateTrips(trips);
            final Fragment fragment = getFragmentManager().findFragmentByTag(FRAGMENT_LIST_VIEW);
            ((RecyclerListViewFragment) fragment).refreshAdapter();
        }
    }

    @Override
    public void updateTripsError() {
        loadingManager.dismissLoading();
        pw_loading.setVisibility(View.GONE);
        container.setVisibility(View.GONE);
        tv_no_item.setVisibility(View.VISIBLE);
    }

    @Override
    public void updateLightPositions(List<DataLightPosition> lightPositions) {
        loadingManager.dismissLoading();
        if (lightPositions != null) {
            lights = lightPositions;
            EventBus.getDefault().post(new TripLightPointsEvent(lightPositions, expendedListPosition));
        }
    }

    @Override
    public void updateLightPositionsError() {
        loadingManager.dismissLoading();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        PresentationMobile selected = spinnerAdapter.getItem(position);
        mobileSelected(selected);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void onEvent(TripDetailEvent event) {
        Intent intent = new Intent(TripsActivity.this, TripDetailActivity.class);
        event.setMobileId(String.valueOf(selectedMobile.getMobile()));
        intent.putExtra(TripDetailActivity.EXTRA_DETAIL, event);

        TripsBundle bundle = new TripsBundle();
        String startTime = event.getIntervalData().startPosTimeStamp;
        String endTime = event.getIntervalData().stopPosTimeStamp;
        bundle.endDate = endTime;
        bundle.startDate = startTime;
        bundle.mobileId = String.valueOf(selectedMobile.getMobile());
        Gson gson = new Gson();
        String tripData = gson.toJson(bundle);

        intent.putExtra(TripDetailActivity.EXTRA_QUERY, tripData);
        startActivity(intent);
    }

    public void onEvent(TripExpandableItemAdapter.TripExpanded event) {
        if (event != null && event.data != null) {
            if (selectedMobile != null) {
                loadingManager.showLoading(getString(R.string.please_wait), true);
                expendedListPosition = event.listPosition;
                IntervalData data = event.data;
                presenter.getLightPositions(String.valueOf(selectedMobile.getMobile()), data.startPosTimeStamp, data.stopPosTimeStamp);
                presenter.getLightPositions(String.valueOf(selectedMobile.getMobile()), data.startPosTimeStamp, data.stopPosTimeStamp);
//                mTripBundle =
            }
        }
    }

    public AbstractExpandableDataProvider getDataProvider() {
        final Fragment fragment = getFragmentManager().findFragmentByTag(FRAGMENT_TAG_DATA_PROVIDER);
        return ((TripDataProviderFragment) fragment).getDataProvider();
    }
}
