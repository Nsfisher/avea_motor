package me.bt.motor.ui.items;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.carlosdelachica.easyrecycleradapters.adapter.EasyViewHolder;

import butterknife.ButterKnife;
import butterknife.InjectView;
import me.bt.avea.presentation.model.PresentationMobile;
import me.bt.motor.R;

public class MobileViewHolder extends EasyViewHolder<PresentationMobile> {

  @InjectView(R.id.tv_middle) TextView tv_middle;
  @InjectView(R.id.iv_check) ImageView iv_check;

  public MobileViewHolder(Context context, ViewGroup parent) {
    super(context, parent, R.layout.item_mobile);
    ButterKnife.inject(this, itemView);
  }

  @Override public void bindTo(PresentationMobile mobile) {
    tv_middle.setText(mobile.getAlias());
    if (mobile.isChecked()) {
      iv_check.setVisibility(View.VISIBLE);
    } else {
      iv_check.setVisibility(View.GONE);
    }
  }
}
