package me.bt.motor.network;

import me.bt.base.util.Constants;

/**
 * Created by yenerciftci on 20/08/15.
 */
public class CheckAppVersionRequest extends BaseRequest {

    @Override
    public String toString() {
        return Constants.APP_URL + CHECK_APP_VER;
    }
}
