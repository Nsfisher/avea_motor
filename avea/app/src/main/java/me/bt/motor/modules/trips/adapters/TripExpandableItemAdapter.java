package me.bt.motor.modules.trips.adapters;

import android.app.FragmentManager;
import android.content.Context;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.h6ah4i.android.widget.advrecyclerview.expandable.RecyclerViewExpandableItemManager;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractExpandableItemAdapter;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractExpandableItemViewHolder;

import de.greenrobot.event.EventBus;
import me.bt.avea.domain.entities.IntervalData;
import me.bt.motor.modules.trips.TripsActivity;
import me.bt.motor.modules.trips.common.TripDetailEvent;
import me.bt.motor.modules.trips.common.TripLightPointsEvent;
import me.bt.motor.modules.trips.common.compat.MorphButtonCompat;
import me.bt.motor.modules.trips.common.data.AbstractExpandableDataProvider;
import me.bt.motor.modules.trips.common.utils.ViewUtils;
import me.bt.motor.util.Utils;
import me.bt.motor.R;

public class TripExpandableItemAdapter
        extends AbstractExpandableItemAdapter<TripExpandableItemAdapter.GroupViewHolder, TripExpandableItemAdapter.MapViewHolder> {
    private static final String TAG = "MyExpandableItemAdapter";
    private final FragmentManager fragmentManager;
    private final Context context;
    private RecyclerViewExpandableItemManager itemManager;
    private AbstractExpandableDataProvider mProvider;


    public TripExpandableItemAdapter(AbstractExpandableDataProvider dataProvider, FragmentManager fragmentManager, Context context,
                                     RecyclerViewExpandableItemManager itemManager) {
        this.fragmentManager = fragmentManager;
        this.itemManager = itemManager;
        mProvider = dataProvider;
        this.context = context;
        setHasStableIds(true);
    }


    public void updateLightPoints(TripLightPointsEvent event) {
        if (mProvider != null && mProvider.getGroupCount() > event.expendedListPosition) {

            try {
                if (itemManager != null) {
                    for (int k = 0; k < mProvider.getGroupCount(); k++) {
                        if (itemManager.isGroupExpanded(k) && k != event.expendedListPosition) {
                            itemManager.collapseGroup(k);
                        }
                    }
                }
            } catch (Exception ex) {

            }
            AbstractExpandableDataProvider.ChildData item = mProvider.getChildItem(event.expendedListPosition, 0);
            item.setLightPositions(event.dataLightPositionList);
            notifyItemChanged(event.expendedListPosition);
        }
    }

    public OnExpandListener getOnExpandListener() {
        return onExpandListener;
    }

    public void setOnExpandListener(OnExpandListener onExpandListener) {
        this.onExpandListener = onExpandListener;
    }

    public class TripExpanded {
        TripExpanded(IntervalData data, int listPosition) {
            this.data = data;
            this.listPosition = listPosition;
        }

        public IntervalData data = null;
        public int listPosition = 0;
    }

    @Override
    public int getGroupCount() {
        return mProvider.getGroupCount();
    }

    @Override
    public int getChildCount(int groupPosition) {
        return mProvider.getChildCount(groupPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return mProvider.getGroupItem(groupPosition).getGroupId();
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return mProvider.getChildItem(groupPosition, childPosition).getChildId();
    }

    @Override
    public int getGroupItemViewType(int groupPosition) {
        return 0;
    }

    @Override
    public int getChildItemViewType(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public GroupViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View v = inflater.inflate(R.layout.item_trips_header, parent, false);
        return new GroupViewHolder(v);
    }

    @Override
    public MapViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View v = inflater.inflate(R.layout.item_trips_map, parent, false);
        return new MapViewHolder(v, fragmentManager);
    }


    @Override
    public void onBindGroupViewHolder(GroupViewHolder holder, int groupPosition, int viewType) {
        final AbstractExpandableDataProvider.BaseData item = mProvider.getGroupItem(groupPosition);
        IntervalData data = item.getIntervalData();
        holder.itemView.setClickable(true);
        final int expandState = holder.getExpandStateFlags();

        if (data == null) {
            return;
        }
        if (data.startPosTimeStamp != null) {
            holder.tv_start.setText(data.startLbsLocation);
        }

        if (data.stopPosTimeStamp != null) {
            holder.tv_finish.setText(data.stopLbsLocation);
        }

        if (data.deltaTime != null) {
            try {
                double totalTime = Double.parseDouble(data.deltaTime);
                int hour = (int) (totalTime / 60);
                int minute = (int) (totalTime % 60);
                StringBuilder builder = new StringBuilder();
                if (hour > 0) {
                    builder.append(hour);
                    builder.append(":");
                }
                if (hour > 0 && minute < 10) {
                    builder.append("0");
                }
                builder.append(minute);
                holder.tv_total_time.setText(builder.toString());
            } catch (Exception ex) {
                holder.tv_total_time.setText(data.deltaTime);
            }
        }

        if (data.speedAvg != null) {
            int average = 0;
            try {
                average = Utils.round(Double.parseDouble(data.speedAvg));
            } catch (Exception e) {

            }
            holder.tv_avg_speed.setText(String.valueOf(average));
        }

        if (data.eventCount != null) {
            holder.tv_ride.setText(data.eventCount);
        }

        if (data.distanceGps != null) {
            double kmDistance = Double.parseDouble(data.distanceGps) / 1000;
            holder.tv_distance.setText(String.valueOf(Utils.round(kmDistance)));
        }

        if (data.startPosTimeStamp != null) {
            holder.tv_date.setText(Utils.formatDate(data.startPosTimeStamp));
        }

        if ((expandState & RecyclerViewExpandableItemManager.STATE_FLAG_IS_UPDATED) != 0) {

        }
    }

    @Override
    public void onBindChildViewHolder(MapViewHolder holder, final int groupPosition, final int childPosition, int viewType) {
        final AbstractExpandableDataProvider.ChildData item = mProvider.getChildItem(groupPosition, childPosition);
        final AbstractExpandableDataProvider.GroupData groupItem = mProvider.getGroupItem(groupPosition);

        int bgResId;
        bgResId = R.drawable.bg_item_normal_state;
        holder.mContainer.setBackgroundResource(bgResId);
        final IntervalData data = item.getIntervalData();

        if (holder.iv_detail != null) {
            holder.iv_detail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EventBus.getDefault().post(new TripDetailEvent(groupItem.getIntervalData(), item.getLightPositions()));
                }
            });
        }
        if (holder.mv_map != null) {
            holder.mv_map.onCreate(null);
            holder.mv_map.getMap().getUiSettings().setMapToolbarEnabled(false);
            holder.mv_map.getMap().setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                @Override
                public void onMapClick(LatLng latLng) {

                }
            });
            holder.mv_map.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });

            holder.mv_map.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    try {
                        Thread.sleep(100);
                        final AbstractExpandableDataProvider.ChildData item = mProvider.getChildItem(groupPosition, childPosition);
                        if (item.getLightPositions() == null) {
                            item.setLightPositions(TripsActivity.lights);
                        }
                        Utils.setupMapWithPoints(googleMap, item.getLightPositions(), context, item.getIntervalData());
//                        notifyDataSetChanged();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    Log.e("expand", "map");
                }
            });

            Utils.setupMapWithPoints(holder.mv_map.getMap(), item.getLightPositions(), context, item.getIntervalData());

        }
    }

    @Override
    public boolean onCheckCanExpandOrCollapseGroup(GroupViewHolder holder, int groupPosition, int x, int y, boolean expand) {
        if (mProvider.getGroupItem(groupPosition).isPinnedToSwipeLeft()) {
            return false;
        }

        if (!(holder.itemView.isEnabled() && holder.itemView.isClickable())) {
            return false;
        }

        final View containerView = holder.mContainer;
        final View dragHandleView = holder.mDragHandle;

        final int offsetX = containerView.getLeft() + (int) (ViewCompat.getTranslationX(containerView) + 0.5f);
        final int offsetY = containerView.getTop() + (int) (ViewCompat.getTranslationY(containerView) + 0.5f);
        boolean canExpand = !ViewUtils.hitTest(dragHandleView, x - offsetX, y - offsetY);

        if (expand && canExpand) {
            final AbstractExpandableDataProvider.GroupData item = mProvider.getGroupItem(groupPosition);
            IntervalData data = item.getIntervalData();
            EventBus.getDefault().post(new TripExpanded(data, groupPosition));
            //TODO hakan close others
            Log.e("expand", "canExpand");
            if (getOnExpandListener() != null) {
                getOnExpandListener().onExpanded(groupPosition);
                return false;
            }
        }
        return canExpand;
    }

    private OnExpandListener onExpandListener;

    public interface OnExpandListener {
        void onExpanded(int index);
    }

    public static abstract class MyBaseViewHolder extends AbstractExpandableItemViewHolder {
        public ViewGroup mContainer;
        public View mDragHandle;

        public MyBaseViewHolder(View v) {
            super(v);
            mContainer = (ViewGroup) v.findViewById(R.id.container);
            mDragHandle = v.findViewById(R.id.drag_handle);
            mDragHandle.setVisibility(View.GONE);
        }
    }

    public static class GroupViewHolder extends MyBaseViewHolder {
        public MorphButtonCompat mMorphButton;
        public TextView tv_date;
        public TextView tv_start;
        public TextView tv_finish;
        public TextView tv_total_time;
        public TextView tv_distance;
        public TextView tv_ride;
        public TextView tv_avg_speed;

        public GroupViewHolder(View v) {
            super(v);
            mMorphButton = new MorphButtonCompat(v.findViewById(R.id.indicator));
            tv_date = (TextView) v.findViewById(R.id.tv_date);
            tv_start = (TextView) v.findViewById(R.id.tv_start);
            tv_finish = (TextView) v.findViewById(R.id.tv_finish);
            tv_total_time = (TextView) v.findViewById(R.id.tv_total_time);
            tv_distance = (TextView) v.findViewById(R.id.tv_distance);
            tv_ride = (TextView) v.findViewById(R.id.tv_ride);
            tv_avg_speed = (TextView) v.findViewById(R.id.tv_avg_speed);
        }
    }

    public static class MapViewHolder extends MyBaseViewHolder {
        public FrameLayout fl_map;
        public ImageView iv_detail;
        public MapView mv_map;

        public MapViewHolder(View v, FragmentManager fragmentManager) {
            super(v);
            fl_map = (FrameLayout) v.findViewById(R.id.fl_map);
            mv_map = (MapView) v.findViewById(R.id.mv_map);
            iv_detail = (ImageView) v.findViewById(R.id.iv_detail);
        }
    }
}
