package me.bt.motor.di;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import me.bt.avea.domain.repository.AddUserRepository;
import me.bt.avea.domain.repository.GeneralRepository;
import me.bt.avea.domain.repository.LightEventsRepository;
import me.bt.avea.domain.repository.LightPositionsRepository;
import me.bt.avea.domain.repository.MobilesRepository;
import me.bt.avea.domain.repository.RegisterRepository;
import me.bt.avea.domain.repository.SuggestionRepository;
import me.bt.avea.domain.repository.TripsRepository;
import me.bt.avea.repository.adduser.AddUserRepositoryImp;
import me.bt.avea.repository.adduser.datasources.AddUserNetworkDataSource;
import me.bt.avea.repository.general.GeneralRepositoryImp;
import me.bt.avea.repository.general.datasources.GeneralNetworkDataSource;
import me.bt.avea.repository.lightevents.LightEventsNetworkDataSource;
import me.bt.avea.repository.lightevents.LightPositionsNetworkDataSource;
import me.bt.avea.repository.lightevents.datasources.LightEventsRepositoryImp;
import me.bt.avea.repository.lightevents.datasources.LightPositionsRepositoryImp;
import me.bt.avea.repository.mobiles.MobilesRepositoryImp;
import me.bt.avea.repository.mobiles.datasources.MobileBddDataSource;
import me.bt.avea.repository.mobiles.datasources.MobilesNetworkDataSource;
import me.bt.avea.repository.register.RegisterRepositoryImp;
import me.bt.avea.repository.register.datasources.RegisterNetworkDataSource;
import me.bt.avea.repository.suggestion.datasource.SuggestionNetworkDataSource;
import me.bt.avea.repository.suggestion.datasource.SuggestionRepositoryImp;
import me.bt.avea.repository.trips.TripsNetworkDataSource;
import me.bt.avea.repository.trips.datasources.TripsRepositoryImp;

@Module(
        complete = false,
        library = true)
public class AveaRepositoryModule {

    @Provides
    @Singleton
    RegisterRepository provideRegisterRepository(RegisterNetworkDataSource networkDataSource) {
        return new RegisterRepositoryImp(networkDataSource);
    }

    @Provides
    @Singleton
    MobilesRepository provideMobileRepository(MobilesNetworkDataSource networkDataSource,
                                              MobileBddDataSource bddDataSource) {
        return new MobilesRepositoryImp(networkDataSource, bddDataSource);
    }

    @Provides
    @Singleton
    AddUserRepository provideAddUserRepository(AddUserNetworkDataSource networkDataSource) {
        return new AddUserRepositoryImp(networkDataSource);
    }

    @Provides
    @Singleton
    SuggestionRepository provideSuggestionRepository(SuggestionNetworkDataSource networkDataSource) {
        return new SuggestionRepositoryImp(networkDataSource);
    }

    @Provides
    @Singleton
    LightEventsRepository provideLightEventsRepository(LightEventsNetworkDataSource networkDataSource) {
        return new LightEventsRepositoryImp(networkDataSource);
    }

    @Provides
    @Singleton
    TripsRepository provideTripsRepository(TripsNetworkDataSource networkDataSource) {
        return new TripsRepositoryImp(networkDataSource);
    }

    @Provides
    @Singleton
    GeneralRepository provideGeneralRepositoryImp(GeneralNetworkDataSource networkDataSource) {
        return new GeneralRepositoryImp(networkDataSource);
    }

    @Provides
    @Singleton
    LightPositionsRepository provideLightPositionsRepository(LightPositionsNetworkDataSource networkDataSource) {
        return new LightPositionsRepositoryImp(networkDataSource);
    }
}
