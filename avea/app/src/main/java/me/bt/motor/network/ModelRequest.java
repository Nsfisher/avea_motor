package me.bt.motor.network;

import me.bt.base.util.Constants;

/**
 * Created by yenerciftci on 30/07/15.
 */
public class ModelRequest extends BaseRequest {
    public String markaId;

    @Override
    public String toString() {
        return Constants.APP_URL + MODELS
                + "token=" + token + "&markaId=" + markaId;
    }
}
