package me.bt.motor.ui.items;

import android.content.Context;
import android.content.res.Resources;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.carlosdelachica.easyrecycleradapters.adapter.EasyViewHolder;
import me.bt.avea.domain.entities.Region;
import me.bt.avea.presentation.model.PresentationMobile;
import me.bt.motor.R;

public class DefaultMobileViewHolder extends EasyViewHolder<PresentationMobile> {

  private final Resources resources;
  @InjectView(R.id.tv_name) TextView tv_name;
  @InjectView(R.id.rb_select) RadioButton rb_select;

  public DefaultMobileViewHolder(Context context, ViewGroup parent, Resources resources) {
    super(context, parent, R.layout.item_default_mobile);
    ButterKnife.inject(this, itemView);
    this.resources = resources;
  }

  @Override public void bindTo(PresentationMobile presentationMobile) {
    if (presentationMobile == null) {
      return;
    }
    if (presentationMobile.alias != null) {
      tv_name.setText(presentationMobile.alias);
    }
    if(presentationMobile.checked){
      rb_select.setChecked(true);
    }
  }
}
