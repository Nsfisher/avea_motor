package me.bt.motor.modules.trips.common;

import java.util.List;

import me.bt.avea.domain.entities.DataLightPosition;

public class TripLightPointsEvent {
    public List<DataLightPosition> dataLightPositionList;
    public int expendedListPosition = 0;

    public TripLightPointsEvent(List<DataLightPosition> dataLightPositionList, int expendedListPosition) {
        this.dataLightPositionList = dataLightPositionList;
        this.expendedListPosition = expendedListPosition;
    }
}
