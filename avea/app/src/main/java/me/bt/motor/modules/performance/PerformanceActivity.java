package me.bt.motor.modules.performance;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import butterknife.InjectView;
import me.bt.avea.domain.entities.EventScore;
import me.bt.avea.domain.entities.EventScoreLg;
import me.bt.avea.domain.entities.LightTrips;
import me.bt.avea.domain.interactions.general.GeneralInteractor;
import me.bt.avea.domain.interactions.general.events.GeneralEvent;
import me.bt.avea.presentation.general.GeneralPresenter;
import me.bt.avea.presentation.general.GeneralView;
import me.bt.avea.presentation.map.MobileMapPresenter;
import me.bt.avea.presentation.map.MobileMapView;
import me.bt.avea.presentation.model.PresentationMobile;
import me.bt.base.ui.BaseActivity;
import me.bt.base.ui.elevation.ElevationHandler;
import me.bt.base.ui.loading.LoadingManager;
import me.bt.motor.R;
import me.bt.motor.modules.calendar.CalendarActivity;
import me.bt.motor.modules.general.GeneralModule;
import me.bt.motor.modules.map.MobileMapModule;
import me.bt.motor.modules.map.adapters.MobileSpinnerAdapter;
import me.bt.motor.ui.widget.MarkViewY;
import me.bt.motor.util.Utils;
import me.bt.opensource.andexert.calendarlistview.library.SimpleMonthAdapter;

public class PerformanceActivity extends BaseActivity implements MobileMapView, GeneralView, AdapterView.OnItemSelectedListener {

    @Inject
    MobileMapPresenter mobilePresenter;
    @Inject
    GeneralPresenter generalPresenter;
    @Inject
    LoadingManager loadingManager;
    @InjectView(R.id.toolbar)
    Toolbar toolbar;
    @InjectView(R.id.sp_mobiles)
    Spinner sp_mobiles;
    @InjectView(R.id.iv_right)
    ImageView iv_right;
    @InjectView(R.id.tv_interval)
    TextView tv_interval;
    @InjectView(R.id.tv_total_time)
    TextView tv_total_time;
    @InjectView(R.id.tv_distance)
    TextView tv_distance;
    @InjectView(R.id.tv_ride)
    TextView tv_ride;
    @InjectView(R.id.tv_avg_speed)
    TextView tv_avg_speed;
    @InjectView(R.id.mv_score)
    MarkViewY mv_score;
    @InjectView(R.id.ll_detail)
    LinearLayout ll_detail;
    @InjectView(R.id.cv_score_root)
    CardView cv_score_root;
    @Inject
    ElevationHandler elevationHandler;
    private MobileSpinnerAdapter spinnerAdapter;
    private static final int REQUEST_PICK_DATE = 1238;
    private int selectedIndex = 1;
    private Date endDate = new Date();
    private Date startDate = new Date();
    private List<PresentationMobile> presentationMobiles;
    private PresentationMobile selectedMobile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initToolbar();
        initDrawer(this, toolbar);
        setupUI();
        startServiceCalls();
    }

    private void setupUI() {
        Calendar cal = Calendar.getInstance();
        Calendar endCal = Calendar.getInstance();
//        cal.add(Calendar.DAY_OF_WEEK, -7);
//        startDate = cal.getTime();
//
//
//        endCal.set(Calendar.HOUR_OF_DAY, 0);
//        endCal.set(Calendar.MINUTE, 0);
//        endCal.set(Calendar.SECOND, 0);
//
//
//        cal.set(Calendar.HOUR_OF_DAY, 0);
//        cal.set(Calendar.MINUTE, 0);
//        cal.set(Calendar.SECOND, 0);
//        endDate = endCal.getTime();


        endCal.set(Calendar.HOUR_OF_DAY, 0);
        endCal.set(Calendar.MINUTE, 0);
        endCal.set(Calendar.SECOND, 0);

        cal.add(Calendar.DAY_OF_WEEK, -7);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        startDate = cal.getTime();
        endDate = new Date();

    }

    private void initToolbar() {
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            elevationHandler.setDefaultElevation(toolbar);
            iv_right.setVisibility(View.VISIBLE);
            iv_right.setImageResource(R.drawable.ic_event_white);
            iv_right.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showDatePickerDialog();
                }
            });
        }
    }

    private void showDatePickerDialog() {
        MaterialDialog.Builder builder = new MaterialDialog.Builder(this);
        builder.title(R.string.filter)
                .items(R.array.dates_performance)
                .itemsCallbackSingleChoice(selectedIndex, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        selectedIndex = which;
                        Calendar cal = Calendar.getInstance();
                        Calendar endCal = Calendar.getInstance();
                        switch (which) {
                            case 0:
                                cal.add(Calendar.DAY_OF_WEEK, -1);


                                endCal.set(Calendar.HOUR_OF_DAY, 0);
                                endCal.set(Calendar.MINUTE, 0);
                                endCal.set(Calendar.SECOND, 0);

                                cal.set(Calendar.HOUR_OF_DAY, 0);
                                cal.set(Calendar.MINUTE, 0);
                                cal.set(Calendar.SECOND, 0);

                                startDate = cal.getTime();
                                endDate = endCal.getTime();
                                break;
                            case 1:


                                endCal.set(Calendar.HOUR_OF_DAY, 0);
                                endCal.set(Calendar.MINUTE, 0);
                                endCal.set(Calendar.SECOND, 0);

                                cal.add(Calendar.DAY_OF_WEEK, -7);
                                cal.set(Calendar.HOUR_OF_DAY, 0);
                                cal.set(Calendar.MINUTE, 0);
                                cal.set(Calendar.SECOND, 0);
                                startDate = cal.getTime();
                                endDate = new Date();
                                break;
                            case 2:
                                cal.add(Calendar.MONTH, -1);

                                endCal.set(Calendar.HOUR_OF_DAY, 0);
                                endCal.set(Calendar.MINUTE, 0);
                                endCal.set(Calendar.SECOND, 0);

                                cal.set(Calendar.HOUR_OF_DAY, 0);
                                cal.set(Calendar.MINUTE, 0);
                                cal.set(Calendar.SECOND, 0);
                                startDate = cal.getTime();
                                endDate = new Date();
                                break;
                            case 3:
                                startActivityForResult(new Intent(PerformanceActivity.this, CalendarActivity.class), REQUEST_PICK_DATE);
                                return true;
                        }
                        getPerformanceFromServer();
                        return true;
                    }
                })
                .positiveText(R.string.ok)
                .negativeText(R.string.cancel)
                .build()
                .show();
    }


    private void startServiceCalls() {
        mobilePresenter.getMobiles();
    }

    @Override
    public int onCreateViewId() {
        return R.layout.activity_performance;
    }

    protected List<Object> getModules() {
        return Arrays.<Object>asList(new MobileMapModule(this), new GeneralModule(this));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_PICK_DATE) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null && data.getExtras() != null) {
                    Bundle extras = data.getExtras();
                    if (extras.containsKey(CalendarActivity.END_DATE) && extras.containsKey(CalendarActivity.START_DATE)) {
                        SimpleMonthAdapter.CalendarDay startDate = (SimpleMonthAdapter.CalendarDay) extras.get(CalendarActivity.START_DATE);
                        SimpleMonthAdapter.CalendarDay endDate = (SimpleMonthAdapter.CalendarDay) extras.get(CalendarActivity.END_DATE);
                        this.startDate = startDate.getDate();
                        this.endDate = endDate.getDate();
                        getPerformanceFromServer();
                    }
                }
            }
        }
    }

    @Override
    public void updateMobiles(List<PresentationMobile> presentationMobiles) {
        this.presentationMobiles = presentationMobiles;
        sp_mobiles.setVisibility(View.VISIBLE);
        spinnerAdapter = new MobileSpinnerAdapter(this, presentationMobiles);
        sp_mobiles.setAdapter(spinnerAdapter);
        sp_mobiles.setOnItemSelectedListener(this);

        int defaultPosition = Utils.getDefaultMobilePosition(presentationMobiles, getAppPreferences());
        if (defaultPosition >= 0 && presentationMobiles.size() > defaultPosition) {
            sp_mobiles.setSelection(defaultPosition);
        }
    }


    @Override
    public void updateMobilesError() {

    }

    @Override
    protected void onPause() {
        super.onPause();
        mobilePresenter.onPause();
        generalPresenter.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mobilePresenter.onResume();
        generalPresenter.onResume();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        selectedMobile = presentationMobiles.get(position);
        saveSelectedIdToDisk(selectedMobile);
        getPerformanceFromServer();
    }

    private void getPerformanceFromServer() {
        loadingManager.showLoading(getString(R.string.please_wait));

        generalPresenter.getLightTrips(String.valueOf(selectedMobile.getMobile().intValue()), startDate, endDate);
        tv_interval.setText(Utils.getLocalDateString(startDate) + " - " + Utils.getLocalDateStringWithYear(endDate));
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void generalResponse(GeneralEvent response) {
        if (response.getRequestType() == GeneralInteractor.REQUEST_LIGHTTRIPS) {
            if (response.getError() == null) {
                LightTrips lightTrips = (LightTrips) response.getData();
                tv_avg_speed.setText(String.format("%.1f", Double.parseDouble(lightTrips.averageSpeed)));

                double kmDistance = Double.parseDouble(lightTrips.distanceTotal) / 1000;
                tv_distance.setText(String.valueOf((int) kmDistance));

                tv_ride.setText(lightTrips.count);
                try {
                    double totalTime = Double.parseDouble(lightTrips.timeTotal);
                    int hour = (int) (totalTime / 60);
                    int minute = (int) (totalTime % 60);
                    StringBuilder builder = new StringBuilder();
                    if (hour > 0) {
                        builder.append(hour);
                        builder.append(":");
                    }
                    builder.append(minute);
                    tv_total_time.setText(builder.toString());
                } catch (Exception ex) {
                    tv_total_time.setText(lightTrips.timeTotal);
                }

                generalPresenter.getEventScore(String.valueOf(selectedMobile.getMobile()), startDate, endDate);
            } else {
                loadingManager.dismissLoading();
            }
        } else if (response.getRequestType() == GeneralInteractor.REQUEST_EVENT_SCORE) {
            loadingManager.dismissLoading();
            EventScore eventScore = (EventScore) response.getData();
            if (response.getError() == null) {
                int score = (int) Double.parseDouble(eventScore.score);

//                } else {
//                    cv_score_root.setVisibility(View.VISIBLE);
//                }
                cv_score_root.setVisibility(View.VISIBLE);
                mv_score.setVisibility(View.VISIBLE);
                mv_score.setMark(score);
//                    mv_score.setMark(0);
//                    mv_score.setMark(0);

//                }
            }
            ll_detail.removeAllViews();
            int count = 0;

            if (eventScore != null) {
                for (EventScoreLg eventScoreLg : eventScore.lg) {
                    try {
                        count += Integer.parseInt(eventScoreLg.count);
                    } catch (Exception e) {
                        count += 0;
                    }

                    RelativeLayout rl_root = (RelativeLayout) getLayoutInflater().inflate(R.layout.item_performance, null);
                    TextView tv_description = (TextView) rl_root.findViewById(R.id.tv_description);
                    TextView tv_order_number = (TextView) rl_root.findViewById(R.id.tv_order_number);
                    TextView tv_times = (TextView) rl_root.findViewById(R.id.tv_times);
                    tv_description.setText(eventScoreLg.eventCode);
                    tv_order_number.setText(eventScoreLg.exceptionNo);
                    tv_times.setText(eventScoreLg.count);
                    ll_detail.addView(rl_root);
                }
            }
            tv_ride.setText(String.valueOf(count));
        }
    }
}
