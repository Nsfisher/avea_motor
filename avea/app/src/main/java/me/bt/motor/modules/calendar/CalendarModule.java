package me.bt.motor.modules.calendar;

import dagger.Module;
import me.bt.base.di.ActivityModule;

@Module(
    addsTo = ActivityModule.class,
    library = true,
    injects = CalendarActivity.class) public class CalendarModule {

  public CalendarModule() {
  }
}

