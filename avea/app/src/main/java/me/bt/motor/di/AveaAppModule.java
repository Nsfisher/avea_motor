package me.bt.motor.di;

import dagger.Module;
import me.bt.motor.AveaApplication;
import me.bt.base.di.AppModule;

@Module(
    addsTo = AppModule.class,
    injects = AveaApplication.class,
    library = true) public class AveaAppModule {

}
