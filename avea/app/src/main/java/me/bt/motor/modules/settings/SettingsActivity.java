package me.bt.motor.modules.settings;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import butterknife.InjectView;
import butterknife.OnClick;
import me.bt.avea.data.preferences.PrefConstants;
import me.bt.avea.domain.entities.Terms;
import me.bt.avea.domain.entities.TermsParameter;
import me.bt.avea.domain.interactions.general.GeneralInteractor;
import me.bt.avea.domain.interactions.general.events.GeneralEvent;
import me.bt.avea.presentation.general.GeneralPresenter;
import me.bt.avea.presentation.general.GeneralView;
import me.bt.base.ui.BaseActivity;
import me.bt.base.ui.elevation.ElevationHandler;
import me.bt.base.ui.loading.LoadingManager;
import me.bt.base.util.AndroidUtils;
import me.bt.motor.BuildConfig;
import me.bt.motor.R;
import me.bt.motor.modules.general.GeneralModule;
import me.bt.motor.modules.login.LoginActivity;
import me.bt.motor.modules.newmobile.NewMobileActivity;

public class SettingsActivity extends BaseActivity implements GeneralView {
    @Inject
    ElevationHandler elevationHandler;
    @InjectView(R.id.toolbar)
    Toolbar toolbar;
    @InjectView(R.id.tv_app_version)
    TextView tv_app_version;
    @InjectView(R.id.tv_default_mobile)
    TextView tv_default_mobile;
    @InjectView(R.id.sw_traffic)
    Switch sw_traffic;
    @InjectView(R.id.rl_log_out)
    RelativeLayout rl_log_out;
    @InjectView(R.id.rl_default_mobile)
    RelativeLayout rl_default_mobile;
    @InjectView(R.id.rl_area_definations)
    RelativeLayout rl_area_definations;
    @InjectView(R.id.rl_notifications)
    RelativeLayout rl_notifications;
    @InjectView(R.id.rl_add_new_mobile)
    RelativeLayout rl_add_new_mobile;
    @Inject
    GeneralPresenter generalPresenter;
    @Inject
    LoadingManager loadingManager;
    private Terms terms;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupUI();
        initToolbar();
        initDrawer(this, toolbar);

        boolean isTraffic = getAppPreferences().getBoolean(PrefConstants.TRAFFIC_ENABLED, false);
        sw_traffic.setChecked(isTraffic);

    }

    @OnClick(R.id.rl_log_out)
    void onLogoutClicked() {
        loadingManager.showLoading(R.string.loading_message);
        generalPresenter.logout();
    }

    @OnClick(R.id.rl_default_mobile)
    void onDefaultMobileClicked() {
        Intent intent = new Intent(this, DefaultMobileActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.rl_notifications)
    void onNotificationsClicked() {
        Intent intent = new Intent(this, SettingsNotificationsActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.rl_area_definations)
    void onAreaDefinationsClicked() {
        Intent intent = new Intent(this, AreaDefinitionActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.rl_add_new_mobile)
    void onAddNewMobile() {
        Intent intent = new Intent(this, NewMobileActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.rl_legal)
    void onLegalClicked() {
        if (terms == null) {
            loadingManager.showLoading(R.string.loading_message);
            generalPresenter.getTerms();
        } else {
            openLegalPage();
        }
    }

    private void openLegalPage() {
        if (terms != null) {
            for (TermsParameter parameter : terms.parameter) {
                if (parameter.name.startsWith("CONDITIONS_")) {
                    AndroidUtils.openUrlWithActivity(parameter.data, this);
                    break;
                }
            }
        }
    }

    @Override
    public int onCreateViewId() {
        return R.layout.activity_settings;
    }

    protected List<Object> getModules() {
        return Arrays.<Object>asList(new SettingsModule(), new GeneralModule(this));
    }

    private void setupUI() {
        boolean trafficEnabled = getAppPreferences().getBoolean(PrefConstants.TRAFFIC_ENABLED, false);
        String defaultMobile = getAppPreferences().getString(PrefConstants.DEFAULT_MOBILE_ALIAS, "");

        sw_traffic.setChecked(trafficEnabled);
        tv_app_version.setText(BuildConfig.VERSION_NAME);
        tv_default_mobile.setText(defaultMobile);


        setupListeners();
    }

    @Override
    protected void onResume() {
        super.onResume();
        generalPresenter.onResume();
        tv_default_mobile.setText(getAppPreferences().getString(PrefConstants.DEFAULT_MOBILE_ALIAS, ""));
    }

    @Override
    protected void onPause() {
        super.onPause();
        generalPresenter.onPause();
    }

    private void setupListeners() {
        sw_traffic.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                getAppPreferences().put(PrefConstants.TRAFFIC_ENABLED, isChecked);
            }
        });
    }

    private void initToolbar() {
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            elevationHandler.setDefaultElevation(toolbar);
            toolbar.setTitle(R.string.activity_settings);
        }
    }

    @Override
    public void generalResponse(GeneralEvent response) {
        loadingManager.dismissLoading();
        if (response.getRequestType() == GeneralInteractor.REQUEST_TERMS) {
            if (response.getError() == null) {
                terms = (Terms) response.getData();
                openLegalPage();
            }
        } else if (response.getRequestType() == GeneralInteractor.REQUEST_LOGOUT) {
            if (response.getError() == null) {
                getAppPreferences().put(PrefConstants.TOKEN, "");
                Intent intent = new Intent(this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        }
    }
}

