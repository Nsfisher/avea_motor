package me.bt.motor.modules.settings;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import butterknife.InjectView;
import me.bt.avea.data.preferences.PrefConstants;
import me.bt.avea.domain.entities.NotificationList;
import me.bt.avea.domain.entities.SettingsNotification;
import me.bt.avea.domain.interactions.general.GeneralInteractor;
import me.bt.avea.domain.interactions.general.events.GeneralEvent;
import me.bt.motor.modules.general.GeneralModule;
import me.bt.motor.network.NotificationSaveRequest;
import me.bt.avea.presentation.general.GeneralPresenter;
import me.bt.avea.presentation.general.GeneralView;
import me.bt.motor.ui.widget.SwitchableTextLayout;
import me.bt.motor.R;
import me.bt.base.ui.BaseActivity;
import me.bt.base.ui.elevation.ElevationHandler;
import me.bt.base.ui.loading.LoadingManager;
import me.bt.motor.util.Utils;

public class SettingsNotificationsActivity extends BaseActivity implements GeneralView, CompoundButton.OnCheckedChangeListener {

    private static final String JSON = "application/json; charset=utf-8";

    @Inject
    LoadingManager loadingManager;
    @InjectView(R.id.toolbar)
    Toolbar toolbar;
    @Inject
    ElevationHandler elevationHandler;
    @Inject
    GeneralPresenter generalPresenter;
    @InjectView(R.id.ll_switch_wrapper)
    LinearLayout ll_switch_wrapper;

    private GeneralEvent mResponse;
    private OkHttpClient mClient;
    private Gson mGson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mClient = new OkHttpClient();
        Utils.installCertificatePinning(mClient);
        mGson = new Gson();
        initToolbar();
        setupUI();
        startServiceCalls();
    }

    private void startServiceCalls() {
        generalPresenter.getNotificationList();
        loadingManager.showLoading(R.string.loading_message);
    }

    private void setupUI() {
        String defaultMobile = getAppPreferences().getString(PrefConstants.DEFAULT_MOBILE_ALIAS, "");

    }

    private void initToolbar() {
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            toolbar.setTitle(R.string.activity_notifications);
            elevationHandler.setDefaultElevation(toolbar);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View back) {
                    onBackPressed();
                }
            });
        }
    }

    @Override
    public int onCreateViewId() {
        return R.layout.activity_settings_notifications;
    }

    protected List<Object> getModules() {
        return Arrays.<Object>asList(new SettingsModule(), new GeneralModule(this));
    }

    @Override
    protected void onPause() {
        super.onPause();
        generalPresenter.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        generalPresenter.onResume();
    }

    @Override
    public void generalResponse(GeneralEvent response) {
        loadingManager.dismissLoading();
        if (response.getRequestType() == GeneralInteractor.REQUEST_NOTIFICATION_LIST) {
            if (response.getError() == null) {
                mResponse = response;
                NotificationList notificationList = (NotificationList) mResponse.getData();

                ll_switch_wrapper.removeAllViewsInLayout();
                SwitchableTextLayout switchableTextLayout;
                for (SettingsNotification setting : notificationList.notification) {
                    switchableTextLayout = new SwitchableTextLayout(this);
                    switchableTextLayout.setText(setting.description);
                    switchableTextLayout.getSwSwitch().setTag(setting.key);
                    if (setting.status.equals("1")) {
                        switchableTextLayout.getSwSwitch().setChecked(true);
                    }
                    ll_switch_wrapper.addView(switchableTextLayout);
                    switchableTextLayout.getSwSwitch().setOnCheckedChangeListener(this);
                }

//                for (SettingsNotification notification : notificationList.notification) {
//                    if (notification.key.equalsIgnoreCase(SPEED_EXCEEDED)) {
//                        sw_speed_exceed.setChecked(1 == Integer.parseInt(notification.status));
//                        sw_speed_exceed.setTag(SPEED_EXCEEDED);
//                    } else if (notification.key.equalsIgnoreCase(CUSTPOINT_ENGAGE)) {
//                        sw_customer_point.setChecked(1 == Integer.parseInt(notification.status));
//                        sw_customer_point.setTag(CUSTPOINT_ENGAGE);
//                    } else if (notification.key.equalsIgnoreCase(CUSTPOINT_LEAVE)) {
//                        sw_customer_outside.setChecked(1 == Integer.parseInt(notification.status));
//                        sw_customer_outside.setTag(CUSTPOINT_LEAVE);
//                    } else if (notification.key.equalsIgnoreCase(TOWING)) {
//                        sw_alarm.setChecked(1 == Integer.parseInt(notification.status));
//                        sw_alarm.setTag(TOWING);
//                    } else if (notification.key.equalsIgnoreCase(MOBILE_INACTIVE)) {
//                        sw_passive.setChecked(1 == Integer.parseInt(notification.status));
//                        sw_passive.setTag(MOBILE_INACTIVE);
//                    } else if (notification.key.equalsIgnoreCase(IGNITION_ON)) {
//                        sw_ignition.setChecked(1 == Integer.parseInt(notification.status));
//                        sw_ignition.setTag(IGNITION_ON);
//                    }
//                }
//                sw_speed_exceed.setOnCheckedChangeListener(this);
//                sw_customer_point.setOnCheckedChangeListener(this);
//                sw_customer_outside.setOnCheckedChangeListener(this);
//                sw_alarm.setOnCheckedChangeListener(this);
//                sw_passive.setOnCheckedChangeListener(this);
//                sw_ignition.setOnCheckedChangeListener(this);

            }

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        String key = (String) buttonView.getTag();
        String value = "0";
        if (isChecked) value = "1";


        NotificationSaveRequest notif = new NotificationSaveRequest();
        notif.key = key;
        notif.value = value;
        notif.token = getAppPreferences().getString(PrefConstants.TOKEN, "");

        RequestBody body = RequestBody.create(MediaType.parse(JSON), "");

        Request request = new Request.Builder()
                .url(notif.toString())
                .put(body)
                .build();

        mClient.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Request request, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Response response) throws IOException {

                if (!response.isSuccessful()) {
                    Log.e("settings", response.body().toString());
                }

//                String resp = response.body().string();
//                final BaseResponse checkAppVersionResponse = mGson.fromJson(resp, BaseResponse.class);
//
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
////                        rpCheckApp(checkAppVersionResponse);
//                    }
//                });
            }
        });

    }
}
