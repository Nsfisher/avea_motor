package me.bt.motor.ui.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import me.bt.motor.R;

/**
 * Created by yenerciftci on 25/08/15.
 */
public class SwitchableTextLayout extends LinearLayout {
    private View mView;
    private TextView mTvSwitchText;
    private Switch swSwitch;

    public SwitchableTextLayout(Context context) {
        super(context);
        init();
    }

    public SwitchableTextLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SwitchableTextLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public SwitchableTextLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {

        mView = LayoutInflater.from(getContext()).inflate(
                R.layout.layout_switchable_text, this);

        mTvSwitchText = (TextView) mView.findViewById(R.id.tv_switch_text);
        swSwitch = (Switch) mView.findViewById(R.id.sw_switch);

    }

    public void setText(String text) {
        mTvSwitchText.setText(text);
    }

    public Switch getSwSwitch() {
        return swSwitch;
    }

    public void setSwSwitch(Switch swSwitch) {
        this.swSwitch = swSwitch;
    }
}
