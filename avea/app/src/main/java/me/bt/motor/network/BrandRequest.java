package me.bt.motor.network;

import me.bt.base.util.Constants;

/**
 * Created by yenerciftci on 30/07/15.
 */
public class BrandRequest extends BaseRequest {
    @Override
    public String toString() {
        return Constants.APP_URL + BRANDS
                + "token=" + token;
    }
}
