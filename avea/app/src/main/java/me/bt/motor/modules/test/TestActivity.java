package me.bt.motor.modules.test;

import android.location.Location;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.carlosdelachica.easyrecycleradapters.adapter.EasyViewHolder;
import com.carlosdelachica.easyrecycleradapters.recycler_view_manager.EasyRecyclerViewManager;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import butterknife.InjectView;
import butterknife.OnClick;
import me.bt.avea.data.preferences.PrefConstants;
import me.bt.avea.domain.entities.Suggestion;
import me.bt.avea.presentation.model.PresentationMobile;
import me.bt.avea.presentation.test.TestPresenter;
import me.bt.avea.presentation.test.TestView;
import me.bt.base.ui.BaseActivity;
import me.bt.base.ui.elevation.ElevationHandler;
import me.bt.base.ui.errors.ErrorManager;
import me.bt.base.ui.imageloader.ImageLoader;
import me.bt.motor.R;
import pl.charmas.android.reactivelocation.ReactiveLocationProvider;
import rx.functions.Action1;

public class TestActivity extends BaseActivity implements TestView, EasyViewHolder.OnItemClickListener {

  @Inject TestPresenter presenter;
  @Inject ErrorManager errorManager;
  @Inject ElevationHandler elevationHandler;
  @InjectView(R.id.btn_register) Button btn_register;
  @InjectView(R.id.btn_get_mobiles) Button btn_get_mobiles;
  @InjectView(R.id.btn_search) Button btn_search;
  @InjectView(R.id.tv_token) TextView tv_token;
  @InjectView(R.id.tv_empty) TextView tv_empty;
  @InjectView(R.id.tv_suggestion) TextView tv_suggestion;
  @InjectView(R.id.et_suggestion) EditText et_suggestion;
  @InjectView(R.id.recyclerView) RecyclerView recyclerView;
  @Inject ImageLoader imageLoader;
  @Inject ReactiveLocationProvider locationProvider;
  private EasyRecyclerViewManager recyclerViewManager;

  @OnClick(R.id.btn_register) void onRegisterClicked() {
    presenter.registerUser("ia", "ia123", "Infotech Araclari", "tr");
  }

  @OnClick(R.id.btn_get_mobiles) void onGetMobilesClicked() {
    presenter.getMobiles();
  } //TODO hakan remove all

  @OnClick(R.id.btn_search) void onSearchForSuggestion() {
    locationProvider.getLastKnownLocation().subscribe(new Action1<Location>() {
      @Override public void call(Location location) {
        presenter.getSuggestion(et_suggestion.getText().toString(), location.getLatitude(), location.getLongitude());
      }
    });
  }

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    initUi();
  }

  @Override public int onCreateViewId() {
    return R.layout.activity_test;
  }

  protected List<Object> getModules() {
    return Arrays.<Object>asList(new TestModule(this));
  }

  @Override protected void onPause() {
    super.onPause();
    presenter.onPause();
  }

  @Override protected void onResume() {
    super.onResume();
    presenter.onResume();
  }

  private void initUi() {
    initRecyleView();
  }

  private void initRecyleView() {
    //MobileViewHolderFactory mobileViewHolderFactory = new MobileViewHolderFactory(this, imageLoader);
    //EasyRecyclerAdapter adapter = new EasyRecyclerAdapter(mobileViewHolderFactory, PresentationMobile.class, MobileViewHolder.class);
    //recyclerViewManager = new EasyRecyclerViewManager.Builder(recyclerView, adapter).emptyLoadingListTextView(tv_empty)
    //    .loadingListTextColor(android.R.color.black)
    //    .divider(R.drawable.list_divider)
    //    .clickListener(this)
    //    .build();
  }

  @Override public void showLoginError() {
  }

  @Override public void loginComplete() {
    tv_token.setText(getAppPreferences().getString(PrefConstants.TOKEN, ""));
  }

  @Override public void refreshMobiles(List<PresentationMobile> mobiles) {
    recyclerViewManager.addAll(mobiles);
  }

  @Override public void showRefreshMobilesError() {

  }

  @Override public void refreshSuggestions(List<Suggestion> suggestions) {
  }

  @Override public void showRefreshSuggestionsError() {

  }

  @Override public void onItemClick(int position, View view) {
    int mobileId = ((PresentationMobile) recyclerViewManager.getItem(position)).getMobile();
    presenter.getLightEvents(String.valueOf(mobileId), 10);
  }
}
