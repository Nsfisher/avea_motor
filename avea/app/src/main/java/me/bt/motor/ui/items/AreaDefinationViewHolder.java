package me.bt.motor.ui.items;

import android.content.Context;
import android.content.res.Resources;
import android.view.ViewGroup;
import android.widget.TextView;

import com.carlosdelachica.easyrecycleradapters.adapter.EasyViewHolder;

import butterknife.ButterKnife;
import butterknife.InjectView;
import me.bt.avea.domain.entities.Region;
import me.bt.motor.R;

public class AreaDefinationViewHolder extends EasyViewHolder<Region> {

  private final Resources resources;
  @InjectView(R.id.tv_name) TextView tv_name;

  public AreaDefinationViewHolder(Context context, ViewGroup parent, Resources resources) {
    super(context, parent, R.layout.item_area_defination);
    ButterKnife.inject(this, itemView);
    this.resources = resources;
  }

  @Override public void bindTo(Region region) {
    if (region == null) {
      return;
    }
    if (region.name != null) {
      tv_name.setText(region.name);
    }
  }
}
