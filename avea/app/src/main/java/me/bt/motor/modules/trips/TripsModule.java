package me.bt.motor.modules.trips;

import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;
import me.bt.avea.domain.entities.Mobile;
import me.bt.avea.domain.interactions.lightevents.LightPositionsInteractor;
import me.bt.avea.domain.interactions.mobiles.MobilesInteractor;
import me.bt.avea.domain.interactions.trips.TripsInteractor;
import me.bt.motor.modules.tripdetail.TripDetailActivity;
import me.bt.avea.presentation.model.PresentationMobile;
import me.bt.avea.presentation.trips.TripsPresenter;
import me.bt.avea.presentation.trips.TripsView;
import me.bt.base.di.ActivityModule;
import me.bt.base.domain.abstractions.Bus;
import me.bt.base.domain.interactors.InteractorInvoker;
import me.bt.base.presentation.model.mapper.base.ListMapper;

@Module(
    addsTo = ActivityModule.class,
    library = true,
    complete = false,
    injects = {TripsActivity.class, TripDetailActivity.class}) public class TripsModule { //TODO hakan one module for mobiles

  private TripsView tripsView;

  public TripsModule(TripsView tripsView) {
    this.tripsView = tripsView;
  }

  @Provides @Singleton TripsPresenter provideTripsPresenter(Bus bus, InteractorInvoker interactorInvoker,
      MobilesInteractor mobilesInteractor, ListMapper<Mobile, PresentationMobile> listMapper, TripsInteractor tripsInteractor,
      LightPositionsInteractor lightPositionsInteractor) {
    return new TripsPresenter(bus, interactorInvoker, tripsView, mobilesInteractor, tripsInteractor, lightPositionsInteractor, listMapper);
  }
}

