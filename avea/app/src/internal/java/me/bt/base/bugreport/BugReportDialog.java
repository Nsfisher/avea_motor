package me.bt.base.bugreport;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import me.bt.motor.R;

import static me.bt.base.bugreport.BugReportView.Report;
import static me.bt.base.bugreport.BugReportView.ReportDetailsListener;

public final class BugReportDialog extends AlertDialog implements ReportDetailsListener {
  private ReportListener listener;

  public BugReportDialog(Context context) {
    super(context);

    final BugReportView view = (BugReportView) LayoutInflater.from(context).inflate(R.layout.bugreport_view, null);
    view.setBugReportListener(this);

    setTitle("Report a bug");
    setView(view);
    setButton(Dialog.BUTTON_NEGATIVE, "Cancel", (OnClickListener) null);
    setButton(Dialog.BUTTON_POSITIVE, "Submit", new OnClickListener() {
      @Override public void onClick(DialogInterface dialog, int which) {
        if (listener != null) {
          listener.onBugReportSubmit(view.getReport());
        }
      }
    });
  }

  public void setReportListener(ReportListener listener) {
    this.listener = listener;
  }

  @Override protected void onStart() {
    getButton(Dialog.BUTTON_POSITIVE).setEnabled(false);
  }

  @Override public void onStateChanged(boolean valid) {
    getButton(Dialog.BUTTON_POSITIVE).setEnabled(valid);
  }

  public interface ReportListener {
    void onBugReportSubmit(Report report);
  }
}
