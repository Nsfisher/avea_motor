package me.bt.base;

import me.bt.motor.di.AveaAppModule;
import me.bt.base.di.AppModule;

final class Modules {
  private Modules() {
  }

  static Object[] list(BaseApplication app) {
    return new Object[] {
        new AveaAppModule(), new AppModule(app), new DebugModule()
    };
  }
}
