package me.bt.base.data.api;

import java.util.Arrays;
import java.util.Collections;
import me.bt.base.data.api.model.RepositoriesResponse;

import static me.bt.base.data.api.MockRepositories.BUTTERKNIFE;
import static me.bt.base.data.api.MockRepositories.DAGGER;
import static me.bt.base.data.api.MockRepositories.JAVAPOET;
import static me.bt.base.data.api.MockRepositories.OKHTTP;
import static me.bt.base.data.api.MockRepositories.OKIO;
import static me.bt.base.data.api.MockRepositories.PICASSO;
import static me.bt.base.data.api.MockRepositories.RETROFIT;
import static me.bt.base.data.api.MockRepositories.SQLBRITE;
import static me.bt.base.data.api.MockRepositories.TELESCOPE;
import static me.bt.base.data.api.MockRepositories.U2020;
import static me.bt.base.data.api.MockRepositories.WIRE;

public enum MockRepositoriesResponse {
  SUCCESS("Success", new RepositoriesResponse(Arrays.asList( //
      BUTTERKNIFE, //
      DAGGER, //
      JAVAPOET, //
      OKHTTP, //
      OKIO, //
      PICASSO, //
      RETROFIT, //
      SQLBRITE, //
      TELESCOPE, //
      U2020, //
      WIRE))),
  ONE("One", new RepositoriesResponse(Collections.singletonList(DAGGER))),
  EMPTY("Empty", new RepositoriesResponse(null));

  public final String name;
  public final RepositoriesResponse response;

  MockRepositoriesResponse(String name, RepositoriesResponse response) {
    this.name = name;
    this.response = response;
  }

  @Override public String toString() {
    return name;
  }
}
