package me.bt.base.ui.debug;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.PowerManager;
import android.support.v4.view.GravityCompat;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.jakewharton.madge.MadgeFrameLayout;
import com.jakewharton.scalpel.ScalpelFrameLayout;
import com.mattprecious.telescope.TelescopeLayout;

import javax.inject.Inject;
import javax.inject.Singleton;

import butterknife.ButterKnife;
import butterknife.InjectView;
import me.bt.base.bugreport.BugReportLens;
import me.bt.base.data.LumberYard;
import me.bt.base.data.PixelGridEnabled;
import me.bt.base.data.PixelRatioEnabled;
import me.bt.base.data.ScalpelEnabled;
import me.bt.base.data.ScalpelWireframeEnabled;
import me.bt.base.data.SeenDebugDrawer;
import me.bt.base.data.prefs.BooleanPreference;
import me.bt.base.ui.AppContainer;
import me.bt.base.util.EmptyActivityLifecycleCallbacks;
import me.bt.motor.R;
import rx.Observable;
import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;

import static android.content.Context.POWER_SERVICE;
import static android.os.PowerManager.ACQUIRE_CAUSES_WAKEUP;
import static android.os.PowerManager.FULL_WAKE_LOCK;
import static android.os.PowerManager.ON_AFTER_RELEASE;
import static android.view.WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED;

@Singleton public final class DebugAppContainer implements AppContainer {
  private final LumberYard lumberYard;
  private final BooleanPreference seenDebugDrawer;
  private final Observable<Boolean> pixelGridEnabled;
  private final Observable<Boolean> pixelRatioEnabled;
  private final Observable<Boolean> scalpelEnabled;
  private final Observable<Boolean> scalpelWireframeEnabled;

  @Inject public DebugAppContainer(LumberYard lumberYard, @SeenDebugDrawer BooleanPreference seenDebugDrawer,
      @PixelGridEnabled Observable<Boolean> pixelGridEnabled, @PixelRatioEnabled Observable<Boolean> pixelRatioEnabled,
      @ScalpelEnabled Observable<Boolean> scalpelEnabled, @ScalpelWireframeEnabled Observable<Boolean> scalpelWireframeEnabled) {
    this.lumberYard = lumberYard;
    this.seenDebugDrawer = seenDebugDrawer;
    this.pixelGridEnabled = pixelGridEnabled;
    this.pixelRatioEnabled = pixelRatioEnabled;
    this.scalpelEnabled = scalpelEnabled;
    this.scalpelWireframeEnabled = scalpelWireframeEnabled;
  }

  /**
   * Show the activity over the lock-screen and wake up the device. If you launched the app manually
   * both of these conditions are already true. If you deployed from the IDE, however, this will
   * save you from hundreds of power button presses and pattern swiping per day!
   */
  public static void riseAndShine(Activity activity) {
    activity.getWindow().addFlags(FLAG_SHOW_WHEN_LOCKED);
    PowerManager power = (PowerManager) activity.getSystemService(POWER_SERVICE);
    PowerManager.WakeLock lock = power.newWakeLock(FULL_WAKE_LOCK | ACQUIRE_CAUSES_WAKEUP | ON_AFTER_RELEASE, "wakeup!");
    lock.acquire();
    lock.release();
  }

  @Override public ViewGroup bind(final Activity activity) {
    activity.setContentView(R.layout.debug_activity_frame);

    final ViewHolder viewHolder = new ViewHolder();
    ButterKnife.inject(viewHolder, activity);

    final Context drawerContext = new ContextThemeWrapper(activity, R.style.Theme_U2020_Debug);
    final DebugView debugView = new DebugView(drawerContext);
    viewHolder.debugDrawer.addView(debugView);

    // Set up the contextual actions to watch views coming in and out of the content area.
    ContextualDebugActions contextualActions = debugView.getContextualDebugActions();
    contextualActions.setActionClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        viewHolder.drawerLayout.closeDrawers();
      }
    });
    viewHolder.content.setOnHierarchyChangeListener(HierarchyTreeChangeListener.wrap(contextualActions));

    viewHolder.drawerLayout.setDrawerShadow(R.drawable.debug_drawer_shadow, GravityCompat.END);
    viewHolder.drawerLayout.setDrawerListener(new DebugDrawerLayout.SimpleDrawerListener() {
      @Override public void onDrawerOpened(View drawerView) {
        debugView.onDrawerOpened();
      }
    });

    TelescopeLayout.cleanUp(activity); // Clean up any old screenshots.
    viewHolder.telescopeLayout.setLens(new BugReportLens(activity, lumberYard));

    // If you have not seen the debug drawer before, show it with a message
    if (!seenDebugDrawer.get()) {
      viewHolder.drawerLayout.postDelayed(new Runnable() {
        @Override public void run() {
          viewHolder.drawerLayout.openDrawer(GravityCompat.END);
          Toast.makeText(drawerContext, R.string.debug_drawer_welcome, Toast.LENGTH_LONG).show();
        }
      }, 1000);
      seenDebugDrawer.set(true);
    }

    final CompositeSubscription subscriptions = new CompositeSubscription();
    setupMadge(viewHolder, subscriptions);
    setupScalpel(viewHolder, subscriptions);

    final Application app = activity.getApplication();
    app.registerActivityLifecycleCallbacks(new EmptyActivityLifecycleCallbacks() {
      @Override public void onActivityDestroyed(Activity lifecycleActivity) {
        if (lifecycleActivity == activity) {
          subscriptions.unsubscribe();
          app.unregisterActivityLifecycleCallbacks(this);
        }
      }
    });

    riseAndShine(activity);
    return viewHolder.content;
  }

  private void setupMadge(final ViewHolder viewHolder, CompositeSubscription subscriptions) {
    subscriptions.add(pixelGridEnabled.subscribe(new Action1<Boolean>() {
      @Override public void call(Boolean enabled) {
        viewHolder.madgeFrameLayout.setOverlayEnabled(enabled);
      }
    }));
    subscriptions.add(pixelRatioEnabled.subscribe(new Action1<Boolean>() {
      @Override public void call(Boolean enabled) {
        viewHolder.madgeFrameLayout.setOverlayRatioEnabled(enabled);
      }
    }));
  }

  private void setupScalpel(final ViewHolder viewHolder, CompositeSubscription subscriptions) {
    subscriptions.add(scalpelEnabled.subscribe(new Action1<Boolean>() {
      @Override public void call(Boolean enabled) {
        viewHolder.content.setLayerInteractionEnabled(enabled);
      }
    }));
    subscriptions.add(scalpelWireframeEnabled.subscribe(new Action1<Boolean>() {
      @Override public void call(Boolean enabled) {
        viewHolder.content.setDrawViews(!enabled);
      }
    }));
  }

  static class ViewHolder {
    @InjectView(R.id.debug_drawer_layout) DebugDrawerLayout drawerLayout;
    @InjectView(R.id.debug_drawer) ViewGroup debugDrawer;
    @InjectView(R.id.telescope_container) TelescopeLayout telescopeLayout;
    @InjectView(R.id.madge_container) MadgeFrameLayout madgeFrameLayout;
    @InjectView(R.id.debug_content) ScalpelFrameLayout content;
  }
}
