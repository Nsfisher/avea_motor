package me.bt.base.ui;

import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;
import me.bt.base.ui.debug.DebugAppContainer;
import me.bt.base.ui.debug.DebugView;
import me.bt.base.ui.debug.SocketActivityHierarchyServer;

@Module(
    injects = {
        DebugAppContainer.class, DebugView.class,
    },
    complete = false,
    library = true,
    overrides = true) public class DebugUiModule {
  @Provides @Singleton AppContainer provideAppContainer(DebugAppContainer debugAppContainer) {
    return debugAppContainer;
  }

  @Provides @Singleton ActivityHierarchyServer provideActivityHierarchyServer() {
    return new SocketActivityHierarchyServer();
  }
}
