package me.bt.base;

import dagger.Module;
import me.bt.base.data.DebugDataModule;
import me.bt.base.di.AppModule;
import me.bt.base.ui.DebugUiModule;

@Module(
    addsTo = AppModule.class,
    includes = {
        DebugUiModule.class, DebugDataModule.class
    },
    overrides = true) public final class DebugModule {
}
