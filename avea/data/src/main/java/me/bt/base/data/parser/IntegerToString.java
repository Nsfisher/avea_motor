package me.bt.base.data.parser;

import com.mobandme.android.transformer.parser.AbstractParser;

public class IntegerToString extends AbstractParser<Integer, String> {
  @Override protected String onParse(Integer value) {
    return String.valueOf(value);
  }
}
