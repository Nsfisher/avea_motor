package me.bt.base.data.parser;

import com.mobandme.android.transformer.parser.AbstractParser;

public class StringToInteger extends AbstractParser<String, Integer> {
  @Override protected Integer onParse(String value) {
    Integer returnValue;
    try {
      returnValue = Integer.valueOf(value);
    } catch (Exception ex) {
      returnValue = 0;
    }
    return returnValue;
  }
}
