package me.bt.base.data.parser;

import com.mobandme.android.transformer.parser.AbstractParser;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class StringToDate extends AbstractParser<String, Date> {
  @Override protected Date onParse(String value) {
    Calendar calendar = GregorianCalendar.getInstance();

    try {
      SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
      calendar.setTime(dateFormatter.parse(value));
    } catch (ParseException e) {
    }

    return calendar.getTime();
  }
}
