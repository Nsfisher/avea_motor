package me.bt.base.data.parser;

import com.mobandme.android.transformer.parser.AbstractParser;

public class StringToDouble extends AbstractParser<String, Double> {
  @Override protected Double onParse(String value) {
    Double returnValue;
    try {
      returnValue = Double.valueOf(value);
    } catch (Exception ex) {
      returnValue = (double) 0;
    }
    return returnValue;
  }
}
