package me.bt.base.data.parser;

import com.mobandme.android.transformer.parser.AbstractParser;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateToString extends AbstractParser<Date, String> {

  @Override protected String onParse(Date value) {
    SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
    return dateFormatter.format(value.getTime());
  }
}
