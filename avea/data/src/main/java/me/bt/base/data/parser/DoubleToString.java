package me.bt.base.data.parser;

import com.mobandme.android.transformer.parser.AbstractParser;

public class DoubleToString extends AbstractParser<Double, String> {
  @Override protected String onParse(Double value) {
    return String.valueOf(value);
  }
}
