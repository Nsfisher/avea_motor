package me.bt.avea.data.repository.mobiles.datasources.api.implementations;

import android.app.Application;
import com.mobandme.android.transformer.Transformer;
import java.util.ArrayList;
import java.util.List;
import me.bt.avea.data.preferences.PrefConstants;
import me.bt.avea.data.repository.mobiles.datasources.api.MobileApiService;
import me.bt.avea.data.repository.mobiles.datasources.api.entities.ApiSuggestion;
import me.bt.avea.data.repository.mobiles.datasources.api.responses.ApiGetSuggestionsResponse;
import me.bt.avea.domain.entities.Suggestion;
import me.bt.avea.repository.suggestion.datasource.SuggestionNetworkDataSource;
import me.bt.avea.repository.suggestion.datasource.exceptions.SuggestionNetworkException;
import net.grandcentrix.tray.TrayAppPreferences;

public class SuggestionNetworkDataSourceImp implements SuggestionNetworkDataSource {

  private static final Transformer transformer = new Transformer.Builder().build(ApiSuggestion.class);
  private final TrayAppPreferences preferences;
  private final MobileApiService apiService;
  private final Application application;

  public SuggestionNetworkDataSourceImp(MobileApiService apiService, Application application, TrayAppPreferences preferences) {
    this.apiService = apiService;
    this.application = application;
    this.preferences = preferences;
  }

  @Override public List<Suggestion> getSuggestion(String keyword, double latitude, double longitude) throws SuggestionNetworkException {
    try {
      String token = preferences.getString(PrefConstants.TOKEN, "");
      ApiGetSuggestionsResponse response = apiService.getSuggestions(token, keyword, 10, latitude, longitude);
      List<Suggestion> suggestions = new ArrayList<>();
      if (response.getDataSuggestion() != null) {
        for (ApiSuggestion apiSuggestion : response.getDataSuggestion()) {
          suggestions.add(transformer.transform(apiSuggestion, Suggestion.class));
        }
      }
      return suggestions;
    } catch (Throwable e) {
      throw new SuggestionNetworkException();
    }
  }
}
