package me.bt.avea.data.repository.mobiles.datasources.api.responses;

import com.google.gson.annotations.Expose;

import me.bt.avea.data.repository.mobiles.datasources.api.entities.ApiMobile;

public class ApiGetMobileResponse {
    @Expose
    private ApiMobile mobile = new ApiMobile();

    public ApiMobile getMobile() {
        return mobile;
    }

    public void setMobile(ApiMobile mobile) {
        this.mobile = mobile;
    }
}
