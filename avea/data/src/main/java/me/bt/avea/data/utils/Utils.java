package me.bt.avea.data.utils;

public class Utils {

  public static String convertDateFormatForServer(String date) {
    String initial = date;
    try {
      StringBuilder builder = new StringBuilder();
      builder.append(date.substring(0, 4));
      builder.append("-");
      builder.append(date.substring(4, 6));
      builder.append("-");
      builder.append(date.substring(6, 8));
      builder.append(" ");
      builder.append(date.substring(8, 10));
      builder.append(":");
      builder.append(date.substring(10, 12));
      builder.append(":");
      builder.append(date.substring(12, 14));
      return builder.toString();
    } catch (Exception ex) {
      return initial;
    }
  }
}
