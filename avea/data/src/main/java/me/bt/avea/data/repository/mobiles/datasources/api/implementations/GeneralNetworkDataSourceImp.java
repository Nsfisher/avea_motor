package me.bt.avea.data.repository.mobiles.datasources.api.implementations;

import android.app.Application;

import com.mobandme.android.transformer.Transformer;

import net.grandcentrix.tray.TrayAppPreferences;

import java.util.Date;

import me.bt.avea.data.preferences.PrefConstants;
import me.bt.avea.data.repository.mobiles.datasources.api.MobileApiService;
import me.bt.avea.data.repository.mobiles.datasources.api.entities.ApiEventScore;
import me.bt.avea.data.repository.mobiles.datasources.api.entities.ApiGeneralResponse;
import me.bt.avea.data.repository.mobiles.datasources.api.entities.ApiLightTrips;
import me.bt.avea.data.repository.mobiles.datasources.api.entities.ApiMobile;
import me.bt.avea.data.repository.mobiles.datasources.api.entities.ApiNotificationList;
import me.bt.avea.data.repository.mobiles.datasources.api.entities.ApiRegions;
import me.bt.avea.data.repository.mobiles.datasources.api.entities.ApiTerms;
import me.bt.avea.domain.entities.BrandList;
import me.bt.avea.domain.entities.BrandModelList;
import me.bt.avea.domain.entities.EngineTypeList;
import me.bt.avea.domain.entities.EventScore;
import me.bt.avea.domain.entities.FuelTypeList;
import me.bt.avea.domain.entities.GeneralResponse;
import me.bt.avea.domain.entities.LightTrips;
import me.bt.avea.domain.entities.NotificationList;
import me.bt.avea.domain.entities.Regions;
import me.bt.avea.domain.entities.Terms;
import me.bt.avea.repository.general.datasources.GeneralNetworkDataSource;
import me.bt.avea.repository.general.datasources.exceptions.BrandListNetworkException;
import me.bt.avea.repository.general.datasources.exceptions.BrandModelListNetworkException;
import me.bt.avea.repository.general.datasources.exceptions.EngineTypeListNetworkException;
import me.bt.avea.repository.general.datasources.exceptions.EventScoreNetworkException;
import me.bt.avea.repository.general.datasources.exceptions.ForgetPasswordNetworkException;
import me.bt.avea.repository.general.datasources.exceptions.FuelTypeListNetworkException;
import me.bt.avea.repository.general.datasources.exceptions.LightTripsNetworkException;
import me.bt.avea.repository.general.datasources.exceptions.LogoutNetworkException;
import me.bt.avea.repository.general.datasources.exceptions.NotificationListNetworkException;
import me.bt.avea.repository.general.datasources.exceptions.RegionsNetworkException;
import me.bt.avea.repository.general.datasources.exceptions.TermsNetworkException;
import me.bt.avea.util.AveaUtils;
import me.bt.base.data.BuildConfig;

public class GeneralNetworkDataSourceImp implements GeneralNetworkDataSource {

    private final static String ORDER_ASC = "ASC";
    private final static String ORDER_DESC = "DESC";
    private final static String ORDER_PARAM_ALIAS = "ALIAS";
    private static final Transformer transformer = new Transformer.Builder().build(ApiMobile.class);
    private final TrayAppPreferences appPreferences;
    private MobileApiService apiService;

    public GeneralNetworkDataSourceImp(MobileApiService apiService, Application application) {
        this.apiService = apiService;
        appPreferences = new TrayAppPreferences(application);
    }

    @Override
    public LightTrips getLightTrips(String mobileId, Date startDate, Date endDate) throws LightTripsNetworkException {
        try {
            String token = appPreferences.getString(PrefConstants.TOKEN, "");
            ApiLightTrips response =
                    apiService.getLightTrips(token, mobileId, AveaUtils.getDateString(startDate), AveaUtils.getDateString(endDate));
            return transformer.transform(response, LightTrips.class);
        } catch (Throwable e) {
            throw new LightTripsNetworkException();
        }
    }

    @Override
    public EventScore getEventScore(String mobileId, Date startDate, Date endDate) throws EventScoreNetworkException {
        try {
            String token = appPreferences.getString(PrefConstants.TOKEN, "");
            ApiEventScore response =
                    apiService.getEventScore(token, mobileId, AveaUtils.getDateString(startDate), AveaUtils.getDateString(endDate));
            return transformer.transform(response, EventScore.class);
        } catch (Throwable e) {
            throw new EventScoreNetworkException();
        }
    }

    @Override
    public Regions getRegions() throws RegionsNetworkException {
        try {
            String token = appPreferences.getString(PrefConstants.TOKEN, "");
            ApiRegions response = apiService.getRegions(token);
            return transformer.transform(response, Regions.class);
        } catch (Throwable e) {
            throw new RegionsNetworkException();
        }
    }

    @Override
    public Terms getTerms() throws TermsNetworkException {
        try {
            String token = appPreferences.getString(PrefConstants.TOKEN, "");
            ApiTerms response = apiService.getTerms(token, BuildConfig.QUERY_MODE);
            return transformer.transform(response, Terms.class);
        } catch (Throwable e) {
            throw new TermsNetworkException();
        }
    }

    @Override
    public GeneralResponse forgetPassword(String email) throws ForgetPasswordNetworkException {
        try {
            String token = appPreferences.getString(PrefConstants.TOKEN, "");
            ApiGeneralResponse response = apiService.forgetPassword(token, email);
            return transformer.transform(response, GeneralResponse.class);
        } catch (Throwable e) {
            throw new ForgetPasswordNetworkException();
        }
    }

    @Override
    public NotificationList getNotificationList() throws NotificationListNetworkException {
        try {
            String token = appPreferences.getString(PrefConstants.TOKEN, "");
            ApiNotificationList response = apiService.getNotificationList(token);
            return transformer.transform(response, NotificationList.class);
        } catch (Throwable e) {
            throw new NotificationListNetworkException();
        }
    }

    @Override
    public BrandList getBrandList() throws BrandListNetworkException {
        try {
            String token = appPreferences.getString(PrefConstants.TOKEN, "");
            return apiService.getBrands(token);
        } catch (Throwable e) {
            throw new BrandListNetworkException();
        }
    }

    @Override
    public BrandModelList getBrandModels(String brandId) throws BrandModelListNetworkException {
        try {
            String token = appPreferences.getString(PrefConstants.TOKEN, "");
            return apiService.getBrandModels(token, brandId);
        } catch (Throwable e) {
            throw new BrandModelListNetworkException();
        }
    }

    @Override
    public EngineTypeList getEngineType(String modelId) throws EngineTypeListNetworkException {
        try {
            String token = appPreferences.getString(PrefConstants.TOKEN, "");
            return apiService.getEngineType(token, modelId);
        } catch (Throwable e) {
            throw new EngineTypeListNetworkException();
        }
    }

    @Override
    public FuelTypeList getFuelTypes() throws FuelTypeListNetworkException {
        try {
            String token = appPreferences.getString(PrefConstants.TOKEN, "");
            return apiService.getFuelTypes(token);
        } catch (Throwable e) {
            throw new FuelTypeListNetworkException();
        }
    }

    @Override
    public GeneralResponse logout() throws LogoutNetworkException {
        try {
            String token = appPreferences.getString(PrefConstants.TOKEN, "");
            ApiGeneralResponse response = apiService.logout(token);
            return transformer.transform(response, GeneralResponse.class);
        } catch (Throwable e) {
            throw new LogoutNetworkException();
        }
    }
}
