package me.bt.avea.data.repository.mobiles.datasources.api.responses;

import com.google.gson.annotations.Expose;

import java.util.List;

import me.bt.avea.data.repository.mobiles.datasources.api.entities.ApiDataLightPosition;

public class ApiGetLightPositionsResponse {
    @Expose
    private List<ApiDataLightPosition> dataLightPosition;

    public List<ApiDataLightPosition> getDataLightPosition() {
        return dataLightPosition;
    }

    public void setDataLightPosition(List<ApiDataLightPosition> dataLightPosition) {
        this.dataLightPosition = dataLightPosition;
    }
}
