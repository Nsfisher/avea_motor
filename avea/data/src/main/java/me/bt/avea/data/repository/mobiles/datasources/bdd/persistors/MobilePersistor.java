package me.bt.avea.data.repository.mobiles.datasources.bdd.persistors;

import java.sql.SQLException;
import me.bt.avea.data.repository.DatabaseHelper;
import me.bt.avea.data.repository.mobiles.datasources.bdd.entities.BddMobile;

public class MobilePersistor implements Persistor<BddMobile> {

  private DatabaseHelper helper;

  public MobilePersistor(DatabaseHelper helper) {
    this.helper = helper;
  }

  @Override public void persist(BddMobile data) throws SQLException {
    if (data != null) {
      helper.getMobileDao().create(data);
    }
  }
}
