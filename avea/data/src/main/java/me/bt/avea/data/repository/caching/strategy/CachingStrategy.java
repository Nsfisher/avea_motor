package me.bt.avea.data.repository.caching.strategy;

public interface CachingStrategy<T> {
  boolean isValid(T data);
}
