package me.bt.avea.data.repository.mobiles.datasources.api.responses;

import com.google.gson.annotations.Expose;
import java.util.ArrayList;
import java.util.List;
import me.bt.avea.data.repository.mobiles.datasources.api.entities.ApiIntervalData;

public class ApiGetTripsResponse {
  @Expose private List<ApiIntervalData> dataTrip = new ArrayList<>();
  @Expose private String averageSpeed;
  @Expose private String count;
  @Expose private String distanceTotal;
  @Expose private String timeTotal;

  public String getTimeTotal() {
    return timeTotal;
  }

  public void setTimeTotal(String timeTotal) {
    this.timeTotal = timeTotal;
  }

  public String getDistanceTotal() {
    return distanceTotal;
  }

  public void setDistanceTotal(String distanceTotal) {
    this.distanceTotal = distanceTotal;
  }

  public String getCount() {
    return count;
  }

  public void setCount(String count) {
    this.count = count;
  }

  public String getAverageSpeed() {
    return averageSpeed;
  }

  public void setAverageSpeed(String averageSpeed) {
    this.averageSpeed = averageSpeed;
  }

  public List<ApiIntervalData> getDataTrip() {
    return dataTrip;
  }

  public void setDataTrip(List<ApiIntervalData> dataTrip) {
    this.dataTrip = dataTrip;
  }

  public List<ApiIntervalData> getIntervalDatas() {
    return dataTrip;
  }

  public void setIntervalDatas(List<ApiIntervalData> dataTrip) {
    this.dataTrip = dataTrip;
  }
}
