package me.bt.avea.data.repository.mobiles.datasources.api.entities;

import com.google.gson.annotations.Expose;
import com.mobandme.android.transformer.compiler.Mappable;
import com.mobandme.android.transformer.compiler.Mapped;
import java.util.List;
import me.bt.avea.domain.entities.Terms;
import me.bt.avea.domain.entities.TermsParameter;

@Mappable(with = Terms.class) public class ApiTerms {
  @Expose @Mapped public List<TermsParameter> parameter;
}
