package me.bt.avea.data.repository.mobiles.datasources.api.implementations;

import android.app.Application;
import com.mobandme.android.transformer.Transformer;
import java.util.ArrayList;
import java.util.List;
import me.bt.avea.data.preferences.PrefConstants;
import me.bt.avea.data.repository.mobiles.datasources.api.MobileApiService;
import me.bt.avea.data.repository.mobiles.datasources.api.entities.ApiDataLightPosition;
import me.bt.avea.data.repository.mobiles.datasources.api.entities.ApiLightEvent;
import me.bt.avea.data.repository.mobiles.datasources.api.responses.ApiGetLightPositionsResponse;
import me.bt.avea.data.utils.Utils;
import me.bt.avea.domain.entities.DataLightPosition;
import me.bt.avea.repository.lightevents.LightPositionsNetworkDataSource;
import me.bt.avea.repository.lightevents.datasources.exceptions.LightPositionsNetworkException;
import net.grandcentrix.tray.TrayAppPreferences;

public class LightPositionsNetworkDataSourceImp implements LightPositionsNetworkDataSource {

  private static final Transformer transformer = new Transformer.Builder().build(ApiLightEvent.class);
  private final TrayAppPreferences preferences;
  private final MobileApiService apiService;
  private final Application application;

  public LightPositionsNetworkDataSourceImp(MobileApiService apiService, Application application, TrayAppPreferences preferences) {
    this.apiService = apiService;
    this.application = application;
    this.preferences = preferences;
  }

  @Override
  public List<DataLightPosition> getLightPositions(String mobileId, String startDate, String endDate, int maxPoints, int queryMode,
      int lineMode) throws LightPositionsNetworkException {
    try {
      String token = preferences.getString(PrefConstants.TOKEN, "");
      ApiGetLightPositionsResponse response = apiService.getLightPositions(mobileId, token, Utils.convertDateFormatForServer(startDate),
          Utils.convertDateFormatForServer(endDate), maxPoints, queryMode, lineMode);
      List<DataLightPosition> lightPositions = new ArrayList<>();
      if (response.getDataLightPosition() != null) {
        for (ApiDataLightPosition apiDataLightPosition : response.getDataLightPosition()) {
          lightPositions.add(transformer.transform(apiDataLightPosition, DataLightPosition.class));
        }
      }
      return lightPositions;
    } catch (Throwable e) {
      throw new LightPositionsNetworkException();
    }
  }
}