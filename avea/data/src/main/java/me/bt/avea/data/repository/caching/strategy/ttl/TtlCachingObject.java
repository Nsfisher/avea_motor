package me.bt.avea.data.repository.caching.strategy.ttl;

public interface TtlCachingObject {
  long getPersistedTime();
}
