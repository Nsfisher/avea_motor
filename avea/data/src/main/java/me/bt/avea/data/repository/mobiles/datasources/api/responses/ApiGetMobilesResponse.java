package me.bt.avea.data.repository.mobiles.datasources.api.responses;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

import me.bt.avea.data.repository.mobiles.datasources.api.entities.ApiMobile;

public class ApiGetMobilesResponse {
    @Expose
    private List<ApiMobile> mobile = new ArrayList<>();

    public List<ApiMobile> getMobile() {
        return mobile;
    }

    public void setMobile(List<ApiMobile> mobile) {
        this.mobile = mobile;
    }
}
