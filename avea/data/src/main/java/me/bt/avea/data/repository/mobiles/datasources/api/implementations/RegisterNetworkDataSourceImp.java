package me.bt.avea.data.repository.mobiles.datasources.api.implementations;

import android.app.Application;

import net.grandcentrix.tray.TrayAppPreferences;

import me.bt.avea.data.preferences.PrefConstants;
import me.bt.avea.data.repository.mobiles.datasources.api.MobileApiService;
import me.bt.avea.data.repository.mobiles.datasources.api.responses.ApiRegisterResponse;
import me.bt.avea.domain.interactions.register.exceptions.RegisterException;
import me.bt.avea.repository.register.datasources.RegisterNetworkDataSource;
import me.bt.avea.repository.register.datasources.exceptions.RegisterNetworkException;

public class RegisterNetworkDataSourceImp implements RegisterNetworkDataSource {

    private MobileApiService apiService;
    private Application application;

    public RegisterNetworkDataSourceImp(MobileApiService apiService, Application application) {
        this.apiService = apiService;
        this.application = application;
    }

    @Override
    public String register(String username, String password, String company, String language) throws RegisterNetworkException {
        ApiRegisterResponse response = null;
        try {
            response = apiService.register(username, password, "9", company, language);
            if (response != null && response.getToken() != null && response.getStatus().equals("0")) {
                final TrayAppPreferences appPreferences = new TrayAppPreferences(application);
                appPreferences.put(PrefConstants.TOKEN, response.getToken());
                return response.getToken();
            } else {
                RegisterException exception = new RegisterException();
                if (response != null) {
                    exception.errorDesc = response.getErrorDesc();
                }
                throw exception;
            }
        } catch (Throwable e) {
            RegisterNetworkException exc = new RegisterNetworkException();
            if (response != null) {
                exc.errorDesc = response.errorDesc;
            }
            throw exc;
        }
    }
}
