package me.bt.avea.data.repository.mobiles.datasources.api.responses;

import com.google.gson.annotations.Expose;

public class ApiRegisterResponse extends ApiBaseResponse {
    @Expose
    private String token;
//    @Expose
//    public String status;
//    @Expose
//    private String errorDesc;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

//    public String getErrorDesc() {
//        return errorDesc;
//    }
//
//    public void setErrorDesc(String errorDesc) {
//        this.errorDesc = errorDesc;
//    }
//
//    public String getStatus() {
//        return status;
//    }
//
//    public void setStatus(String status) {
//        this.status = status;
//    }
}
