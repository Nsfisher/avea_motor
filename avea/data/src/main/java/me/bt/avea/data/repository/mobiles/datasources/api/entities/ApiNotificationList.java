package me.bt.avea.data.repository.mobiles.datasources.api.entities;

import com.google.gson.annotations.Expose;
import com.mobandme.android.transformer.compiler.Mappable;
import com.mobandme.android.transformer.compiler.Mapped;
import java.util.List;
import me.bt.avea.domain.entities.NotificationList;
import me.bt.avea.domain.entities.SettingsNotification;

@Mappable(with = NotificationList.class) public class ApiNotificationList {
  @Expose @Mapped public List<SettingsNotification> notification;
}
