package me.bt.avea.data.repository.mobiles.datasources.api.entities;

import com.google.gson.annotations.Expose;
import com.mobandme.android.transformer.compiler.Mappable;
import com.mobandme.android.transformer.compiler.Mapped;
import me.bt.avea.domain.entities.LogEvent;

@Mappable(with = LogEvent.class) //TODO hakan remove
public class ApiLogEvent {
  @Expose @Mapped public String count;
  @Expose @Mapped public String eventCode;
  @Expose @Mapped public String exceptionNo;

  public String getExceptionNo() {
    return exceptionNo;
  }

  public void setExceptionNo(String exceptionNo) {
    this.exceptionNo = exceptionNo;
  }

  public String getEventCode() {
    return eventCode;
  }

  public void setEventCode(String eventCode) {
    this.eventCode = eventCode;
  }

  public String getCount() {
    return count;
  }

  public void setCount(String count) {
    this.count = count;
  }
}
