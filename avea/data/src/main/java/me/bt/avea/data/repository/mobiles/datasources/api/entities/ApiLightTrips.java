package me.bt.avea.data.repository.mobiles.datasources.api.entities;

import com.google.gson.annotations.Expose;
import com.mobandme.android.transformer.compiler.Mappable;
import com.mobandme.android.transformer.compiler.Mapped;
import me.bt.avea.domain.entities.LightTrips;

@Mappable(with = LightTrips.class) public class ApiLightTrips {

  @Expose @Mapped public String doubleVal;
  @Expose @Mapped public String distanceTotal;
  @Expose @Mapped public String timeTotal;
  @Expose @Mapped public String count;
  @Expose @Mapped public String averageSpeed;
}
