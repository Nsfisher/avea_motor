package me.bt.avea.data.repository.mobiles.datasources.api;

import me.bt.avea.data.repository.mobiles.datasources.api.entities.ApiEventScore;
import me.bt.avea.data.repository.mobiles.datasources.api.entities.ApiGeneralResponse;
import me.bt.avea.data.repository.mobiles.datasources.api.entities.ApiLightTrips;
import me.bt.avea.data.repository.mobiles.datasources.api.entities.ApiMobile;
import me.bt.avea.data.repository.mobiles.datasources.api.entities.ApiNotificationList;
import me.bt.avea.data.repository.mobiles.datasources.api.entities.ApiRegions;
import me.bt.avea.data.repository.mobiles.datasources.api.entities.ApiTerms;
import me.bt.avea.data.repository.mobiles.datasources.api.responses.ApiBaseResponse;
import me.bt.avea.data.repository.mobiles.datasources.api.responses.ApiGetLightEventsResponse;
import me.bt.avea.data.repository.mobiles.datasources.api.responses.ApiGetLightPositionsResponse;
import me.bt.avea.data.repository.mobiles.datasources.api.responses.ApiGetMobileResponse;
import me.bt.avea.data.repository.mobiles.datasources.api.responses.ApiGetMobilesResponse;
import me.bt.avea.data.repository.mobiles.datasources.api.responses.ApiGetSuggestionsResponse;
import me.bt.avea.data.repository.mobiles.datasources.api.responses.ApiGetTripsResponse;
import me.bt.avea.data.repository.mobiles.datasources.api.responses.ApiRegisterResponse;
import me.bt.avea.domain.entities.BrandList;
import me.bt.avea.domain.entities.BrandModelList;
import me.bt.avea.domain.entities.EngineTypeList;
import me.bt.avea.domain.entities.FuelTypeList;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import retrofit.http.Query;

public interface MobileApiService {

    @POST("/registeractivationpost")
    ApiRegisterResponse register(@Query("username") String username,
                                 @Query("password") String password,
                                 @Query("deviceType") String deviceType,
                                 @Query("company") String company,
                                 @Query("language") String language);

    @GET("/registerWithDetails")
    ApiRegisterResponse registerWithDetails(@Query("username") String username,
                                            @Query("password") String password,
                                            @Query("company") String company,
                                            @Query("lang") String lang,
                                            @Query("deviceType") int deviceType,
                                            @Query("deviceInfo") String deviceInfo);

    @PUT("/logout")
    ApiGeneralResponse logout(@Query("token") String token);

    @GET("/mobiles")
    ApiGetMobilesResponse getMobiles(@Query("token") String token,
                                     @Query("orderParam") String orderParam,
                                     @Query("ordering") String ordering);

    @GET("/mobiles")
    ApiGetMobileResponse getMobilesOneRow(@Query("token") String token,
                                          @Query("orderParam") String orderParam,
                                          @Query("ordering") String ordering);

    @GET("/mobiles/{id}")
    ApiMobile getMobile(@Path("id") String mobileId,
                        @Query("token") String token);

    @GET("/adduser")
    ApiBaseResponse addUser(@Query("token") String token,
                            @Query("activation") String activation,
                            @Query("username") String username,
                            @Query("password") String password,
                            @Query("plaka") String plaka,
                            @Query("takmaIsim") String takmaIsim,
                            @Query("marka") String marka,
                            @Query("model") String model,
                            @Query("yakitTipi") String yakitTipi,
                            @Query("motorBilgisi") String motorBilgisi);

    @GET("/suggestions")
    ApiGetSuggestionsResponse getSuggestions(@Query("token") String token,
                                             @Query("keyword") String keyword,
                                             @Query("count") int count,
                                             @Query("fromLatitude") double fromLatitude,
                                             @Query("fromLongitude") double fromLongitude);

    @GET("/lightevents/{id}")
    ApiGetLightEventsResponse getLightEvents(@Path("id") String mobileId,
                                             @Query("token") String token,
                                             @Query("itemNumber") int count);

    @GET("/lightevents")
    ApiGetLightEventsResponse getLightEvents(@Query("token") String token,
                                             @Query("itemNumber") int count);

    @GET("/lightpositions/{id}")
    ApiGetLightPositionsResponse getLightPositions(@Path("id") String mobileId,
                                                   @Query("token") String token,
                                                   @Query("startTime") String startTime,
                                                   @Query("endTime") String endTime,
                                                   @Query("maxPosition") int maxPosition,
                                                   @Query("queryMode") int queryMode,
                                                   @Query("lineMode") int lineMode);

    @GET("/log/events/{id}")
    ApiMobile getLogEvents(@Path("id") String mobileId,
                           @Query("token") String token);

    @GET("/log/positions/{id}")
    ApiMobile getLogPositions(@Path("id") String mobileId,
                              @Query("token") String token);

    @GET("/terms")
    ApiTerms getTerms(@Query("token") String token,
                      @Query("queryMode") String queryMode);

    @GET("/intervaldata/trips/{id}")
    ApiGetTripsResponse getIntervalData(@Path("id") String mobileId,
                                        @Query("token") String token,
                                        @Query(value = "startTime", encodeValue = false) String startTime,
                                        @Query(value = "endTime", encodeValue = false) String endTime);

    @GET("/lighttrips")
    ApiLightTrips getLightTrips(@Query("token") String token,
                                @Query("mobileId") String mobileId,
                                @Query(value = "startTime", encodeValue = false) String startTime,
                                @Query(value = "endTime", encodeValue = false) String endTime);

    @GET("/score/event/{id}")
    ApiEventScore getEventScore(@Query("token") String token,
                                @Path("id") String mobileId,
                                @Query(value = "startTime", encodeValue = false) String startTime,
                                @Query(value = "endTime", encodeValue = false) String endTime);

    @GET("/customerpoint")
    ApiRegions getRegions(@Query("token") String token);

    @GET("/retrievepassword/")
    ApiGeneralResponse forgetPassword(@Query("token") String token,
                                      @Query("username") String email);

    @GET("/notification/list")
    ApiNotificationList getNotificationList(@Query("token") String token);

    @GET("/brands/")
    BrandList getBrands(@Query("token") String token);

    @GET("/brand/models/")
    BrandModelList getBrandModels(@Query("token") String token,
                                  @Query("markaId") String markaId);

    @GET("/model/series")
    EngineTypeList getEngineType(@Query("token") String token,
                                 @Query("modelId") String modelId);

    @GET("/fueltypes")
    FuelTypeList getFuelTypes(@Query("token") String token);

}
