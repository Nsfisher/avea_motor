package me.bt.avea.data.repository.mobiles.datasources.bdd.persistors;

import java.sql.SQLException;

public interface Persistor<T> {
  void persist(T data) throws SQLException;
}
