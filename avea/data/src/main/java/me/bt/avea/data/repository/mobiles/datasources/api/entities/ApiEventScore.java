package me.bt.avea.data.repository.mobiles.datasources.api.entities;

import com.google.gson.annotations.Expose;
import com.mobandme.android.transformer.compiler.Mappable;
import com.mobandme.android.transformer.compiler.Mapped;
import java.util.List;
import me.bt.avea.domain.entities.EventScore;
import me.bt.avea.domain.entities.EventScoreLg;

@Mappable(with = EventScore.class) public class ApiEventScore {

  @Expose @Mapped public String errorCount;
  @Expose @Mapped public String score;
  @Expose @Mapped public List<EventScoreLg> lg;
}
