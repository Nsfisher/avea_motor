package me.bt.avea.data.repository.mobiles.datasources.api.implementations;

import android.app.Application;
import com.mobandme.android.transformer.Transformer;
import me.bt.avea.data.preferences.PrefConstants;
import me.bt.avea.data.repository.mobiles.datasources.api.MobileApiService;
import me.bt.avea.data.repository.mobiles.datasources.api.responses.ApiBaseResponse;
import me.bt.avea.domain.entities.BaseResponse;
import me.bt.avea.repository.adduser.datasources.AddUserNetworkDataSource;
import me.bt.avea.repository.adduser.datasources.exceptions.AddUserNetworkException;
import net.grandcentrix.tray.TrayAppPreferences;

public class AddUserNetworkDataSourceImp implements AddUserNetworkDataSource {

  private static final Transformer transformer = new Transformer.Builder().build(ApiBaseResponse.class);
  private final TrayAppPreferences appPreferences;
  private MobileApiService apiService;
  private Application application;

  public AddUserNetworkDataSourceImp(MobileApiService apiService, Application application) {
    this.apiService = apiService;
    this.application = application;
    appPreferences = new TrayAppPreferences(application);
  }

  @Override public BaseResponse addUser(String activation, String username, String password, String plaka, String nickname, String brand,
      String model, String fuelType, String engineInfo) throws AddUserNetworkException {
    try {
      String token = appPreferences.getString(PrefConstants.TOKEN, "");
      ApiBaseResponse response =
          apiService.addUser(token, activation, username, password, plaka, nickname, brand, model, fuelType, engineInfo);
      BaseResponse baseResponse = transformer.transform(response, BaseResponse.class);
      return baseResponse;
    } catch (Throwable e) {
      throw new AddUserNetworkException();
    }
  }
}
