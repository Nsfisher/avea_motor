package me.bt.avea.data.preferences;

public class PrefConstants {
    public static final String TOKEN = "TOKEN";
    public static final String DEFAULT_MOBILE_ALIAS = "DEFAULT_MOBILE_ALIAS";
    public static final String DEFAULT_MOBILE_ID = "DEFAULT_MOBILE_ID";
    public static final String DEFAULT_MOBILE_ID_INDEX = "DEFAULT_MOBILE_ID_INDEX";
    public static final String DEFAULT_SAVED_ID = "DEFAULT_SAVED_ID";
    public static final String TRAFFIC_ENABLED = "TRAFFIC_ENABLED";
    public static final String U_NAME = "U_NAME";
    public static final String P_WORD = "P_WORD";
    public static final String IS_REMEMBER_CHECKED = "IS_REMEMBER_CHECKED";
    public static final String DONT_SHOW_AREA_DIALOG = "DONT_SHOW_AREA_DIALOG";
    public static final String MINOR_UPDATE_DATE = "MINOR_UPDATE_DATE";

    public static final String GCM_ID = "gcm_id";
}
