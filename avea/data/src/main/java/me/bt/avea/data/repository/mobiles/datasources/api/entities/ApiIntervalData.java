package me.bt.avea.data.repository.mobiles.datasources.api.entities;

import com.google.gson.annotations.Expose;
import com.mobandme.android.transformer.compiler.Mappable;
import com.mobandme.android.transformer.compiler.Mapped;
import java.util.List;
import me.bt.avea.domain.entities.IntervalData;
import me.bt.avea.domain.entities.LogEvent;

@Mappable(with = IntervalData.class) public class ApiIntervalData {
  @Expose @Mapped public String alias;
  @Expose @Mapped public String startPosTimeStamp;
  @Expose @Mapped public String stopPosTimeStamp;
  @Expose @Mapped public String startLbsLocation;
  @Expose @Mapped public String stopLbsLocation;
  @Expose @Mapped public String distanceTako;
  @Expose @Mapped public String deltaTime;
  @Expose @Mapped public String stopKm;
  @Expose @Mapped public String speedMax;
  @Expose @Mapped public String speedAvg;
  @Expose @Mapped public String startKm;
  @Expose @Mapped public String rolantiTotal;
  @Expose @Mapped public String distanceGps;
  @Expose @Mapped public String speedExceedTime;
  @Expose @Mapped public String eventCount;
  @Expose @Mapped public List<LogEvent> logEvents;

  public List<LogEvent> getLogEvents() {
    return logEvents;
  }

  public void setLogEvents(List<LogEvent> logEvents) {
    this.logEvents = logEvents;
  }

  public String getEventCount() {
    return eventCount;
  }

  public void setEventCount(String eventCount) {
    this.eventCount = eventCount;
  }

  public String getSpeedExceedTime() {
    return speedExceedTime;
  }

  public void setSpeedExceedTime(String speedExceedTime) {
    this.speedExceedTime = speedExceedTime;
  }

  public String getAlias() {
    return alias;
  }

  public void setAlias(String alias) {
    this.alias = alias;
  }

  public String getStartPosTimeStamp() {
    return startPosTimeStamp;
  }

  public void setStartPosTimeStamp(String startPosTimeStamp) {
    this.startPosTimeStamp = startPosTimeStamp;
  }

  public String getStopPosTimeStamp() {
    return stopPosTimeStamp;
  }

  public void setStopPosTimeStamp(String stopPosTimeStamp) {
    this.stopPosTimeStamp = stopPosTimeStamp;
  }

  public String getStartLbsLocation() {
    return startLbsLocation;
  }

  public void setStartLbsLocation(String startLbsLocation) {
    this.startLbsLocation = startLbsLocation;
  }

  public String getStopLbsLocation() {
    return stopLbsLocation;
  }

  public void setStopLbsLocation(String stopLbsLocation) {
    this.stopLbsLocation = stopLbsLocation;
  }

  public String getDistanceTako() {
    return distanceTako;
  }

  public void setDistanceTako(String distanceTako) {
    this.distanceTako = distanceTako;
  }

  public String getDeltaTime() {
    return deltaTime;
  }

  public void setDeltaTime(String deltaTime) {
    this.deltaTime = deltaTime;
  }

  public String getStopKm() {
    return stopKm;
  }

  public void setStopKm(String stopKm) {
    this.stopKm = stopKm;
  }

  public String getSpeedMax() {
    return speedMax;
  }

  public void setSpeedMax(String speedMax) {
    this.speedMax = speedMax;
  }

  public String getSpeedAvg() {
    return speedAvg;
  }

  public void setSpeedAvg(String speedAvg) {
    this.speedAvg = speedAvg;
  }

  public String getStartKm() {
    return startKm;
  }

  public void setStartKm(String startKm) {
    this.startKm = startKm;
  }

  public String getRolantiTotal() {
    return rolantiTotal;
  }

  public void setRolantiTotal(String rolantiTotal) {
    this.rolantiTotal = rolantiTotal;
  }

  public String getDistanceGps() {
    return distanceGps;
  }

  public void setDistanceGps(String distanceGps) {
    this.distanceGps = distanceGps;
  }
}
