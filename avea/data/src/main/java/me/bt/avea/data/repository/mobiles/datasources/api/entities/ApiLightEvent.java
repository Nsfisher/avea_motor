package me.bt.avea.data.repository.mobiles.datasources.api.entities;

import com.google.gson.annotations.Expose;
import com.mobandme.android.transformer.compiler.Mappable;
import com.mobandme.android.transformer.compiler.Mapped;
import me.bt.avea.domain.entities.LightEvent;

@Mappable(with = LightEvent.class) public class ApiLightEvent {
  @Expose @Mapped public String alias;
  @Expose @Mapped public String eventCode;
  @Expose @Mapped public String eventDescription;
  @Expose @Mapped public String eventValue1;
  @Expose @Mapped public String eventValue2;
  @Expose @Mapped public String eventValue3;
  @Expose @Mapped public String exceptionDescription;
  @Expose @Mapped public String exceptionNo;
  @Expose @Mapped public String lbsLocation;
  @Expose @Mapped public String mobile;
  @Expose @Mapped public String posLatitude;
  @Expose @Mapped public String posLongitude;
  @Expose @Mapped public String posSpeed;
  @Expose @Mapped public String posTimeStamp;
  @Expose @Mapped public String posTimeStr;
  @Expose @Mapped public String transactionId;
}
