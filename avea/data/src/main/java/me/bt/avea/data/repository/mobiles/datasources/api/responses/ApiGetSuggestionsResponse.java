package me.bt.avea.data.repository.mobiles.datasources.api.responses;

import com.google.gson.annotations.Expose;
import java.util.List;
import me.bt.avea.data.repository.mobiles.datasources.api.entities.ApiSuggestion;

public class ApiGetSuggestionsResponse {
  @Expose private List<ApiSuggestion> dataSuggestion;

  public List<ApiSuggestion> getDataSuggestion() {
    return dataSuggestion;
  }

  public void setDataSuggestion(List<ApiSuggestion> dataSuggestion) {
    this.dataSuggestion = dataSuggestion;
  }
}
