package me.bt.avea.data.repository.mobiles.datasources.bdd.entities;

import com.j256.ormlite.field.DatabaseField;
import com.mobandme.android.transformer.compiler.Mappable;
import com.mobandme.android.transformer.compiler.Mapped;

import me.bt.avea.data.repository.caching.strategy.ttl.TtlCachingObject;
import me.bt.avea.domain.entities.Mobile;

@Mappable(with = Mobile.class) public class BddMobile implements TtlCachingObject {

  public static final String FIELD_ID = "id";
  public static final String FIELD_MOBILE_ID = "mobileid";
  @DatabaseField public long persistedTime;
  @DatabaseField @Mapped public Integer activityStatus;
  @DatabaseField @Mapped public String addr1;
  @DatabaseField @Mapped public String addr2;
  @DatabaseField @Mapped public String alias;
  @DatabaseField @Mapped public Integer icon_type;
  @DatabaseField @Mapped public String imageName;
  @DatabaseField @Mapped public Integer kontakDurumu;
  @DatabaseField @Mapped public String lbsCustomerPoint;
  @DatabaseField @Mapped public Double lbsCustomerPointDistance;
  @DatabaseField @Mapped public String lbsPoint;
  @DatabaseField @Mapped public Double lbsPointDistance;
  @DatabaseField(columnName = FIELD_MOBILE_ID) @Mapped public Integer mobile;
  @DatabaseField @Mapped public Integer motorBlokajFlag;
  @DatabaseField @Mapped public Integer motorBlokajStatus;
  @DatabaseField @Mapped public Integer msg_flag;
  @DatabaseField @Mapped public Integer posDirectionValue;
  @DatabaseField @Mapped public Double posLatitude;
  @DatabaseField @Mapped public Double posLongitude;
  @DatabaseField @Mapped public Integer posSpeed;
  @DatabaseField @Mapped public String posTimestamp;
  @DatabaseField @Mapped public String posTimestampStrtime;
  @DatabaseField @Mapped public String SDisi3;
  @DatabaseField @Mapped public String SIsi1;
  @DatabaseField @Mapped public String SIsi2;
  @DatabaseField @Mapped public String SKapi;
  @DatabaseField @Mapped public String SNem;
  @DatabaseField @Mapped public Integer speedExceedLimit;
  @DatabaseField @Mapped public Integer speedExceedMax;
  @DatabaseField @Mapped public Integer speedLimit;
  @DatabaseField @Mapped public Integer telemetryFlag;
  @DatabaseField @Mapped public Integer themeIconId;
  @DatabaseField(generatedId = true, columnName = FIELD_ID) private int id;

  public static String getFieldId() {
    return FIELD_ID;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public Integer getActivityStatus() {
    return activityStatus;
  }

  public void setActivityStatus(Integer activityStatus) {
    this.activityStatus = activityStatus;
  }

  public String getAddr1() {
    return addr1;
  }

  public void setAddr1(String addr1) {
    this.addr1 = addr1;
  }

  public String getAddr2() {
    return addr2;
  }

  public void setAddr2(String addr2) {
    this.addr2 = addr2;
  }

  public String getAlias() {
    return alias;
  }

  public void setAlias(String alias) {
    this.alias = alias;
  }

  public Integer getIcon_type() {
    return icon_type;
  }

  public void setIcon_type(Integer icon_type) {
    this.icon_type = icon_type;
  }

  public String getImageName() {
    return imageName;
  }

  public void setImageName(String imageName) {
    this.imageName = imageName;
  }

  public Integer getKontakDurumu() {
    return kontakDurumu;
  }

  public void setKontakDurumu(Integer kontakDurumu) {
    this.kontakDurumu = kontakDurumu;
  }

  public String getLbsCustomerPoint() {
    return lbsCustomerPoint;
  }

  public void setLbsCustomerPoint(String lbsCustomerPoint) {
    this.lbsCustomerPoint = lbsCustomerPoint;
  }

  public Double getLbsCustomerPointDistance() {
    return lbsCustomerPointDistance;
  }

  public void setLbsCustomerPointDistance(Double lbsCustomerPointDistance) {
    this.lbsCustomerPointDistance = lbsCustomerPointDistance;
  }

  public String getLbsPoint() {
    return lbsPoint;
  }

  public void setLbsPoint(String lbsPoint) {
    this.lbsPoint = lbsPoint;
  }

  public Double getLbsPointDistance() {
    return lbsPointDistance;
  }

  public void setLbsPointDistance(Double lbsPointDistance) {
    this.lbsPointDistance = lbsPointDistance;
  }

  public Integer getMobile() {
    return mobile;
  }

  public void setMobile(Integer mobile) {
    this.mobile = mobile;
  }

  public Integer getMotorBlokajFlag() {
    return motorBlokajFlag;
  }

  public void setMotorBlokajFlag(Integer motorBlokajFlag) {
    this.motorBlokajFlag = motorBlokajFlag;
  }

  public Integer getMotorBlokajStatus() {
    return motorBlokajStatus;
  }

  public void setMotorBlokajStatus(Integer motorBlokajStatus) {
    this.motorBlokajStatus = motorBlokajStatus;
  }

  public Integer getMsg_flag() {
    return msg_flag;
  }

  public void setMsg_flag(Integer msg_flag) {
    this.msg_flag = msg_flag;
  }

  public Integer getPosDirectionValue() {
    return posDirectionValue;
  }

  public void setPosDirectionValue(Integer posDirectionValue) {
    this.posDirectionValue = posDirectionValue;
  }

  public Double getPosLatitude() {
    return posLatitude;
  }

  public void setPosLatitude(Double posLatitude) {
    this.posLatitude = posLatitude;
  }

  public Double getPosLongitude() {
    return posLongitude;
  }

  public void setPosLongitude(Double posLongitude) {
    this.posLongitude = posLongitude;
  }

  public Integer getPosSpeed() {
    return posSpeed;
  }

  public void setPosSpeed(Integer posSpeed) {
    this.posSpeed = posSpeed;
  }

  public String getPosTimestamp() {
    return posTimestamp;
  }

  public void setPosTimestamp(String posTimestamp) {
    this.posTimestamp = posTimestamp;
  }

  public String getPosTimestampStrtime() {
    return posTimestampStrtime;
  }

  public void setPosTimestampStrtime(String posTimestampStrtime) {
    this.posTimestampStrtime = posTimestampStrtime;
  }

  public String getSDisi3() {
    return SDisi3;
  }

  public void setSDisi3(String SDisi3) {
    this.SDisi3 = SDisi3;
  }

  public String getSIsi1() {
    return SIsi1;
  }

  public void setSIsi1(String SIsi1) {
    this.SIsi1 = SIsi1;
  }

  public String getSIsi2() {
    return SIsi2;
  }

  public void setSIsi2(String SIsi2) {
    this.SIsi2 = SIsi2;
  }

  public String getSKapi() {
    return SKapi;
  }

  public void setSKapi(String SKapi) {
    this.SKapi = SKapi;
  }

  public String getSNem() {
    return SNem;
  }

  public void setSNem(String SNem) {
    this.SNem = SNem;
  }

  public Integer getSpeedExceedLimit() {
    return speedExceedLimit;
  }

  public void setSpeedExceedLimit(Integer speedExceedLimit) {
    this.speedExceedLimit = speedExceedLimit;
  }

  public Integer getSpeedExceedMax() {
    return speedExceedMax;
  }

  public void setSpeedExceedMax(Integer speedExceedMax) {
    this.speedExceedMax = speedExceedMax;
  }

  public Integer getSpeedLimit() {
    return speedLimit;
  }

  public void setSpeedLimit(Integer speedLimit) {
    this.speedLimit = speedLimit;
  }

  public Integer getTelemetryFlag() {
    return telemetryFlag;
  }

  public void setTelemetryFlag(Integer telemetryFlag) {
    this.telemetryFlag = telemetryFlag;
  }

  public Integer getThemeIconId() {
    return themeIconId;
  }

  public void setThemeIconId(Integer themeIconId) {
    this.themeIconId = themeIconId;
  }

  @Override public long getPersistedTime() {
    return persistedTime;
  }

  public void setPersistedTime(long persistedTime) {
    this.persistedTime = persistedTime;
  }
}
