package me.bt.avea.data.repository.mobiles.datasources.api.responses;

import com.google.gson.annotations.Expose;
import java.util.List;
import me.bt.avea.data.repository.mobiles.datasources.api.entities.ApiLightEvent;

public class ApiGetLightEventsResponse {
  @Expose private List<ApiLightEvent> getEvent;

  public List<ApiLightEvent> getGetEvent() {
    return getEvent;
  }

  public void setGetEvent(List<ApiLightEvent> getEvent) {
    this.getEvent = getEvent;
  }
}
