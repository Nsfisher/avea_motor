package me.bt.avea.data.repository.mobiles.datasources.api.implementations;

import android.app.Application;

import com.mobandme.android.transformer.Transformer;

import net.grandcentrix.tray.TrayAppPreferences;

import java.util.ArrayList;
import java.util.List;

import me.bt.avea.data.preferences.PrefConstants;
import me.bt.avea.data.repository.mobiles.datasources.api.MobileApiService;
import me.bt.avea.data.repository.mobiles.datasources.api.entities.ApiMobile;
import me.bt.avea.data.repository.mobiles.datasources.api.responses.ApiGetMobileResponse;
import me.bt.avea.data.repository.mobiles.datasources.api.responses.ApiGetMobilesResponse;
import me.bt.avea.domain.entities.Mobile;
import me.bt.avea.repository.mobiles.datasources.MobilesNetworkDataSource;
import me.bt.avea.repository.mobiles.datasources.exceptions.MobilesNetworkException;

public class MobilesNetworkDataSourceImp implements MobilesNetworkDataSource {

    private static final String ORDER_ASC = "ASC";
    private static final String ORDER_DESC = "DESC";
    private static final String ORDER_PARAM_ALIAS = "ALIAS";
    private static final Transformer transformer = new Transformer.Builder().build(ApiMobile.class);
    private final TrayAppPreferences appPreferences;
    private MobileApiService apiService;

    public MobilesNetworkDataSourceImp(MobileApiService apiService, Application application) {
        this.apiService = apiService;
        appPreferences = new TrayAppPreferences(application);
    }

    @Override
    public List<Mobile> getMobiles() throws MobilesNetworkException {
        String token = appPreferences.getString(PrefConstants.TOKEN, "");
        try {
            ApiGetMobilesResponse response = apiService.getMobiles(token, ORDER_PARAM_ALIAS, ORDER_ASC);

            List<Mobile> contacts = new ArrayList<>();
            if (response.getMobile() != null) {
                for (ApiMobile apiMobile : response.getMobile()) {
                    contacts.add(transformer.transform(apiMobile, Mobile.class));
                }
            }
            return contacts;
        } catch (Throwable e) {
            try {

                ApiGetMobileResponse response = apiService.getMobilesOneRow(token, ORDER_PARAM_ALIAS, ORDER_ASC);
                List<Mobile> contacts = new ArrayList<>();
                if (response.getMobile() != null) {
                    contacts.add(transformer.transform(response.getMobile(), Mobile.class));
                }
                return contacts;

            } catch (Exception ex) {
                throw new MobilesNetworkException();
            }


        }
    }

    @Override
    public Mobile getMobile(String mobileId) throws MobilesNetworkException {
        try {
            String token = appPreferences.getString(PrefConstants.TOKEN, "");
            ApiMobile response = apiService.getMobile(mobileId, token);
            return transformer.transform(response, Mobile.class);
        } catch (Throwable e) {
            throw new MobilesNetworkException();
        }
    }
}
