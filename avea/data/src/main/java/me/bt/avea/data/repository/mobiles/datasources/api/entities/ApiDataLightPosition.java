package me.bt.avea.data.repository.mobiles.datasources.api.entities;

import com.google.gson.annotations.Expose;
import com.mobandme.android.transformer.compiler.Mappable;
import com.mobandme.android.transformer.compiler.Mapped;
import me.bt.avea.domain.entities.DataLightPosition;

@Mappable(with = DataLightPosition.class) public class ApiDataLightPosition {
  @Expose @Mapped public String eventCode;
  @Expose @Mapped public String eventDescription;
  @Expose @Mapped public String latitude;
  @Expose @Mapped public String location;
  @Expose @Mapped public String longitude;

  public String getEventCode() {
    return eventCode;
  }

  public void setEventCode(String eventCode) {
    this.eventCode = eventCode;
  }

  public String getEventDescription() {
    return eventDescription;
  }

  public void setEventDescription(String eventDescription) {
    this.eventDescription = eventDescription;
  }

  public String getLatitude() {
    return latitude;
  }

  public void setLatitude(String latitude) {
    this.latitude = latitude;
  }

  public String getLocation() {
    return location;
  }

  public void setLocation(String location) {
    this.location = location;
  }

  public String getLongitude() {
    return longitude;
  }

  public void setLongitude(String longitude) {
    this.longitude = longitude;
  }
}
