package me.bt.avea.data.repository.mobiles.datasources.api.entities;

import com.google.gson.annotations.Expose;
import com.mobandme.android.transformer.compiler.Mappable;
import com.mobandme.android.transformer.compiler.Mapped;
import java.util.List;
import me.bt.avea.domain.entities.Region;
import me.bt.avea.domain.entities.Regions;

@Mappable(with = Regions.class) public class ApiRegions {
  @Expose @Mapped public List<Region> region;
}
