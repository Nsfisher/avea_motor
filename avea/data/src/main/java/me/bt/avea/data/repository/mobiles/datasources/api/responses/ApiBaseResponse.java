package me.bt.avea.data.repository.mobiles.datasources.api.responses;

import com.google.gson.annotations.Expose;
import com.mobandme.android.transformer.compiler.Mappable;
import com.mobandme.android.transformer.compiler.Mapped;
import com.mobandme.android.transformer.compiler.Parse;

import me.bt.avea.domain.entities.BaseResponse;
import me.bt.base.data.parser.IntegerToString;
import me.bt.base.data.parser.StringToInteger;

@Mappable(with = BaseResponse.class)
public class ApiBaseResponse {
    @Mapped
    @Expose
    public String data;
    @Mapped
    @Expose
    public String errorDesc;
    @Mapped
    @Parse(originToDestinationWith = StringToInteger.class, destinationToOriginWith = IntegerToString.class)
    @Expose
    public String
            status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getErrorDesc() {
        return errorDesc;
    }

    public void setErrorDesc(String errorDesc) {
        this.errorDesc = errorDesc;
    }
}
