package me.bt.avea.data.repository.mobiles.datasources.api.entities;

import com.google.gson.annotations.Expose;
import com.mobandme.android.transformer.compiler.Mappable;
import com.mobandme.android.transformer.compiler.Mapped;
import com.mobandme.android.transformer.compiler.Parse;

import me.bt.avea.domain.entities.Mobile;
import me.bt.base.data.parser.DoubleToString;
import me.bt.base.data.parser.IntegerToString;
import me.bt.base.data.parser.StringToDouble;
import me.bt.base.data.parser.StringToInteger;

@Mappable(with = Mobile.class)
public class ApiMobile {
    @Expose
    @Mapped
    @Parse(originToDestinationWith = StringToInteger.class, destinationToOriginWith = IntegerToString.class)
    public String
            activityStatus = "";
    @Expose
    @Mapped
    public String addr1 = "";
    @Expose
    @Mapped
    public String addr2 = "";
    @Expose
    @Mapped
    public String alias = "";
    @Expose
    @Mapped
    @Parse(originToDestinationWith = StringToInteger.class, destinationToOriginWith = IntegerToString.class)
    public String
            icon_type = "";
    @Expose
    @Mapped
    public String imageName = "";
    @Expose
    @Mapped
    @Parse(originToDestinationWith = StringToInteger.class, destinationToOriginWith = IntegerToString.class)
    public String
            kontakDurumu = "";
    @Expose
    @Mapped
    public String lbsCustomerPoint = "";
    @Expose
    @Mapped
    @Parse(originToDestinationWith = StringToDouble.class, destinationToOriginWith = DoubleToString.class)
    public String
            lbsCustomerPointDistance = "";
    @Expose
    @Mapped
    public String lbsPoint = "";
    @Expose
    @Mapped
    @Parse(originToDestinationWith = StringToDouble.class, destinationToOriginWith = DoubleToString.class)
    public String
            lbsPointDistance = "";
    @Expose
    @Mapped
    @Parse(originToDestinationWith = StringToInteger.class, destinationToOriginWith = IntegerToString.class)
    public String
            mobile = "";
    @Expose
    @Mapped
    @Parse(originToDestinationWith = StringToInteger.class, destinationToOriginWith = IntegerToString.class)
    public String
            motorBlokajFlag = "";
    @Expose
    @Mapped
    @Parse(originToDestinationWith = StringToInteger.class, destinationToOriginWith = IntegerToString.class)
    public String
            motorBlokajStatus = "";
    @Expose
    @Mapped
    @Parse(originToDestinationWith = StringToInteger.class, destinationToOriginWith = IntegerToString.class)
    public String
            msg_flag = "";
    @Expose
    @Mapped
    @Parse(originToDestinationWith = StringToInteger.class, destinationToOriginWith = IntegerToString.class)
    public String
            posDirectionValue = "";
    @Expose
    @Mapped
    @Parse(originToDestinationWith = StringToDouble.class, destinationToOriginWith = DoubleToString.class)
    public String
            posLatitude = "";
    @Expose
    @Mapped
    @Parse(originToDestinationWith = StringToDouble.class, destinationToOriginWith = DoubleToString.class)
    public String
            posLongitude = "";
    @Expose
    @Mapped
    @Parse(originToDestinationWith = StringToInteger.class, destinationToOriginWith = IntegerToString.class)
    public String
            posSpeed = "";
    @Expose
    @Mapped
    public String posTimestamp = "";
    @Expose
    @Mapped
    public String posTimestampStrtime = "";
    @Expose
    @Mapped
    public String SDisi3 = "";
    @Expose
    @Mapped
    public String SIsi1 = "";
    @Expose
    @Mapped
    public String SIsi2 = "";
    @Expose
    @Mapped
    public String SKapi = "";
    @Expose
    @Mapped
    public String SNem = "";
    @Expose
    @Mapped
    @Parse(originToDestinationWith = StringToInteger.class, destinationToOriginWith = IntegerToString.class)
    public String
            speedExceedLimit = "";
    @Expose
    @Mapped
    @Parse(originToDestinationWith = StringToInteger.class, destinationToOriginWith = IntegerToString.class)
    public String
            speedExceedMax = "";
    @Expose
    @Mapped
    @Parse(originToDestinationWith = StringToInteger.class, destinationToOriginWith = IntegerToString.class)
    public String
            speedLimit = "";
    @Expose
    @Mapped
    @Parse(originToDestinationWith = StringToInteger.class, destinationToOriginWith = IntegerToString.class)
    public String
            telemetryFlag = "";
    @Expose
    @Mapped
    @Parse(originToDestinationWith = StringToInteger.class, destinationToOriginWith = IntegerToString.class)
    public String
            themeIconId = "";

    public String getThemeIconId() {
        return themeIconId;
    }

    public void setThemeIconId(String themeIconId) {
        this.themeIconId = themeIconId;
    }

    public String getActivityStatus() {
        return activityStatus;
    }

    public void setActivityStatus(String activityStatus) {
        this.activityStatus = activityStatus;
    }

    public String getAddr1() {
        return addr1;
    }

    public void setAddr1(String addr1) {
        this.addr1 = addr1;
    }

    public String getAddr2() {
        return addr2;
    }

    public void setAddr2(String addr2) {
        this.addr2 = addr2;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getIcon_type() {
        return icon_type;
    }

    public void setIcon_type(String icon_type) {
        this.icon_type = icon_type;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getKontakDurumu() {
        return kontakDurumu;
    }

    public void setKontakDurumu(String kontakDurumu) {
        this.kontakDurumu = kontakDurumu;
    }

    public String getLbsCustomerPoint() {
        return lbsCustomerPoint;
    }

    public void setLbsCustomerPoint(String lbsCustomerPoint) {
        this.lbsCustomerPoint = lbsCustomerPoint;
    }

    public String getLbsCustomerPointDistance() {
        return lbsCustomerPointDistance;
    }

    public void setLbsCustomerPointDistance(String lbsCustomerPointDistance) {
        this.lbsCustomerPointDistance = lbsCustomerPointDistance;
    }

    public String getLbsPoint() {
        return lbsPoint;
    }

    public void setLbsPoint(String lbsPoint) {
        this.lbsPoint = lbsPoint;
    }

    public String getLbsPointDistance() {
        return lbsPointDistance;
    }

    public void setLbsPointDistance(String lbsPointDistance) {
        this.lbsPointDistance = lbsPointDistance;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getMotorBlokajFlag() {
        return motorBlokajFlag;
    }

    public void setMotorBlokajFlag(String motorBlokajFlag) {
        this.motorBlokajFlag = motorBlokajFlag;
    }

    public String getMotorBlokajStatus() {
        return motorBlokajStatus;
    }

    public void setMotorBlokajStatus(String motorBlokajStatus) {
        this.motorBlokajStatus = motorBlokajStatus;
    }

    public String getMsg_flag() {
        return msg_flag;
    }

    public void setMsg_flag(String msg_flag) {
        this.msg_flag = msg_flag;
    }

    public String getPosDirectionValue() {
        return posDirectionValue;
    }

    public void setPosDirectionValue(String posDirectionValue) {
        this.posDirectionValue = posDirectionValue;
    }

    public String getPosLatitude() {
        return posLatitude;
    }

    public void setPosLatitude(String posLatitude) {
        this.posLatitude = posLatitude;
    }

    public String getPosLongitude() {
        return posLongitude;
    }

    public void setPosLongitude(String posLongitude) {
        this.posLongitude = posLongitude;
    }

    public String getPosSpeed() {
        return posSpeed;
    }

    public void setPosSpeed(String posSpeed) {
        this.posSpeed = posSpeed;
    }

    public String getPosTimestamp() {
        return posTimestamp;
    }

    public void setPosTimestamp(String posTimestamp) {
        this.posTimestamp = posTimestamp;
    }

    public String getPosTimestampStrtime() {
        return posTimestampStrtime;
    }

    public void setPosTimestampStrtime(String posTimestampStrtime) {
        this.posTimestampStrtime = posTimestampStrtime;
    }

    public String getSDisi3() {
        return SDisi3;
    }

    public void setSDisi3(String SDisi3) {
        this.SDisi3 = SDisi3;
    }

    public String getSIsi1() {
        return SIsi1;
    }

    public void setSIsi1(String SIsi1) {
        this.SIsi1 = SIsi1;
    }

    public String getSIsi2() {
        return SIsi2;
    }

    public void setSIsi2(String SIsi2) {
        this.SIsi2 = SIsi2;
    }

    public String getSKapi() {
        return SKapi;
    }

    public void setSKapi(String SKapi) {
        this.SKapi = SKapi;
    }

    public String getSNem() {
        return SNem;
    }

    public void setSNem(String SNem) {
        this.SNem = SNem;
    }

    public String getSpeedExceedLimit() {
        return speedExceedLimit;
    }

    public void setSpeedExceedLimit(String speedExceedLimit) {
        this.speedExceedLimit = speedExceedLimit;
    }

    public String getSpeedExceedMax() {
        return speedExceedMax;
    }

    public void setSpeedExceedMax(String speedExceedMax) {
        this.speedExceedMax = speedExceedMax;
    }

    public String getSpeedLimit() {
        return speedLimit;
    }

    public void setSpeedLimit(String speedLimit) {
        this.speedLimit = speedLimit;
    }

    public String getTelemetryFlag() {
        return telemetryFlag;
    }

    public void setTelemetryFlag(String telemetryFlag) {
        this.telemetryFlag = telemetryFlag;
    }
}
