package me.bt.avea.data.repository.mobiles.datasources.api.entities;

import com.google.gson.annotations.Expose;
import com.mobandme.android.transformer.compiler.Mappable;
import com.mobandme.android.transformer.compiler.Mapped;
import me.bt.avea.domain.entities.GeneralResponse;

@Mappable(with = GeneralResponse.class) public class ApiGeneralResponse {
  @Expose @Mapped public String data;
  @Expose @Mapped public String errorDesc;
  @Expose @Mapped public String status;
}
