package me.bt.avea.data.repository.mobiles.datasources.api.implementations;

import android.app.Application;
import com.mobandme.android.transformer.Transformer;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import me.bt.avea.data.preferences.PrefConstants;
import me.bt.avea.data.repository.mobiles.datasources.api.MobileApiService;
import me.bt.avea.data.repository.mobiles.datasources.api.entities.ApiIntervalData;
import me.bt.avea.data.repository.mobiles.datasources.api.responses.ApiGetTripsResponse;
import me.bt.avea.domain.entities.IntervalData;
import me.bt.avea.repository.trips.TripsNetworkDataSource;
import me.bt.avea.repository.trips.datasources.exceptions.TripsNetworkException;
import me.bt.avea.util.AveaUtils;
import net.grandcentrix.tray.TrayAppPreferences;

public class TripsNetworkDataSourceImp implements TripsNetworkDataSource {

  private static final Transformer transformer = new Transformer.Builder().build(ApiIntervalData.class);
  private final TrayAppPreferences preferences;
  private final MobileApiService apiService;
  private final Application application;

  public TripsNetworkDataSourceImp(MobileApiService apiService, Application application, TrayAppPreferences preferences) {
    this.apiService = apiService;
    this.application = application;
    this.preferences = preferences;
  }

  @Override public List<IntervalData> getTrips(String mobileId, Date startTime, Date endTime) throws TripsNetworkException {
    try {
      String token = preferences.getString(PrefConstants.TOKEN, "");
      ApiGetTripsResponse response =
          apiService.getIntervalData(mobileId, token, AveaUtils.getDateString(startTime), AveaUtils.getDateString(endTime));
      List<IntervalData> intervalDatas = new ArrayList<>();
      if (response.getIntervalDatas() != null) {
        for (ApiIntervalData apiIntervalData : response.getIntervalDatas()) {
          intervalDatas.add(transformer.transform(apiIntervalData, IntervalData.class));
        }
      }
      return intervalDatas;
    } catch (Throwable e) {
      throw new TripsNetworkException();
    }
  }
}
