package me.bt.avea.data.repository.caching.strategy.nullsafe;

import me.bt.avea.data.repository.caching.strategy.CachingStrategy;

public class NotNullCachingStrategy<T> implements CachingStrategy<T> {
  @Override public boolean isValid(T data) {
    return data != null;
  }
}
