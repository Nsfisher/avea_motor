package me.bt.avea.data.repository.mobiles.datasources.api.implementations;

import android.app.Application;
import com.mobandme.android.transformer.Transformer;
import java.util.ArrayList;
import java.util.List;
import me.bt.avea.data.preferences.PrefConstants;
import me.bt.avea.data.repository.mobiles.datasources.api.MobileApiService;
import me.bt.avea.data.repository.mobiles.datasources.api.entities.ApiLightEvent;
import me.bt.avea.data.repository.mobiles.datasources.api.responses.ApiGetLightEventsResponse;
import me.bt.avea.domain.entities.LightEvent;
import me.bt.avea.repository.lightevents.LightEventsNetworkDataSource;
import me.bt.avea.repository.lightevents.datasources.exceptions.LightEventsNetworkException;
import net.grandcentrix.tray.TrayAppPreferences;

public class LightEventsNetworkDataSourceImp implements LightEventsNetworkDataSource {

  private static final Transformer transformer = new Transformer.Builder().build(ApiLightEvent.class);
  private final TrayAppPreferences preferences;
  private final MobileApiService apiService;
  private final Application application;

  public LightEventsNetworkDataSourceImp(MobileApiService apiService, Application application, TrayAppPreferences preferences) {
    this.apiService = apiService;
    this.application = application;
    this.preferences = preferences;
  }

  @Override public List<LightEvent> getLightEvents(String mobileId, int itemNumber) throws LightEventsNetworkException {
    try {
      String token = preferences.getString(PrefConstants.TOKEN, "");
      ApiGetLightEventsResponse response = apiService.getLightEvents(mobileId, token, itemNumber);
      List<LightEvent> lightEvents = new ArrayList<>();
      if (response.getGetEvent() != null) {
        for (ApiLightEvent apiLightEvent : response.getGetEvent()) {
          lightEvents.add(transformer.transform(apiLightEvent, LightEvent.class));
        }
      }
      return lightEvents;
    } catch (Throwable e) {
      throw new LightEventsNetworkException();
    }
  }

  @Override public List<LightEvent> getLightEvents(int itemNumber) throws LightEventsNetworkException {
    try {
      String token = preferences.getString(PrefConstants.TOKEN, "");
      ApiGetLightEventsResponse response = apiService.getLightEvents(token, itemNumber);
      List<LightEvent> lightEvents = new ArrayList<>();
      if (response.getGetEvent() != null) {
        for (ApiLightEvent apiLightEvent : response.getGetEvent()) {
          lightEvents.add(transformer.transform(apiLightEvent, LightEvent.class));
        }
      }
      return lightEvents;
    } catch (Throwable e) {
      throw new LightEventsNetworkException();
    }
  }
}
