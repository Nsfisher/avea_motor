package me.bt.avea.data.repository.mobiles.datasources.api.entities;

import com.google.gson.annotations.Expose;
import com.mobandme.android.transformer.compiler.Mappable;
import com.mobandme.android.transformer.compiler.Mapped;
import com.mobandme.android.transformer.compiler.Parse;

import me.bt.avea.domain.entities.Suggestion;
import me.bt.base.data.parser.DoubleToString;
import me.bt.base.data.parser.StringToDouble;

@Mappable(with = Suggestion.class)
public class ApiSuggestion {
    @Expose
    @Mapped
    @Parse(originToDestinationWith = StringToDouble.class, destinationToOriginWith = DoubleToString.class)
    public String
            distance;
    @Expose
    @Mapped
    public String fromTable;
    @Expose
    @Mapped
    public String id;
    @Expose
    @Mapped
    @Parse(originToDestinationWith = StringToDouble.class, destinationToOriginWith = DoubleToString.class)
    public String
            latitude;
    @Expose
    @Mapped
    @Parse(originToDestinationWith = StringToDouble.class, destinationToOriginWith = DoubleToString.class)
    public String
            longitude;
    @Expose
    @Mapped
    public String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getFromTable() {
        return fromTable;
    }

    public void setFromTable(String fromTable) {
        this.fromTable = fromTable;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}
