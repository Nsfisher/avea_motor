package me.bt.avea.data.repository.mobiles.datasources.bdd;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.mobandme.android.transformer.Transformer;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import me.bt.avea.data.repository.caching.strategy.CachingStrategy;
import me.bt.avea.data.repository.caching.strategy.list.ListCachingStrategy;
import me.bt.avea.data.repository.mobiles.datasources.bdd.entities.BddMobile;
import me.bt.avea.data.repository.mobiles.datasources.bdd.persistors.Persistor;
import me.bt.avea.domain.entities.Mobile;
import me.bt.avea.repository.mobiles.datasources.MobileBddDataSource;
import me.bt.avea.repository.mobiles.datasources.exceptions.DeleteMobileException;
import me.bt.avea.repository.mobiles.datasources.exceptions.InvalidCacheException;
import me.bt.avea.repository.mobiles.datasources.exceptions.ObtainBddMobileException;
import me.bt.avea.repository.mobiles.datasources.exceptions.ObtainMobileBddException;
import me.bt.avea.repository.mobiles.datasources.exceptions.PersistMobileBddException;
import me.bt.avea.repository.mobiles.datasources.exceptions.UnknownObtainMobileException;
import me.bt.avea.repository.mobiles.datasources.exceptions.UnknownPersistMobileException;

public class MobileBddDataSourceImp implements MobileBddDataSource {

    private static final Transformer transformer = new Transformer.Builder().build(BddMobile.class);
    private final Dao<BddMobile, Integer> daoMobile;
    private final Persistor<BddMobile> persistor;
    private final CachingStrategy<BddMobile> cachingStrategy;
    private final ListCachingStrategy<BddMobile> listCachingStrategy;

    public MobileBddDataSourceImp(Persistor<BddMobile> persistor, Dao<BddMobile, Integer> daoMobile,
                                  CachingStrategy<BddMobile> cachingStrategy, ListCachingStrategy<BddMobile> listCachingStrategy) {
        this.daoMobile = daoMobile;
        this.persistor = persistor;
        this.cachingStrategy = cachingStrategy;
        this.listCachingStrategy = listCachingStrategy;
    }

    @Override
    public List<Mobile> getMobiles() throws ObtainMobileBddException, UnknownObtainMobileException, InvalidCacheException {
        try {
            List<BddMobile> bddContacts = daoMobile.queryForAll();
            if (!listCachingStrategy.isValid(bddContacts)) {
                deleteBddMobiles(listCachingStrategy.candidatesToPurgue(bddContacts));
                throw new InvalidCacheException();
            }
            ArrayList<Mobile> contacts = new ArrayList<>();
            for (BddMobile bddContact : bddContacts) {
                contacts.add(transformer.transform(bddContact, Mobile.class));
            }
            return contacts;
        } catch (java.sql.SQLException e) {
            throw new ObtainMobileBddException();
        } catch (Throwable e) {
            throw new UnknownObtainMobileException();
        }
    }

    @Override
    public void persist(List<Mobile> mobiles) throws PersistMobileBddException, UnknownPersistMobileException {
        try {
            for (Mobile mobile : mobiles) {
                BddMobile bddContact = transformer.transform(mobile, BddMobile.class);
                bddContact.setPersistedTime(System.currentTimeMillis());
                persistor.persist(bddContact);
            }
        } catch (SQLException e) {
            throw new PersistMobileBddException();
        } catch (Throwable e) {
            throw new UnknownPersistMobileException();
        }
    }

    @Override
    public Mobile obtain(String mobileId) throws InvalidCacheException, ObtainBddMobileException {
        //try {
        //  BddMobile bddMobile =
        //      daoMobile.queryBuilder().where().eq(BddMobile.FIELD_MOBILE_ID, md5).queryForFirst();
        //  if (!cachingStrategy.isValid(bddMobile)) {
        //    throw new InvalidCacheException();
        //  }
        //  return transformer.transform(bddMobile, Mobile.class);
        //} catch (Throwable e) {
        //  throw new ObtainBddMobileException();
        //}
        //TODO hakan
        return null;
    }

    @Override
    public void delete(List<Mobile> purgue) throws DeleteMobileException {
        if (purgue != null && purgue.size() > 0) {
            try {
                List<Integer> deleteMobileIds = new ArrayList<>();
                for (Mobile purgueMobile : purgue) {
                    deleteMobileIds.add(purgueMobile.getMobile());
                }
                internalDeleteMobiles(deleteMobileIds);
            } catch (Throwable e) {
                throw new DeleteMobileException();
            }
        }
    }

    private void deleteBddMobiles(List<BddMobile> purgue) throws SQLException {
        if (purgue != null && purgue.size() > 0) {
            List<Integer> deleteMd5s = new ArrayList<>();
            for (BddMobile purgeMobile : purgue) {
                deleteMd5s.add(purgeMobile.getMobile());
            }
            internalDeleteMobiles(deleteMd5s);
        }
    }

    private void internalDeleteMobiles(List<Integer> deleteMobileIds) throws SQLException {
        DeleteBuilder<BddMobile, Integer> deleteBuilder = daoMobile.deleteBuilder();
        deleteBuilder.where().in(BddMobile.FIELD_MOBILE_ID, deleteMobileIds);
        deleteBuilder.delete();
    }
}
