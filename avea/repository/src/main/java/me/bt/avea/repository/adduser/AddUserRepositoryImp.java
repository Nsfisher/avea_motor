package me.bt.avea.repository.adduser;

import me.bt.avea.domain.entities.BaseResponse;
import me.bt.avea.domain.interactions.adduser.exceptions.AddUserException;
import me.bt.avea.domain.repository.AddUserRepository;
import me.bt.avea.repository.adduser.datasources.AddUserNetworkDataSource;
import me.bt.avea.repository.adduser.datasources.exceptions.AddUserNetworkException;

public class AddUserRepositoryImp implements AddUserRepository {

  private final AddUserNetworkDataSource networkDataSource;

  public AddUserRepositoryImp(AddUserNetworkDataSource networkDataSource) {
    this.networkDataSource = networkDataSource;
  }

  @Override public BaseResponse addUser(String activation, String username, String password, String plaka, String nickname, String brand,
      String model, String fuelType, String engineInfo) throws AddUserException {
    try {
      return networkDataSource.addUser(activation, username, password, plaka, nickname, brand, model, fuelType, engineInfo);
    } catch (AddUserNetworkException e) {
      throw new AddUserException();
    }
  }
}
