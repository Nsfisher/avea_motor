package me.bt.avea.repository.mobiles.datasources;

import java.util.List;
import me.bt.avea.domain.entities.Mobile;
import me.bt.avea.repository.mobiles.datasources.exceptions.MobilesNetworkException;
import me.bt.avea.repository.mobiles.datasources.exceptions.UnknownObtainMobileException;

public interface MobilesNetworkDataSource {
  List<Mobile> getMobiles() throws MobilesNetworkException, UnknownObtainMobileException;

  Mobile getMobile(String mobileId) throws MobilesNetworkException;
}
