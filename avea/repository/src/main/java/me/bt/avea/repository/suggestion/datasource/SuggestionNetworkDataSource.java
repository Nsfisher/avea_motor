package me.bt.avea.repository.suggestion.datasource;

import java.util.List;
import me.bt.avea.domain.entities.Suggestion;
import me.bt.avea.repository.suggestion.datasource.exceptions.SuggestionNetworkException;

public interface SuggestionNetworkDataSource {
  List<Suggestion> getSuggestion(String keyword, double latitude, double longitude) throws SuggestionNetworkException;
}
