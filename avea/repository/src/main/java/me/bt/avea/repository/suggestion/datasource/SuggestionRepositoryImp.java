package me.bt.avea.repository.suggestion.datasource;

import java.util.List;

import me.bt.avea.domain.entities.Suggestion;
import me.bt.avea.domain.interactions.suggestion.exceptions.SuggestionException;
import me.bt.avea.domain.repository.SuggestionRepository;
import me.bt.avea.repository.suggestion.datasource.exceptions.SuggestionNetworkException;

public class SuggestionRepositoryImp implements SuggestionRepository {

    private final SuggestionNetworkDataSource networkDataSource;

    public SuggestionRepositoryImp(SuggestionNetworkDataSource networkDataSource) {
        this.networkDataSource = networkDataSource;
    }

    @Override
    public List<Suggestion> getSuggestion(String keyword, double latitude, double longitude) throws SuggestionException {
        try {
            return networkDataSource.getSuggestion(keyword, latitude, longitude);
        } catch (SuggestionNetworkException e) {
            throw new SuggestionException();
        }
    }
}
