package me.bt.avea.repository.trips;

import java.util.Date;
import java.util.List;
import me.bt.avea.domain.entities.IntervalData;
import me.bt.avea.repository.trips.datasources.exceptions.TripsNetworkException;

public interface TripsNetworkDataSource {
  List<IntervalData> getTrips(String mobileId, Date startTime, Date endTime) throws TripsNetworkException;
}
