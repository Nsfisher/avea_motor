package me.bt.avea.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AveaUtils {

  public static String getDateString(Date date) {
    if (date != null) {
      final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd+HH:mm:ss");
      return dateFormat.format(date);
    } else {
      return null;
    }
  }

  public static boolean isValidEmailAddress(String email) {
    String ePattern =
        "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
    java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
    java.util.regex.Matcher m = p.matcher(email);
    return m.matches();
  }
}
