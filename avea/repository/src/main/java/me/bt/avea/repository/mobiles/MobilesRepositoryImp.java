package me.bt.avea.repository.mobiles;

import java.util.List;
import me.bt.avea.domain.entities.Mobile;
import me.bt.avea.domain.interactions.mobiles.exception.MobileException;
import me.bt.avea.domain.interactions.mobiles.exception.MobilesException;
import me.bt.avea.domain.repository.MobilesRepository;
import me.bt.avea.repository.mobiles.datasources.MobileBddDataSource;
import me.bt.avea.repository.mobiles.datasources.MobilesNetworkDataSource;
import me.bt.avea.repository.mobiles.datasources.exceptions.InvalidCacheException;
import me.bt.avea.repository.mobiles.datasources.exceptions.MobilesNetworkException;
import me.bt.avea.repository.mobiles.datasources.exceptions.ObtainMobileBddException;
import me.bt.avea.repository.mobiles.datasources.exceptions.PersistMobileBddException;
import me.bt.avea.repository.mobiles.datasources.exceptions.UnknownObtainMobileException;
import me.bt.avea.repository.mobiles.datasources.exceptions.UnknownPersistMobileException;

public class MobilesRepositoryImp implements MobilesRepository {

  private final MobilesNetworkDataSource networkDataSource;
  private final MobileBddDataSource bddDataSource;

  public MobilesRepositoryImp(MobilesNetworkDataSource networkDataSource, MobileBddDataSource bddDataSource) {
    this.networkDataSource = networkDataSource;
    this.bddDataSource = bddDataSource;
  }

  @Override public List<Mobile> getMobiles() throws MobilesException {
    List<Mobile> mobiles = null;
    try {
      mobiles = bddDataSource.getMobiles();
    } catch (ObtainMobileBddException | InvalidCacheException | UnknownObtainMobileException exception) {
      try {
        mobiles = networkDataSource.getMobiles();
        bddDataSource.persist(mobiles);
      } catch (UnknownObtainMobileException | MobilesNetworkException networkException) {
        throw new MobilesException();
      } catch (PersistMobileBddException | UnknownPersistMobileException persistException) {
        persistException.printStackTrace();
      }
    }
    return mobiles;
  }

  @Override public List<Mobile> getMobilesSkipCache() throws MobilesException {
    List<Mobile> mobiles = null;
    try {
      mobiles = networkDataSource.getMobiles();
      bddDataSource.persist(mobiles);
    } catch (UnknownObtainMobileException | MobilesNetworkException networkException) {
      throw new MobilesException();
    } catch (PersistMobileBddException | UnknownPersistMobileException persistException) {
      persistException.printStackTrace();
    }
    return mobiles;
  }

  @Override public Mobile getMobile(String mobileId) throws MobileException { //TODO hakan get from db
    try {
      return networkDataSource.getMobile(mobileId);
    } catch (MobilesNetworkException e) {
      throw new MobileException();
    }
  }
}
