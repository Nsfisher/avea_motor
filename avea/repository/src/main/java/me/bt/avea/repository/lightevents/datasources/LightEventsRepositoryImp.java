package me.bt.avea.repository.lightevents.datasources;

import java.util.List;
import me.bt.avea.domain.entities.LightEvent;
import me.bt.avea.domain.interactions.lightevents.exceptions.LightEventsException;
import me.bt.avea.domain.repository.LightEventsRepository;
import me.bt.avea.repository.lightevents.LightEventsNetworkDataSource;
import me.bt.avea.repository.lightevents.datasources.exceptions.LightEventsNetworkException;

public class LightEventsRepositoryImp implements LightEventsRepository {

  private final LightEventsNetworkDataSource networkDataSource;

  public LightEventsRepositoryImp(LightEventsNetworkDataSource networkDataSource) {
    this.networkDataSource = networkDataSource;
  }

  @Override public List<LightEvent> getLightEvents(String mobileId, int itemNumber) throws LightEventsException {
    try {
      return networkDataSource.getLightEvents(mobileId, itemNumber);
    } catch (LightEventsNetworkException e) {
      throw new LightEventsException();
    }
  }

  @Override public List<LightEvent> getLightEvents(int itemNumber) throws LightEventsException {
    try {
      return networkDataSource.getLightEvents(itemNumber);
    } catch (LightEventsNetworkException e) {
      throw new LightEventsException();
    }
  }
}
