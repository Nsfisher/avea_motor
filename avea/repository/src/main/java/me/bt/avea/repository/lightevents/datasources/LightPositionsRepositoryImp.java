package me.bt.avea.repository.lightevents.datasources;

import java.util.List;
import me.bt.avea.domain.entities.DataLightPosition;
import me.bt.avea.domain.interactions.lightevents.exceptions.LightPositionsException;
import me.bt.avea.domain.repository.LightPositionsRepository;
import me.bt.avea.repository.lightevents.LightPositionsNetworkDataSource;
import me.bt.avea.repository.lightevents.datasources.exceptions.LightPositionsNetworkException;

public class LightPositionsRepositoryImp implements LightPositionsRepository {

  private final LightPositionsNetworkDataSource networkDataSource;

  public LightPositionsRepositoryImp(LightPositionsNetworkDataSource networkDataSource) {
    this.networkDataSource = networkDataSource;
  }

  @Override
  public List<DataLightPosition> getLightPositions(String mobileId, String startDate, String endDate, int maxPoints, int queryMode,
      int lineMode) throws LightPositionsException {
    try {
      return networkDataSource.getLightPositions(mobileId, startDate, endDate, maxPoints, queryMode, lineMode);
    } catch (LightPositionsNetworkException e) {
      throw new LightPositionsException();
    }
  }
}
