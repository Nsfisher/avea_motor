package me.bt.avea.repository.register.datasources;

import me.bt.avea.repository.register.datasources.exceptions.RegisterNetworkException;

public interface RegisterNetworkDataSource {
  String register(String username, String password, String company, String language) throws RegisterNetworkException;
}
