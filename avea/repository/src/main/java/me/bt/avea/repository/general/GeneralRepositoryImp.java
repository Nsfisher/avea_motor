package me.bt.avea.repository.general;

import java.util.Date;

import me.bt.avea.domain.entities.BrandList;
import me.bt.avea.domain.entities.BrandModelList;
import me.bt.avea.domain.entities.EngineTypeList;
import me.bt.avea.domain.entities.EventScore;
import me.bt.avea.domain.entities.FuelTypeList;
import me.bt.avea.domain.entities.GeneralResponse;
import me.bt.avea.domain.entities.LightTrips;
import me.bt.avea.domain.entities.NotificationList;
import me.bt.avea.domain.entities.Regions;
import me.bt.avea.domain.entities.Terms;
import me.bt.avea.domain.interactions.general.exceptions.BrandListException;
import me.bt.avea.domain.interactions.general.exceptions.BrandModelListException;
import me.bt.avea.domain.interactions.general.exceptions.EngineTypeListException;
import me.bt.avea.domain.interactions.general.exceptions.EventScoreException;
import me.bt.avea.domain.interactions.general.exceptions.ForgetPasswordException;
import me.bt.avea.domain.interactions.general.exceptions.FuelTypeListException;
import me.bt.avea.domain.interactions.general.exceptions.LightTripsException;
import me.bt.avea.domain.interactions.general.exceptions.LogoutException;
import me.bt.avea.domain.interactions.general.exceptions.NotificationListException;
import me.bt.avea.domain.interactions.general.exceptions.RegionsException;
import me.bt.avea.domain.interactions.general.exceptions.TermsException;
import me.bt.avea.domain.repository.GeneralRepository;
import me.bt.avea.repository.general.datasources.GeneralNetworkDataSource;
import me.bt.avea.repository.general.datasources.exceptions.BrandListNetworkException;
import me.bt.avea.repository.general.datasources.exceptions.BrandModelListNetworkException;
import me.bt.avea.repository.general.datasources.exceptions.EngineTypeListNetworkException;
import me.bt.avea.repository.general.datasources.exceptions.EventScoreNetworkException;
import me.bt.avea.repository.general.datasources.exceptions.ForgetPasswordNetworkException;
import me.bt.avea.repository.general.datasources.exceptions.FuelTypeListNetworkException;
import me.bt.avea.repository.general.datasources.exceptions.LightTripsNetworkException;
import me.bt.avea.repository.general.datasources.exceptions.LogoutNetworkException;
import me.bt.avea.repository.general.datasources.exceptions.NotificationListNetworkException;
import me.bt.avea.repository.general.datasources.exceptions.RegionsNetworkException;
import me.bt.avea.repository.general.datasources.exceptions.TermsNetworkException;

public class GeneralRepositoryImp implements GeneralRepository {

  private final GeneralNetworkDataSource networkDataSource;

  public GeneralRepositoryImp(GeneralNetworkDataSource networkDataSource) {
    this.networkDataSource = networkDataSource;
  }

  @Override public LightTrips getLightTrips(String mobileId, Date startDate, Date endDate) throws LightTripsException {
    try {
      return networkDataSource.getLightTrips(mobileId, startDate, endDate);
    } catch (LightTripsNetworkException e) {
      throw new LightTripsException();
    }
  }

  @Override public EventScore getEventScore(String mobileId, Date startDate, Date endDate) throws EventScoreException {
    try {
      return networkDataSource.getEventScore(mobileId, startDate, endDate);
    } catch (EventScoreNetworkException e) {
      throw new EventScoreException();
    }
  }

  @Override public Regions getRegions() throws RegionsException {
    try {
      return networkDataSource.getRegions();
    } catch (RegionsNetworkException e) {
      throw new RegionsException();
    }
  }

  @Override public Terms getTerms() throws TermsException {
    try {
      return networkDataSource.getTerms();
    } catch (TermsNetworkException e) {
      throw new TermsException();
    }
  }

  @Override public GeneralResponse forgetPassword(String email) throws ForgetPasswordException {
    try {
      return networkDataSource.forgetPassword(email);
    } catch (ForgetPasswordNetworkException e) {
      throw new ForgetPasswordException();
    }
  }

  @Override public NotificationList getNotificationList() throws NotificationListException {
    try {
      return networkDataSource.getNotificationList();
    } catch (NotificationListNetworkException e) {
      throw new NotificationListException();
    }
  }

  @Override public BrandList getBrands() throws BrandListException {
    try {
      return networkDataSource.getBrandList();
    } catch (BrandListNetworkException e) {
      throw new BrandListException();
    }
  }

  @Override public BrandModelList getBrandModels(String brandId) throws BrandModelListException {
    try {
      return networkDataSource.getBrandModels(brandId);
    } catch (BrandModelListNetworkException e) {
      throw new BrandModelListException();
    }
  }

  @Override public EngineTypeList getEngineType(String modelId) throws EngineTypeListException {
    try {
      return networkDataSource.getEngineType(modelId);
    } catch (EngineTypeListNetworkException e) {
      throw new EngineTypeListException();
    }
  }

  @Override public FuelTypeList getFuelTypes() throws FuelTypeListException {
    try {
      return networkDataSource.getFuelTypes();
    } catch (FuelTypeListNetworkException e) {
      throw new FuelTypeListException();
    }
  }

  @Override public GeneralResponse logout() throws LogoutException {
    try {
      return networkDataSource.logout();
    } catch (LogoutNetworkException e) {
      throw new LogoutException();
    }
  }
}
