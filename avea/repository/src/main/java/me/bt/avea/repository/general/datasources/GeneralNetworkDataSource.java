package me.bt.avea.repository.general.datasources;

import java.util.Date;

import me.bt.avea.domain.entities.BrandList;
import me.bt.avea.domain.entities.BrandModelList;
import me.bt.avea.domain.entities.EngineTypeList;
import me.bt.avea.domain.entities.EventScore;
import me.bt.avea.domain.entities.FuelTypeList;
import me.bt.avea.domain.entities.GeneralResponse;
import me.bt.avea.domain.entities.LightTrips;
import me.bt.avea.domain.entities.NotificationList;
import me.bt.avea.domain.entities.Regions;
import me.bt.avea.domain.entities.Terms;
import me.bt.avea.repository.general.datasources.exceptions.BrandListNetworkException;
import me.bt.avea.repository.general.datasources.exceptions.BrandModelListNetworkException;
import me.bt.avea.repository.general.datasources.exceptions.EngineTypeListNetworkException;
import me.bt.avea.repository.general.datasources.exceptions.EventScoreNetworkException;
import me.bt.avea.repository.general.datasources.exceptions.ForgetPasswordNetworkException;
import me.bt.avea.repository.general.datasources.exceptions.FuelTypeListNetworkException;
import me.bt.avea.repository.general.datasources.exceptions.LightTripsNetworkException;
import me.bt.avea.repository.general.datasources.exceptions.LogoutNetworkException;
import me.bt.avea.repository.general.datasources.exceptions.NotificationListNetworkException;
import me.bt.avea.repository.general.datasources.exceptions.RegionsNetworkException;
import me.bt.avea.repository.general.datasources.exceptions.TermsNetworkException;

public interface GeneralNetworkDataSource {
  LightTrips getLightTrips(String mobileId, Date startDate, Date endDate) throws LightTripsNetworkException;

  EventScore getEventScore(String mobileId, Date startDate, Date endDate) throws EventScoreNetworkException;

  Regions getRegions() throws RegionsNetworkException;

  Terms getTerms() throws TermsNetworkException;

  GeneralResponse forgetPassword(String email) throws ForgetPasswordNetworkException;

  NotificationList getNotificationList() throws NotificationListNetworkException;

  BrandList getBrandList() throws BrandListNetworkException;

  BrandModelList getBrandModels(String brandId) throws BrandModelListNetworkException;

  EngineTypeList getEngineType(String modelId) throws EngineTypeListNetworkException;

  FuelTypeList getFuelTypes() throws FuelTypeListNetworkException;

  GeneralResponse logout() throws LogoutNetworkException;
}
