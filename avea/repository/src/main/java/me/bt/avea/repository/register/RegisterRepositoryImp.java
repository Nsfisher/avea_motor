package me.bt.avea.repository.register;

import me.bt.avea.domain.interactions.register.exceptions.RegisterException;
import me.bt.avea.domain.repository.RegisterRepository;
import me.bt.avea.repository.register.datasources.RegisterNetworkDataSource;
import me.bt.avea.repository.register.datasources.exceptions.RegisterNetworkException;

public class RegisterRepositoryImp implements RegisterRepository {

    private final RegisterNetworkDataSource networkDataSource;

    public RegisterRepositoryImp(RegisterNetworkDataSource networkDataSource) {
        this.networkDataSource = networkDataSource;
    }

    @Override
    public String register(String username, String password, String company, String language) throws RegisterException {
        try {
            String resp = networkDataSource.register(username, password, company, language);
            return resp;
        } catch (RegisterNetworkException e) {
            RegisterException registerException = new RegisterException();
            registerException.errorDesc = e.errorDesc;
            throw registerException;
        }
    }
}
