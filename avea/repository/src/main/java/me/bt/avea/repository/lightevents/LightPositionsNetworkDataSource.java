package me.bt.avea.repository.lightevents;

import java.util.List;
import me.bt.avea.domain.entities.DataLightPosition;
import me.bt.avea.repository.lightevents.datasources.exceptions.LightPositionsNetworkException;

public interface LightPositionsNetworkDataSource {
  List<DataLightPosition> getLightPositions(String mobileId, String startDate, String endDate, int maxPoints, int queryMode, int lineMode)
      throws LightPositionsNetworkException;
}
