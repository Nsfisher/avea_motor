package me.bt.avea.repository.lightevents;

import java.util.List;
import me.bt.avea.domain.entities.LightEvent;
import me.bt.avea.repository.lightevents.datasources.exceptions.LightEventsNetworkException;

public interface LightEventsNetworkDataSource {
  List<LightEvent> getLightEvents(String mobileId, int itemNumber) throws LightEventsNetworkException;

  List<LightEvent> getLightEvents(int itemNumber) throws LightEventsNetworkException;
}
