package me.bt.avea.repository.trips.datasources;

import java.util.Date;
import java.util.List;
import me.bt.avea.domain.entities.IntervalData;
import me.bt.avea.domain.interactions.trips.exceptions.TripsException;
import me.bt.avea.domain.repository.TripsRepository;
import me.bt.avea.repository.trips.TripsNetworkDataSource;
import me.bt.avea.repository.trips.datasources.exceptions.TripsNetworkException;

public class TripsRepositoryImp implements TripsRepository {

  private final TripsNetworkDataSource networkDataSource;

  public TripsRepositoryImp(TripsNetworkDataSource networkDataSource) {
    this.networkDataSource = networkDataSource;
  }

  @Override public List<IntervalData> getTrips(String mobileId, Date startTime, Date endTime) throws TripsException {
    try {
      return networkDataSource.getTrips(mobileId, startTime, endTime);
    } catch (TripsNetworkException e) {
      throw new TripsException();
    }
  }
}
