package me.bt.avea.repository.adduser.datasources;

import me.bt.avea.domain.entities.BaseResponse;
import me.bt.avea.repository.adduser.datasources.exceptions.AddUserNetworkException;

public interface AddUserNetworkDataSource {
  BaseResponse addUser(String activation, String username, String password, String plaka, String nickname, String brand, String model,
      String fuelType, String engineInfo) throws AddUserNetworkException;
}
