package me.bt.avea.repository.mobiles.datasources;

import java.util.List;
import me.bt.avea.domain.entities.Mobile;
import me.bt.avea.repository.mobiles.datasources.exceptions.DeleteMobileException;
import me.bt.avea.repository.mobiles.datasources.exceptions.InvalidCacheException;
import me.bt.avea.repository.mobiles.datasources.exceptions.ObtainBddMobileException;
import me.bt.avea.repository.mobiles.datasources.exceptions.ObtainMobileBddException;
import me.bt.avea.repository.mobiles.datasources.exceptions.PersistMobileBddException;
import me.bt.avea.repository.mobiles.datasources.exceptions.UnknownObtainMobileException;
import me.bt.avea.repository.mobiles.datasources.exceptions.UnknownPersistMobileException;

public interface MobileBddDataSource {
  List<Mobile> getMobiles() throws ObtainMobileBddException, InvalidCacheException, UnknownObtainMobileException;

  void persist(List<Mobile> mobiles) throws PersistMobileBddException, UnknownPersistMobileException;

  Mobile obtain(String mobileId) throws InvalidCacheException, ObtainBddMobileException;

  void delete(List<Mobile> deleted) throws DeleteMobileException;
}
